#include <unistd.h>

#include <QtCore>
#include <QThread>
#include <QTime>
#include <QPointF>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QFont>
#include <QFontMetrics>
#include <QOpenGLWidget>
#include <QByteArray>

#include <qwt_matrix_raster_data.h>
#include <qwt_painter.h>
#include <qwt_picker.h>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_marker.h>
#include <qwt_scale_draw.h>
#include <qwt_scale_engine.h>
#include <qwt_scale_widget.h>
#include <qwt_series_data.h>
#include <qwt_symbol.h>
#include <qwt_text_label.h>

#include "rsdk.h"
#include "apex_memmove.h"

#include "displaydockwidget.h"
#include "ui_displaydockwidget.h"
#include "sgsmooth.h"

struct timeval start_time, end_time;
struct timeval start_time2, end_time2;
struct timeval start_time3, end_time3;

static float spectrumSamps[DTTSP_SNAP_SAMPLES];
static double xes[DTTSP_SNAP_SAMPLES];
static double yes[DTTSP_SNAP_SAMPLES];
static float avgSamps[DTTSP_SNAP_SAMPLES];
static float phaseSamps[DTTSP_SNAP_SAMPLES];
static float lastSpectrumSamps[256][DTTSP_SNAP_SAMPLES];

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

// clip a value to a range
double clip(double n, double lower, double upper)
{
    double min1 = std::min ( n, upper);
    double max1 = std::max (lower, min1);
    return max1;
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

// a direct copy of Uwe's excellent Zoomer ScrollZoomer QwtPlotZoomer implementation
class Zoomer: public ScrollZoomer
{
public:
    Zoomer( QWidget *canvas ):
        ScrollZoomer( canvas )
    {
        setRubberBandPen( QPen( Qt::red, 2, Qt::DotLine ) );
        setRubberBandPen(QPen(QColor("gold")));
    }

    //QRectF zoomBase() const { return ScrollZoomer::selection()); }

    virtual QwtText trackerTextF(const QPointF &pos) const
    {
        QColor bg(0,0,0,64);

        //QwtText text = QwtPlotZoomer::trackerTextF( pos );
        //QRectF r = QwtPlotZoomer::zoomRect();
        QString rStr = QLocale(QLocale::English).toString(pos.x(), 'f', 0) + " Hz " +
                       QLocale(QLocale::English).toString(pos.y(), 'f', 0) +" dBm";
        QwtText text(rStr);
        text.setBackgroundBrush(QBrush(bg));
        text.setColor(QColor(195,195,195,192));
        return text;
    }

    //    virtual void rescale()
    //    {
    //        QwtScaleWidget *scaleWidget = plot()->axisWidget( xAxis() );
    //        QwtScaleDraw *sd = scaleWidget->scaleDraw();
    //
    //        double minExtent = 0.0;
    //        if ( zoomRectIndex() > 0 )
    //        {
    //            // When scrolling in horizontal direction
    //            // the plot is jumping in horizontal direction
    //            // because of the different widths of the labels
    //            // So we better use a fixed extent.
    //
    //            minExtent = sd->spac:w() + sd->maxTickLength() + 1;
    //            minExtent += sd->labelSize(scaleWidget->font(), 1000 ).width();
    //
    //        }
    //
    //        sd->setMinimumExtent( minExtent );
    //
    //        ScrollZoomer::rescale();
    //    }
};

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

DisplayDockWidget::DisplayDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::DisplayDockWidget),
    m_spectrumInterval(1),
    m_waterfallInterval(0),
    fGrid(nullptr),
    sGrid(nullptr),
    dbGrid(nullptr),
    initSpectrum(true),
    yAxisMinY(-120),
    yAxisMaxY(-40),
    lastSecond(QTime::currentTime()),
    m_spectrumColor(QColor(0, 255, 255, 255)),
    m_fillColor(QColor(0, 255, 255, 128)),
    m_phaseColor(QColor(0, 255, 0, 255)),
    m_dbGridColor("magenta"),
    m_sGridColor("magenta"),
    m_titleColor("navajowhite"),
    m_fGridColor("red"),
    m_bandColor(QColor(41, 255, 255, 108)),
    m_gradBrush(QBrush(QColor(m_spectrumColor), Qt::LinearGradientPattern)),
    filterCoeff(11),
    fillTrace(false),
    bandScopeOn(false),
    oScopeOn(false),
    titleOn(false),
    m_bandOn(false),
    m_pageTune(true),
    m_rebuildBands(false),
    smoothValue(3),
    m_scrollDecay(150),
    m_scrolling(false),
    m_scrollLastDirection(0)
{

    /* housekeeping */

    bandMarkers.clear();
    lastBandZones.clear();

    gettimeofday(&start_time, NULL);
    gettimeofday(&end_time, NULL);
    memset((void*)lastDiff, 0, NUM_KELEMS * (sizeof(unsigned long long)));

    //setTitleBarWidget(&dummyTitleWidget);

    // centered on F=10MHz
    xAxisMaxX = 10192000;
    xAxisMinX = 9808000;

    ui->setupUi(this);

    fGrid = new FreqGrid;
    sGrid = new SGrid;
    dbGrid = new dBGrid;
    oscope = new OScope(ui->splitter);
    setOScopeOn(false);

    bandscope = new BandScope(ui->splitter);
    setBandScopeOn(false);

    setMinY(-120);
    setMaxY(-65);

    // initial defaults overriden later by Q_PROPERTYs
    propSetSpectrumColor(m_spectrumColor);
    propSetSGridColor(m_sGridColor);
    propSetDbGridColor(m_dbGridColor);
    propSetFGridColor(m_fGridColor);
    propSetTitleColor(m_titleColor);
    propSetFillColor(m_fillColor);
    propSetBandColor(m_bandColor);

    // graticule center frequency
    rx1FreqMark.setLineStyle(QwtPlotMarker::VLine);
    rx1FreqMark.attach(ui->DispPlot);
    rx1FreqMark.setLinePen(m_titleColor, (qreal)1.0, Qt::SolidLine);
    rx1FreqMark.setYValue(-200.0);
    rx1FreqMark.setZ(-175.0);
    rx1FreqMark.setLabelAlignment(Qt::AlignTop|Qt::AlignLeft);

    // graticule subRx frequency
    rx2FreqMark.setLineStyle(QwtPlotMarker::VLine);
    rx2FreqMark.setLinePen(Qt::cyan, (qreal)1.0, Qt::SolidLine);
    rx2FreqMark.setYValue(-200.0);
    rx2FreqMark.setLabelAlignment(Qt::AlignTop|Qt::AlignLeft);

    // setup the spectrum display QwtPlot
    ui->DispPlot->setAxisAutoScale(QwtPlot::xTop, false);
    ui->DispPlot->setAxisAutoScale(QwtPlot::xBottom, false);
    ui->DispPlot->setAxisAutoScale(QwtPlot::yLeft, false);
    ui->DispPlot->setAxisAutoScale(QwtPlot::yRight, false);

    ui->DispPlot->enableAxis(QwtPlot::yRight, false);
    ui->DispPlot->enableAxis(QwtPlot::yLeft, false);
    ui->DispPlot->enableAxis(QwtPlot::xTop, false);
    ui->DispPlot->enableAxis(QwtPlot::xBottom, false);

    ui->DispPlot->setAxisScale(QwtPlot::xTop, 0, DTTSP_SNAP_SAMPLES, 0); // process_scope, _panadapter, _phase in DttSP uses 4k chunks
    ui->DispPlot->setAxisScale(QwtPlot::yLeft, yAxisMinY, yAxisMaxY, 0);
    ui->DispPlot->setCanvasBackground(* new QBrush(Qt::black));

    // pick up mouse over filter regions
    /*
    ui->DispPlot->setMouseTracking(true);
    ui->DispPlot->canvas()->setMouseTracking(true);
    ui->waterfallGraphicsView->setMouseTracking(true); // ??
    this->setMouseTracking(true);
    setMouseTracking(true);
    */

    // settings for the drawing canvas
    QwtPlotCanvas *cnvs = (QwtPlotCanvas *)ui->DispPlot->canvas();
    cnvs->setPaintAttribute(QwtPlotCanvas::BackingStore, false);
    cnvs->setPaintAttribute(QwtPlotCanvas::Opaque, false);
    cnvs->setPaintAttribute(QwtPlotCanvas::HackStyledBackground, false);
    cnvs->setPaintAttribute(QwtPlotCanvas::ImmediatePaint, true);
    if(cnvs->testPaintAttribute(QwtPlotCanvas::BackingStore))
    {
        cnvs->setAttribute(Qt::WA_PaintOnScreen, true);
        cnvs->setAttribute(Qt::WA_NoSystemBackground, true);
    }

    setFillTrace(false);

    spectrumCurve.setStyle(QwtPlotCurve::Lines);
    spectrumCurve.setCurveAttribute(QwtPlotCurve::Fitted, true);
    spectrumCurve.setPen(Qt::darkCyan, 1.0, Qt::SolidLine);

    spectrumCurve.setItemAttribute(QwtPlotItem::Margins, true);
    spectrumCurve.setRenderHint(QwtPlotItem::RenderAntialiased);
    spectrumCurve.setPaintAttribute(QwtPlotCurve::ClipPolygons, true);
    spectrumCurve.setPaintAttribute(QwtPlotCurve::FilterPoints, true);
    spectrumCurve.setPaintAttribute(QwtPlotCurve::MinimizeMemory, true);
    spectrumCurve.setPaintAttribute(QwtPlotCurve::ImageBuffer, true);
    spectrumCurve.setOrientation(Qt::Vertical);
    spectrumCurve.setRawSamples(xes, yes, DTTSP_SNAP_SAMPLES);
    spectrumCurve.setItemAttribute(QwtPlotItem::AutoScale, true);
    spectrumCurve.attach(ui->DispPlot);
    spectrumCurve.setZ(-40.0);

    phaseCurve.setStyle(QwtPlotCurve::Lines);
    phaseCurve.setPen(Qt::green, 0.0,Qt::SolidLine);
    //phaseCurve.setRenderHint(QwtPlotItem::RenderAntialiased);

    rx1FilterZone.attach(ui->DispPlot);
    rx1FilterZone.setOrientation(Qt::Vertical);
    rx1FilterZone.setZ(-150.0);
    QBrush b = rx1FilterZone.brush();
    QColor c = b.color();
    c.setAlphaF(0.70);
    b.setColor(c);
    rx1FilterZone.setBrush(b);

    //rx2FilterZone.attach(ui->DispPlot);
    rx2FilterZone.setOrientation(Qt::Vertical);
    rx2FilterZone.setZ(-150.0);
    b = rx2FilterZone.brush();
    c = b.color();
    c.setAlphaF(0.70);
    b.setColor(c);
    rx2FilterZone.setBrush(b);

    //bandZone.attach(ui->DispPlot);
    //bandZone.setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
    //bandZone.setZ(-150.0);
    //QBrush b(m_spectrumColor);
    //bandZone.setBrush(b);
    //bandZone.setLegendMode(QwtPlotShapeItem::LegendColor);

    scene = new QGraphicsScene(this);
    /*
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    QOpenGLWidget *glw = new QOpenGLWidget;
    QSurfaceFormat gfxFmt;
    gfxFmt.setSamples(8);
    glw->setFormat(gfxFmt);
    ui->waterfallGraphicsView->setViewport(glw);

    ui->waterfallGraphicsView->setCacheMode(QGraphicsView::CacheNone);
    ui->waterfallGraphicsView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    ui->waterfallGraphicsView->setOptimizationFlags(QGraphicsView::DontAdjustForAntialiasing |
                                           QGraphicsView::DontClipPainter |
                                           QGraphicsView::DontSavePainterState);
                                           */
    ui->waterfallGraphicsView->setAttribute(Qt::WA_PaintOnScreen, true);
    ui->waterfallGraphicsView->setAttribute(Qt::WA_NoSystemBackground, true);
    ui->waterfallGraphicsView->setScene(scene);

    QPixmap *pixmap = new QPixmap(100, 33);
    pixmap->fill(QColor("black"));
    wfPixmap = scene->addPixmap(*pixmap);

    // allow zooming of the display
    //initZoom();
    setFilterCoeffs(11);

    // setup the worker thread for periodic collection of DttSP sample data
    snapper = new Worker2(Process_Panadapter, 0, &spectrumSamps);
    snapper->setRate(0);
    snapper->stopTimer();
    //snapper->moveToThread(&collectThread);
    //snapper->moveToThread(QApplication::instance()->thread());
    //collectThread.setObjectName(QString("rsdk display"));
    connect (snapper, SIGNAL(sampleDone()), this, SLOT(drawWaterfall()));
    connect (snapper, SIGNAL(sampleDone()), this, SLOT(drawPlot()));

    //drawSpectrumTimer.stop();
    //drawSpectrumTimer.setInterval(0);
    //connect(&drawSpectrumTimer, SIGNAL(timeout()), this, SLOT(drawPlot()));

    //drawWaterfallTimer.stop();
    //drawWaterfallTimer.setInterval(0);
    //connect(&drawWaterfallTimer, SIGNAL(timeout()), this, SLOT(drawWaterfall()));

    connect(theRsdk->PlotDrawOptionsDock, SIGNAL(spxLGradientChanged(const QLinearGradient &)), this, SLOT(setLGrad(const QLinearGradient &)));
}

DisplayDockWidget::~DisplayDockWidget()
{
    snapper->stopTimer();
    //drawWaterfallTimer.stop();
    //drawSpectrumTimer.stop();

    //collectThread.requestInterruption();
    //collectThread.quit();
    //while (collectThread.isRunning())
    //    collectThread.wait();

    removeLastBandZones();
    removeBandMarkers();

    /*
    if (zoom) // zoom is new in start()
       delete zoom;
       */
    delete snapper;
    delete dbGrid;
    delete sGrid;
    delete fGrid;
    delete wfPixmap;
    delete scene;
    delete bandscope;
    delete oscope;
    delete ui;
}

void DisplayDockWidget::removeLastBandZones()
{
    std::list<struct band *>::iterator i = lastBandZones.begin();
    std::list<struct band *>::iterator end = lastBandZones.end();
    while(i != end)
    {
        lastBandZones.erase(i++);
        end = lastBandZones.end();
    }
}

void DisplayDockWidget::removeBandMarkers()
{
    std::list<QwtPlotMarker *>::iterator i = bandMarkers.begin();
    std::list<QwtPlotMarker *>::iterator end = bandMarkers.end();
    while(i != end)
    {
        //delete (*i)->symbol();
        delete *i;
        bandMarkers.erase(i++);
        end = bandMarkers.end();
    }
}

bool DisplayDockWidget::rebuildBands() const
{
    return m_rebuildBands;
}

void DisplayDockWidget::setRebuildBands(bool rebuildBands)
{
    m_rebuildBands = rebuildBands;
}

void DisplayDockWidget::start()
{
    memset(xes, 0, sizeof(double) * DTTSP_SNAP_SAMPLES);
    memset(yes, 0, sizeof(double) * DTTSP_SNAP_SAMPLES);
    memset(avgSamps, 0, sizeof(float) * DTTSP_SNAP_SAMPLES);
    memset(phaseSamps, 0, sizeof(float) * DTTSP_SNAP_SAMPLES);
    memset(spectrumSamps, 0, sizeof(float) * DTTSP_SNAP_SAMPLES);

    // tie the display dock colors to the plot draw controller window
    // after everthing is ready to receive signals
    if (theRsdk->PlotDrawOptionsDock)
    {
        // pass back some colors to the plotdrawoptions dock widget
        // so they present the correct colors
        propSetSpectrumColor(m_spectrumColor);
        propSetSGridColor(m_sGridColor);
        propSetDbGridColor(m_dbGridColor);
        propSetFGridColor(m_fGridColor);
        propSetTitleColor(m_titleColor);
        propSetFillColor(m_fillColor);
        propSetBandColor(m_bandColor);
    }

    // warm up the drawing intervals
    setSpectrumInterval(m_spectrumInterval);
    setWaterfallInterval(m_waterfallInterval);

    setXAxis(theRsdk->rozy->getRxFreq(theRsdk->rozy->curMercury));
    //QRectF size(xAxisMinX, yAxisMinY, xAxisMaxX-xAxisMinX, yAxisMaxY-yAxisMinY);
    //zoom->setZoomBase(size);

    //collectThread.start();
    snapper->startTimer();
    //drawWaterfallTimer.start();
    //drawSpectrumTimer.start();
}

/*!
 * \brief DisplayDockWidget::initZoom
 *  Initialize a zoomer for this display
 */
void DisplayDockWidget::initZoom()
{
    // the zoomer
    zoom = new Zoomer(ui->DispPlot->canvas());
    zoom->setAxis(QwtPlot::xBottom, QwtPlot::yLeft);
    //zoom->setTrackerMode(QwtPlotPicker::ActiveOnly);

    // ctrl + and ctrl - zoom in and out, ctrl 0 resets zoom to base
    zoom->setKeyPattern(QwtEventPattern::KeyRedo, Qt::Key_Plus, Qt::ControlModifier);
    zoom->setKeyPattern(QwtEventPattern::KeyUndo, Qt::Key_Minus, Qt::ControlModifier);
    zoom->setKeyPattern(QwtEventPattern::KeyHome, Qt::Key_0, Qt::ControlModifier);

    connect (zoom, SIGNAL(zoomed(QRectF)), this, SLOT(on_zoomed(QRectF)));
}

/**
 * @brief DisplayDockWidget::setXAxis
 * @param freq is the new center frequency for the receiver
 */
void DisplayDockWidget::setXAxis(const qlonglong &freq)
{
    const int sampRate = theRsdk->rozy->sampleRate;

    // uint ndx = zoom->zoomRectIndex();
    //QRectF curSize = zoom->zoomBase();
    double xL, xH;
    bool setit = false;
    xL = freq - (sampRate/2);
    xH = freq + (sampRate/2);

    if (xL != xAxisMinX)
    {
        xAxisMinX = xL;
        setit = true;
    }
    if (xH != xAxisMaxX)
    {
        xAxisMaxX = xH;
        setit = true;
    }

    if (setit)
    {
        //bool pstate = zoom->blockSignals(true);
        ui->DispPlot->setAxisScale(QwtPlot::xBottom, xAxisMinX, xAxisMaxX, 0);
        //zoom->blockSignals(pstate);
    }
}

QLinearGradient DisplayDockWidget::getLgrad() const
{
    return lgrad;
}

void DisplayDockWidget::setLGrad(const QLinearGradient &lg)
{
    lgrad = lg;
    resizeLGrad();
    setFillTrace(fillTrace);
}

void DisplayDockWidget::resizeLGrad()
{
    int ndx = ui->splitter->indexOf(ui->DispPlot);
    double w = ui->splitter->widget(ndx)->width();
    double h = ui->splitter->widget(ndx)->height();
    double h1 = ui->DispPlot->titleLabel()->height();
    double h2;
    if (titleOn)
        h2 = h - h1;
    else
        h2 = h;

    QPointF start(w/2.0, h2);
    QPointF final(w/2.0, 0.0);
    lgrad.setStart(start);
    lgrad.setFinalStop(final);
}

bool DisplayDockWidget::getSpectrumEnable() const
{
    return m_spectrumEnable;
}

void DisplayDockWidget::setSpectrumEnable(bool spectrumEnable)
{
    m_spectrumEnable = spectrumEnable;

    int ndx = ui->splitter->indexOf(ui->DispPlot);

    if (!m_spectrumEnable) {
        //drawSpectrumTimer.stop();
        if (!m_waterfallEnable) {
            snapper->stopTimer();
        }
        if (ui && ui->splitter) {
            if (ndx>-1) {
                ui->splitter->widget(ndx)->hide();
            }
        }
    } else {
        //drawSpectrumTimer.start();
        if (!snapper->isRunning())
            snapper->startTimer();
        if (ui && ui->splitter) {
            if (ndx>-1)
                ui->splitter->widget(ndx)->show();
        }
    }
}

bool DisplayDockWidget::getWaterfallEnable() const
{
    return m_waterfallEnable;
}

void DisplayDockWidget::setWaterfallEnable(bool waterfallEnable)
{
    m_waterfallEnable = waterfallEnable;

    int ndx = ui->splitter->indexOf(ui->waterfallGraphicsView);

    if (!m_waterfallEnable) {
        //drawWaterfallTimer.stop();
        if (!m_spectrumEnable) {
            snapper->stopTimer();
        }
        if (ui && ui->splitter) {
            if (ndx>-1)
                ui->splitter->widget(ndx)->hide();
        }
    } else {
        //drawWaterfallTimer.start();
        if (!snapper->isRunning())
            snapper->startTimer();
        if (ui && ui->splitter) {
            if (ndx>-1)
                ui->splitter->widget(ndx)->show();
        }
    }
}

// update the x axis values to center around the hpsdr center frequency
void DisplayDockWidget::updateXAxis(const qlonglong &freq)
{
    setXAxis(freq);
}

void DisplayDockWidget::drawWaterfall()
{
    /*
    gettimeofday(&end_time3, NULL);
    unsigned long long diff = timeval_diff(&start_time3, &end_time3);
    gettimeofday(&start_time3, NULL);
    qDebug() << __func__ << " diff " << diff/1000.0 << " mSec" <<  " tid " << QThread::currentThreadId();
    */

    if (m_waterfallEnable)
    {
        if (isVisible())
        {
            double preAmpOffset = (theRsdk->HpsdrDock->getPreAmp() == hpsdr::Preamp_Off) ? 0.0 : 20.0;

            if (theRsdk->PlotDrawOptionsDock->getWaterfallOn()) {
                QSize waterfallSize = ui->waterfallGraphicsView->size();

                // silently ignore invalid pixmap operations
                // as either size of zero causes invalid pixmap
                if (waterfallSize.width()==0)
                    waterfallSize.setWidth(1);
                if (waterfallSize.height()==0)
                    waterfallSize.setHeight(1);

                const QRect rect (0, 0, waterfallSize.width(), waterfallSize.height());
                QPixmap wPixmap = wfPixmap->pixmap();
                // update the waterfall
                if (wPixmap.rect() != rect) {
                    // need to scale the pixmap
                    wPixmap = wPixmap.scaled(waterfallSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
                    ui->waterfallGraphicsView->setSceneRect(0, 0, waterfallSize.width(), waterfallSize.height());
                }

                // the new canvas, we will copy the old to the new +1 line to make room for a new line of waterfall data
                QPainter painter(&wPixmap);
                painter.setRenderHint(QPainter::Antialiasing, true);
                painter.setRenderHint(QPainter::TextAntialiasing, true);
                painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
                painter.setRenderHint(QPainter::HighQualityAntialiasing, true);
                painter.setRenderHint(QPainter::NonCosmeticDefaultPen, true);

                // shift the pixmap down one line
                if (m_waterfallInterval < 0)
                    wPixmap.scroll(0, -1, wPixmap.rect());
                else if (m_waterfallInterval > 0)
                    wPixmap.scroll(0, 1, wPixmap.rect());

                /* Generate pixel using optimization via gprof */
                QColor color(0x7f, 0x7f, 0x7f, 0xff);
                double cwidth = wPixmap.width();
                const double snapSamps = (double)DTTSP_SNAP_SAMPLES;
                const double slope = (snapSamps-1)/cwidth;

                double pk;
                for (int i=0; i<cwidth; i++)
                {
                    double val = i*slope;
                    int lndx = (int)floor(val);
                    int rndx = (int)ceil(val + slope);

                    //just pick the sample in the middle of the narrow range of samples
                    //int ndx = val + ((rndx - lndx) / 2);

                    // peak detect the narrow range of samples
                    pk = -10000.0;
                    for (int x=lndx; x<rndx; )
                    {
                        double val = (double)spectrumSamps[x++];
                        if (val > pk)
                            pk = val;
                    }
                    double max3 = pk;

                    // or use a simple average of the narrow range of samples
//                    double max2 = 0.0;
//                    for (int x=lndx; x<rndx; )
//                    {
//                        max2 += (double)spectrumSamps[x++];
//                    }
//                    double max3 = max2 / (rndx - lndx);

                    double max = max3 + -43.0 - preAmpOffset;
                    double top = max - yAxisMinY;
                    double bot = yAxisMaxY - yAxisMinY;

                    double lmax;
                    if (max < yAxisMinY)
                        lmax = 0.0;
                    else
                        lmax = abs(top/bot);

                    color = theRsdk->PlotDrawOptionsDock->getSpxGradientColorAtPct(lgrad, lmax);
                    painter.setPen(color);
                    if (m_waterfallInterval < 0) // backwards, just for the fun of it
                        painter.drawPoint(i, wPixmap.height()-1);
                    else if (m_waterfallInterval > 0)
                        painter.drawPoint(i, 0);
                }

                QTime t = QTime::currentTime();       // center frequency filter zone
                // only draw once every ten seconds so we don't smear down the waterfall label
                if (!(t.second()%10) && (lastSecond.second() - t.second())) {
                    lastSecond = t;
                    painter.setPen(QColor(Qt::white));
                    painter.drawText(QPointF(5.0,10.0), t.toString());
                }

                wfPixmap->setPixmap(wPixmap);

                // unhide the waterfall
                if (!ui->waterfallGraphicsView->isVisible())
                    ui->waterfallGraphicsView->setVisible(true);
            } else {
                // hide the waterfall
                if (ui->waterfallGraphicsView->isVisible())
                    ui->waterfallGraphicsView->setVisible(false);
            }

        }
    }
    //ui->DispPlot->repaint();
}

void DisplayDockWidget::drawPlot()
{
    /*
    gettimeofday(&end_time2, NULL);
    unsigned long long diff = timeval_diff(&start_time2, &end_time2);
    gettimeofday(&start_time2, NULL);
    qDebug() << __func__ << " diff " << diff/1000.0 << " mSec" <<  " tid " << QThread::currentThreadId();
    */

    if (m_spectrumEnable)
    {
        if (isVisible())
        {
            const int sampRate = theRsdk->rozy->sampleRate;

            //if (!m_pageTune) {
            updateXAxis(theRsdk->rozy->getRxFreq(theRsdk->rozy->curMercury));
            //}

            double preAmpOffset = (theRsdk->HpsdrDock->getPreAmp() == hpsdr::Preamp_Off) ? 0.0 : 20.0;

            double yMinAvg = 0.0;
            int yMaxNow = -1000;

            if (theRsdk->PlotDrawOptionsDock->getSpectrumOn()) {

                int i;
                float max = 0.0;
                float min = 0.0;
                double slope = ((double)sampRate)/(double)DTTSP_SNAP_SAMPLES;

                for(i=0; i<DTTSP_SNAP_SAMPLES; i++) {

                    float val = spectrumSamps[i];

                    for (int x=0; x<filterCoeff; x++)
                    {
                        val += lastSpectrumSamps[x][i];
                    }

                    avgSamps[i] = val/(filterCoeff+1);

                    if (avgSamps[i]>max)
                        max = avgSamps[i];
                    if (avgSamps[i]<min)
                        min = avgSamps[i];

                    // peak detect the yAxisMaxY value
                    int t = (int)(max + -43.0 - preAmpOffset);
                    yMaxNow = t > yMaxNow ? t : yMaxNow;

                    int b = (int)(min + -43.0 - preAmpOffset);
                    yMinAvg = b < yMinAvg ? b : yMinAvg;

                    xes[i] = (double)i * slope + (double)xAxisMinX;
                    yes[i] = avgSamps[i] + (-43.0) - preAmpOffset;
                }

                if (filterCoeff) {
                    for (int x=filterCoeff; x>0; x--)
                        apex_memcpy(lastSpectrumSamps[x], lastSpectrumSamps[x-1], DTTSP_SNAP_SAMPLES * sizeof(float));
                    apex_memcpy(lastSpectrumSamps[0], spectrumSamps, DTTSP_SNAP_SAMPLES*sizeof(float));
                }

                //spectrumCurve.setStyle(QwtPlotCurve::Lines);
                //spectrumCurve.setPen(QPen(lgrad, 1.0));
                spectrumCurve.setBaseline(yAxisMinY);

                ui->DispPlot->setAxisScale(QwtPlot::yLeft, yAxisMinY, yAxisMaxY, 0);
                //ui->DispPlot->setAxisScale(QwtPlot::xBottom, xAxisMinX, xAxisMaxX, 0);

            } else {
                spectrumCurve.setStyle(QwtPlotCurve::NoCurve);
            }

            // update the axis values
            if (theRsdk->PlotDrawOptionsDock->getYMinAutoScale()) {
                int n = yMinAvg;
                n = n + 5 - 1 - (n - 1) % 5;
                bool pstate = theRsdk->PlotDrawOptionsDock->blockSignals(true);
                theRsdk->PlotDrawOptionsDock->setYMin(n);
                theRsdk->PlotDrawOptionsDock->blockSignals(pstate);
                setMinY(n);
            }

            if (theRsdk->PlotDrawOptionsDock->getYMaxAutoScale()) {
                int n = yMaxNow - 5 - 1 - ((yMaxNow - 1) % 5);
                if(n > theRsdk->PlotDrawOptionsDock->yMax()) {
                    bool pstate = theRsdk->PlotDrawOptionsDock->blockSignals(true);
                    theRsdk->PlotDrawOptionsDock->setYMax(n);
                    theRsdk->PlotDrawOptionsDock->blockSignals(pstate);
                    setMaxY(n);
                }
            }

            // make sure scales are set before further calculations with them
            ui->DispPlot->updateAxes();

            static const QFont f("Arial", 32, QFont::Bold);

            if (titleOn) {
                ui->DispPlot->titleLabel()->setFont(f);
                QwtText modeText = QString::fromStdString(theRsdk->rozy->getModeStr(theRsdk->rozy->getMode()));
                QwtText labelText = theRsdk->VfoDock->freqLabel() + " " + QString::fromStdString(theRsdk->rozy->getModeStr(theRsdk->rozy->getMode()));
                if (theRsdk->getXmit()) {
                    labelText.setColor(QColor("darkRed"));
                } else {
                    labelText.setColor(m_titleColor);
                }
                ui->DispPlot->setTitle(labelText);
            } else {
                ui->DispPlot->titleLabel()->setFont(f);
                QwtText labelText("");
                ui->DispPlot->setTitle(labelText);
            }

            //! don't use the theRsdk->rozy frequency here!
            //!qlonglong freq = theRsdk->rozy->getRxFreq(0);

            //! use the vfo frequency in pagetune mode
            //! the radio stays at the same center frequency for RX
            //! TX is set as the actual vfo frequency always
            //! Doing the RX like this (offsetting the revceive frequency
            //! in passband) allows page tune mode to move the rx frequency display


            qlonglong freq;
            //if (theRsdk->VfoDock->pageTune())
            freq = theRsdk->VfoDock->getFreq();
            //else
            //    freq = theRsdk->rozy->getRxFreq(0);

            //qlonglong centerFreq = theRsdk->rozy->rxFreq[theRsdk->rozy->curMercury][0];

            //centerFreq.setLabel(label);
            rx1FreqMark.setXValue(freq);
            rx1FreqMark.setLinePen(m_titleColor, (qreal)1.0, Qt::SolidLine);

            // center frequency filter zone
            double hiFiltFreq = theRsdk->rozy->DSPFiltHi[0];
            double loFiltFreq = theRsdk->rozy->DSPFiltLo[0];
            hiFiltFreq += freq;
            loFiltFreq += freq;
            if ((theRsdk->rozy->getMode() == CWU) || (theRsdk->rozy->getMode() == CWL)) {
                // CW modes center around center frequency marker, I guess
                double diff = abs(hiFiltFreq - loFiltFreq);
                rx1FilterZone.setInterval(freq - (diff/2), freq + (diff/2));

            } else if (loFiltFreq <= hiFiltFreq) {
                rx1FilterZone.setInterval(loFiltFreq, hiFiltFreq);
            } else {
                rx1FilterZone.setInterval(hiFiltFreq, loFiltFreq);
            }

            if (theRsdk->VfoDock->subRxOn()) {
                //qlonglong f2 = theRsdk->VfoDock->freqR2();
                qlonglong subRxFreq = theRsdk->rozy->getRxFreq(1);
                rx2FreqMark.attach(ui->DispPlot);
                rx2FilterZone.attach(ui->DispPlot);
                rx2FreqMark.setXValue(subRxFreq-1);

                // subRx frequency filter zone
                hiFiltFreq = theRsdk->rozy->DSPFiltHi[1];
                loFiltFreq = theRsdk->rozy->DSPFiltLo[1];
                hiFiltFreq += subRxFreq;
                loFiltFreq += subRxFreq;
                if ((theRsdk->rozy->getMode() == CWU) || (theRsdk->rozy->getMode() == CWL)) {
                    // CW modes center around center frequency marker, I guess
                    double diff = abs(hiFiltFreq - loFiltFreq);
                    rx2FilterZone.setInterval(subRxFreq - (diff/2), subRxFreq + (diff/2));

                } else if (loFiltFreq <= hiFiltFreq) {
                    rx2FilterZone.setInterval(loFiltFreq, hiFiltFreq);
                } else {
                    rx2FilterZone.setInterval(hiFiltFreq, loFiltFreq);
                }
            } else {
                rx2FilterZone.detach();
                rx2FreqMark.detach();
            }

            if (m_bandOn) {
                std::list<struct band *>bandList = theRsdk->BandDock->bandsForRange(xAxisMinX, xAxisMaxX);
                // if changed, this walks the list each and every time, be careful with the band list
                // using std::list with != causes a complete traversal of the list
                // checking for equality on each element
                if ((lastBandZones != bandList) ||
                        (m_rebuildBands == true)) {
                    removeLastBandZones();
                    removeBandMarkers();
                    lastBandZones = bandList;
                    m_rebuildBands = false;
                    ui->DispPlot->replot();

                    //const double top = yAxisMaxY - 5.0;
                    const double top = yAxisMaxY;
                    const double bot = yAxisMinY;

                    std::list<struct band *>::iterator i = bandList.begin();

                    while (i != bandList.end()) {
                        // convert back into Hz
                        const double s = (*i)->start * 1e3;
                        const double e = (*i)->end * 1e3;
                        const double w = e - s;
                        const double h = fabs(top - bot);
                        const QPointF tl(s, top);
                        const QPointF br(e, bot);

                        // this is probably a bug, as canvasMap may not update until replot below
                        const QwtScaleMap xMap = ui->DispPlot->canvasMap(QwtPlot::xBottom);
                        const QwtScaleMap yMap = ui->DispPlot->canvasMap(QwtPlot::yLeft);
                        const QPointF tl0 = QwtScaleMap::transform(xMap, yMap, tl);
                        const QPointF br0 = QwtScaleMap::transform(xMap, yMap, br);

                        QString cx ((*i)->color.c_str());
                        QColor color(cx);
                        color.setAlpha(m_bandColor.alpha());
                        const QBrush b(color);

                        QwtSymbol *sym = new QwtSymbol();
                        sym->setSize(fabs(br0.x() - tl0.x()), fabs(tl0.y() - br0.y()));
                        sym->setStyle(QwtSymbol::Rect);
                        sym->setBrush(b);
                        sym->setPen(m_bandColor, 0.0, Qt::SolidLine);

                        std::string stdStr = (*i)->desc + " " + (*i)->prefMode + " " + (((*i)->hamClass == "Any") ? "" : (*i)->hamClass);
                        QString str(stdStr.c_str());
                        const QFont f2("Arial", 10, QFont::Normal);

                        QwtPlotMarker *m = new QwtPlotMarker();
                        m->setLineStyle(QwtPlotMarker::NoLine);
                        m->setLabelAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                        QwtText text(str);
                        text.setColor(QColor("white"));
                        text.setFont(f2);
                        m->setLabel(text);
                        m->setSymbol(sym);
                        m->setZ(-200);

                        const QFontMetrics fm(f2);
                        const double strWidth = fm.width(str);
                        m->setValue(s+((w-strWidth)*0.5), yAxisMinY + (h*0.5));

                        bandMarkers.push_back(m);
                        m->attach(ui->DispPlot);

                        i++;
                    }
                }
            } else {
                removeBandMarkers();
                removeLastBandZones();
            }
            // replot all
            ui->DispPlot->replot();
        }
    }
}

QBrush DisplayDockWidget::gradBrush() const
{
    return m_gradBrush;
}

int DisplayDockWidget::getWaterfallInterval() const
{
    return m_waterfallInterval;
}

void DisplayDockWidget::setWaterfallInterval(int waterfallInterval)
{
    m_waterfallInterval = waterfallInterval; // milliseconds
    //drawWaterfallTimer.stop();
    //drawWaterfallTimer.setInterval(abs(m_waterfallInterval));
    //drawWaterfallTimer.start();

    // don't do this
    //snapper->stopTimer();
    //snapper->setRate(abs(m_waterfallInterval));
    //snapper->startTimer();

    emit waterfallIntervalChanged(m_waterfallInterval, false);
}

int DisplayDockWidget::getSpectrumInterval() const
{
    return m_spectrumInterval;
}

void DisplayDockWidget::setSpectrumInterval(int spectrumInterval)
{
    m_spectrumInterval = spectrumInterval; // milliseconds
    //drawSpectrumTimer.stop();
    //drawSpectrumTimer.setInterval(m_spectrumInterval);
    //drawSpectrumTimer.start();
    emit spectrumIntervalChanged(m_spectrumInterval, false); // don't sendSig
}

double DisplayDockWidget::getXAxisMaxX() const
{
    return xAxisMaxX;
}

void DisplayDockWidget::setXAxisMaxX(double value)
{
    xAxisMaxX = value;
}

double DisplayDockWidget::getXAxisMinX() const
{
    return xAxisMinX;
}

void DisplayDockWidget::setXAxisMinX(double value)
{
    xAxisMinX = value;
}


bool DisplayDockWidget::pageTune() const
{
    return m_pageTune;
}

void DisplayDockWidget::setPageTune(bool pageTune)
{
    m_pageTune = pageTune;
    emit freqChanged(theRsdk->VfoDock->getFreq());
}

bool DisplayDockWidget::bandOn() const
{
    return m_bandOn;
}

void DisplayDockWidget::setBandOn(bool bandOn)
{
    m_bandOn = bandOn;
    if (!m_bandOn)
    {
        removeLastBandZones();
        removeBandMarkers();
    }
}

void DisplayDockWidget::setBandColor(const QColor &color)
{
    m_bandColor = color;
    removeLastBandZones();
    removeBandMarkers();
}

void DisplayDockWidget::propSetBandColor(const QColor &value)
{
    m_bandColor = value;
    setBandColor(value);
    emit bandColorChanged(m_bandColor);
}

QColor DisplayDockWidget::bandColor() const
{
    return m_bandColor;
}

QColor DisplayDockWidget::fGridColor() const
{
    return m_fGridColor;
}

QColor DisplayDockWidget::sGridColor() const
{
    return m_sGridColor;
}

QColor DisplayDockWidget::dbGridColor() const
{
    return m_dbGridColor;
}

void DisplayDockWidget::setFGridColor(const QColor &value)
{
    m_fGridColor = value;
    fGrid->setColor(m_fGridColor);
}

void DisplayDockWidget::setDbGridColor(const QColor &value)
{
    m_dbGridColor = value;
    dbGrid->setColor(m_dbGridColor);
}

bool DisplayDockWidget::freqGridOn() const
{
    return m_freqGridOn;
}

void DisplayDockWidget::setFreqGridOn(bool freqGridOn)
{
    m_freqGridOn = freqGridOn;
}

bool DisplayDockWidget::getTitleOn() const
{
    return titleOn;
}

void DisplayDockWidget::setTitleOn(bool value)
{
    titleOn = value;
    // title takes up space, account for it
    resizeLGrad();
    setFillTrace(fillTrace);
}

bool DisplayDockWidget::getBandScopeOn() const
{
    return bandScopeOn;
}

void DisplayDockWidget::setBandScopeOn(bool enable)
{
    bandScopeOn = enable;

    if (!ui || !ui->splitter)
        return;

    int ndx = ui->splitter->indexOf(bandscope);
    if (ndx<0)
        return;

    if (enable)
    {
        ui->splitter->widget(ndx)->show();
    }
    else
    {
        ui->splitter->widget(ndx)->hide();
    }
}

bool DisplayDockWidget::getOScopeOn() const
{
    return oScopeOn;
}

void DisplayDockWidget::setOScopeOn(bool enable)
{
    oScopeOn = enable;

    if (!ui || !ui->splitter)
        return;

    int ndx = ui->splitter->indexOf(oscope);
    if (ndx<0)
        return;

    if (enable)
    {
        ui->splitter->widget(ndx)->show();
    }
    else
    {
        ui->splitter->widget(ndx)->hide();
    }
}

bool DisplayDockWidget::getFillTrace() const
{
    return fillTrace;
}

QColor DisplayDockWidget::fillColor() const
{
    return m_fillColor;
}

void DisplayDockWidget::setFillTrace(bool value)
{
    fillTrace = value;

    if (value==false) {
        spectrumCurve.setBrush(Qt::NoBrush);
        spectrumCurve.setPen(m_spectrumColor, 1.0, Qt::SolidLine);
        spectrumCurve.setCurveAttribute(QwtPlotCurve::Fitted, false);
        spectrumCurve.setStyle(QwtPlotCurve::Lines);
    } else {
        resizeLGrad();
        spectrumCurve.setBrush(lgrad);
        spectrumCurve.setPen(QPen(lgrad, 1.0));
        spectrumCurve.setCurveAttribute(QwtPlotCurve::Fitted, true);
        spectrumCurve.setStyle(QwtPlotCurve::Sticks);
    }
}

void DisplayDockWidget::setFillColor(const QColor &fillColor)
{
    m_fillColor = fillColor;
    setFillTrace(fillTrace);
}

void DisplayDockWidget::propSetFillColor(const QColor &value)
{
    m_fillColor = value;
    setFillTrace(fillTrace);
    emit fillColorChanged(m_fillColor);
}

void DisplayDockWidget::setGradBrush(const QBrush &gradBrush)
{
    m_gradBrush = gradBrush;
    setFillTrace(fillTrace);
}

QColor DisplayDockWidget::titleColor() const
{
    return m_titleColor;
}

void DisplayDockWidget::setTitleColor(const QColor &titleColor)
{
    m_titleColor = titleColor;
}

void DisplayDockWidget::propSetTitleColor(const QColor &value)
{
    m_titleColor = value;
    emit titleColorChanged(m_titleColor);
}

void DisplayDockWidget::propSetSGridColor(const QColor &value)
{
    m_sGridColor = value;
    emit sGridColorChanged(m_sGridColor);
}

void DisplayDockWidget::propSetDbGridColor(const QColor &value)
{
    m_dbGridColor = value;
    dbGrid->setColor(m_dbGridColor);
    emit dbGridColorChanged(m_dbGridColor);
}

void DisplayDockWidget::propSetFGridColor(const QColor &value)
{
    m_fGridColor = value;
    fGrid->setColor(m_fGridColor);
    emit fGridColorChanged(m_fGridColor);
}

void DisplayDockWidget::setSGridColor(const QColor &value)
{
    m_sGridColor = value;
    sGrid->setColor(m_sGridColor);
}

QColor DisplayDockWidget::getSpectrumColor() const
{
    return m_spectrumColor;
}

//back set the color to the plotdrawoptions spectrum color widget
//so it shows correct color
void DisplayDockWidget::propSetSpectrumColor(const QColor &value)
{
    m_spectrumColor = value;
    emit spectrumColorChanged(m_spectrumColor);
}

void DisplayDockWidget::setSpectrumColor(const QColor &value)
{
    m_spectrumColor = value;
    setFillTrace(fillTrace);
}

int DisplayDockWidget::getMaxY() const
{
    return yAxisMaxY;
}

void DisplayDockWidget::setMaxY(int value)
{
    if (value != yAxisMaxY)
    {
        yAxisMaxY = value;
        setRebuildBands(true);
        emit yMaxChanged(yAxisMaxY);
        emit rebuildBandsChanged(true); // why would you send false? hmmm...
    }
}

void DisplayDockWidget::enableSGrid(bool enable)
{
    if (enable)
    {
        sGrid->attach(ui->DispPlot);
    }
    else
    {
        sGrid->detach();
    }
}

void DisplayDockWidget::enableDbGrid(bool enable)
{
    if (enable)
    {
        dbGrid->attach(ui->DispPlot);
    }
    else
    {
        dbGrid->detach();
    }
}

void DisplayDockWidget::enableFGrid(bool enable)
{
    if (enable)
    {
        fGrid->attach(ui->DispPlot);
    }
    else
    {
        fGrid->detach();
    }
}

int DisplayDockWidget::getMinY() const
{
    return yAxisMinY;
}

void DisplayDockWidget::setMinY(int value)
{
    if (value != yAxisMinY)
    {
        yAxisMinY = value;
        setRebuildBands(true);
        emit yMinChanged(yAxisMinY);
    }
}

bool DisplayDockWidget::event(QEvent *event)
{
    //if(event->type() == QEvent::Paint)
    //{
        //drawWaterfall();
    //} else
    if(event->type() == QEvent::HoverMove)
    {
        //QHoverEvent *e = dynamic_cast<QHoverEvent*>(event);
        //QPointF p = e->posF();

        const QwtScaleMap xMap = ui->DispPlot->canvasMap(QwtPlot::xBottom);
        const QwtScaleMap yMap = ui->DispPlot->canvasMap(QwtPlot::yLeft);
        //const QPointF tl0 = QwtScaleMap::transform(xMap, yMap, tl);
        //const QPointF br0 = QwtScaleMap::transform(xMap, yMap, br);

        //QwtInterval i = rx1FilterZone.interval();
    }
    else if(event->type() == QEvent::Wheel)
    {
        const int numKElems = NUM_KELEMS;

        // measure elapsed time diff in microseconds
        gettimeofday(&end_time, NULL);
        unsigned long long diff = timeval_diff(&start_time, &end_time);
        gettimeofday(&start_time, NULL);

        // smoothing, if we choose to use it
        if (numKElems>1)
        {
            unsigned long long val = diff;
            // average this val with the previous vals
            for (int x=0; x<numKElems; x++)
            {
                if (!lastDiff[x])
                    lastDiff[x] = diff;
                val = val + lastDiff[x];
            }
            // and save the current averages value
            lastDiff[0] = val/numKElems;
            // move the averaging filter down a notch
            for (int x=numKElems-1; x>0; x--)
                lastDiff[x] = lastDiff[x-1];
        }

        QWheelEvent *ev2 = dynamic_cast<QWheelEvent*>(event);
        double dr = (double)ev2->angleDelta().y() / 120;

        // short circuit direction changes
        if ((m_scrollLastDirection!=0) && (signbit(dr) != signbit(m_scrollLastDirection)))
        {
            if (m_scrolling)
            {
                QWheelEvent *ev = new QWheelEvent(QPointF(0,0),
                                                  QPoint(0, 0),
                                                  QPoint(0, 0),
                                                  QPoint(0, 0),
                                                  0,
                                                  Qt::Vertical,
                                                  ev2->buttons(),
                                                  ev2->modifiers(),
                                                  Qt::ScrollEnd);
                theRsdk->m_app->removePostedEvents(this, QEvent::Wheel);
                theRsdk->m_app->postEvent(this, ev, Qt::LowEventPriority);
            }

            m_scrollLastDirection = 0; // no last scrolling direction
            m_scrolling = false;
            for (int x=0; x<numKElems; x++)
                lastDiff[x] = 7400;
            dr =  dr < -1.0 ? -1.0 : dr > 1.0 ? 1.0 : dr; // slow down the transition to the other direction
        }

        double absdr = abs(dr);
        double tr = (double)theRsdk->getRsdkTuneRate();

        double linearity = 2.0;
        double intersection = 1.0;

        double nvel = (1.0/(intersection+linearity)) * (absdr*(absdr*linearity));
        // range limit the result
        // we always want to move at least 1.0
        if (nvel < 1)
            nvel = 1;

        //if (numKElems>1)
        //    diff = lastDiff[0];

        QPoint normjump(0,120);
        QPoint midjump(0,240);

        // keep things going in the right direction
        if (dr < 0)
        {
            nvel = -nvel;
            normjump.setY(-(normjump.y()));
            midjump.setY(-midjump.y());
        }

        // maintain the sign
        m_scrollLastDirection = dr;

        // regular scroll events start here
        // unless we're already scrolling, in which case
        // we may want to add speed a/o time
        if (ev2->phase() == Qt::NoScrollPhase)
        {
            if (!m_scrolling)
            {
                if (absdr>=10.0)
                {
                    m_scrolling = true;

                    QWheelEvent *ev = new QWheelEvent(QPoint(0,0),
                                                      QPoint(0,0),
                                                      QPoint(0,0),
                                                      normjump,
                                                      0,
                                                      Qt::Vertical,
                                                      ev2->buttons(),
                                                      ev2->modifiers(),
                                                      Qt::ScrollBegin);
                    //theRsdk->m_app->removePostedEvents(this, QEvent::Wheel);
                    theRsdk->m_app->postEvent(this, ev, Qt::LowEventPriority);
                }
            }
            else // if (m_scrolling) // add time, speed to scroll as we've obviously twisted the wheel while scrolling
            {
                m_scrollDecay = m_scrollDecay + 150; // increment delay 150 msec.

            }
        }
        else if (ev2->phase() == Qt::ScrollBegin)
        {
            gettimeofday(&m_scrollDecayStart, NULL);
            QWheelEvent *ev = new QWheelEvent(QPointF(0,0),
                                              ev2->globalPos(),
                                              ev2->pixelDelta(),
                                              normjump,
                                              0,
                                              Qt::Vertical,
                                              ev2->buttons(),
                                              ev2->modifiers(),
                                              Qt::ScrollUpdate);
            //theRsdk->m_app->removePostedEvents(this, QEvent::Wheel);
            theRsdk->m_app->postEvent(this, ev, Qt::LowEventPriority);
        }
        else if (ev2->phase() == Qt::ScrollUpdate)
        {
            unsigned long long diff = timeval_diff(&m_scrollDecayStart, &end_time);
            double elapsed = (double)diff / (m_scrollDecay*1000); // end after m_scrollDecay milliseconds, approx
            QWheelEvent *ev;
            if (elapsed >= 1.0) // end after m_scrollDecay milliseconds, approx
            {
                ev = new QWheelEvent(QPointF(0,0),
                                     ev2->globalPos(),
                                     ev2->pixelDelta(),
                                     midjump,
                                     0,
                                     Qt::Vertical,
                                     ev2->buttons(),
                                     ev2->modifiers(),
                                     Qt::ScrollEnd);
            }
            else
            {
                if (elapsed < 0.10)
                {
                    ev = new QWheelEvent(QPointF(0,0),
                                         ev2->globalPos(),
                                         ev2->pixelDelta(),
                                         midjump,
                                         0,
                                         Qt::Vertical,
                                         ev2->buttons(),
                                         ev2->modifiers(),
                                         Qt::ScrollUpdate);
                }
                else if (elapsed < 0.25)
                {
                    ev = new QWheelEvent(QPointF(0,0),
                                         ev2->globalPos(),
                                         ev2->pixelDelta(),
                                         midjump,
                                         0,
                                         Qt::Vertical,
                                         ev2->buttons(),
                                         ev2->modifiers(),
                                         Qt::ScrollUpdate);
                }
                else if (elapsed < 0.50)
                {
                    ev = new QWheelEvent(QPointF(0,0),
                                         ev2->globalPos(),
                                         ev2->pixelDelta(),
                                         midjump,
                                         0,
                                         Qt::Vertical,
                                         ev2->buttons(),
                                         ev2->modifiers(),
                                         Qt::ScrollUpdate);
                }

                else if (elapsed < 0.80)
                {
                    ev = new QWheelEvent(QPointF(0,0),
                                         ev2->globalPos(),
                                         ev2->pixelDelta(),
                                         midjump,
                                         0,
                                         Qt::Vertical,
                                         ev2->buttons(),
                                         ev2->modifiers(),
                                         Qt::ScrollUpdate);
                }
                else //if (/* last of jump */)
                {
                    ev = new QWheelEvent(QPointF(0,0),
                                         ev2->globalPos(),
                                         ev2->pixelDelta(),
                                         normjump,
                                         0,
                                         Qt::Vertical,
                                         ev2->buttons(),
                                         ev2->modifiers(),
                                         Qt::ScrollUpdate);
                }

            }
            theRsdk->m_app->postEvent(this, ev, Qt::HighEventPriority);
        }
        else if (ev2->phase() == Qt::ScrollEnd)
        {
            m_scrolling = false;
            m_scrollDecay = 150; // reset

            for (int x=0; x<numKElems; x++)
                lastDiff[x] = 7400;
        }

        //emit freqChanged(theRsdk->VfoDock->getFreq() + (qlonglong)(nvel * tr));
        qlonglong f = theRsdk->rozy->getRxFreq(0);
        if (theRsdk->VfoDock->pageTune())
            f = theRsdk->VfoDock->getFreq();
        f += (qlonglong)(nvel * tr);
        //emit freqChanged(f);
        theRsdk->setRsdkFreq(f);
    }
    else if (event->type() == QEvent::KeyRelease)
    {
        QString key = dynamic_cast<QKeyEvent*>(event)->text();
    }
    else if (event->type() == QEvent::MouseButtonRelease)
    {
        //QPointF pos = dynamic_cast<QMouseEvent*>(event)->posF();
        m_activeDragXPos = 0.0;
        m_activeSlideXPos = 0.0;
    }
    else if (event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *e = dynamic_cast<QMouseEvent*>(event);
        if (e->buttons() == Qt::LeftButton)
        {
            QPoint pos = e->pos();
            m_activeDragXPos = pos.x();
            /*
             * we don't do this here because it starts the drag wherever the mouse
             * happens to be, not from the current frequency
             *
            QwtScaleMap xmap = ui->DispPlot->canvasMap(QwtPlot::xBottom);
            double v1 = xmap.invTransform(pos.x());
            double tr = (double)theRsdk->getRsdkTuneRate();
            v1 -= fmod(v1, tr);
            qlonglong f = (qlonglong)v1;
            emit freqChanged(f);
            */
        }
        else if (e->buttons() == Qt::MiddleButton)
        {
            // "slide" needs page tune mode
            if (theRsdk->VfoDock->pageTune())
            {
                QPoint pos = e->pos();
                m_activeSlideXPos = pos.x();
            }
        }
        else if (e->buttons() == Qt::RightButton)
        {
            theRsdk->VfoDock->nextTuneInc();
        }
    }
    else if (event->type() == QEvent::MouseMove)
    {
        QPoint pos = dynamic_cast<QMouseEvent*>(event)->pos();
        if (m_activeDragXPos)
        {
            // drag to new frequency
            double w = (double)ui->DispPlot->canvas()->width() - 4;
            double diff = (m_activeDragXPos - (double)pos.x());
            double fdiff = diff * ((xAxisMaxX - xAxisMinX) / w);
            double tr = (double)theRsdk->getRsdkTuneRate();
            if (fabs(fdiff) >= tr)
            {
                fdiff -= fmod(fdiff, tr);

                qlonglong f = theRsdk->rozy->getRxFreq(0);
                if (theRsdk->VfoDock->subRxOn())
                {
                   f = theRsdk->VfoDock->freqR2();
                   f -= (qlonglong)fdiff;
                }
                else if (theRsdk->VfoDock->pageTune())
                {
                    f = theRsdk->VfoDock->getFreq();
                    f -= (qlonglong)fdiff;
                } else {
                    f += (qlonglong)fdiff;
                }
                //emit freqChanged(f);
                theRsdk->setRsdkFreq(f);
                m_activeDragXPos = pos.x();
            }
        }
        // middle mouse button slide the display and the frequency
        if (m_activeSlideXPos)
        {
            // slide to new frequency
            double w = (double)ui->DispPlot->canvas()->width() - 4;
            double diff = (m_activeSlideXPos - (double)pos.x());
            double fdiff = diff * ((xAxisMaxX - xAxisMinX) / w);
            double tr = (double)theRsdk->getRsdkTuneRate();
            if (fabs(fdiff) >= tr)
            {
                // this gets a little complicated
                fdiff -= fmod(fdiff, tr);
                qlonglong centerf = theRsdk->rozy->getRxFreq(0);
                qlonglong vfof = theRsdk->VfoDock->getFreq();
                centerf += (qlonglong)fdiff;
                theRsdk->rozy->rxFreq[theRsdk->rozy->curMercury][0] = centerf;
                theRsdk->setRsdkFreq(vfof);
                m_activeSlideXPos = pos.x();
            }
        }
    }
    else if (event->type() == QEvent::MouseButtonDblClick)
    {
        // double click to this frequency
        QPoint pos = dynamic_cast<QMouseEvent*>(event)->pos();
        m_activeDragXPos = pos.x();
        QwtScaleMap xmap = ui->DispPlot->canvasMap(QwtPlot::xBottom);
        double v1 = xmap.invTransform(pos.x());
        //double tr = (double)theRsdk->getRsdkTuneRate();
        //double v2 = fmod(v1, tr);
        qlonglong f = (qlonglong)round(v1);
        //emit freqChanged(f);
        theRsdk->setRsdkFreq(f);
    }
    else if (event->type() == QEvent::Resize ||
             event->type() == QEvent::LayoutRequest)
    {
        m_rebuildBands = true;
        resizeLGrad();
        setFilterCoeffs(filterCoeff);
        setFillTrace(fillTrace);
    }
    else if (event->type() == QEvent::Show)
    {
        resizeLGrad();
        if (m_waterfallEnable || m_spectrumEnable)
            if (!snapper->isRunning())
                snapper->startTimer();
        //drawWaterfallTimer.start();
    }
    else if (event->type() == QEvent::Hide)
    {
        snapper->stopTimer();
        //drawWaterfallTimer.stop();
    }

    return QDockWidget::event(event);
}

int DisplayDockWidget::getFilterCoeffs() const
{
    return filterCoeff;
}

void DisplayDockWidget::setFilterCoeffs(int value)
{
    filterCoeff = value;

    spectrumCurve.setRawSamples(xes, yes, DTTSP_SNAP_SAMPLES);
    //ui->DispPlot->setAxisScale(QwtPlot::xTop, 0, DTTSP_SNAP_SAMPLES, 0);
    setXAxis(theRsdk->rozy->getRxFreq(theRsdk->rozy->curMercury));
    //QRectF size(xAxisMinX, yAxisMinY, xAxisMaxX-xAxisMinX, yAxisMaxY-yAxisMinY);
    //zoom->setZoomBase(size);
}

void DisplayDockWidget::on_splitter_splitterMoved(int pos, int index)
{
    (void)pos; // unused, shut up the compiler
    (void)index;
    m_rebuildBands = true;
    resizeLGrad();
    setFilterCoeffs(filterCoeff);
    setFillTrace(fillTrace);
}

void DisplayDockWidget::on_zoomed(const QRectF &rect)
{
    (void)rect;
    /*
     double x = rect.x();
     double w = rect.width();
     double s = x + w;
    if (s > x) {
        xAxisMaxX = s;
        xAxisMinX = x;
    } else {
        xAxisMaxX = x;
        xAxisMinX = s;
    }
    */
}
