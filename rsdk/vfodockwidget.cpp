#include <iostream>
#include <stdio.h>
//#include <QTextStream>
#include <QMetaType>
#include <QMessageBox>
#include <QMouseEvent>
#include <QWheelEvent>

#include "rsdk.h"

#include "vfodockwidget.h"
#include "ui_vfodockwidget.h"

// TODO FIXME
extern rsdk *r;

using namespace std;

QList<QString> VfoDockWidget::tuneRates  = QList<QString>() \
            << QString("1Hz")   \
            << QString("5Hz")   \
            << QString("10Hz")  \
            << QString("25Hz")  \
            << QString("50Hz")  \
            << QString("100Hz") \
            << QString("250Hz") \
            << QString("500Hz") \
            << QString("1KHz")  \
            << QString("2.5KHz")\
            << QString("5KHz")  \
            << QString("10KHz") \
            << QString("25KHz") \
            << QString("50KHz") \
            << QString("100KHz");

VfoDockWidget::VfoDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::VfoDockWidget),
    m_freqLabelA(""),
    m_freqLabelB(""),
    m_freqLabelR2(""),
    m_tuneInc(vfoTuneInc_1KHz),
    m_freqMode(vfoFreqMode_KHz),
    m_currVfo(vfoSelect_A),
    m_currRx(1),
    m_cal(0.0),
    m_calFreq(0.0),
    m_calEnable(false),
    m_subRxOn(false),
    m_pageTune(false)
{
    // register the enums, sigh
    qRegisterMetaType<vfoTuneInc>();
    qRegisterMetaType<vfoFreqMode>();
    qRegisterMetaType<vfoCurrSel>();

    // always at least one on the stack
    m_fA = 10125000L;
    m_fB = 14250000L;

    ui->setupUi(this);

    QString val;
    foreach (val, tuneRates) { ui->tuneRateComboBox->addItem(val); }

    connect(ui->tuneRateComboBox, SIGNAL(activated(QString)), this, SLOT(setTuneRate(QString)));

    m_freqLabelA = makeFreqLabel(m_fA);
    m_freqLabelB = makeFreqLabel(m_fB);
    m_freqLabelR2 = makeFreqLabel(m_fA);

    connect(ui->calLineEdit, SIGNAL(editingFinished()), this, SLOT(calLineEdit_editingFinished()));
    connect(ui->vfoALineEdit, SIGNAL(editingFinished()), this, SLOT(vfoALineEdit_editingFinished()));
    connect(ui->vfoBLineEdit, SIGNAL(editingFinished()), this, SLOT(vfoBLineEdit_editingFinished()));
    connect(ui->vfoR2LineEdit, SIGNAL(editingFinished()), this, SLOT(vfoR2LineEdit_editingFinished()));
    connect(ui->vfoMainLineEdit, SIGNAL(editingFinished()), this, SLOT(vfoMainLineEdit_editingFinished()));
    connect(ui->freqModeBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(freqModeKeyPress(QAbstractButton*)));
    connect(ui->calTB, SIGNAL(toggled(bool)), this, SLOT(calTB_toggled(bool)));
    connect(ui->enaCalTB, SIGNAL(toggled(bool)), this, SLOT(setCalEnable(bool)));
}

VfoDockWidget::~VfoDockWidget()
{
    //qDeleteAll(m_freqH);
    delete ui;
}

void VfoDockWidget::start()
{
    setCurrVfo(m_currVfo);
    setFreqMode(m_freqMode);
    setTuneInc(m_tuneInc);
    setSubRxOn(m_subRxOn);
    setPageTune(m_pageTune);
    setCal(m_cal);
    setCalFreq(m_calFreq);
    setCalEnable(m_calEnable);

    doFreqLabels(true);

    emit subRxChanged(m_subRxOn);
    emit pageTuneChanged(m_pageTune);
}

void VfoDockWidget::setOpacity(bool b)
{
    if (b)
        setWindowOpacity(0.80);
}

void VfoDockWidget::updateHandler()
{
    // what about page tune? this will just get the center frequency
    //qlonglong f = theRsdk->rozy->getRxFreq(theRsdk->rozy->getCurMercury());
    qlonglong f = theRsdk->rozy->getRxFreq(theRsdk->rozy->getCurMercury());

    // adjust here for page tune? what are we trying to do here...???
    // we're trying to make sure something external that sets the theRsdk->rozy->freq
    // gets noticed and we update the vfo accordingly
    qlonglong vfof = getFreq();

    // track so we don't have to actually update all the time
    if (!m_pageTune ) {
        if (!m_subRxOn) {
            if ((int)f != (int)vfof) {
                setFreq(f, false);
            }
        }
    }
}

qlonglong VfoDockWidget::getFreqA() const
{
    return m_fA;
}

void VfoDockWidget::setFreqA(qlonglong f)
{
    setFreqA(f, true);
}

void VfoDockWidget::setFreqA(qlonglong f, bool sendSig)
{
    m_fA = f;
    QString label = makeFreqLabel(m_fA);
    setFreqLabelXX(&m_fA, label, ui->vfoALineEdit);
    m_freqLabelA = label;
    doFreqLabels(sendSig);
}

qlonglong VfoDockWidget::getFreqB() const
{
    return m_fB;
}

void VfoDockWidget::setFreqB(qlonglong f)
{
    setFreqB(f, true);
}

void VfoDockWidget::setFreqB(qlonglong f, bool sendSig)
{
    m_fB = f;
    QString label = makeFreqLabel(m_fB);
    setFreqLabelXX(&m_fB, label, ui->vfoBLineEdit);
    m_freqLabelB = label;
    doFreqLabels(sendSig);
}

qlonglong VfoDockWidget::getFreq() const
{

    //if (ui->vfoR2_PB->isChecked())
    //    return freqR2();
    if (ui->vfoB_PB->isChecked())
        return getFreqB();
    else
        return getFreqA();
}

void VfoDockWidget::setFreq(qlonglong f, bool sendSig)
{
    if (f < 1LL)
        f = 1LL;

    if (ui->vfoR2_PB->isChecked())
        setFreqR2(f, sendSig);
    else if (ui->vfoB_PB->isChecked())
        setFreqB(f, sendSig);
    else
        setFreqA(f, sendSig);
}

QString VfoDockWidget::freqLabel() const
{
    if (ui->vfoB_PB->isChecked())
        return m_freqLabelB;
    else
        return m_freqLabelA;
}

void VfoDockWidget::modeChanged(SDRMODE_DTTSP mode)
{
    (void)mode;
    doFreqLabels(true);
}

// keep track of transmit state for anything that needs it
bool VfoDockWidget::getXmit() const
{
    return m_xmit;
}

void VfoDockWidget::setXmit(bool xmit)
{
    m_xmit = xmit;

    if (m_xmit)
    {
        ui->vfoMainLineEdit->setStyleSheet("QLineEdit { border: 1px solid red }");
    } else {
        ui->vfoMainLineEdit->setStyleSheet("QLineEdit { border: 1px solid #1e1e1e }");
    }
}

// ohso ugly
void VfoDockWidget::doFreqLabels(bool sendSig)
{
    // set the frequency to the label's new value
    setFreqLabelXX(&m_fA, m_freqLabelA, ui->vfoALineEdit);
    setFreqLabelXX(&m_fB, m_freqLabelB, ui->vfoBLineEdit);
    setFreqLabelXX(&m_freqR2, m_freqLabelR2, ui->vfoR2LineEdit);

    // emit the notifications
    if (ui->vfoA_PB->isChecked())
    {
        QString label = m_freqLabelA;
        label.append(" ");
        if (r && theRsdk->ModeDock)
        {
            theRsdk->ModeDock->setMode((int)theRsdk->rozy->getMode(), false);
            label.append(theRsdk->ModeDock->getModeStr());
        }

        bool pstate = ui->vfoMainLineEdit->blockSignals(true);
        ui->vfoMainLineEdit->setText(label);
        ui->vfoMainLineEdit->blockSignals(pstate);

        if (sendSig)
            emit freqChanged(m_fA);
    }
    else if (ui->vfoB_PB->isChecked())
    {
        QString label = m_freqLabelB;
        label.append(" ");
        if (r && theRsdk->ModeDock)
        {
            theRsdk->ModeDock->setMode((int)theRsdk->rozy->getMode(), false);
            label.append(theRsdk->ModeDock->getModeStr());
        }

        bool pstate = ui->vfoMainLineEdit->blockSignals(true);
        ui->vfoMainLineEdit->setText(label);
        ui->vfoMainLineEdit->blockSignals(pstate);

        if (sendSig)
            emit freqChanged(m_fB);
    }
    else if (ui->vfoR2_PB->isChecked())
    {
        QString label = m_freqLabelR2;
        bool pstate = ui->vfoR2LineEdit->blockSignals(true);
        ui->vfoR2LineEdit->setText(label);
        ui->vfoR2LineEdit->blockSignals(pstate);

        //if (sendSig)
            //emit subRxFreqChanged(m_freqR2);
    }
}

QString VfoDockWidget::makeFreqLabel(qlonglong freq)
{
    QString label;
    if (m_freqMode == vfoFreqMode_KHz)
        label = QLocale(QLocale::English).toString((double)freq/1000.0, 'f', 3)+ " KHz";
    else if (m_freqMode == vfoFreqMode_MHz)
        label = QLocale(QLocale::English).toString((double)freq/1000000.0, 'f', 6)+ " MHz";
    else //if (m_freqMode == vfoFreqMode_Hz)
        label = QLocale(QLocale::English).toString((double)freq, 'f', 0) + " Hz";
    return label;
}

QString VfoDockWidget::freqLabelR2() const
{
    return m_freqLabelR2;
}

void VfoDockWidget::setFreqLabelR2(const QString &freqLabelR2)
{
    m_freqLabelR2 = freqLabelR2;
    doFreqLabels(true);
}

// this is a bit odd
// the fReg ends up being set to the value in the freqLabel
// then the label is remade and reapplied to the line edit it came from
void VfoDockWidget::setFreqLabelXX(qlonglong *fReg, QString &freqLabel, QLineEdit *le)
{
    bool ok;
    double mult = 1.0;

    if (freqLabel.contains("MHz"))
    {
        mult = 1e6;
    }
    else if (freqLabel.contains("KHz"))
    {
        mult = 1e3;
    }
    else //if (label.contains("Hz")) // default
    {
        mult = 1.0;
    }

    // remove 'Hz' and ',' from string to make a long long
    QRegExp rx ( "[a-zA-Z]|,");
    freqLabel.remove(rx);

    double cval = QLocale(QLocale::English).toDouble(freqLabel, &ok);
    cval = cval * mult;

    if (ok==true)
    {
        long long val = (long long) cval;
        // round to the tune increment value as a convenience
        val = val - (val % tuneRate_asInt());

        // relative offset of current value
        if (freqLabel.size() && (freqLabel.at(0)=='+' || freqLabel.at(0)=='-'))
            *fReg += val;
        else
            // absolute new value
            *fReg = val;
    }

    freqLabel = makeFreqLabel(*fReg);
    bool pstate = le->blockSignals(true);
    le->setText(freqLabel);
    le->blockSignals(pstate);
}

bool VfoDockWidget::pageTune() const
{
    return m_pageTune;
}

void VfoDockWidget::setPageTune(bool pageTune)
{
    m_pageTune = pageTune;
    bool pstate = ui->pageTune_TB->blockSignals(true);
    ui->pageTune_TB->setChecked(m_pageTune);
    ui->pageTune_TB->blockSignals(pstate);
    emit pageTuneChanged(m_pageTune);
}

void VfoDockWidget::on_pageTune_TB_toggled(bool checked)
{
    setPageTune(checked);
}

bool VfoDockWidget::subRxOn() const
{
    return m_subRxOn;
}

void VfoDockWidget::setSubRxOn(bool subRxOn)
{
    m_subRxOn = subRxOn;
    if (m_subRxOn)
    {
        // set initial R2 (aka subRx) to current VFO freq
        qlonglong f = getFreq();
        setFreqR2(f, true);
    }
    emit subRxChanged(m_subRxOn);
}

qlonglong VfoDockWidget::freqR2() const
{
    return m_freqR2;
}

void VfoDockWidget::setFreqR2(const qlonglong &freqR2, bool sendSig)
{
    // unconst it as we will range limit it
    qlonglong f = freqR2;

    // range limit the subRX so OSC doesn't roll past ends
    // m_freqR2 will be the subRx, so possible range is
    // F+-1/2SAMPRATE, where F is vfo current freq (either A or B)
    const int sampRate = theRsdk->getRsdkSampleRate();
    double lof = getFreq() - ( sampRate / 2);
    double hif = getFreq() + ( sampRate / 2);
    if (f < lof)
        f = lof;
    else if (f > hif)
        f = hif;

    m_freqR2 = f;

    QString fLabel = makeFreqLabel(m_freqR2);
    setFreqLabelXX(&m_freqR2, fLabel, ui->vfoR2LineEdit);
    m_freqLabelR2 = fLabel;
    doFreqLabels(sendSig);

    theRsdk->rozy->setFreq(m_freqR2, 1);
    //if (sendSig)
        //emit subRxFreqChanged(m_freqR2);
}

bool VfoDockWidget::calEnable() const
{
    return m_calEnable;
}

void VfoDockWidget::setCalEnable(bool calEnable)
{
    m_calEnable = calEnable;
    if (m_calEnable)
    {
        ui->enaCalTB->setStyleSheet("background-color:darkGreen;");
    }
    else
    {
        ui->enaCalTB->setStyleSheet("background-color:grey50;");
    }
    setFreq(getFreq(), true);
}

double VfoDockWidget::calFreq() const
{
    return m_calFreq;
}

void VfoDockWidget::setCalFreq(double calFreq)
{
    m_calFreq = calFreq;
}

double VfoDockWidget::cal() const
{
    double ret = 0.0;

    if (m_calEnable)
        ret = m_cal;

    return ret;
}

// set the Hz/MHz calibration oscillator
void VfoDockWidget::setCal(double cal)
{
    // Hz/MHz
    m_cal = cal;
    //double calHzPerMHz = 1e6 * (1.0 - m_cal);
    QString label = QLocale(QLocale::English).toString(m_cal, 'f', 2)+ " Hz/MHz";
    ui->calLineEdit->setText(label);
    /*
    if (r && theRsdk->ModeDock)
        theRsdk->ModeDock->setMode((int)theRsdk->rozy->getMode(), false);
        */
    emit freqChanged(getFreq());
}

int VfoDockWidget::currRx() const
{
    return m_currRx;
}

void VfoDockWidget::setCurrRx(int currRx)
{
    m_currRx = currRx;
}


VfoDockWidget::vfoFreqMode VfoDockWidget::freqMode() const
{
    return m_freqMode;
}

void VfoDockWidget::setFreqMode(const vfoFreqMode &freqMode)
{
    m_freqMode = freqMode;
    setFreqModeBtn();
}

VfoDockWidget::vfoCurrSel VfoDockWidget::currVfo() const
{
    return m_currVfo;
}

void VfoDockWidget::setCurrVfo(const vfoCurrSel &currVfo)
{
    m_currVfo = currVfo;
    if (m_currVfo == vfoSelect_B)
        ui->vfoB_PB->click();
    else //if (m_currVfo == vfoSelect_A)
        ui->vfoA_PB->click();
}

QString VfoDockWidget::freqLabelB() const
{
    return m_freqLabelB;
}

void VfoDockWidget::setFreqLabelB(const QString &freqLabelB)
{
    m_freqLabelB = freqLabelB;
    setFreqLabelXX(&m_fB, m_freqLabelB, ui->vfoBLineEdit);
    if (ui->vfoB_PB->isChecked())
        emit freqChanged(m_fB);
}

void VfoDockWidget::nextTuneInc()
{
    // this is bad form
    int rate = (int)m_tuneInc;
    rate++;
    // range limit to the max
    if (rate > (int)vfoTuneInc_FINAL)
        m_tuneInc = vfoTuneInc_1Hz;
    else
        m_tuneInc = (vfoTuneInc)rate;
    setTuneInc(m_tuneInc);
}

int VfoDockWidget::nextTuneIncPeek()
{
    int rate = (int)m_tuneInc;
    rate++;
    // range limit to the max
    if (rate >= (int)vfoTuneInc_FINAL)
        rate = (int)vfoTuneInc_FINAL-1; // evil chicanery because there's no way to find the number of enum entries.
    return rate;
}

void VfoDockWidget::prevTuneInc()
{
    int rate = (int)m_tuneInc;
    rate--;
    if (rate<0)
        rate = 0;
    m_tuneInc = (vfoTuneInc)rate;
}

int VfoDockWidget::prevTuneIncPeek()
{
    int rate = (int)m_tuneInc;
    rate--;
    if (rate<0)
        rate = 0;
    return rate;
}

QString VfoDockWidget::freqLabelA() const
{
    return m_freqLabelA;
}

void VfoDockWidget::setFreqLabelA(const QString &freqLabelA)
{
    m_freqLabelA = freqLabelA;
    setFreqLabelXX(&m_fA, m_freqLabelA, ui->vfoALineEdit);
    if (ui->vfoA_PB->isChecked())
        emit freqChanged(m_fA);
}

int VfoDockWidget::tuneRate_asInt() const
{
    int ret = 0;
    switch(m_tuneInc)
    {
        case vfoTuneInc_1Hz:
            ret = 1;
            break;
        case vfoTuneInc_5Hz:
            ret = 5;
            break;
        case vfoTuneInc_10Hz:
            ret = 10;
            break;
        case vfoTuneInc_25Hz:
            ret = 25;
            break;
        case vfoTuneInc_50Hz:
            ret = 50;
            break;
        case vfoTuneInc_100Hz:
            ret = 100;
            break;
        case vfoTuneInc_250Hz:
            ret = 250;
            break;
        case vfoTuneInc_500Hz:
            ret = 500;
            break;
        case vfoTuneInc_1KHz:
            ret = 1000;
            break;
        case vfoTuneInc_2_5KHz:
            ret = 2500;
            break;
        case vfoTuneInc_5KHz:
            ret = 5000;
            break;
        case vfoTuneInc_10KHz:
            ret = 10000;
            break;
        case vfoTuneInc_25KHz:
            ret = 25000;
            break;
        case vfoTuneInc_50KHz:
            ret = 50000;
            break;
        case vfoTuneInc_100KHz:
            ret = 100000;
            break;
        default: // default shall be 1KHz
            ret = 1000;
            break;
    }
    return ret;
}

VfoDockWidget::vfoTuneInc VfoDockWidget::tuneInc() const
{
    return m_tuneInc;
}

int VfoDockWidget::tuneIncByNdx(const int ndx)
{
    int ret;
    switch(ndx)
    {
    case vfoTuneInc_1Hz:
        ret = 1;
        break;
    case vfoTuneInc_5Hz:
        ret = 5;
        break;
    case vfoTuneInc_10Hz:
        ret = 10;
        break;
    case vfoTuneInc_25Hz:
        ret = 25;
        break;
    case vfoTuneInc_50Hz:
        ret = 50;
        break;
    case vfoTuneInc_100Hz:
        ret = 100;
        break;
    case vfoTuneInc_250Hz:
        ret = 250;
        break;
    case vfoTuneInc_500Hz:
        ret = 500;
        break;
    case vfoTuneInc_1KHz:
        ret = 1000;
        break;
    case vfoTuneInc_2_5KHz:
        ret = 2500;
        break;
    case vfoTuneInc_5KHz:
        ret = 5000;
        break;
    case vfoTuneInc_10KHz:
        ret = 10000;
        break;
    case vfoTuneInc_25KHz:
        ret = 25000;
        break;
    case vfoTuneInc_50KHz:
        ret = 50000;
        break;
    case vfoTuneInc_100KHz:
        ret = 100000;
        break;
    default: // default shall be 1KHz
        ret = 1000;
        break;
    }
    return ret;
}

void VfoDockWidget::setTuneInc(const vfoTuneInc &tuneInc)
{
    m_tuneInc = tuneInc;
    int tr = tuneRate_asInt();
    setTuneInc(tr, true);
}

void VfoDockWidget::setTuneInc(const int tuneRate)
{
    setTuneInc(tuneRate, true);
}

void VfoDockWidget::setTuneInc(const int tuneRate, bool update)
{
    if (tuneRate<0)
        return;

    int rate = tuneRate;

    QString theRate = "1KHz"; // default value

    if(rate >= 0 && rate <=1)
    {
        m_tuneInc = vfoTuneInc_1Hz;
        theRate = "1Hz";
    }
    else if(rate > 1 && rate <= 5)
    {
        m_tuneInc = vfoTuneInc_5Hz;
        theRate = "5Hz";
    }
    else if(rate > 5 && rate <= 10)
    {
        m_tuneInc = vfoTuneInc_10Hz;
        theRate = "10Hz";
    }
    else if(rate > 10 && rate <= 25)
    {
        m_tuneInc = vfoTuneInc_25Hz;
        theRate = "25Hz";
    }
    else if(rate > 25 && rate <= 50)
    {
        m_tuneInc = vfoTuneInc_50Hz;
        theRate = "50Hz";
    }
    else if(rate > 50 && rate <= 100)
    {
        m_tuneInc = vfoTuneInc_100Hz;
        theRate = "100Hz";
    }
    else if(rate > 100 && rate <= 250)
    {
        m_tuneInc = vfoTuneInc_250Hz;
        theRate = "250Hz";
    }
    else if(rate > 250 && rate <= 500)
    {
        m_tuneInc = vfoTuneInc_500Hz;
        theRate = "500Hz";
    }
    else if(rate > 500 && rate <= 1000)
    {
        m_tuneInc = vfoTuneInc_1KHz;
        theRate = "1KHz";
    }
    else if(rate > 1000 && rate <= 2500)
    {
        m_tuneInc = vfoTuneInc_2_5KHz;
        theRate = "2.5KHz";
    }
    else if(rate > 2500 && rate <= 5000)
    {
        m_tuneInc = vfoTuneInc_5KHz;
        theRate = "5KHz";
    }
    else if(rate > 5000 && rate <= 10000)
    {
        m_tuneInc = vfoTuneInc_10KHz;
        theRate = "10KHz";
    }
    else if(rate > 10000 && rate <= 25000)
    {
        m_tuneInc = vfoTuneInc_25KHz;
        theRate = "25KHz";
    }
    else if(rate > 25000 && rate <= 50000)
    {
        m_tuneInc = vfoTuneInc_50KHz;
        theRate = "50KHz";
    }
    else if(rate > 50000)
    {
        m_tuneInc = vfoTuneInc_100KHz;
        theRate = "100KHz";
    }
    if (update)
        setTuneRate(theRate);
}


double VfoDockWidget::convLabelToValue(QString &label)
{
    double cval = 0.0;
    if (label.size())
    {
        // remove any 'MHz', 'KHz', 'Hz' and ',' from string to make a double
        QRegExp rx ( "[a-zA-Z]|,");
        label.remove(rx);

        bool ok = false;
        cval = QLocale(QLocale::English).toDouble(label, &ok);
        if (ok)
        {
            if (m_freqMode == vfoFreqMode_MHz)
            {
                cval = cval * 1e6;
            }
            else if (m_freqMode == vfoFreqMode_KHz)
            {
                cval = cval * 1e3;
            }
        }
    }
    return cval;
}

void VfoDockWidget::calLineEdit_editingFinished()
{
    double dval = 0.0;

    QString newCal = ui->calLineEdit->displayText();
    if (newCal.size())
    {
        // remove any text not a digit
        //QRegExp rx ( "[a-zA-Z]|,");
        //newCal.remove(rx);

        bool ok = false;
        dval = QLocale(QLocale::English).toDouble(newCal, &ok);
        if (ok)
        {
            setCal(dval);
        }
    }
}

void VfoDockWidget::vfoALineEdit_editingFinished()
{
    QString newFreq = ui->vfoALineEdit->displayText();
    double cval = convLabelToValue(newFreq);
    if (cval)
    {
        m_freqLabelA = makeFreqLabel((qlonglong)cval);
        // retain relative mode, minuses are maintained anyway as negative values
        if (newFreq.at(0)=='+')
            m_freqLabelA.prepend("+");
    }
    doFreqLabels(true);
}

void VfoDockWidget::vfoBLineEdit_editingFinished()
{
    QString newFreq = ui->vfoBLineEdit->displayText();
    double cval = convLabelToValue(newFreq);
    if (cval)
    {
        m_freqLabelB = makeFreqLabel((qlonglong)cval);
        // retain relative mode, minuses are maintained anyway as negative values
        if (newFreq.at(0)=='+')
            m_freqLabelB.prepend("+");
    }
    doFreqLabels(true);
}

void VfoDockWidget::vfoR2LineEdit_editingFinished()
{
    QString newFreq = ui->vfoR2LineEdit->displayText();
    double cval = convLabelToValue(newFreq);
    if (cval)
    {
        m_freqLabelR2 = makeFreqLabel((qlonglong)cval);
        // retain relative mode, minuses are maintained anyway as negative values
        if (newFreq.at(0)=='+')
            m_freqLabelR2.prepend("+");
    }
    doFreqLabels(true);
}

void VfoDockWidget::vfoMainLineEdit_editingFinished()
{
    QString newFreq = ui->vfoMainLineEdit->displayText();
    double cval = convLabelToValue(newFreq);
    if (cval)
    {
        if (newFreq.at(0)=='+' || newFreq.at(0)=='-')
            setFreq(getFreq() + (qlonglong)cval, true);
        else
            setFreq((qlonglong)cval, true);
    }
}

void VfoDockWidget::setFreqModeBtn()
{
    switch(m_freqMode)
    {
        case vfoFreqMode_MHz:
            if (!ui->MHzRB->isChecked())
                ui->MHzRB->setChecked(true);
            break;
        case vfoFreqMode_KHz:
            if (!ui->KHzRB->isChecked())
                ui->KHzRB->setChecked(true);
            break;
        case vfoFreqMode_Hz:
        default:
            if (!ui->HzRB->isChecked())
                ui->HzRB->setChecked(true);
            break;
    }

}

void VfoDockWidget::freqModeKeyPress(QAbstractButton *btn)
{
    QString btnName=btn->objectName();

    if (btnName.contains("MHz"))
    {
        m_freqMode = vfoFreqMode_MHz;
    }
    else if (btnName.contains("KHz"))
    {
        m_freqMode = vfoFreqMode_KHz;
    }
    else //if (btnName.contains("KHz")) // default
    {
        m_freqMode = vfoFreqMode_Hz;
    }

    setFreqModeBtn();

    doFreqLabels(true);
}

void VfoDockWidget::on_A2B_PB_released()
{
    qlonglong f= getFreqA();
    setFreqB(f, true);
}

void VfoDockWidget::on_B2A_PB_released()
{
    qlonglong f= getFreqB();
    setFreqA(f, true);
}

// simulate radio buttons buttons,
void VfoDockWidget::on_vfoB_PB_toggled(bool checked)
{
    if (checked)
    {
        if (ui->vfoA_PB->isChecked())
            ui->vfoA_PB->setChecked(false);
        setCurrVfo(vfoSelect_B);
        doFreqLabels(true);

    }
}

void VfoDockWidget::on_vfoA_PB_toggled(bool checked)
{
    if (checked)
    {
        if (ui->vfoB_PB->isChecked())
            ui->vfoB_PB->setChecked(false);
        setCurrVfo(vfoSelect_A);
        doFreqLabels(true);
    }
}

void VfoDockWidget::on_vfoR2_PB_toggled(bool checked)
{
    m_subRxOn = checked;
    doFreqLabels(true);
    setSubRxOn(checked);
}

// move the subRx to the active VFO
void VfoDockWidget::on_S2V_PB_released()
{
    // hmmm, not the best but rsdk setRsdkFreq checks for subRxOn
    // and won't send the theRsdk->rozy->setFreq() if it is on
    // so we call out here and set the new frequency
    if(m_subRxOn)
    {
        const qlonglong f = freqR2();
        setFreq(f, true);
        doFreqLabels(true);
    }
}

void VfoDockWidget::calTB_toggled(bool checked)
{
   if (checked)
   {
       // really cal?
//       QMessageBox confirmBox;
//       confirmBox.setText("Calibration requested");
//       confirmBox.setInformativeText("Continue?");
//       confirmBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
//       confirmBox.setDefaultButton(QMessageBox::No);
//       if (confirmBox.exec() == QMessageBox::No)
//       {
//
//           bool pstate = ui->calTB->blockSignals(true);
//           ui->calTB->setChecked(false);
//           ui->calTB->blockSignals(pstate);
//           return;
//       }
//
//       if (!m_calEnable)
//           // should fire setCalEnable
//           ui->enaCalTB->setChecked(true);
//
       // switch to 1 Hz tune rate to calibrate
       m_prevTuneRate = tuneRate_asInt();
       // and block all tune rate changes until we switch off the calibration mode (see below)
       ui->tuneRateComboBox->blockSignals(true);
       setTuneInc(1);

       // disable previous calibration value
       setCal(0.0);

       // calibration mode turned on, remember current frequency for later use
       m_calFreq = (double)getFreq();
   }
   else
   {
       // calibration mode turned off, at zero-beat, difference ratio in Hz/MHz is calibration value
       double freq = (double) getFreq();
       double val = m_calFreq / freq;
       double calHzPerMHz = 1e6 * (1.0 - val);
       setCal(calHzPerMHz);

       // revert to original frequency
       setFreq((qlonglong)m_calFreq, true);

       // and now m_cal will be used to setRXOsc, setTXOsc in DttSP
       // clear the cal frequency, also used elsewhere to
       // determine if in calibration mode or not, clear it now
       m_calFreq = 0.0;

       // and back to the previous tune rate
       setTuneInc(m_prevTuneRate);
       // and re-enable the tune rate combo box
       ui->tuneRateComboBox->blockSignals(false);
       m_prevTuneRate = 0;
   }
}

bool VfoDockWidget::event(QEvent *event)
{
    if(event->type() == QEvent::Wheel)
    {
        int delta = dynamic_cast<QWheelEvent*>(event)->delta();
        int numDegrees = delta/8;
        int numSteps = numDegrees/15;
        qlonglong f = theRsdk->getRsdkFreq();
        int tuneRate = theRsdk->getRsdkTuneRate();
        qlonglong nf = f + (numSteps * tuneRate);
        emit freqChanged(nf);
    }

    return QDockWidget::event(event);
}

void VfoDockWidget::setTuneRate(QString rate)
{
    int val = 0;
    double cval = 0.0;
    double mult = 1;

    if (rate.size()) {

        int ndx = ui->tuneRateComboBox->findText(rate);

        if (rate.contains("KHz"))
            mult = 1000.0;
        else if (rate.contains("MHz"))
            mult = 1000000.0;

        // remove any 'MHz', 'KHz', 'Hz' and ',' from string to make a double
        QRegExp regex ("[a-zA-Z]|,");
        rate.remove(regex);

        bool ok = false;
        cval = QLocale(QLocale::English).toDouble(rate, &ok);
        if (ok) {
            val = (int) (cval * mult);
            if (ndx >= 0) {
                ui->tuneRateComboBox->setCurrentIndex(ndx);
            }
        }
    }

    if (val) {
        // avoid circular calls
        setTuneInc(val, false);
    }
}
