#ifndef BANDSCOPE_H
#define BANDSCOPE_H

#include <fftw3.h>

#include <QWidget>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QWidget>
#else
#include <QtGui/QWidget>
#endif

#include <QTimer>

#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_zoneitem.h>

#include <qwt_color_map.h>
#include <qwt_plot_legenditem.h>
#include <qwt_plot_rasteritem.h>
#include <qwt_plot_zoomer.h>
#include <qwt_scale_div.h>
#include <qwt_scale_engine.h>
#include <qwt_scale_map.h>

#include "buffer.h"
#include "radioozy.h"
#include "rsdkdockwidget.h"

#define MAX_BANDSCOPE_FREQ 61.40e6
#define BANDSCOPE_NUM_SAMPLES 4096

class BScopeGrid;
class bhfilter;

namespace Ui {
class BandScope;
}

class BandScope : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QColor m_traceColor READ traceColor WRITE setTraceColor)
    Q_PROPERTY(double m_smooth READ smooth WRITE setSmooth)

public:
    explicit BandScope(QWidget *parent = nullptr);
    ~BandScope();

    double *time_buf;
    fftw_complex *freq_buf;
    fftw_plan plan;

    double xes[BANDSCOPE_NUM_SAMPLES];
    double yes[BANDSCOPE_NUM_SAMPLES];
    double lastSamps[64][BANDSCOPE_NUM_SAMPLES];

    bool bandScopeEnabled() const;

    bool event (QEvent *event);

    QColor traceColor() const;

    double smooth() const;

public slots:
    void setBandScopeEnabled(bool bandScopeEnabled);
    void plotBandScope();
    void setTraceColor(const QColor &traceColor);
    void setSmooth(double smooth);
    void setSmooth(int smooth);

signals:
    void bandScopeColorChanged(QColor color);
    void freqChanged(qlonglong newFreq);

private:
    Ui::BandScope *ui;

    QwtPlotCurve bandScopeCurve;
    BScopeGrid *grid;
    QwtPlotMarker freqMarker;
    QwtPlotZoneItem IFZone;

    bhfilter *bhf;
    QColor m_traceColor;
    QColor m_gridColor;
    bool m_bandScopeEnabled;
    QTimer sampTimer;
    double yMin, yMax;
    bool avgInit;
    QwtLinearScaleEngine m_scale;
    double m_activeDragXPos;
    double m_smooth;
};

#endif // BANDSCOPE_H
