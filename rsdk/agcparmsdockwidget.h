#ifndef AGCPARMSDOCKWIDGET_H
#define AGCPARMSDOCKWIDGET_H

#include <QDockWidget>
#include <QGridLayout>

#include "rangecontrol.h"
#include "rsdkdockwidget.h"

namespace Ui {
class AgcParmsDockWidget;
}

class AgcParmsDockWidget : public rsdkDockWidget
{
    Q_OBJECT

public:
    explicit AgcParmsDockWidget(QWidget *parent = 0);
    ~AgcParmsDockWidget();
    void start();

public slots:
    void updateValues();

public:
    RangeControl *attackRC;
    RangeControl *decayRC;
    RangeControl *hangRC;
    //RangeControl *hangThresholdRC;
    RangeControl *slopeRC;

private:
    Ui::AgcParmsDockWidget *ui;
};

#endif // AGCPARMSDOCKWIDGET_H
