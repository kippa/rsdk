#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include </usr/include/libusb-1.0/libusb.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sstream>
#include <string.h>

#include "list.h"
#include "exception.h"
#include "common.h"
#include "radiofile.h"
#include "radiousb.h"
#include "radioozy.h"
#include "hpsdr.h"
#include "flxr.h"
#include "stringConvert.h"

// DttSP exports
#include "dttsp.h"

// local c-struct of the ozyfw-sdr1k.hex file
// so we don't have to send along a file
// but can be overwritten with command line file name
#include "ozyfw-sdr1k.h"
// and another for the fpga code base, for the same reason
#include "Ozy_Janus_rbf.h"

// testing something in DttSP
#define TXTHREAD 1

RadioOzy *rozy;

// The DSP filter bandwidths laid out in SDRMODE_DTTSP index array
// ulitmately these may get modified by the user, TODO
// then keep the changed values, and defaults, in a db?
// not sure yet where else to place this, so here it is for now
const char *filtersByMode[][10] = {
    { "5.0K" , "4.4K", "3.8K", "3.3K", "2.9K", "2.7K", "2.4K", "2.1K", "1.8K", "1.0K" }, // LSB
    { "5.0K" , "4.4K", "3.8K", "3.3K", "2.9K", "2.7K", "2.4K", "2.1K", "1.8K", "1.0K" }, // USB
    { "10.0K", "7.5K", "5.0K", "4.4K", "4.0K", "3.3K", "2.4K", "2.1K", "1.8K", "1.0K" }, // DSB
    { "2.4K" , "1.8K", "1.6K", "1.4K", "1.2K", "1.0K", "800",  "600",  "400",  "200"  }, // CWL
    { "2.4K" , "1.8K", "1.6K", "1.4K", "1.2K", "1.0K", "800",  "600",  "400",  "200"  }, // CWU
    { "10.0K", "7.5K", "5.0K", "4.4K", "4.0K", "3.3K", "2.4K", "2.1K", "1.8K", "1.0K" }, // FMN
    { "10.0K", "7.5K", "5.0K", "4.4K", "4.0K", "3.3K", "2.4K", "2.1K", "1.8K", "1.0K" }, // AM
    { "5.0K" , "4.4K", "3.8K", "3.3K", "2.9K", "2.7K", "2.4K", "2.1K", "1.8K", "1.0K" }, // DIGU
    { "5.0K" , "4.4K", "3.8K", "3.3K", "2.9K", "2.7K", "2.4K", "2.1K", "1.8K", "1.0K" }, // SPEC
    { "5.0K" , "4.4K", "3.8K", "3.3K", "2.9K", "2.7K", "2.4K", "2.1K", "1.8K", "1.0K" }, // DIGL
    { "10.0K" , "9.0K", "8.0K", "7.0K", "6.0K", "5.0K", "4.4K", "4.0K", "3.3K", "2.4K" }, // SAM
    { "5.0K" , "4.4K", "3.8K", "3.3K", "2.9K", "2.7K", "2.4K", "2.1K", "1.8K", "1.0K" }, // DRM
};


/*************************************************************************/
/*************************************************************************/
/*************************************************************************/
// hex conversion stuff is improperly placed here
static int hex2int(string &value)
{
    int total, pos;
    typedef map<char,int> HexMapType;
    // map hex digits to decimal values
    static HexMapType HexMap;
    HexMap['0'] = 0; HexMap['1'] = 1; HexMap['2'] = 2; HexMap['3'] = 3;
    HexMap['4'] = 4; HexMap['5'] = 5; HexMap['6'] = 6; HexMap['7'] = 7;
    HexMap['8'] = 8; HexMap['9'] = 9; HexMap['A'] = 10; HexMap['a'] = 10;
    HexMap['B'] = 11; HexMap['b'] = 11; HexMap['C'] = 12; HexMap['c'] = 12;
    HexMap['D'] = 13; HexMap['d'] = 13; HexMap['E'] = 14; HexMap['e'] = 14;
    HexMap['F'] = 15; HexMap['f'] = 15;

    total=pos=0;

    // work backward to convert hex digits to base10
    string::reverse_iterator iter = value.rbegin();
    for ( ; iter != value.rend(); ++iter)
    {
        HexMapType::iterator HexMapIter = HexMap.begin();
        for ( ; HexMapIter != HexMap.end(); HexMapIter++)
            if (HexMapIter->first == *iter)
                total += (HexMapIter->second << (pos++ * 4));
    }
    return total;
}

// test and convert a string like 0xNN or 0XNN,
// if no 0x or 0X return -1 error
static long parseHexToInt(string &value)
{
    string parsed_str;
    const char * HexDigits = "0";
    int hex = value.find_first_not_of (HexDigits, 0); // look for an 'x' or 'X' to start the hex value
    if (hex > 0)
    {
        // first character past any zeroes must be x or X
        if (value[hex] == 'x' || value[hex] == 'X')
            ++hex; // skip past the 'x' or 'X'
        // parse out the desired hex digits from the "0x" or "0X"
        parsed_str = value.substr(hex, value.size());
    }
    else
    {
        parsed_str = value;
    }

    return hex2int(parsed_str);
}

static int chex2int (char *value, int count)
{
    string strval;
    for (int i=0; i<count; i++)
        strval += value[i];
    return parseHexToInt(strval);
}

/*************************************************************************/
/*************************************************************************/
/*************************************************************************/

// the xmit and receive endpoints
void *ozy_ep6_ep2_io_thread(void* arg)
{
    RadioOzy *r = static_cast<RadioOzy *>(arg);
    int kill = 0;
    int suspend = 0;
    while(!kill)
    {
        if (!suspend)
        {
            usbBuf_t *ob = usb_get_buffer(r->rxBufs, r/* user data */);
            if(ob)
            {
                if((r->Read(ob, true))<0)
                {
                    //fprintf(stderr, "%s: Read failed\n", __func__);
                    break;
                }
                // save to file the raw USB packets arriving from HPSDR
                // this to allow playback of raw HPSDR packets which include all
                // operating data, that's the goal
                else if(r->getRecordHPSDRRaw())
                {
                    if((r->Write(ob, 512, false)))
                    {
                        fprintf(stderr, "%s: Write failed\n", __func__);
                    }
                }
            }
            else
            {
                // no buffers available, wait a bit to let things settle
                my_usleep(1000);
            }
        }

        pthread_mutex_lock(&r->ep6_ep2_thread_lock);
        kill = r->ep6_ep2_thread_request_kill;
        suspend = r->ep6_ep2_thread_request_suspend;
        pthread_mutex_unlock(&r->ep6_ep2_thread_lock);
    }
    // effectively pthread_exit(0);
    // see man page pthread_create()
    return nullptr;
}

// the bandscope endpoint
void *ozy_ep4_io_thread(void* arg)
{
    RadioOzy *r = static_cast<RadioOzy *>(arg);
    int kill = 0;
    int suspend = 0;
    while(!kill)
    {
        if (!suspend)
        {
            usbBuf_t *ob = usb_get_buffer(r->bsBufs, r/* user data */);
            if(ob)
            {
                if((r->Read(ob, true))<0)
                {
                    //fprintf(stderr, "%s: Read failed\n", __func__);
                    break;
                }
            }
            else
            {
                my_usleep(1000);
            }
        }

        pthread_mutex_lock(&r->ep4_thread_lock);
        kill = r->ep4_thread_request_kill;
        suspend = r->ep4_thread_request_suspend;
        pthread_mutex_unlock(&r->ep4_thread_lock);
    }
    // effectively pthread_exit(0);
    // see man page pthread_create()
    return nullptr;
}

#ifdef ICOM_SERIAL_REMOTE
void *ozy_icom_serial_remote_thread(void* arg)
{
    RadioOzy *r = static_cast<RadioOzy *>(arg);
    int kill = 0;
    int suspend = 0;
    while(!kill)
    {
        if (!suspend)
        {
            // dummy for now
            // read serial port and collect commands here
            ssize_t nb = r->icsr->read_cmd();
            if (nb <= 0)
                my_usleep(100000);
        }

        pthread_mutex_lock(&r->icom_serial_remote_thread_lock);
        kill = r->icom_serial_remote_thread_request_kill;
        suspend = r->icom_serial_remote_thread_request_suspend;
        pthread_mutex_unlock(&r->icom_serial_remote_thread_lock);
    }
    // effectively pthread_exit(0);
    // see man page pthread_create()
    return nullptr;
}
#endif // ICOM_SERIAL_REMOTE

// scan the devices on the I2C bus in various hpsdr cards
// this reads serial numbers, overloads from the receivers
// and serial number, fwd and reverse powers from pennylane and alex
// TODO FIXME this "hangs" for long periods when in 48K sample rate
// I think this is a starvation issue and trying to figure it out
// fixed, switch ctl transfers to sync IO. Much better behavior now.
void *ozy_i2c_scan_thread(void* arg)
{
    RadioOzy *r = static_cast<RadioOzy *>(arg);
    int kill = 0;
    int suspend = 0;
    int len=2;
    unsigned char dummy[8];

    while(!kill)
    {
        if (!suspend)
        {
            r->readOzyI2C(0x10, dummy, len); // merc #0 fw version
            my_usleep(10000);
            r->readOzyI2C(0x15, dummy, len); // pennylane fw version
            my_usleep(10000);
            r->readOzyI2C(0x16, dummy, len); // pennylane ALC (FWD power)
            my_usleep(10000);
            r->readOzyI2C(0x17, dummy, len); // alex FWD power
            my_usleep(10000);
            r->readOzyI2C(0x18, dummy, len); // alex REV power
            my_usleep(10000);
        }

        pthread_mutex_lock(&r->i2c_scan_thread_lock);
        kill = r->i2c_scan_thread_request_kill;
        suspend = r->i2c_scan_thread_request_suspend;
        pthread_mutex_unlock(&r->i2c_scan_thread_lock);

    }
    // effectively pthread_exit(0);
    // see man page pthread_create()
    return nullptr;
}

int XrunCb(void *arg)
{
    fprintf(stderr, "%s: arg %p \n", __func__, arg);
    return 0;
}

// initialize the ozy firmware and fpga code
int RadioOzy::initOzyUSBDevice()
{
    int retval = 0;

    // devHandle may not have connected in RadioUsb, if so then bypass the usb setup
    if (devHandle) {

        // check if firmware load needed?

        resetCpu(1);
        loadFW(nullptr);
        resetCpu(0);
        sleep(1);

        // reopen the device, cause it closed when we resetCpu(0)
        if ((retval = libusbOpenDev(OZY_VID, OZY_PID))<0) {
            if (retval == -2)
            {
                fprintf(stderr, "Open USB device VID=0x%x PID=0x%x failed, Device not found!\n", OZY_VID, OZY_PID);
                fprintf(stderr, "Try to initialize Ozy at VID 0x%x PID 0x%x\n", OZY_VID, OZY_PID);

            }
            else
            {
                THROW_EXCEPTION (ERROR_LEVEL, retval, "libusbOpenDev failed retval %d\n", retval);
            }
        }

        setLED(1, 1);
        loadFPGA(nullptr);
        setLED(1, 0);

        // do audio setup during fpga init
        setup_audio();

        // must wait for fpga to awaken
        //sleep(1);
        //my_usleep(25000);

    }
    return retval;
}

void RadioOzy::setup_audio(void)
{
    // write_i2c routines to set janus and pennylane audio chip

    // Analog Audio Path Control (Address: 0000100)
    // Bit 0 Mic Boost +20dB 0 = off 1 = on
    // Bit 1 Mic Mute 0 = off 1 = on
    // Bit 2 In Select 0 = line 1 = mic

    //        1E 00 - Reset chip
    //        12 01 - set digital interface active
    //        08 15 - D/A on, mic input, mic 20dB boost
    //        08 14 - ditto but no mic boost
    //        0C 00 - All chip power on
    //        0E 02 - Slave, 16 bit, I2S
    //        10 00 - 48k, Normal mode
    //        0A 00 - turn D/A mute off

    // reset chip
    unsigned char buf[] = {0x1e, 0x00};
    writeOzyI2C(0x1b, buf, 2);

    // set  digital interface active
    buf[0] = 0x12; buf[1] = 0x01;
    writeOzyI2C(0x1b, buf, 2);

    // turn D/A mute off and make noise
    buf[0] = 0x0A; buf[1] = 0x00;
    writeOzyI2C(0x1b, buf, 2);

    // D/A on, mic input, mic 20dB boost
    buf[0] = 0x08; buf[1] = 0x15;
    writeOzyI2C(0x1b, buf, 2);

    // all chip power up
    buf[0] = 0x0C; buf[1] = 0x00;
    writeOzyI2C(0x1b, buf, 2);

    // slave, 16 bits, i2c
    buf[0] = 0x0E; buf[1] = 0x02;
    writeOzyI2C(0x1b, buf, 2);

    // 48k, normal mode
    buf[0] = 0x10; buf[1] = 0x00;
    writeOzyI2C(0x1b, buf, 2);

}


RadioOzy::RadioOzy(const char * const name_prm, ASYNCCB cb_prm, void *cbdata_prm)
    : RadioUsb(name_prm, cb_prm, cbdata_prm, OZY_VID, OZY_PID),
      curMercury(0),
      curMercuryMode(0), // from mercuryselectdockwidget enum MERMODE
      numSamples(1024), // DEFSIZE from DttSP/defs.h in project DttSP, important to link these together here
      sampleCount(0), sampleCountTotal(0),
      retSampleCount(0), retSamplePosition(0), sampleRate(48000),
      ncReal(0), ncImag(0), ncGain(1.0),
      ref10M(hpsdr::Ref10M_Atlas), ref122M(hpsdr::Ref122M_Mercury),
      config(hpsdr::Config_Mercury),
      micSrc(hpsdr::Mic_Janus),
      modeClassE(hpsdr::Class_Regular),
      alexAtt(hpsdr::Att_0dB),
      txAnt(hpsdr::txAnt_1),
      rxAnt(hpsdr::rxAnt_RX1),
      preampOn(hpsdr::Preamp_On),
      micGain(hpsdr::MicGain_0dB),
      micLine(hpsdr::MicIn),
      tuneMode(false),
      guiXmit(false), packetXmit(false),
      vswr(1.0),
      mode(USB),
      xmit20dBAtt(true),
      keyerEnabled(false),
      running(false),
      recordHPSDRRaw(false),
      txC0ndx(0),
      numCmdListEntries(sizeof(tx_C0_cmd_list)),
      amCarrierLevel(100.0)
{

    // check return someday
    (void)initOzyUSBDevice();

    setAmCarrierLevel(amCarrierLevel);

    // initialize the state for the pthreads data collectors
    ep4_thread_request_kill=0;
    ep4_thread_request_suspend=0;
    ep6_ep2_thread_request_kill=0;
    ep6_ep2_thread_request_suspend=0;
    i2c_scan_thread_request_kill=0;
    i2c_scan_thread_request_suspend=0;
#ifdef ICOM_SERIAL_REMOTE
    icom_serial_remote_thread_request_kill=0;
    icom_serial_remote_thread_request_suspend=0;
#endif // ICOM_SERIAL_REMOTE

    pthread_mutex_init(&ep6_ep2_thread_lock, nullptr);
    pthread_mutex_init(&ep4_thread_lock, nullptr);
    pthread_mutex_init(&i2c_scan_thread_lock, nullptr);
#ifdef ICOM_SERIAL_REMOTE
    pthread_mutex_init(&icom_serial_remote_thread_lock, nullptr);
#endif // ICOM_SERIAL_REMOTE

    // lock during sample rate changes so critical data io will briefly stop
    pthread_mutex_init(&sampRateLock, nullptr);

    // TODO FIXME
    // testing adding flxmlrpc server to accept client requests
    xmlsrv = new flxr(); // parm is this radioozy

#ifdef ICOM_SERIAL_REMOTE
    icsr = new icom_serial(this);
#endif // ICOM_SERIAL_REMOTE

    // TODO FIXME
    // testing icom serial remote via pty so wsjtx wsprx can control radioozy


    // for RX
    curMercury=0;
    curMercuryMode=0;
    for (int merc=0; merc<OZY_MAX_MERCURY; merc++)
    {
        Aud_L_out[merc] =  new float[numSamples];
        Aud_R_out[merc] =  new float[numSamples];

        for (int subrx=0; subrx<OZY_MAX_SUBRX; subrx++)
        {
            I_in[subrx] =  new float[numSamples];
            Q_in[subrx] =  new float[numSamples];
            rxFreq[merc][subrx] = 10125000LL;
        }
    }

    // for TX
    Mic_R_in =  new float[numSamples];
    Mic_L_in =  new float[numSamples];
    I_out =  new float[numSamples];
    Q_out =  new float[numSamples];

    txBuf = nullptr;

    // prime ozy by sending several packets to start
    primeOzy();

    //jclient->start();

}

RadioOzy::~RadioOzy()
{
    delete xmlsrv;

    killThreads();

    pthread_mutex_destroy(&sampRateLock);

    for (int i=0; i<OZY_MAX_MERCURY; i++)
    {
        delete[] Aud_L_out[i];
        delete[] Aud_R_out[i];
    }

    for (int i=0; i<OZY_MAX_SUBRX; i++)
    {
        delete[] I_in[i];
        delete[] Q_in[i];
    }
    delete[] Mic_R_in;
    delete[] Mic_L_in;
    delete[] I_out;
    delete[] Q_out;
}

int RadioOzy::Read(usbBuf_t *buf_prm, bool wait_prm)
{
    if (!devHandle)
        return 0;

    if(buf_prm)
    {
        memset(buf_prm->buf, 0x0, buf_prm->size);
        if((libusbOzyXferDev(buf_prm->ep, buf_prm->buf, buf_prm->size, buf_prm))<0)
        {
            //fprintf(stderr, "%s: libusb_xfer_ozy failed\n", __func__);
            return -1;
        }
        if (wait_prm==false)
        {
            // callback right now if !wait_prm, otherwise async callback will call cb
            cb(this, buf_prm->buf, wait_prm);
        }
        return 0;
    }
    else
    {
        THROW_EXCEPTION(ERROR_LEVEL, 0, "No OB!");
        return -1;
    }
}

int RadioOzy::Write(usbBuf_t *buf_prm, int64 len_prm, bool wait_prm)
{
    (void)buf_prm;
    (void)len_prm;
    (void)wait_prm;
    //fprintf(stderr, "%s: buf_prm %p len_prm %lld wait_prm %d\n", __func__, buf_prm, len_prm, wait_prm);
    //RadioUsb::Write(buf_prm->buf, len_prm, wait_prm);
    return 0;
}

int
RadioOzy::usbDoCtlPkt(int req_type, int cmd, int addr, unsigned char *buf, int len)
{
    int rc;
    //usbBuf_t *pkt;

    if (!devHandle)
        return -1;
    if (!buf)
        return -1;
    if ((req_type != VENDOR_REQ_TYPE_IN) && (req_type != VENDOR_REQ_TYPE_OUT))
        return -1;

    // use sync IO
    rc = libusb_control_transfer(
                devHandle, /* request handle */
                req_type, /* bmRequestType */
                cmd, /* bRequest */
                libusb_cpu_to_le16(addr),/* wValue */
                libusb_cpu_to_le16(0), /* wIndex */
                buf, /* data */
                libusb_cpu_to_le16(len), /*wLength */
                OZY_IO_TIMEOUT);

    if (rc < 0)
    {
        fprintf(stderr, "%s: libusb_control_transfer failed (%s)\n", __func__, libusb_error_name(rc));
    }

    // should be xfr->xfer_len? hmmm
    return rc;
}

usbBuf_t *
RadioOzy::usbCmd(int cmd)
{
    int rc;
    usbBuf_t *pkt;

    if (!devHandle)
    {
        fprintf(stderr, "%s: devHandle NULL\n", __func__);
        return nullptr;
    }

    pkt = usb_get_buffer(rxBufs, this);
    if (!pkt)
    {
        fprintf(stderr, "%s: usb_get_buffer failed\n", __func__);
        return nullptr;
    }

tryagain:

    libusb_fill_control_setup(pkt->buf, VRT_VENDOR_IN, VRQ_SDR1K_CTL, cmd, 0, pkt->size);
    libusb_fill_control_transfer(usbCtl, devHandle, pkt->buf, libusbOzyHandleCb, pkt, OZY_IO_TIMEOUT);
    rc=libusb_submit_transfer(usbCtl);
    if (rc)
    {
        if (rc==LIBUSB_ERROR_BUSY)
        {
            goto tryagain;
        }
        else if (rc)
        {
            usb_put_buffer(rxBufs, pkt);
            return nullptr;
        }
    }

    return pkt;
}

int RadioOzy::getOzyFWString(void)
{
    usbBuf_t *pkt;

    if (!devHandle)
    {
        fprintf(stderr, "%s: devHandle NULL\n", __func__);
        return -1;
    }

    pkt = usbCmd(SDR1KCTRL_READ_VERSION);
    if (!pkt)
    {
        fprintf(stderr, "%s: usb_get_buffer failed\n", __func__);
        return -1;
    }

    memset(ozyFWStr, 0, 16);
    memcpy(ozyFWStr, pkt->buf, 16);

    return 0;
}

int RadioOzy::readOzyI2C(unsigned int i2c_addr, unsigned char *buf, unsigned int len)
{
    if (!devHandle)
        return -1;

    return usbDoCtlPkt(VENDOR_REQ_TYPE_IN, VENDOR_REQ_I2C_READ, i2c_addr, buf, len);
}

int RadioOzy::writeOzyI2C(unsigned int i2c_addr, unsigned char *buf, unsigned int len)
{
    if (!devHandle)
        return -1;

    return usbDoCtlPkt(VENDOR_REQ_TYPE_OUT, VENDOR_REQ_I2C_WRITE, i2c_addr, buf, len);
}

int RadioOzy::loadFW(char *fileName)
{
    (void)fileName;

    int cnt = 0;
    unsigned char wbuf[256];

    // the default ozy firmware load is in c struct ozyfw_sdr1k_hex {...}
    // this is quite large, allocate it and throw it away when done here
    char *buf = (char *) malloc (ozyfw_sdr1k_hex_len);
    if (!buf)
    {
        fprintf(stderr, "%s: malloc failed (%s)", __func__, strerror(errno));
        return -1;
    }

    // make a copy for strtok_r to mess up
    memmove(buf, ozyfw_sdr1k_hex, ozyfw_sdr1k_hex_len);

    // get contents line by line
    char *p = nullptr;
    char *line = strtok_r(buf, "\n", &p);
    do {

        if (line[0] != ':')
        {
            fprintf(stderr, "%s: inconsistent firmware load", __func__);
            free(buf);
            return -1;
        }

        // process line of input here
        int val, len, type, addr;
        unsigned char chksum, my_chksum;

        len = chex2int(&line[1], 2);
        addr = chex2int(&line[3], 4);
        type = chex2int(&line[7], 2);

        switch(type)
        {
            case 0: /* record */
                my_chksum = (unsigned char)(len +(addr & 0xff) +((addr>>8) + type));
                for (int i=0; i<len; i++)
                {
                    val = chex2int(&line[9]+(i*2), 2);
                    wbuf[i] = (unsigned char)val;
                    my_chksum += wbuf[i];
                }
                // get the file's idea of the chksum
                val = chex2int(&line[9]+(len*2), 2);
                chksum = (unsigned char) val;
                if (((chksum + my_chksum) & 0xff) != 0)
                {
                    fprintf(stderr, "%s: bad checksum\n", __func__);
                    free(buf);
                    return -1;
                }
                // write the collected buffer out to ozy
                // ozywriteram
                val = writeRAM(addr, wbuf, len);
                if (val < 1)
                {
                    fprintf(stderr, "%s: bad write\n", __func__);
                    free(buf);
                    return -1;
                }
                break;

            case 1: /* EOF */
                break;

            default: /* invalid, should not happen unless data is corrupt */
                fprintf(stderr, "%s: unknown ozy firmware type\n", __func__);
                free(buf);
                return -1;
        }

    } while ((line = strtok_r(nullptr, "\n", &p)));

    free(buf);
    return cnt;
}

int RadioOzy::loadFPGA(char *fileName)
{
    (void)fileName;

    int rc;
    int bytesXfrd = 0;

    // setup the libusb to send a request to fpga load
    rc = libusb_control_transfer(
                devHandle, /* request handle */
                VENDOR_REQ_TYPE_OUT, /* bmRequestType */
                VENDOR_REQ_FPGA_LOAD, /* bRequest */
                0, /* wValue */
                FL_BEGIN, /* wIndex */
                nullptr, /* data */
                0, /*wLength */
                OZY_IO_TIMEOUT);

    if (rc < 0)
    {
        fprintf(stderr, "%s: FPGA write intialization failed (%s)\n", __func__, libusb_error_name(rc));
        return -1;
    }
    //point at the ozy fpga code in
    // TODO FIXME for FILE *
    unsigned char *buf = Ozy_Janus_rbf;
    unsigned char *addr, *start = buf;

    for (addr = start; addr < start + Ozy_Janus_rbf_len; addr += MAX_EPO_PACKET_SIZE, buf += MAX_EPO_PACKET_SIZE)
    {
        // load one buffer at a time
        rc = libusb_control_transfer(
                    devHandle, /* request handle */
                    VENDOR_REQ_TYPE_OUT, /* bmRequestType */
                    VENDOR_REQ_FPGA_LOAD, /* bRequest */
                    0, /* wValue */
                    FL_XFER, /* wIndex */
                    buf, /* data */
                    MAX_EPO_PACKET_SIZE, /*wLength */
                    OZY_IO_TIMEOUT);
        if (rc < 0)
        {
            fprintf(stderr, "%s: fpga FL_XFER failed at byte 0x%x (%s)\n", __func__, bytesXfrd, libusb_error_name(rc));
            return -1;
        }
        bytesXfrd += MAX_EPO_PACKET_SIZE;
    }

    // shutdown this fpga code load
    rc = libusb_control_transfer(
                devHandle, /* request handle */
                VENDOR_REQ_TYPE_OUT, /* bmRequestType */
                VENDOR_REQ_FPGA_LOAD, /* bRequest */
                0, /* wValue */
                FL_END, /* wIndex */
                nullptr, /* data */
                0, /*wLength */
                OZY_IO_TIMEOUT);

    if (rc < 0)
    {
        fprintf(stderr, "%s: fpga FL_END failed (%s)\n", __func__, libusb_error_name(rc));
        return -1;
    }

    return bytesXfrd;
}

int RadioOzy::setLED(int led, int on)
{
    int rc, val;

    if (on)
    {
        val = 1;
    }
    else
    {
        val = 0;
    }

    rc = libusb_control_transfer(
                devHandle, /* request handle */
                VENDOR_REQ_TYPE_OUT, /* bmRequestType */
                VENDOR_REQ_SET_LED, /* bRequest */
                val, /* wValue */
                led, /* wIndex */
                nullptr, /* data */
                0, /*wLength */
                OZY_IO_TIMEOUT);

    if (rc < 0)
    {
        fprintf(stderr, "%s: failed (%s)\n", __func__, libusb_error_name(rc));
        return -1;
    }

    return 0;
}


int RadioOzy::writeRAM(int start, unsigned char *buf, int count)
{
    int pktsize = MAX_EPO_PACKET_SIZE;
    int len = count;
    int bytes_written = 0;
    int addr, bytes_this_write, nsize;

    for (addr = start; addr < start + len; addr += pktsize, buf += pktsize)
    {
        nsize = len + start - addr;
        if (nsize > pktsize)
            nsize = pktsize;

        bytes_this_write = libusb_control_transfer(
                    devHandle, /* request handle */
                    0x40, /* bmRequestType */
                    0xa0, /* bRequest */
                    addr, /* wValue */
                    0, /* wIndex */
                    buf, /* data */
                    nsize, /*wLength */
                    OZY_IO_TIMEOUT);

        if (bytes_this_write >= 0)
        {
            bytes_written += bytes_this_write;
        }
        else
        {
            return bytes_this_write;
        }
    }

    return bytes_written;
}

int RadioOzy::resetCpu(int reset)
{
    int cnt;
    unsigned char write_buf = 0;

    if (reset)
        write_buf = 1;

     cnt = writeRAM(0xe600, &write_buf, 1);

     if (cnt < 0)
     {
         fprintf(stderr, "%s: writeRAM failed", __func__);
     }

     return cnt;
}

int RadioOzy::primeOzy()
{
    for (int i=0; i<12; i++)
    {
        usbBuf_t *pkt = usb_get_buffer(txBufs, this);
        if (!pkt)
        {
            fprintf(stderr, "%s: usb_get_buffer failed\n", __func__);
            THROW_EXCEPTION(ERROR_LEVEL, 0, "usb_get_buffer failed!");
        }
        OzyRx1Packet_t *ob=(OzyRx1Packet_t*)pkt->buf;
        ob->sync[0] = OZY_SYNC;
        ob->sync[1] = OZY_SYNC;
        ob->sync[2] = OZY_SYNC;
        ob->cmd[0] = 0;
        ob->cmd[1] = HPSDR_SAMPLE_RATE_384000 | HPSDR_10MHZ_REF_MERCURY | HPSDR_122_88_SRC_MERCURY | HPSDR_CONFIG_BOTH | HPSDR_MIC_SRC_PENELOPE;
        //ob->cmd[1] = HPSDR_SAMPLE_RATE_48000 | HPSDR_10MHZ_REF_ATLAS | HPSDR_122_88_SRC_MERCURY | HPSDR_CONFIG_MERCURY | HPSDR_MIC_SRC_PENELOPE;
        ob->cmd[2] = HPSDR_MODE_REGULAR;
        ob->cmd[3] = HPSDR_ALEX_ATT_0DB | HPSDR_PREAMP_OFF | HPSDR_LT2208_DITHER_ON | HPSDR_LT2208_RANDOM_ON | HPSDR_ALEX_RX_ANT_RX1 | HPSDR_ALEX_RX_OUT_OFF ;
        ob->cmd[4] = HPSDR_DUPLEX_OFF | HPSDR_TOTAL_RX_1RX | HPSDR_MERCURY_COMMON_OFF;
        libusbOzyXferDev(OZY_EP_TX_DATA, pkt->buf, OZY_BUFFER_SIZE, pkt);
    }

    int rc = getOzyFWString();
    if (rc < 0)
        return -1;

    fprintf(stderr, "Ozy firmware revision: 0x");
    for (int i=0; i<3; i++)
        fprintf(stderr, "%2.2X", ozyFWStr[i]);
    fprintf(stderr, "\n");

    return 0;
}

int RadioOzy::startThreads(void)
{
    int rc;
    // attributes?? possibly

    // create a thread to read/write to EP6/EP2
    if ((rc=pthread_create(&ep6_ep2_io_thread_id,nullptr,&ozy_ep6_ep2_io_thread,this))!=0)
    {
        // TODO FIXME
        //fprintf(stderr,"pthread_create failed on ozy_ep6_io_thread: rc=%d\n", rc);
    }
    pthread_setname_np(ep6_ep2_io_thread_id, "rsdk ep6/2 io");

    // create a thread to read bandscope data EP4
    if ((rc=pthread_create(&ep4_io_thread_id,nullptr,&ozy_ep4_io_thread,this))!=0)
    {
        // TODO FIXME
        //fprintf(stderr,"pthread_create failed on ozy_ep4_io_thread: rc=%d\n", rc);
    }
    pthread_setname_np(ep4_io_thread_id, "rsdk ep4 io");

    // create a thread to read various i2c devices:
    // - mercury RX (0x10, 0x11, 0x12, 0x13) {ADC_OVERLAD, SERIAL_#}
    // - pennylane TX (0x15) {FW version #}
    // - pennylane ALC (0x16) {pennylane FWD power)
    // - alex FWD (0x17) {FWD power} // 12 bits
    // - alex REV (0x18) {REV power}

    if ((rc=pthread_create(&i2c_scan_thread_id,nullptr,&ozy_i2c_scan_thread,this))!=0)
    {
        // TODO FIXME
        fprintf(stderr,"pthread_create failed on ozy_i2c_scan_thread: rc=%d\n", rc);
    }
    pthread_setname_np(i2c_scan_thread_id, "rsdk i2c scan");

#ifdef ICOM_SERIAL_REMOTE
    if ((rc=pthread_create(&icom_serial_remote_thread_id,nullptr,&ozy_icom_serial_remote_thread,this))!=0)
    {
        // TODO FIXME
        //fprintf(stderr,"pthread_create failed on ozy_icom_serial_remote_thread: rc=%d\n", rc);
    }
    pthread_setname_np(icom_serial_remote_thread_id, "rsdk icom");
#endif // ICOM_SERIAL_REMOTE

    return rc;
}

int RadioOzy::killThreads()
{
    // pthread_cancel(ep6_ep2_io_thread_id);

    int ret = suspendThreads();

    pthread_mutex_lock(&ep6_ep2_thread_lock);
    ep6_ep2_thread_request_kill = 1;
    pthread_mutex_unlock(&ep6_ep2_thread_lock);

    pthread_mutex_lock(&ep4_thread_lock);
    ep4_thread_request_kill = 1;
    pthread_mutex_unlock(&ep4_thread_lock);

    pthread_mutex_lock(&i2c_scan_thread_lock);
    i2c_scan_thread_request_kill = 1;
    pthread_mutex_unlock(&i2c_scan_thread_lock);

#ifdef ICOM_SERIAL_REMOTE
    pthread_mutex_lock(&icom_serial_remote_thread_lock);
    icom_serial_remote_thread_request_kill = 1;
    pthread_mutex_unlock(&icom_serial_remote_thread_lock);
#endif // ICOM_SERIAL_REMOTE

    pthread_join(ep6_ep2_io_thread_id, nullptr);
    pthread_join(ep4_io_thread_id, nullptr);
    pthread_join(i2c_scan_thread_id, nullptr);

#ifdef ICOM_SERIAL_REMOTE
    pthread_join(icom_serial_remote_thread_id, nullptr);
#endif // ICOM_SERIAL_REMOTE

    return ret;
}

/** /ingroup RadioOzy
 * Unsuspend the reads and writes on the end point threads
 * this restarts the the USB buffer end point reads and writes
 * \returns 0
 */
int RadioOzy::goThreads(void)
{
    pthread_mutex_lock(&ep6_ep2_thread_lock);
    ep6_ep2_thread_request_suspend = 0;
    pthread_mutex_unlock(&ep6_ep2_thread_lock);

    pthread_mutex_lock(&ep4_thread_lock);
    ep4_thread_request_suspend = 0;
    pthread_mutex_unlock(&ep4_thread_lock);

    pthread_mutex_lock(&i2c_scan_thread_lock);
    i2c_scan_thread_request_suspend = 0;
    pthread_mutex_unlock(&i2c_scan_thread_lock);

#ifdef ICOM_SERIAL_REMOTE
    pthread_mutex_lock(&icom_serial_remote_thread_lock);
    icom_serial_remote_thread_request_suspend = 0;
    pthread_mutex_unlock(&icom_serial_remote_thread_lock);
#endif // ICOM_SERIAL_REMOTE

    return 0;
}

/** /ingroup RadioOzy
 * Suspend the reads and writes on the end point threads
 * this will allow buffer maintenance and management to take place
 * \returns 0
 */
int RadioOzy::suspendThreads(void)
{
    pthread_mutex_lock(&ep6_ep2_thread_lock);
    ep6_ep2_thread_request_suspend = 1;
    pthread_mutex_unlock(&ep6_ep2_thread_lock);

    pthread_mutex_lock(&ep4_thread_lock);
    ep4_thread_request_suspend = 1;
    pthread_mutex_unlock(&ep4_thread_lock);

    pthread_mutex_lock(&i2c_scan_thread_lock);
    ep4_thread_request_suspend = 1;
    pthread_mutex_unlock(&i2c_scan_thread_lock);

    return 0;
}

int RadioOzy::getCurMercury() const
{
    return curMercury;
}

void RadioOzy::setCurMercury(int value)
{
    curMercuryMode = value;
    curMercury = value % NUM_MERCURY_CARDS;
}

double RadioOzy::getNcReal() const
{
    return ncReal;
}

void RadioOzy::setNcReal(double value)
{
    // sanitize for normal IQ range
   if (value < -1.0)
       value = -1.0;
   if (value > 1.0)
       value = 1.0;
   ncReal = value;
}

double RadioOzy::getNcImag() const
{
    return ncImag;
}

void RadioOzy::setNcImag(double value)
{
   // sanitize for normal IQ range
   if (value < -1.0)
       value = -1.0;
   if (value > 1.0)
       value = 1.0;
   ncImag = value;
}

// frequency
int RadioOzy::setFreq(const unsigned long long f)
{
    // validate
    if (f>=MAX_HF_FREQ)
    {
        //fprintf(stderr,"%s: Requested frequency %lld > maximum allowable %lld \n", __func__, f, MAX_HF_FREQ);
        return -1;
    }
    curMercury = curMercury % NUM_MERCURY_CARDS; // range limit the receiver choice
    return setFreq(f, curMercury);
}

int RadioOzy::setFreq(const unsigned long long f, const unsigned int &r)
{
    // validate
    if (f>=MAX_HF_FREQ)
    {
        //fprintf(stderr,"%s: Requested frequency %lld > maximum allowable %lld \n", __func__, f, MAX_HF_FREQ);
        return -1;
    }

    unsigned int rx = r % OZY_MAX_SUBRX; // range limit the receiver choice

    // if m1, m2 store in curMercury,
    // if m1-m2 set both to same frequency
    rxFreq[curMercury][rx] = f;
    //txFreq[curMercury] = f;
    if (curMercuryMode==2)
    {
        // set both cards to the same frequency
        rxFreq[0][rx] = f;
        rxFreq[1][rx] = f;
    }

    return 0;
}

unsigned long long RadioOzy::getRxFreq(const unsigned int &r)
{
    unsigned int rx = r % OZY_MAX_SUBRX; // range limit the receiver choice
    return rxFreq[curMercury][rx];
}

unsigned long long RadioOzy::getTxFreq(const unsigned int &r)
{
    (void)r;
    //unsigned int tx = r % OZY_MAX_SUBRX; // range limit the receiver choice
    return txFreq[curMercury];
}

int RadioOzy::setTxFreq(const unsigned long long f)
{
    txFreq[curMercury] = f;
    return 0;
}

int RadioOzy::getSampleRate() const
{
    return sampleRate;
}

// sample rate
int RadioOzy::setSampleRate(hpsdr::HpsdrSampleRateEnum sampleRate_prm)
{
    // hpsdr knows 4 sample rates, 48000 96000 192000 384000
    if(sampleRate_prm == hpsdr::SampleRate48000) {
            this->setSampleRate(48000); // using this-> just to be clear that this is not the call to DttSP
    } else if (sampleRate_prm == hpsdr::SampleRate96000) {
            this->setSampleRate(96000);
    }else if(sampleRate_prm == hpsdr::SampleRate192000) {
            this->setSampleRate(192000);
    } else if(sampleRate_prm == hpsdr::SampleRate384000) {
            this->setSampleRate(384000);
    } else {
        return -1;
    }
    return 0;
}

int RadioOzy::setSampleRate(int sampleRate_prm)
{
    int ret = 0;

    /*
    usb_buffer_take_lock(rxBufs);
    usb_buffer_take_lock(txBufs);
    usb_buffer_take_lock(bsBufs);

    fprintf(stderr, "%s: sampleRate_prm %d\n", __func__, sampleRate_prm);
    fprintf(stderr, "%s: usb_count_buffers rxBufs %d\n", __func__, usb_count_buffers(rxBufs));
    fprintf(stderr, "%s: usb_count_buffers txBufs %d\n", __func__, usb_count_buffers(txBufs));
    fprintf(stderr, "%s: usb_count_buffers bsBufs %d\n", __func__, usb_count_buffers(bsBufs));
    fprintf(stderr, "\n");
    */

    pthread_mutex_lock(&sampRateLock);

    // hpsdr knows 4 sample rates, 48000 96000 192000 384000
    if(sampleRate_prm == 48000) {
            sampleRate = 48000;
            SetSampleRate(48000.0); // this is the call to DttSP
    } else if (sampleRate_prm == 96000) {
            sampleRate = 96000;
            SetSampleRate(96000.0);
    } else if(sampleRate_prm == 192000) {
            sampleRate = 192000;
            SetSampleRate(192000.0);
    } else if(sampleRate_prm == 384000) {
            sampleRate = 384000;
            SetSampleRate(384000.0);
    } else {
        ret = -1;
    }

    pthread_mutex_unlock(&sampRateLock);

    /*
    usb_buffer_release_lock(rxBufs);
    usb_buffer_release_lock(txBufs);
    usb_buffer_release_lock(bsBufs);
    */

    return ret;
}

double RadioOzy::getNcGain() const
{
    return ncGain;
}

void RadioOzy::setNcGain(double value)
{
    value = value < -1.0 ? -1.0 : value > 1.0 ? 1.0 : value;
    ncGain = value;
}

// 10 MHz reference
int RadioOzy::setRef10M(hpsdr::HpsdrRef10MEnum ref10M_prm)
{
    if ((ref10M_prm == hpsdr::Ref10M_Atlas) ||
            (ref10M_prm == hpsdr::Ref10M_Penelope) ||
            (ref10M_prm == hpsdr::Ref10M_Mercury))
    {
        ////fprintf(stderr, "%s: ref10M_prm %x\n", __func__, ref10M_prm);
        ref10M = ref10M_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid ref10M_prm %x\n", __func__, ref10M_prm);
        return -1;
    }
    return 0;
}

// 122.88 MHz reference
int RadioOzy::setRef122M(hpsdr::HpsdrRef122MEnum ref122M_prm)
{
    if ((ref122M_prm == hpsdr::Ref122M_Penelope) ||
            (ref122M_prm == hpsdr::Ref122M_Mercury))
    {
        //fprintf(stderr, "%s: ref122M_prm %x\n", __func__, ref122M_prm);
        ref122M = ref122M_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invlaid ref122M_prm %x\n", __func__, ref122M_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setConfig(unsigned char config_prm)
{
    if ((config_prm == hpsdr::Config_Penelope) ||
            (config_prm == hpsdr::Config_Mercury) ||
            (config_prm == hpsdr::Config_Both))
    {
        config = config_prm;
    }
    else
    {
        return -1;
    }
    return 0;
}

int RadioOzy::setClassE(hpsdr::HpsdrClassEEnum classE_prm)
{
    if ((classE_prm == hpsdr::Class_E) ||
            (classE_prm == hpsdr::Class_Regular))
    {
        //fprintf(stderr, "%s: classE_prm %d\n", __func__, classE_prm);
        modeClassE = classE_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid classE_prm %d\n", __func__, classE_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setAtt(hpsdr::HpsdrAttEnum att_prm)
{
    if ((att_prm == hpsdr::Att_30dB) ||
        (att_prm == hpsdr::Att_20dB) ||
        (att_prm == hpsdr::Att_10dB) ||
        (att_prm == hpsdr::Att_0dB))
    {
        //fprintf(stderr, "%s: att_prm %d\n", __func__, att_prm);
        alexAtt = att_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid att_prm %d\n", __func__, att_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setTxAnt(hpsdr::HpsdrTxAntEnum tx_ant_prm)
{

    if ((tx_ant_prm == hpsdr::txAnt_1) ||
        (tx_ant_prm == hpsdr::txAnt_2) ||
        (tx_ant_prm == hpsdr::txAnt_3))
    {
        //fprintf(stderr, "%s: tx_ant_prm %d\n", __func__, tx_ant_prm);
        txAnt = tx_ant_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid tx_ant_prm %d\n", __func__, tx_ant_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setRxAnt(hpsdr::HpsdrRxAntEnum rx_ant_prm)
{
    if ((rx_ant_prm == hpsdr::rxAnt_None) ||
        (rx_ant_prm == hpsdr::rxAnt_RX1) ||
        (rx_ant_prm == hpsdr::rxAnt_RX2) ||
        (rx_ant_prm == hpsdr::rxAnt_XV))

    {
        //fprintf(stderr, "%s: rx_ant_prm %d\n", __func__, rx_ant_prm);
        rxAnt = rx_ant_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid rx_ant_prm %d\n", __func__, rx_ant_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setRxOut(hpsdr::HpsdrRxOutEnum rx_out_prm)
{
    if ((rx_out_prm== hpsdr::rxOut_Off) ||
        (rx_out_prm== hpsdr::rxOut_On))
    {
        //fprintf(stderr, "%s: rx_out_prm %d\n", __func__, rx_out_prm);
        rxOut = rx_out_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid rx_out_prm %d\n", __func__, rx_out_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setDuplex(hpsdr::HpsdrDuplexEnum duplex_prm)
{
    //fprintf(stderr, "%s: duplex_prm %d\n", __func__, duplex_prm);

    if ((duplex_prm== hpsdr::duplex_Full) ||
        (duplex_prm== hpsdr::duplex_Half))
    {
        //fprintf(stderr, "%s: duplex_prm %d\n", __func__, duplex_prm);
        duplex = duplex_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid duplex_prm %d\n", __func__, duplex_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setPreAmp(hpsdr::HpsdrPreAmpEnum PreAmp_prm)
{
    if ((PreAmp_prm== hpsdr::Preamp_Off) ||
        (PreAmp_prm== hpsdr::Preamp_On))
    {
        //fprintf(stderr, "%s: PreAmp_prm %d\n", __func__, PreAmp_prm);
        preampOn = PreAmp_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid PreAmp_prm %d\n", __func__, PreAmp_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setDither(hpsdr::HpsdrDitherEnum Dither_prm)
{
    if ((Dither_prm== hpsdr::dither_Off) ||
        (Dither_prm== hpsdr::dither_On))
    {
        //fprintf(stderr, "%s: Dither_prm %d\n", __func__, Dither_prm);
        lt2208Dither = Dither_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid Dither_prm %d\n", __func__, Dither_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setRandom(hpsdr::HpsdrRandomEnum Random_prm)
{
    if ((Random_prm== hpsdr::random_Off) ||
        (Random_prm== hpsdr::random_On))
    {
        //fprintf(stderr, "%s: Random_prm %d\n", __func__, Random_prm);
        lt2208Random = Random_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invalid Random_prm %d\n", __func__, Random_prm);
        return -1;
    }
    return 0;
}

int RadioOzy::setMicGain(hpsdr::HpsdrMicGainEnum micGain_prm)
{
    micGain = micGain_prm;

    // lazily using the mic gain boost switch to boost line too
    if (micLine == hpsdr::LineIn)
    {
        setMicLine(micLine);
    }
    else if(micGain == hpsdr::MicGain_0dB)
    {
        unsigned char buf[] = {0x08, 0x14};
        writeOzyI2C(0x1b, buf, 2);
        // if janus ??? TODO FIXME
    }
    else if (micGain_prm == hpsdr::MicGain_20dB)
    {
        unsigned char buf[] = {0x08, 0x15};
        writeOzyI2C(0x1b, buf, 2);
        // if janus ??? TODO FIXME
    }

    return 0;
}

int RadioOzy::setMicLine(hpsdr::HpsdrMicLineEnum micLine_prm)
{
    micLine = micLine_prm;

    if(micLine_prm == hpsdr::MicIn)
    {
        // MIC in
        setMicGain(micGain);
    }
    else if(micLine_prm == hpsdr::LineIn)
    {
        // LINE in, use micGain to boost or not, lazy, I know
        // there really should be a line in gain knob
        if (micGain == hpsdr::MicGain_20dB)
        {
            // line in, mic mute, w/ boost
            unsigned char buf[] = {0x08, 0x10};
            writeOzyI2C(0x1b, buf, 2);
            // left/right  simultaneous vol/mute updates, unmute line in, -18dB gain control
            // which matches my line output from the mixer, YMMV
            buf[0] = 0x01; buf[1] = 0x0b;
            writeOzyI2C(0x1b, buf, 2);
            // if janus ??? TODO FIXME
        }
        else
        {
            // line in, mic mute, no boost
            unsigned char buf[] = {0x08, 0x12};
            writeOzyI2C(0x1b, buf, 2);
            // left/right  simultaneous vol/mute updates, unmute line in, -18dB gain control
            // which matches my line output from the mixer, YMMV
            buf[0] = 0x01; buf[1] = 0x17;
            writeOzyI2C(0x1b, buf, 2);
            // if janus ??? TODO FIXME
        }
    }
    return 0;
}

unsigned char RadioOzy::getDrive() const
{
    return drive;
}

void RadioOzy::setDrive(unsigned char value)
{
    drive = value;
}



SDRMODE_DTTSP RadioOzy::getModeType(string mode)
{
    SDRMODE_DTTSP ret;
    if (strstr(mode.c_str(), "LSB")) {
        ret = LSB;
    } else if (strstr(mode.c_str(),"USB")) {
        ret = USB;
    } else if (strstr(mode.c_str(),"DSB")) {
        ret = DSB;
    } else if (strstr(mode.c_str(),"CW"))  {
        ret = CWL;
    } else if (strstr(mode.c_str(),"CWU")) {
        ret = CWU;
    } else if (strstr(mode.c_str(),"FMN")) {
        ret = FMN;
    } else if (strstr(mode.c_str(),"SAM")) {
        ret = SAM;
    } else if (strstr(mode.c_str(),"AM")) {
        ret = AM;
    } else if (strstr(mode.c_str(),"DIGU")) {
        ret = DIGU;
    } else if (strstr(mode.c_str(),"SPC")) {
        ret = SPEC;
    } else if (strstr(mode.c_str(),"DIGL")) {
        ret = DIGL;
    } else if (strstr(mode.c_str(),"DRM")) {
        ret = DRM;
    } else // default
        ret = USB;
    return ret;
}

string RadioOzy::getModeStr(SDRMODE_DTTSP m) const
{
    string ret;
    if(m == LSB)
        ret = "LSB";
    else if(m == USB)
        ret = "USB";
    else if(m == DSB)
        ret = "DSB";
    else if(m == CWL)
        ret = "CWL";
    else if(m == CWU)
        ret = "CWU";
    else if(m == FMN)
        ret = "FMN";
    else if(m == AM)
        ret = "AM";
    else if(m == DIGU)
        ret = "DIGU";
    else if(m == DIGL)
        ret = "DIGL";
    else if(m == SPEC)
        ret = "SPEC";
    else if(m == SAM)
        ret = "SAM";
    else if(m == DRM)
        ret = "DRM";
    else
        ret = "Unknown mode";
    return ret;
}

int RadioOzy::getNumModes() const
{
    return (int)SDRMODE_DTTSP_MAX - 1;
}

SDRMODE_DTTSP RadioOzy::getMode() const
{
    return mode;
}

void RadioOzy::setMode(const SDRMODE_DTTSP &value, int subrx)
{
    mode = value;

    // RX mode gets set on main and subrx
    SetMode(curMercury, subrx, (SDRMODE_DTTSP)mode);
}

void RadioOzy::setTXMode(const SDRMODE_DTTSP &value)
{
    SetMode(TXTHREAD, 0, (SDRMODE_DTTSP)value);
}

bool RadioOzy::getGuiXmit() const
{
    return guiXmit;
}

void RadioOzy::setGuiXmit(bool value)
{
    guiXmit = value;
}

bool RadioOzy::getPacketXmit() const
{
    return packetXmit;
}

void RadioOzy::setPacketXmit(bool value)
{
    packetXmit = value;
}

bool RadioOzy::getXmit() const
{
    return packetXmit | guiXmit;
}

bool RadioOzy::getXmit20dBAtt() const
{
    return xmit20dBAtt;
}

void RadioOzy::setXmit20dBAtt(bool value)
{
    xmit20dBAtt = value;
}

double RadioOzy::getDSPFiltHi(int subrx) const
{
    return DSPFiltHi[subrx];
}

void RadioOzy::setDSPFiltHi(double value, int subrx)
{
    DSPFiltHi[subrx] = value;
}

double RadioOzy::getDSPFiltLo(int subrx) const
{
    return DSPFiltLo[subrx];
}

void RadioOzy::setDSPFiltLo(double value, int subrx)
{
    DSPFiltLo[subrx] = value;
}

// try not to use this, use setBW(string) below
void RadioOzy::setBW(const double &low, const double &hi, int rx)
{
    double l = low;
    double h = hi;

    SDRMODE_DTTSP mode = getMode();

    if (mode == CWL || mode == CWU) // cw now arrives with offsets stradling center freq of filter
    {
        SetRXFilter(curMercury, rx, (double)h, (double)l);
        SetTXFilter(TXTHREAD, (double)h,(double)l);
    }
    else if(mode == LSB || mode == DIGL)
    {
        if (hi > 0)
            h = -hi;

        if (low > 0)
            l = -low;

        SetRXFilter(curMercury, rx, (double)h, (double)l);
        SetTXFilter(TXTHREAD, (double)h, (double)l);
    }
    else if(mode == AM || mode == SAM || mode == DSB || mode == SPEC || mode == FMN)
    {
        h = abs(hi);
        l = -h;

        SetRXFilter(curMercury, rx, (double)l, (double)h);
        SetTXFilter(TXTHREAD, (double)l,(double)h);
    }
    else if(mode == USB || mode == DIGU)
    {
        h = abs(hi);
        l = abs(low);

        SetRXFilter(curMercury, rx, (double)l, (double)h);
        SetTXFilter(TXTHREAD, (double)l,(double)h);
    }
    DSPFiltHi[rx] = h;
    DSPFiltLo[rx] = l;
}

// set the audio DSP filter lo and hi
void RadioOzy::setBW(const string &val, int rx)
{
    curBW = val;
    stringConvert conv;

    SDRMODE_DTTSP mode = getMode();
    double bw = conv.convertToValues(val);

    const double cwf = (double)keyerFreq;

    double l = cwf-(bw/2.0);
    if (mode == CWL)
        l = -l;
    //else if (mode == CWU)
        //l = l;
    else if (mode == LSB || mode == USB || mode == DIGU || mode == DIGL)
        l = 0.0;
    else if (mode == DSB || mode == AM || mode == SAM)
        l = -bw;

    double h = bw;
    if (mode == CWL)
        h = -(cwf + (bw/2.0));
    else if (mode == CWU)
        h = cwf + (bw/2.0);
    if(mode == LSB || mode == DIGL)
        h = -h;

    setBW(l, h, rx);
}

double RadioOzy::getTxAlcHang()
{
    txAlcHang = GetTXALCHang(TXTHREAD);
    return txAlcHang;
}

void RadioOzy::setTxAlcHang(double value)
{
    txAlcHang = value;
    SetTXALCHang(TXTHREAD, txAlcHang);
}

bool RadioOzy::getTxLevelerEnable()
{
    return GetTXLevelerSt(TXTHREAD);
}

void RadioOzy::setTxLevelerEnable(bool value)
{
    SetTXLevelerSt(TXTHREAD, value);
}

double RadioOzy::getTxLevelerAttack() const
{
    return GetTXLevelerAttack(TXTHREAD);
}

void RadioOzy::setTxLevelerAttack(double value)
{
    SetTXLevelerAttack(TXTHREAD, value);
}

double RadioOzy::getTxLevelerDecay() const
{
    return GetTXLevelerDecay(TXTHREAD);
}

void RadioOzy::setTxLevelerDecay(double value)
{
    SetTXLevelerDecay(TXTHREAD, value);
}

double RadioOzy::getTxLevelerTop() const
{
    return GetTXLevelerTop(TXTHREAD);
}

void RadioOzy::setTxLevelerTop(double value)
{
    SetTXLevelerTop(TXTHREAD, value);
}

double RadioOzy::getTxLevelerHang() const
{
    return GetTXLevelerHang(TXTHREAD);
}

void RadioOzy::setTxLevelerHang(double value)
{
    SetTXLevelerHang(TXTHREAD, value);
}

string RadioOzy::getCurBW() const
{
    return curBW;
}

double RadioOzy::getTxAlcBot()
{
    txAlcBot = GetTXALCBot(TXTHREAD);
    return txAlcBot;
}

void RadioOzy::setTxAlcBot(double value)
{
    txAlcBot = value;
    SetTXALCBot(TXTHREAD, txAlcBot);
}

double RadioOzy::getTxAlcDecay()
{
    txAlcDecay = GetTXALCDecay(TXTHREAD);
    return txAlcDecay;
}

void RadioOzy::setTxAlcDecay(double value)
{
    txAlcDecay = value;
    SetTXALCDecay(TXTHREAD, txAlcDecay);
}

double RadioOzy::getTxAlcAttack()
{
    txAlcAttack = GetTXALCAttack(TXTHREAD);
    return txAlcAttack;
}

void RadioOzy::setTxAlcAttack(double value)
{
    txAlcAttack = value;
    SetTXALCAttack(TXTHREAD, txAlcAttack);
}

bool RadioOzy::getTxAlcEnable()
{
    txAlcEnable = GetTXALCSt(TXTHREAD);
    return txAlcEnable;
}

void RadioOzy::setTxAlcEnable(bool value)
{
    txAlcEnable = value;
    SetTXALCSt(TXTHREAD, txAlcEnable);
}

double RadioOzy::getTxCompanderLevel() const
{
    return txCompanderLevel;
}

void RadioOzy::setTxCompanderLevel(double value)
{
    txCompanderLevel = value;
    SetTXCompand(TXTHREAD, txCompanderLevel);
}

bool RadioOzy::getTxCompanderEnable() const
{
    return txCompanderEnable;
}

void RadioOzy::setTxCompanderEnable(bool value)
{
    txCompanderEnable = value;
    SetTXCompandSt(TXTHREAD, txCompanderEnable);
}

double RadioOzy::getRxCompanderLevel() const
{
    return rxCompanderLevel;
}

void RadioOzy::setRxCompanderLevel(double value)
{
    rxCompanderLevel = value;
    SetRXCompand(0, 0, rxCompanderLevel); // thread 0 subrx 0
    SetRXCompand(0, 1, rxCompanderLevel); // thread 0 subrx 1
}

bool RadioOzy::getRxCompanderEnable() const
{
    return rxCompanderEnable;
}

void RadioOzy::setRxCompanderEnable(bool value)
{
    rxCompanderEnable = value;
    SetRXCompandSt(0, 0, rxCompanderEnable);
    SetRXCompandSt(0, 1, rxCompanderEnable);
}

bool RadioOzy::getTuneMode() const
{
    return tuneMode;
}

void RadioOzy::setTuneMode(bool value)
{
    tuneMode = value;
    // enable the spot tone in tune mode
    SetTXSpot(TXTHREAD, tuneMode);
}

double RadioOzy::getTuneFreq() const
{
    return tuneFreq;
}

void RadioOzy::setTuneFreq(double f)
{
    tuneFreq = f;
    SetTXSpotFreq(TXTHREAD, f);
}

double RadioOzy::getAmCarrierLevel()
{
    // sync up with DttSP
    amCarrierLevel = GetTXAMCarrierLevel(TXTHREAD);
    return amCarrierLevel;
}

void RadioOzy::setAmCarrierLevel(double value)
{
    amCarrierLevel = value;
    // any chance the DttSP value changes w/o updating amCarrierLevel?
    SetTXAMCarrierLevel(TXTHREAD /* tx thread in DttSP */, amCarrierLevel);
}

bool RadioOzy::getRecordHPSDRRaw() const
{
    return recordHPSDRRaw;
}

void RadioOzy::setRecordHPSDRRaw(bool value)
{
    recordHPSDRRaw = value;
}

float RadioOzy::getTxRightGain() const
{
    return txRightGain;
}

void RadioOzy::setTxRightGain(float value)
{
    txRightGain = value;
}

float RadioOzy::getTxLeftGain() const
{
    return txLeftGain;
}

void RadioOzy::setTxLeftGain(float value)
{
    txLeftGain = value;
    SetTXSpotGain(TXTHREAD, (double)value);
}

bool RadioOzy::getKeyerEnabled() const
{
    return keyerEnabled;
}

void RadioOzy::setKeyerEnabled(bool value)
{
    keyerEnabled = value;
}

// CW Keyer stuff
unsigned char RadioOzy::getKeyerHangtime() const
{
    return keyerHangtime;
}

void RadioOzy::setKeyerHangtime(unsigned short value)
{
    keyerHangtime = value;
}

unsigned char RadioOzy::getKeyerPttdelay() const
{
    return keyerPttdelay;
}

void RadioOzy::setKeyerPttdelay(unsigned char value)
{
    keyerPttdelay = value;
}

unsigned char RadioOzy::getKeyerStvolume() const
{
    return keyerStvolume;
}

void RadioOzy::setKeyerStvolume(unsigned char value)
{
    keyerStvolume = value;
}

unsigned char RadioOzy::getKeyerWeight() const
{
    return keyerWeight;
}

void RadioOzy::setKeyerWeight(unsigned char value)
{
    keyerWeight = value;
}

unsigned char RadioOzy::getKeyerSpeed() const
{
    return keyerSpeed;
}

void RadioOzy::setKeyerSpeed(unsigned char value)
{
    keyerSpeed = value;
}

unsigned char RadioOzy::getKeyerMode() const
{
    return keyerMode;
}

void RadioOzy::setKeyerMode(unsigned char value)
{
    // range limit to the HPSDR protocol
    if (value > 3) value = 3;
    keyerMode = value;
}

uint16_t RadioOzy::getKeyerFreq() const
{
    return keyerFreq;
}

void RadioOzy::setKeyerFreq(const double &value)
{
    // range limit to the HPSDR protocol
    double val = value;
    if (val < 0.0)  val = 0.0;
    if (val > 4095) val = 4095.0;

    keyerFreq = (uint16_t) val;
    setTuneFreq((double)keyerFreq);
}

bool RadioOzy::getKeyerReversed() const
{
    return keyerReversed;
}

void RadioOzy::setKeyerReversed(bool value)
{
    keyerReversed = value;
}

bool RadioOzy::getKeyerSpacing() const
{
    return keyerSpacing;
}

void RadioOzy::setKeyerSpacing(bool value)
{
    keyerSpacing = value;
}


int RadioOzy::setMicSrc(hpsdr::HpsdrMicSrcEnum MicSrc_prm)
{
    if ((MicSrc_prm == hpsdr::Mic_Janus) ||
            (MicSrc_prm == hpsdr::Mic_Penelope))
    {
        //fprintf(stderr, "%s: MicSrc_prm %x\n", __func__, MicSrc_prm);
        micSrc = MicSrc_prm;
    }
    else
    {
        //fprintf(stderr, "%s: Invlaid MicSrc_prm %x\n", __func__, MicSrc_prm);
        return -1;
    }
    return 0;
}
