#include "common.h" // string conversion routines for kilo, mega, etc.
#include "rsdk.h"

#include "plotdrawoptionsdockwidget.h"
#include "ui_plotdrawoptionsdockwidget.h"

PlotDrawOptionsDockWidget::PlotDrawOptionsDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::PlotDrawOptionsDockWidget),
    m_phaseOn(false),
    m_spectrumOn(true),
    m_waterfallOn(true),
    m_scopeOn(false),
    m_yMin(-120),
    m_yMax(-60),
    m_yMinAutoScale(false),
    m_yMaxAutoScale(false),
    m_initialized(false),
    m_numCoeffs(11),
    m_snapRate(60),
    m_sGridEnabled(true),
    m_dbGridEnabled(false),
    m_fGridEnabled(true),
    m_titleEnabled(false),
    m_fillTrace(false),
    m_bandScopeEnabled(false),
    m_bandEnabled(true)
{
    ui->setupUi(this);

    ui->yMaxSpinBox->setSuffix(" dBm");
    ui->yMinSpinBox->setSuffix(" dBm");

    connect(ui->yMaxSpinBox, SIGNAL(valueChanged(QString)), this, SLOT(yMaxSpinBoxChanged(QString)));
    connect(ui->yMinSpinBox, SIGNAL(valueChanged(QString)), this, SLOT(yMinSpinBoxChanged(QString)));

    connect (ui->smoothHSlider, SIGNAL(valueChanged(int)), this, SLOT(setNumCoeffs(int)));
    //connect (ui->rateScrollBar, SIGNAL(valueChanged(int)), this, SLOT(rateChanged(int)));
    //connect (ui->wfGainScrollBar, SIGNAL(valueChanged(int)), this, SLOT(wfGainChanged(int)));

    // these are the color selection objects for each button, CSelector objects
    connect (ui->spectrumCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(spectrumColor(QColor)));
    connect (ui->bandScopeCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(bandScopeColor(QColor)));
    connect (ui->titleCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(titleColor(QColor)));
    connect (ui->bandCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(bandColor(QColor)));
    connect (ui->sGridCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(sGridColor(QColor)));
    connect (ui->dbGridCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(dbGridColor(QColor)));
    connect (ui->fGridCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(fGridColor(QColor)));
    connect (ui->scopeCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(scopeColor(QColor)));
    connect (ui->scopePkCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(scopePkColor(QColor)));
    connect (ui->scopeGridCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(scopeGridColor(QColor)));
    connect (ui->waterfallIntervalHSlider, SIGNAL(valueChanged(int)), this, SLOT(setWfInterval(int)));
    connect (ui->spectrumIntervalHSlider, SIGNAL(valueChanged(int)), this, SLOT(setSpInterval(int)));

    connect (ui->spectrumLGradientSelector, SIGNAL(lgPreviewChanged(const QLinearGradient &)), this, SLOT(setSpxLGradient(const QLinearGradient &)));

    // initial settings to be overridden by Q_PROPERTY
    setPhaseOn(m_phaseOn);
    setSpectrumOn(m_spectrumOn);
    setWaterfallOn(m_waterfallOn);
    setScopeOn(m_scopeOn);

    setYMax(m_yMax);
    setYMin(m_yMin);
    ui->yMaxSpinBox->setValue(m_yMax);
    ui->yMinSpinBox->setValue(m_yMin);

    setNumCoeffs(m_numCoeffs);
    setDrawRate(m_snapRate);
    setSGridEnabled(m_sGridEnabled);
    setDbGridEnabled(m_dbGridEnabled);
    setFreqGridEnabled(m_fGridEnabled);
    setTitleEnabled(m_titleEnabled);
    setFillTrace(m_fillTrace);
    setBandScopeEnabled(m_bandScopeEnabled);
    setBandEnabled(m_bandEnabled);

    scopeColor(ui->scopeCSelector->color());
    bandScopeColor(ui->bandCSelector->color());

}

PlotDrawOptionsDockWidget::~PlotDrawOptionsDockWidget()
{
    delete ui;
}

void PlotDrawOptionsDockWidget::start()
{
    m_initialized = true;

    emit fGridEnabled(m_fGridEnabled);
    emit fillTraceChanged(m_fillTrace);
    emit bandScopeChanged(m_bandScopeEnabled);
    emit bandChanged(m_bandEnabled);
    emit titleEnabled(m_titleEnabled);
    emit dbGridEnabled(m_dbGridEnabled);
    emit sGridEnabled(m_sGridEnabled);
    emit numCoeffsChanged(m_numCoeffs);
    emit phaseToggled(m_phaseOn);
    emit spectrumToggled(m_spectrumOn);
    emit waterfallToggled(m_waterfallOn);
    emit scopeToggled(m_scopeOn);

    emit bandScopeColorChanged(ui->bandScopeCSelector->color());
    emit scopeColorChanged(ui->scopeCSelector->color());
    emit scopePkColorChanged(ui->scopePkCSelector->color());
    emit scopeGridColorChanged(ui->scopeGridCSelector->color());

    emit yMinChanged(m_yMin);
    emit yMaxChanged(m_yMax);
}

void PlotDrawOptionsDockWidget::rateChanged(int val)
{
    m_snapRate = (double)val;

    /*
    if(theRsdk->DisplayDock && theRsdk->DisplayDock->snapper)
        theRsdk->DisplayDock->snapper->setRate(m_snapRate);
        */
}

void PlotDrawOptionsDockWidget::on_waterfallTB_toggled(bool checked)
{
    setWaterfallOn(checked);
}

void PlotDrawOptionsDockWidget::on_spectrumTB_toggled(bool checked)
{
    setSpectrumOn(checked);
}

void PlotDrawOptionsDockWidget::on_scopeTB_toggled(bool checked)
{
    setScopeOn(checked);
}

void PlotDrawOptionsDockWidget::on_phaseTB_toggled(bool checked)
{
    setPhaseOn(checked);
}

void PlotDrawOptionsDockWidget::on_dbGridTB_toggled(bool checked)
{
    setDbGridEnabled(checked);
}

void PlotDrawOptionsDockWidget::on_titleTB_toggled(bool checked)
{
    setTitleEnabled(checked);
}

void PlotDrawOptionsDockWidget::on_sGridTB_toggled(bool checked)
{
    setSGridEnabled(checked);
}

void PlotDrawOptionsDockWidget::on_bandTB_toggled(bool checked)
{
    setBandEnabled(checked);
}

void PlotDrawOptionsDockWidget::on_fillTraceTB_toggled(bool checked)
{
    setFillTrace(checked);
}

void PlotDrawOptionsDockWidget::on_bandScopeTB_toggled(bool checked)
{
    setBandScopeEnabled(checked);
}

void PlotDrawOptionsDockWidget::on_fGridTB_toggled(bool checked)
{
    setFreqGridEnabled(checked);
}

bool PlotDrawOptionsDockWidget::fillTrace() const
{
    return m_fillTrace;
}

void PlotDrawOptionsDockWidget::setFillTrace(bool fillTrace)
{
    m_fillTrace = fillTrace;
    bool pstate = ui->fillTraceTB->blockSignals(true);
    ui->fillTraceTB->setChecked(fillTrace);
    ui->fillTraceTB->blockSignals(pstate);
    emit fillTraceChanged(m_fillTrace);
}

bool PlotDrawOptionsDockWidget::bandScopeEnabled() const
{
    return m_bandScopeEnabled;
}

void PlotDrawOptionsDockWidget::setBandScopeEnabled(bool enabled)
{
    m_bandScopeEnabled = enabled;
    bool pstate = ui->bandScopeTB->blockSignals(true);
    ui->bandScopeTB->setChecked(enabled);
    ui->bandScopeTB->blockSignals(pstate);
    emit bandScopeChanged(m_bandScopeEnabled);
}

bool PlotDrawOptionsDockWidget::bandEnabled() const
{
    return m_bandEnabled;
}

void PlotDrawOptionsDockWidget::setBandEnabled(bool bandEnabled)
{
    m_bandEnabled = bandEnabled;
    bool pstate = ui->bandTB->blockSignals(true);
    ui->bandTB->setChecked(bandEnabled);
    ui->bandTB->blockSignals(pstate);
    emit bandChanged(m_bandEnabled);
}

void PlotDrawOptionsDockWidget::bandColor(QColor color)
{
    bool pstate = ui->bandCSelector->blockSignals(true);
    ui->bandCSelector->setColor(color);
    ui->bandCSelector->blockSignals(pstate);
    emit bandColorChanged(color);
}

void PlotDrawOptionsDockWidget::scopeColor(QColor color)
{
    scopeColor(color, true);
}

void PlotDrawOptionsDockWidget::scopeColor(QColor color, bool sendSignal)
{
    bool pstate = ui->scopeCSelector->blockSignals(true);
    ui->scopeCSelector->setColor(color);
    ui->scopeCSelector->blockSignals(pstate);
    if (sendSignal)
        emit scopeColorChanged(color);
}

void PlotDrawOptionsDockWidget::scopePkColor(QColor color)
{
    scopePkColor(color, true);
}

void PlotDrawOptionsDockWidget::scopePkColor(QColor color, bool sendSignal)
{
    bool pstate = ui->scopePkCSelector->blockSignals(true);
    ui->scopePkCSelector->setColor(color);
    ui->scopePkCSelector->blockSignals(pstate);
    if (sendSignal)
        emit scopePkColorChanged(color);
}

void PlotDrawOptionsDockWidget::scopeGridColor(QColor color)
{
    scopeGridColor(color, true);
}

void PlotDrawOptionsDockWidget::scopeGridColor(QColor color, bool sendSignal)
{
    bool pstate = ui->scopeGridCSelector->blockSignals(true);
    ui->scopeGridCSelector->setColor(color);
    ui->scopeGridCSelector->blockSignals(pstate);
    if (sendSignal)
        emit scopeGridColorChanged(color);
}

bool PlotDrawOptionsDockWidget::getFreqGridEnabled() const
{
    return m_fGridEnabled;
}

void PlotDrawOptionsDockWidget::setFreqGridEnabled(bool value)
{
    m_fGridEnabled = value;
    bool pstate = ui->fGridTB->blockSignals(true);
    ui->fGridTB->setChecked(m_fGridEnabled);
    ui->fGridTB->blockSignals(pstate);
    emit fGridEnabled(m_fGridEnabled);
}

void PlotDrawOptionsDockWidget::bandScopeColor(QColor color)
{
    bool pstate = ui->bandScopeCSelector->blockSignals(true);
    ui->bandScopeCSelector->setColor(color);
    ui->bandScopeCSelector->blockSignals(pstate);
    emit bandScopeColorChanged(color);
}

void PlotDrawOptionsDockWidget::fGridColor(QColor color)
{
    bool pstate = ui->fGridCSelector->blockSignals(true);
    ui->fGridCSelector->setColor(color);
    ui->fGridCSelector->blockSignals(pstate);
    emit fGridColorChanged(color);
}

void PlotDrawOptionsDockWidget::on_yMinAutoScaleTB_toggled(bool checked)
{
    if (!checked) {
        // triple negative;)
        if (!ui->yMinSpinBox->isEnabled())
            ui->yMinSpinBox->setDisabled(false);
        if(!m_initialized)
            ui->yMinAutoScaleTB->setChecked(false);
        m_yMinAutoScale = false;
    } else {
        // we want autoscaling on, disable the spinbox
        ui->yMinSpinBox->setDisabled(true);
        if(!m_initialized)
            ui->yMinAutoScaleTB->setChecked(true);
        m_yMinAutoScale = true;
        m_yMin = -50;
    }
    ui->yMinSpinBox->setValue(m_yMin);
    // emitted in the setValue callback
    //emit yMinChanged(_yMin);
}

void PlotDrawOptionsDockWidget::on_yMaxAutoScaleTB_toggled(bool checked)
{
    if (!checked) {
        if (!ui->yMaxSpinBox->isEnabled())
            ui->yMaxSpinBox->setDisabled(false);
        if(!m_initialized)
            ui->yMaxAutoScaleTB->setChecked(false);
        m_yMaxAutoScale = false;
    } else {
        ui->yMaxSpinBox->setDisabled(true);
        if(!m_initialized)
            ui->yMaxAutoScaleTB->setChecked(true);
        m_yMaxAutoScale = true;
        m_yMax = -1000;
    }
    ui->yMaxSpinBox->setValue(m_yMax);
    //emit yMaxChanged(_yMax);
}

bool PlotDrawOptionsDockWidget::getYMaxAutoScale() const
{
    return m_yMaxAutoScale;
}

bool PlotDrawOptionsDockWidget::getYMinAutoScale() const
{
    return m_yMinAutoScale;
}

int PlotDrawOptionsDockWidget::yMax() const
{
    return m_yMax;
}

void PlotDrawOptionsDockWidget::setYMax(int val)
{
    // range limit to _yMin
    m_yMax = val < m_yMin ? m_yMin : val;
    ui->yMaxSpinBox->setValue(m_yMax);
}

int PlotDrawOptionsDockWidget::yMin() const
{
    return m_yMin;
}

void PlotDrawOptionsDockWidget::setYMin(int val)
{
    // range limit to _yMax
    m_yMin = val > m_yMax ? m_yMax : val;
    ui->yMinSpinBox->setValue(m_yMin);
}

bool PlotDrawOptionsDockWidget::getScopeOn() const
{
    return m_scopeOn;
}

void PlotDrawOptionsDockWidget::setScopeOn(bool value)
{
    m_scopeOn = value;
    bool pstate = ui->scopeTB->blockSignals(true);
    ui->scopeTB->setChecked(value);
    ui->scopeTB->blockSignals(pstate);
    emit scopeToggled(m_scopeOn);
}

bool PlotDrawOptionsDockWidget::getWaterfallOn() const
{
    return m_waterfallOn;
}

void PlotDrawOptionsDockWidget::setWaterfallOn(bool value)
{
    m_waterfallOn = value;
    bool pstate = ui->waterfallTB->blockSignals(true);
    ui->waterfallTB->setChecked(value);
    ui->waterfallTB->blockSignals(pstate);
    emit waterfallToggled(m_waterfallOn);
}

bool PlotDrawOptionsDockWidget::getSpectrumOn() const
{
    return m_spectrumOn;
}

void PlotDrawOptionsDockWidget::setSpectrumOn(bool value)
{
    m_spectrumOn = value;
    bool pstate = ui->spectrumTB->blockSignals(true);
    ui->spectrumTB->setChecked(value);
    ui->spectrumTB->blockSignals(pstate);
    emit spectrumToggled(m_spectrumOn);
}

bool PlotDrawOptionsDockWidget::getPhaseOn() const
{
    return m_phaseOn;
}

void PlotDrawOptionsDockWidget::setPhaseOn(bool value)
{
    m_phaseOn = value;
    bool pstate = ui->phaseTB->blockSignals(true);
    ui->phaseTB->setChecked(value);
    ui->phaseTB->blockSignals(pstate);
    emit phaseToggled(m_phaseOn);
}

void PlotDrawOptionsDockWidget::yMaxSpinBoxChanged(QString max)
{
    // remove alpha , to get rid of " hz" from string to make an int
    QRegExp rx ( "[a-zA-Z]|,");
    QString labelVal = max.remove(rx);

    bool ok;
    int val = QLocale(QLocale::English).toInt(labelVal, &ok);

    if (ok==true)
    {
        if (val < m_yMin) {
            m_yMax = m_yMin;
        } else {
            m_yMax = val;
        }
        emit yMaxChanged(m_yMax);
    }
}

double PlotDrawOptionsDockWidget::getDrawRate() const
{
    return m_snapRate;
}

void PlotDrawOptionsDockWidget::setDrawRate(double value)
{
    m_snapRate = value;
    /*
    bool pstate = ui->rateScrollBar->blockSignals(true);
    ui->rateScrollBar->setValue(drawRate);
    ui->rateScrollBar->blockSignals(pstate);
    */
}

int PlotDrawOptionsDockWidget::getNumCoeffs() const
{
    return m_numCoeffs;
}

void PlotDrawOptionsDockWidget::setNumCoeffs(int value)
{
    m_numCoeffs = value;
    bool pstate = ui->smoothHSlider->blockSignals(true);
    ui->smoothHSlider->setValue(m_numCoeffs);
    ui->smoothHSlider->blockSignals(pstate);
    emit numCoeffsChanged(m_numCoeffs);
}

void PlotDrawOptionsDockWidget::yMinSpinBoxChanged(QString min)
{
    // remove alpha , to get rid of " hz" from string to make an int
    QRegExp rx ( "[a-zA-Z]|,");
    QString labelVal = min.remove(rx);

    bool ok;
    int val = QLocale(QLocale::English).toInt(labelVal, &ok);

    if (ok==true)
    {
        if (val > m_yMax) {
            m_yMin = m_yMax;
        } else {
            m_yMin = val;
        }
        emit yMinChanged(m_yMin);
    }
}

bool PlotDrawOptionsDockWidget::getTitleEnabled() const
{
    return m_titleEnabled;
}

void PlotDrawOptionsDockWidget::setTitleEnabled(bool value)
{
    m_titleEnabled = value;
    bool pstate = ui->titleTB->blockSignals(true);
    ui->titleTB->setChecked(value);
    ui->titleTB->blockSignals(pstate);
    emit titleEnabled(m_titleEnabled);
}

void PlotDrawOptionsDockWidget::titleColor(QColor color)
{
    bool pstate = ui->titleCSelector->blockSignals(true);
    ui->titleCSelector->setColor(color);
    ui->titleCSelector->blockSignals(pstate);
    emit titleColorChanged(color);
}

void PlotDrawOptionsDockWidget::spectrumColor(QColor color)
{
    bool pstate = ui->spectrumCSelector->blockSignals(true);
    ui->spectrumCSelector->setColor(color);
    ui->spectrumCSelector->blockSignals(pstate);
    emit  spectrumColorChanged(color);
}

void PlotDrawOptionsDockWidget::setSGridEnabled(bool value)
{
    m_sGridEnabled = value;
    bool pstate = ui->sGridTB->blockSignals(true);
    ui->sGridTB->setChecked(value);
    ui->sGridTB->blockSignals(pstate);
    emit sGridEnabled(m_sGridEnabled);
}

void PlotDrawOptionsDockWidget::dbGridColor(QColor color)
{
    bool pstate = ui->dbGridCSelector->blockSignals(true);
    ui->dbGridCSelector->setColor(color);
    ui->dbGridCSelector->blockSignals(pstate);
    emit dbGridColorChanged(color);
}

void PlotDrawOptionsDockWidget::sGridColor(QColor color)
{
    bool pstate = ui->sGridCSelector->blockSignals(true);
    ui->sGridCSelector->setColor(color);
    ui->sGridCSelector->blockSignals(pstate);
    emit sGridColorChanged(color);
}

bool PlotDrawOptionsDockWidget::getSGridEnabled() const
{
    return m_sGridEnabled;
}

bool PlotDrawOptionsDockWidget::getDbGridEnabled() const
{
    return m_dbGridEnabled;
}

void PlotDrawOptionsDockWidget::setDbGridEnabled(bool value)
{
    m_dbGridEnabled = value;
    bool pstate = ui->dbGridTB->blockSignals(true);
    ui->dbGridTB->setChecked(value);
    ui->dbGridTB->blockSignals(pstate);
    emit dbGridEnabled(m_dbGridEnabled);
}

int PlotDrawOptionsDockWidget::getSpInterval() const
{
    return m_spInterval;
}

void PlotDrawOptionsDockWidget::setSpInterval(int spInterval)
{
    setSpInterval(spInterval, true);
}

void PlotDrawOptionsDockWidget::setSpInterval(int spInterval, bool sendSig)
{
    m_spInterval = spInterval;
    bool pstate = ui->spectrumIntervalHSlider->blockSignals(true);
    ui->spectrumIntervalHSlider->setValue(m_spInterval);
    ui->spectrumIntervalHSlider->blockSignals(pstate);
    if (sendSig)
        emit spectrumIntervalChanged(m_spInterval);
}

QColor PlotDrawOptionsDockWidget::getSpxGradientColorAtPct(const QLinearGradient &lg, const double pct)
{
    return ui->spectrumLGradientSelector->colorAtPct(lg, pct);
}

QLinearGradient PlotDrawOptionsDockWidget::getSpxGradient()
{
    return ui->spectrumLGradientSelector->LGradient();
}

void PlotDrawOptionsDockWidget::setSpxLGradient(const QLinearGradient &lg)
{
    emit spxLGradientChanged(lg);
}

int PlotDrawOptionsDockWidget::getWfInterval() const
{
    return m_wfInterval;
}

void PlotDrawOptionsDockWidget::setWfInterval(int wfInterval)
{
    setWfInterval(wfInterval, true);
}

void PlotDrawOptionsDockWidget::setWfInterval(int wfInterval, bool sendSig)
{
    m_wfInterval = wfInterval;
    bool pstate = ui->waterfallIntervalHSlider->blockSignals(true);
    ui->waterfallIntervalHSlider->setValue(m_wfInterval);
    ui->waterfallIntervalHSlider->blockSignals(pstate);
    if (sendSig)
        emit waterfallIntervalChanged(m_wfInterval);
}

double PlotDrawOptionsDockWidget::getSnapRate() const
{
    return m_snapRate;
}

void PlotDrawOptionsDockWidget::setSnapRate(double snapRate)
{
    m_snapRate = snapRate;
}
