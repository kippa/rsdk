#ifndef MODEDOCKWIDGET_H
#define MODEDOCKWIDGET_H

#include <iostream>
#include <stdio.h>
#include <QTextStream>
#include <QDockWidget>
#include <QDataStream>
#include <QAbstractButton>
#include <QTimer>

#include <dttsp.h>

//#include "serializationcontext.h"
#include "rsdkdockwidget.h"

#include "radioozy.h"
class RadioOzy;
extern RadioOzy *rozy;

namespace Ui {
    class ModeDockWidget;
}

class ModeDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(bool m_autoMode READ getAutoMode WRITE setAutoMode)
    Q_PROPERTY(int m_mode READ getMode WRITE setMode)

    // please see https://woboq.com/blog/q_enum.html, read this!
    //Q_ENUM(SDRMODE_DTTSP);

public:
    explicit ModeDockWidget(QWidget *parent = 0);
    ~ModeDockWidget();

    int getModeType(QString mode); // return the SDRMODE_DTTSP type for the given string
    int whichMode(); // return the mode type of the current selected button

    void setMode (int m);
    void setMode (int m, bool sendSignal);
    void setMode (QString m, bool sendSignal);
    void setMode (const char *m, bool sendSignal);

    bool getAutoMode() const;
    void setAutoMode(bool autoMode);

    int getNumModes() { return SDRMODE_DTTSP_MAX-1; };

    int getMode() const;

signals:
    //void modeChanged(SDRMODE_DTTSP mode);

public slots:
    QString getModeStr(void) const;
    void start();

private slots:
    void modeKeyPress(QAbstractButton *btn);
    void on_ModeAuto_toggled(bool checked);

    void updateMode();

signals:
    //void modeChanged(int mode);

private:
    Ui::ModeDockWidget *ui;

    // mode is now kept in the radioozy and we just call out to get/set it
    // this simplifies the enums, but strings are still a hassle
    int m_mode; // really a SDRMODE_DTTSP type
    bool m_autoMode;

};

#endif // MODEDOCKWIDGET_H
