#ifndef RSDK_H
#define RSDK_H
#include <pthread.h>

#include <QObject>
#include <QWidget>
#include <QString>
#include <QSplitter>
#include <QSignalMapper>
#include <QCommandLineParser>

#include "dbstore.h"

#include "radioozy.h"

#include "modedockwidget.h"
#include "banddockwidget.h"
#include "filterdockwidget.h"
#include "dspdockwidget.h"
#include "vfodockwidget.h"
#include "alexdockwidget.h"
#include "displaydockwidget.h"
#include "smeterdockwidget.h"
#include "hpsdrdockwidget.h"
#include "agcdockwidget.h"
#include "agcparmsdockwidget.h"
#include "rangecontrol.h"
#include "audiodockwidget.h"
#include "txdockwidget.h"
#include "cwdockwidget.h"
#include "txcontroldockwidget.h"
#include "plotdrawoptionsdockwidget.h"
#include "mercuryselectdockwidget.h"
#include "eq10dockwidget.h"
#include "rxparmsdockwidget.h"

#include "oscope.h"

//#include "audioplatform.h"
//class audioPlatform;

#include "ui_rsdk.h"

#include "radioozy.h"

class rsdk : public QMainWindow, private Ui::rsdk
{
    Q_OBJECT

    //Q_PROPERTY(QString propFileName READ getPropFileName WRITE setPropFileName)
    Q_PROPERTY(int m_scopeSnapMode READ getScopeSnap WRITE setScopeSnap)

public:
    explicit rsdk(QWidget *parent = nullptr, QApplication *app = nullptr);
    ~rsdk();
    void closeEvent(QCloseEvent *e);
    void changeEvent(QEvent *e);
    void makeDocks(QWidget *parent);
    // using std::sort requires a static functor
    // this allows sorting rsdkDockWidget by objectName
    struct Less {
        Less(rsdk &r) : m_r(r) {}
        bool operator()(const rsdkDockWidget *d1, const rsdkDockWidget *d2)
        {
            return d1->objectName() < d2->objectName();
        }
        rsdk &m_r;
    };

    QApplication *m_app; // pass in from main.cpp so we can call m_app->shutdown, is public so objects can call app functions

    dbStore *db;
    //audioPlatform *ap;

    RadioOzy *rozy; // hang radio ozy off this handle

    // protect sample rate changes
    pthread_mutex_t sampRateLock;

    // docking widgets need to access each others methods
    QSplitter *centralSplitter;
    ModeDockWidget *ModeDock;
    FilterDockWidget *FilterDock;
    DspDockWidget *DspDock;
    VfoDockWidget *VfoDock;
    AlexDockWidget *AlexDock;
    DisplayDockWidget *DisplayDock;
    SmeterDockWidget *SmeterDock;
    BandDockWidget *BandDock;
    HpsdrDockWidget *HpsdrDock;
    AgcDockWidget *AgcDock;
    AgcParmsDockWidget *AgcParmsDock;
    AudioDockWidget *AudioDock;
    TxDockWidget *TxDock;
    PlotDrawOptionsDockWidget *PlotDrawOptionsDock;
    CwDockWidget *CwDock;
    TXControlDockWidget *TxControlsDock;
    //QMainWindow *mainWin;
    MercurySelectDockWidget *MercurySelectDock;
    EQ10DockWidget *RXEQDock, *TXEQDock;
    RxParmsDockWidget *RxParmsDock;

    QToolBar *tbar;
    QStatusBar *sbar; // we'll roll our own instead of using the mainWin one, so it can dock, move, get tooltips

    bool recordHPSDRRaw() const;
    bool recordIQFloat() const;
    bool recordAudio() const;
    int readProps(QWidget *w, QTextStream &inStream);
    int saveProps(QWidget *w, QTextStream &outStream);

signals:
    void filterLowPropChanged(float);
    void filterHiPropChanged(float);
    void rsdkFreqChanged(qlonglong f);
    void agcModeChanged(void);
    void squelchStateChanged(bool, bool);
    void squelchGainChanged(double);

    void rxDCBStateChanged();
    void txDCBStateChanged();

    void rxDCBGainChanged(double);
    void txDCBGainChanged(double);

private slots:

    // control local recordings
    void setRecordHPSDRRaw(bool recordHPSDRRaw);
    void setRecordIQFloat(bool recordIQFloat);
    void setRecordAudio(bool recordAudio);

    void startAll();
    void stopAll(); // shutdown the QApplication

    void showProps(QObject *obj);

    void onRsdkDestroyed(QObject *arg1);
    void on_actionQuit_triggered();
    void on_actionAll_Windows_triggered(bool checked);
    void on_actionTitle_Bars_triggered(bool checked);
    void on_actionRecord_HPSDR_Raw_triggered(bool checked);

    void on_actionSaveProps_triggered();

    void on_actionLoadProps_triggered();

public slots:
    void storeProps(bool doDialog);
    void loadProps(bool doGui);
    void createDockActions();
    void handleDockMenu(QWidget *dock); // show or hide the dock as the use wishes in the docks menu bar list
    void setDockMenuChecked(QWidget *dock, bool setit);

    int getRsdkSampleRate() const;
    void setRsdkSamplerate(int value);
    void setRsdkSamplerate(hpsdr::HpsdrSampleRateEnum sampleRate);

    // CW interface to rozy
    // CwDock emits to here, and this calls rozy->setups
    // no local knowledge is kept, yet
    void setCwFreq(double f);
    void setCwSpeed(unsigned int wpm);
    void setCwMode(unsigned int mode);
    void setCwWeight(unsigned int weight);
    void setCwPttDelay(unsigned int delay);
    void setCwHangTime(unsigned int hangTime);
    void setCwSpacing(bool spacing);
    void setCwKeysReversed(bool reversed);
    void setCwSideToneVolume(unsigned int volume);
    void setCWInternalKeyer(bool enable);

    // the gains for tx L & R
    void setTxRightGain(float gain);
    void setTxLeftGain(float gain);

    qlonglong getRsdkFreq() const;
    void setRsdkFreq(const qlonglong value);
    void setRsdkFreq(const double value);

    qlonglong getRsdkSubRxFreq() const;
    void setRsdkSubRxFreq(const qlonglong &value);

    int getRsdkTuneRate() const;
    void setRsdkTuneRate(const int tuneRate);

    unsigned char getRsdkTxDriveLevel() const;
    void setRsdkTxDriveLevel(const unsigned int level);

    bool getRsdkTuneMode() const;
    void setRsdkTuneMode(bool enableTune);

    // Hpsdr
    hpsdr::HpsdrAttEnum getRsdkAtt() const;
    void setRsdkAtt(const hpsdr::HpsdrAttEnum &value);

    hpsdr::HpsdrRxAntEnum getRsdkRxant() const;
    void setRsdkRxant(const hpsdr::HpsdrRxAntEnum &value);

    hpsdr::HpsdrTxAntEnum getRsdkTxant() const;
    void setRsdkTxant(const hpsdr::HpsdrTxAntEnum &value);

    hpsdr::HpsdrRxOutEnum getRsdkRxout() const;
    void setRsdkRxout(const hpsdr::HpsdrRxOutEnum &value);

    hpsdr::HpsdrPreAmpEnum getRsdkPreamp() const;
    void setRsdkPreamp(const hpsdr::HpsdrPreAmpEnum &value);

    hpsdr::HpsdrDitherEnum getRsdkDither() const;
    void setRsdkDither(const hpsdr::HpsdrDitherEnum &value);

    hpsdr::HpsdrRandomEnum getRsdkRandom() const;
    void setRsdkRandom(const hpsdr::HpsdrRandomEnum &value);

    hpsdr::HpsdrRef10MEnum getRsdkRef10m() const;
    void setRsdkRef10m(const hpsdr::HpsdrRef10MEnum &value);

    hpsdr::HpsdrRef122MEnum getRsdkRef122m() const;
    void setRsdkRef122m(const hpsdr::HpsdrRef122MEnum &value);

    hpsdr::HpsdrMicSrcEnum getRsdkMicsrc() const;
    void setRsdkMicsrc(const hpsdr::HpsdrMicSrcEnum &value);

    hpsdr::HpsdrDuplexEnum getRsdkDuplex() const;
    void setRsdkDuplex(const hpsdr::HpsdrDuplexEnum &value);

    hpsdr::HpsdrClassEEnum getRsdkClasse() const;
    void setRsdkClasse(const hpsdr::HpsdrClassEEnum &value);

    hpsdr::HpsdrConfigEnum getRsdkConfig() const;
    void setRsdkConfig(const hpsdr::HpsdrConfigEnum &value);

    hpsdr::HpsdrMicGainEnum getRsdkMicGain() const;
    void setRsdkMicGain(const hpsdr::HpsdrMicGainEnum &value);

    hpsdr::HpsdrMicLineEnum getRsdkMicLine() const;
    void setRsdkMicLine(const hpsdr::HpsdrMicLineEnum &value);

    // DttSP
    SDRMODE_DTTSP getRsdkMode() const;
    void setRsdkMode(const SDRMODE_DTTSP &value);
    void setRsdkMode(const char *m);
    void setRsdkMode(const QString &m);

    // link mode to filters
    void setRsdkFilter(double low, double hi);

    AGCMODE getRsdkAgcMode() const;
    void setRsdkAgcMode(const AGCMODE &value);
    void setRsdkAgcAttack(double val);
    void setRsdkAgcDecay(double val);
    void setRsdkAgcHang(double val);
    void setRsdkAgcHangThreshold(double val);
    void setRsdkAgcSlope(double val);

    bool getRsdkANF() const;
    void setRsdkANF(bool value);

    bool getRsdkBIN() const;
    void setRsdkBIN(bool value);

    bool getRsdkMUTE() const;
    void setRsdkMUTE(bool value);

    bool getRsdkNR() const;
    void setRsdkNR(bool value);

    bool getRsdkBNR() const;
    void setRsdkBNR(bool value);

    bool getRsdkNB() const;
    void setRsdkNB(bool value);

    void setRsdkNBVals (double threshold);
    double getRsdkNBVals (void);

    void setRsdkNBHangtime(double hang);
    double getRsdkNBHangtime(int subrx);

    bool getRsdkSDROM() const;
    void setRsdkSDROM(bool value);

    void setRsdkSDROMVals (double threshold);
    double getRsdkSDROMVals (void);

    float getRsdkVol() const;
    void setRsdkVol(float value);

    float getRsdkBal() const;
    void setRsdkBal(float value);

    float getRsdkSubRxVol() const;
    void setRsdkSubRxVol(float value);

    float getRsdkSubRxBal() const;
    void setRsdkSubRxBal(float value);

    bool getXmit();
    void setXmit(bool xmit);

    void enableSubRx(bool enable);

    // diversity settings
    void setCurMerc(MercurySelectDockWidget::MERCMODE merc);
    void setNormI(double value);
    void setNormQ(double value);
    void setGain(double value);

    // DttSP snap modes
    void setScopeSnap(QString snapMode);
    void setScopeSnap(int snapMode);
    int getScopeSnap(void);

    void setPhaseSnap(QString snapMode);
    void setPanadapterSnap(QString snapMode);
    void setSpectrumSnap(QString snapMode);
    void setTxSnap(QString snapMode);

    // make serial numbers of Mercury, Penny Lane, and Ozy into a title bar string
    void setTitle();

    // tx parms
    void setAMCarrierLevel(double level);
    double getAMCarrierLevel();

    void setTXCompanderLevel(double level); // < 0.0 compress, > 0.0 expand, 0.0 = linear
    void setTXCompander(bool enable);
    double getTXCompanderLevel();
    bool getTXCompander();


    void setRXCompanderLevel(double level); // < 0.0 compress, > 0.0 expand, 0.0 = linear
    void setRXCompander(bool enable);
    double getRXCompanderLevel();
    bool getRXCompander();

    void setTXDCBlock(bool on);
    void setTXAGCFF (bool setit);
    void setTXAGCFFCompression (double txcompression);
    void setTXFMDeviation(double deviation);
    void setTXFilter(double low_frequency, double high_frequency);
    void setTXOsc (double newfreq);

    void setTXSquelchSt(bool on);
    void setTXSquelchVal(double value);
    void setTXSquelchAtt(double atten);

    void setTXALCSt (bool on);
    void setTXALCAttack (double attack);
    void setTXALCDecay (double decay);
    void setTXALCBot (double max_agc);
    void setTXALCHang (double hang);

    bool getTXALCSt ();
    double getTXALCAttack ();
    double getTXALCDecay ();
    double getTXALCHang ();
    double getTXALCBot ();

    void setTXLevelerSt (bool setit);
    void setTXLevelerAttack (double attack);
    void setTXLevelerDecay (double decay);
    void setTXLevelerHang (double hang);
    void setTXLevelerTop (double maxgain);

    bool getTXLevelerSt ();
    double getTXLevelerAttack ();
    double getTXLevelerDecay ();
    double getTXLevelerHang ();
    double getTXLevelerTop ();

    // EQ parms
    void setRXEQ10(int *dbs);
    void setRXEQ(bool enable);
    bool getRXEQ();

    void setTXEQ10(int *dbs);
    void setTXEQ(bool enable);
    bool getTXEQ();

    void setRXDCBlock(bool blockit);
    bool getRXDCBlock();
    void setRXDCBlockGain(double val);
    double getRXDCBlockGain();

    bool getTXDCBlock();
    //void setTXDCBlockGain(double val);
    //double getTXDCBlockGain();

    // squelch RX
    void setSquelch(bool enable);
    bool getSquelch();
    void setSquelchVal(double val);
    double getSquelchVal();

    // "property" memories go to this file
    QString getPropFileName() const;
    void setPropFileName(const QString &value);

private:
    QString confFileName; // save the qt meta information to restore state upon restart
    QString dataFileName;
    QString propFileName; // store all DockWidget children's Q_PROPERTYs in this file

    QList <rsdkDockWidget*> dockList;

    qlonglong rsdkFreq;

    QWidget dummyTitleBarWidget;
    int m_restartThreads; // int as bool to take -1 error return
    bool m_minimized;
    bool m_recordHPSDRRaw;
    bool m_recordIQFloat;
    bool m_recordAudio;

    int m_scopeSnapMode; // facilitate tracking the snap mode in DisplayDock->oscsope and DttSP, keep it here for safe keeping

    QSignalMapper *dockActMap;

    // read and/or write the main window position and size to enable restore of last
    void writePos();
    void readPos();
    QCommandLineParser mainParser;

};

class QStringConvert : public QObject
{
    Q_OBJECT

public:
    explicit QStringConvert(QObject *parent = 0);
    ~QStringConvert(void) {};

    float convertToValues(QString inout);
    QString convertToUnits(float nvalue);
};

#endif // RSDK_H
