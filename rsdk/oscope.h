#ifndef OSCOPE_H
#define OSCOPE_H

#include <fftw3.h>

#include <QWidget>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QWidget>
#else
#include <QtGui/QWidget>
#endif

#include <QAbstractButton>
#include <QTimer>

#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_zoneitem.h>

#include <qwt_color_map.h>
#include <qwt_plot_legenditem.h>
#include <qwt_plot_rasteritem.h>
#include <qwt_plot_zoomer.h>
#include <qwt_scale_div.h>
#include <qwt_scale_engine.h>
#include <qwt_scale_map.h>

#include "data-worker.h"
#include "buffer.h"
#include "radioozy.h"

#include "color_selector.hpp"

class OScopeGrid;

// preparing for an experiment with scope samples for long display times
const uint32_t OSCOPE_SAMP_MULT = 1;
const uint32_t OSCOPE_NUM_SAMPS = DTTSP_SNAP_SAMPLES * OSCOPE_SAMP_MULT;

namespace Ui {
    class OScope;
}

class OScope : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(double m_smooth READ smooth WRITE setSmooth)
    Q_PROPERTY(QColor m_pkColor READ pkColor WRITE setPkColor)
    Q_PROPERTY(QColor m_gridColor READ gridColor WRITE setGridColor)
    Q_PROPERTY(QColor m_traceColor READ traceColor WRITE setTraceColor)
    Q_PROPERTY(bool m_yAutoScale READ yAutoScale WRITE setYAutoScale)
    Q_PROPERTY(QString m_snapMode READ getSnapMode() WRITE setSnapMode)

public:
    explicit OScope(QWidget *parent = nullptr);
    ~OScope();

    QColor pkColor() const;
    QColor gridColor() const;
    QColor traceColor() const;

    double *sig_buf;

    bool yAutoScale() const;
    bool showScale() const;

    QString getSnapMode() const;

private:
    double *pkHi;
    double *pkLo;
    double *avg;
    double *xes;
    double *yes;

    float *samps;

    bool oScopeEnabled() const;

    bool event (QEvent *event);

    double smooth() const;
    void clearY();
    bool peakEnabled() const;

public slots:
    void setOScopeEnabled(bool oScopeEnabled);
    void plotOScope();
    void setTraceColor(const QColor &traceColor);
    void setPkColor(const QColor &pkColor);
    void setGridColor(const QColor &gridColor);
    void setYAutoScale(bool yAutoScale);
    void setShowScale(bool showScale);
    void setPeakEnabled(bool enable);
    void setSmooth(double smooth);
    void setSmooth(int smooth);
    void setSnapMode(const QString &value);
    void snapBtnGrpKeyPress(QAbstractButton *b);

signals:
    void colorChanged(QColor color);
    void pkColorChanged(QColor color);
    void gridColorChanged(QColor color);
    void snapChanged(QString snapMode);

private slots:
    void makeBuffers(const int numSamps);
    void deleteBuffers();
    void resizeBuffers(const int numSamps);
    void clearBuffers();

private:
    Ui::OScope *ui;

    QThread collectThread;
    Worker3 *snapper;

    QwtPlotCurve oScopeCurve;
    QwtPlotCurve pkHiCurve;
    QwtPlotCurve pkLoCurve;
    OScopeGrid *grid;
    QwtPlotMarker cursX, cursY;
    QwtPlotZoneItem trigger;

    QColor m_traceColor;
    QColor m_gridColor;
    QColor m_pkColor;
    bool m_oScopeEnabled;
    bool m_pkEnabled;
    QTimer sampTimer;
    double m_yMin, m_yMax;
    bool avgInit;
    QwtLinearScaleEngine m_scale;
    double m_activeDragXPos;
    double m_smooth;
    bool m_yAutoScale;
    bool m_setYNeeded;
    bool m_showScale;
    QString m_snapMode;

    Color_Selector *gridCSelector;
    Color_Selector *peakCSelector;
};

#endif // OSCOPE_H
