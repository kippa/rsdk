#ifndef SGSMOOTH_H
#define SGSMOOTH_H

class sgSmooth
{
public:
    sgSmooth();

    // call to smooth data
    int setCoeffs(int width, int ld);
    int smooth(int np, int nl, int nr, float *in, float *out, int m, int ld);
    void pad(int np, int nl, int nr, float *fin, float *foout);
    void applyCoeffs(int np, int nl, int nr, float *fpad, float *g, float *cn);
    float m_cn[64];
    int m_numCoeffs;

private:
    float m_fpad[8192];
};

#endif // SGSMOOTH_H
