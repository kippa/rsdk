#ifndef RXPARMSDOCKWIDGET_H
#define RXPARMSDOCKWIDGET_H

#include <QList>
#include <QDockWidget>
#include "rsdkdockwidget.h"
#include "rangecontrol.h"

namespace Ui {
class RxParmsDockWidget;
}

class RxParmsDockWidget : public rsdkDockWidget
{
    Q_OBJECT

public:
    explicit RxParmsDockWidget(QWidget *parent = 0);
    ~RxParmsDockWidget();

public slots:
    void start();
    void setnb1(bool setit); // the noise blanker in DttSP
    void setnb2(bool setit); // the SDROM in DttSP

private:
    Ui::RxParmsDockWidget *ui;
    RangeControl *squelchRC;
    RangeControl *dcbRC;
    RangeControl *nb1RC;
    RangeControl *nb1HTRC;
    RangeControl *nb2RC;
    RangeControl *compander;
    QList <RangeControl *>rcList;
};

#endif // RXPARMSDOCKWIDGET_H
