#
# Project created by QtCreator 2013-06-16T21:58:51
#
#-------------------------------------------------
QT += core widgets
#QT += network core gui widgets opengl
#QT += network core gui widgets multimedia multimediawidgets

#CONFIG += static
CONFIG += qt thread ordered

#QT_CONFIG -= no-pkg-config
#QTPLUGIN += qsqlite

#QMAKE_CXXFLAGS_DEBUG += -g -O0 -ffast-math -ftree-vectorize -ftree-vectorizer-verbose=6
#QMAKE_LFLAGS_DEBUG += -g -O0 -ffast-math

QMAKE_CXXFLAGS_DEBUG += -g -O0
QMAKE_CXXFLAGS_RELEASE += -g -O2
#QMAKE_CXXFLAGS_DEBUG += -g -O0 -ffast-math
#QMAKE_CXXFLAGS_RELEASE += -g -O3 -ffast-math -ftree-vectorize -ftree-vectorizer-verbose=6

#QMAKE_CXXFLAGS_RELEASE += -g -O3 -ffast-math
#QMAKE_CXXFLAGS_DEBUG += -g -O0 -ffast-math
#QMAKE_CXXFLAGS_DEBUG += -O2
#QMAKE_CXXFLAGS_DEBUG += -std=c++11 -ffast-math -fno-align-functions -fno-align-loops
#QMAKE_CXXFLAGS += -std=c++11 -ffast-math -fno-align-functions -fno-align-loops
#QMAKE_CXXFLAGS += -std=c++11 -ffast-math -falign-functions=16 -falign-loops=16
#QMAKE_CXXFLAGS += -O2 -g -Wa,-a,-ad -fverbose-asm -save-temps
#QMAKE_CXXFLAGS += -pg
#QMAKE_LFLAGS += --static

message("************************")
message("QT ENV:")
message("QT_SYSROOT :" $$[QT_SYSROOT])
message("QT_VERSION :" $$[QT_VERSION])
message("QT_HOST_BINS :" $$[QT_HOST_BINS])
message("QT_HOST_PREFIX :" $$[QT_HOST_PREFIX])
message("QT_INSTALL_BINS :" $$[QT_INSTALL_BINS])
message("QT_INSTALL_PREFIX :" $$[QT_INSTALL_PREFIX])
message("QT_INSTALL_LIBS :" $$[QT_INSTALL_LIBS])
message("QT_INSTALL_PLUGINS :" $$[QT_INSTALL_PLUGINS])
message("QT_INSTALL_HEADERS :" $$[QT_INSTALL_HEADERS])

TARGET = rsdk
message("target :" $$TARGET)

TEMPLATE = app

SOURCES += main.cpp \
    ../../sqlite3/sqlite3.c \
    rsdk.cpp \
    buffer.c \
    banddockwidget.cpp \
    modedockwidget.cpp \
    filterdockwidget.cpp \
    dspdockwidget.cpp \
    vfodockwidget.cpp \
    displaydockwidget.cpp \
    smeterdockwidget.cpp \
    radiousb.cpp radiofile.cpp radioozy.cpp \
    exception.cpp \
    common.c \
    hpsdrdockwidget.cpp \
    hpsdr.cpp \
    alexdockwidget.cpp \
    agcdockwidget.cpp \
    audiodockwidget.cpp \
    scrollzoomer.cpp \
    scrollbar.cpp \
    plotdrawoptionsdockwidget.cpp \
    cwdockwidget.cpp \
    bandscope.cpp \
    txdockwidget.cpp \
    txcontroldockwidget.cpp \
    ../database/dbstore.cpp \
    mercuryselectdockwidget.cpp \
    oscope.cpp \
        data-worker.cpp \
    sgsmooth.cpp \
    agcparmsdockwidget.cpp \
    rsdkdockwidget.cpp \
    eq10dockwidget.cpp \
    rxparmsdockwidget.cpp \
    jack_rsdk.cpp \
    flxr.cpp \
    stringConvert.cpp \
    icom_serial.cpp

HEADERS  += rsdk.h \
    common.h \
    list.h \
    buffer.h \
    banddockwidget.h \
    modedockwidget.h \
    filterdockwidget.h \
    dspdockwidget.h \
    vfodockwidget.h \
    displaydockwidget.h \
    smeterdockwidget.h \
    radiousb.h radiofile.h radioozy.h \
    exception.h \
    hpsdrdockwidget.h \
    hpsdr.h \
    serializationcontext.h \
    alexdockwidget.h \
    agcdockwidget.h \
    audiodockwidget.h \
    scrollzoomer.h \
    scrollbar.h \
    plotdrawoptionsdockwidget.h \
    cwdockwidget.h \
    bandscope.h \
    txdockwidget.h \
    ../database/dbstore.h \
    txcontroldockwidget.h \
    ../ozy-fw/ozyfw-sdr1k.h \
    ../ozy-fw/Ozy_Janus_rbf.h \
    mercuryselectdockwidget.h \
    oscope.h \
    data-worker.h \
    sgsmooth.h \
    agcparmsdockwidget.h \
    rsdkdockwidget.h \
    eq10dockwidget.h \
    rxparmsdockwidget.h \
    jack_rsdk.h \
    flxr.h \
    stringConvert.h \
    icom_serial.h

FORMS    += rsdk.ui \
    banddockwidget.ui \
    modedockwidget.ui \
    filterdockwidget.ui \
    dspdockwidget.ui \
    vfodockwidget.ui \
    displaydockwidget.ui \
    smeterdockwidget.ui \
    hpsdrdockwidget.ui \
    alexdockwidget.ui \
    agcdockwidget.ui \
    audiodockwidget.ui \
    plotdrawoptionsdockwidget.ui \
    cwdockwidget.ui \
    bandscope.ui \
    txdockwidget.ui \
    txcontroldockwidget.ui \
    mercuryselectdockwidget.ui \
    oscope.ui \
    agcparmsdockwidget.ui \
    eq10dockwidget.ui \
    rxparmsdockwidget.ui

CONFIG += link_pkgconfig

# fftw3 must be installed!
PKGCONFIG += fftw3 fftw3f
LIBS += -lusb-1.0
#LIBS += -L/home/kippa/src/libusb-1.0.21/libusb -lusb-1.0

DEFINES += DO_JACK

#QWT built for Qt5.6 is not the one installed by opensuse zypper rpm
# careful w/ /usr/includes as we don't want to mix Qt versions
# now linux mint 17.3 has a different idea of things
# mint will load qwt 6.0, and we need 6.1
# so you have to do
# 1. Unload existing qwt packages
# 2. Download http://qwt.sourceforge.net/qwtinstall.html
# 3. modify qwtconfig.pro/pri to build the designer plugins
# 4. build and install the 6.1.2 version into /usr/local/qwt-6.1.2/
# 5. export QT_PLUGIN_PATH+="/usr/local/qwt-6.1.2/plugins"
#
# This project now uses this
#
QWT_LOCATION = /usr/local/qwt-6.1.3
INCLUDEPATH+=$${QWT_LOCATION}/include/
#INCLUDEPATH''=$${QWT_LOCATION}/include/
LIBS += -L$${QWT_LOCATION}/lib -lqwt

unix:!macx: LIBS += -L/home/kippa/src/yeppp-1.0.0/binaries/linux/x86_64 -lyeppp -ldl -ljack

INCLUDEPATH += . .. ../database/ ../ozy-fw/ ../../sqlite3
INCLUDEPATH += $$PWD/../../Qt-Color-Picker-master/include

# the color selection widgets from src/Qt-Color-Widgets/

# This line builds into the app itself
include ($$PWD/../../Qt-Color-Picker-master/color_widgets.pri)

# And this line uses a .so shared library instead
#LIBS += -lColorPicker-qt5
#INCLUDEPATH += $$[QT_INSTALL_HEADERS]/QtColorPicker
#message("Using QtColorPicker")

#LIBS += -lQtColorWidgets-Qt5
#INCLUDEPATH += $$[QT_INSTALL_HEADERS]/QtColorWidgets
#message("Using QtColorWidgets")

# the kippa rangecontrol slider thingy
include ($$PWD/../../Qt-Ranger/rangecontrol.pri)

# the kippa eqSlider
include ($$PWD/../../Qt-EqSlider/EqSlider.pri)

# the flxmlrpc remote control stuff
include(../xmlrpc/xmlrpc.pri)
INCLUDEPATH += ../xmlrpc

# the apex_memmove is considerably faster than the c libs
include(../apex_memmove/apex_memmove.pri)
INCLUDEPATH += ../apex_memmove

# after make, run an extra make install to copy this database.file to database.path in the build dir
# don't forget to add and extra step in the project make to do 'make install'
database.path = $$OUT_PWD
database.files += $$PWD/../database/rsdk.db
INSTALLS += database

qss.path = $$OUT_PWD
qss.files = $$PWD/qss/rsdk.qss
INSTALLS += qss

RESOURCES += \
    rsdk.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

# DttSP library
DTTSP_LOCATION = $$PWD/../../DttSP/DttSP
INCLUDEPATH += $${DTTSP_LOCATION} # pick up dttsp.h, it't not currently copied to builddir
DEPENDPATH += $${DTTSP_LOCATION}
unix:!macx: PRE_TARGETDEPS += $${DTTSP_LOCATION}
unix:!macx: LIBS += -L$${DTTSP_LOCATION} -lDttSP

message("INCLUDE_PATH :" $$INCLUDEPATH)
