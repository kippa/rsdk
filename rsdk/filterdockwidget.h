#ifndef FILTERDOCKWIDGET_H
#define FILTERDOCKWIDGET_H

#include <QDockWidget>
#include <QAbstractButton>
#include <QTimer>

#include <dttsp.h>

#include "rsdkdockwidget.h"

#include "modedockwidget.h"
#include "stringConvert.h"

namespace Ui {
    class FilterDockWidget;
}

class FilterDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(double _low READ getLow WRITE setLow)
    Q_PROPERTY(double _hi READ getHi WRITE setHi)

public:
    explicit FilterDockWidget(QWidget *parent = 0);
    ~FilterDockWidget();

public slots:
    void start();

    double getLow() const;
    void setLow(double low);

    double getHi() const;
    void setHi(double hi);

    void setFilterBank(SDRMODE_DTTSP mode);
    QString getCurFilterBtnTxt();
    void setCenterFreq(const qlonglong f);
    void setCenterFreq(const double f);

private slots:
    void filterBtnGrpClick(QAbstractButton *btn);
    void hiSpinBoxChanged(QString hi);
    void loSpinBoxChanged(QString hi);
    void setFilterBtn();
    void updateHandler(); // overload from the base rsdkdockwidet class

signals:
    void filterChanged(double, double);
    void filterLowChanged(double);
    void filterHiChanged(double);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private:
    Ui::FilterDockWidget *ui;
    //void setForMode(const QString &val, const SDRMODE_DTTSP mode);

    stringConvert scv;
    std::string prevBW;
    bool started;
};

#endif // FILTERDOCKWIDGET_H
