/* flxr is an interface between the flxmlrpc library and rozy (RadioOzy). This
 * code does not know about rsdk or above and should work stand alone
 * with the rozy and flxmlrpc to provide a flxmlrpc server that fldigi
 * may connect to for control.
 */

#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <string>
#include <assert.h>

#include "XmlRpc.h"

// testing freq grab from rsdk to get page-tune freq
// so fldigi operates on the correct frequency
// whilst rsdk is in page-tune mode
#include "rsdk.h"
extern rsdk *r;
//

#include "flxr.h"

using namespace std;
using namespace XmlRpc;

XmlRpcServer rig_server;

extern const char *filtersByMode[][10];

// TODO FIXME should be user assignable
#define XML_SERVER_PORT 11214


/*************************************************************************/
/**************************** S-METER ************************************/
/*************************************************************************/
class rig_get_SMeter : public XmlRpcServerMethod {
public:
    rig_get_SMeter(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_smeter", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params; // shut up the compiler for unused variables
        // this is calling out directly to DttSP
        // and then adding a magic offset value
        double val = CalculateRXMeter(r->rozy->curMercury, 0, SIGNAL_STRENGTH) + 120;
        result = (int)round(val);
    }
    std::string help() { return std::string("get SMeter"); }
}rig_get_SMeter(&rig_server);

/*************************************************************************/
/*************************** SIDEBAND ************************************/
/*************************************************************************/

class rig_get_SB : public XmlRpcServerMethod {
public:
    rig_get_SB(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_sideband", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params; // shut up the compiler for unused variables
        string sb = r->rozy->getModeStr(r->rozy->getMode());
        if (sb.find("L") != std::string::npos) {
            result = "L";
        } else {
            result = "U";
        }
    }
    std::string help() { return std::string("get active SB frequency"); }
}rig_get_SB(&rig_server);

class rig_set_SB : public XmlRpcServerMethod {
public:
    rig_set_SB(XmlRpcServer *s) : XmlRpcServerMethod("rig.set_vfo", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)result;
        //unsigned long long f = static_cast<unsigned long long>(double(params[0]));
        //r->rozy->setFreq(f);
    }
    std::string help() { return std::string("set active SB frequency"); }
}rig_set_SB(&rig_server);

/*************************************************************************/
/***************************** VFO ***************************************/
/*************************************************************************/
// TODO FIXME should be user assignable
// TODO FIXME frequency from rsdk for page-tune mode

class rig_get_VFO : public XmlRpcServerMethod {
public:
    rig_get_VFO(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_vfo", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params; // shut up the compiler for unused variables
        // TODO FIXME this is a temporary test, see comments above
        unsigned long long f = r->getRsdkFreq();
        //unsigned long long f = (unsigned long long)r->rozy->getRxFreq(r->rozy->curRx);
        static char freq[32];
        snprintf(freq, sizeof(freq), "%lld", f);
        string rs(freq);
        result = rs;
    }
    std::string help() { return std::string("get active VFO frequency"); }
}rig_get_VFO(&rig_server);

class rig_set_VFO : public XmlRpcServerMethod {
public:
    rig_set_VFO(XmlRpcServer *s) : XmlRpcServerMethod("rig.set_vfo", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)result;
        // TODO FIXME this is a temporary test, see comments above
        r->setRsdkFreq(double(params[0]));
        //unsigned long long f = static_cast<unsigned long long>(double(params[0]));
        //r->rozy->setFreq(f);
    }
    std::string help() { return std::string("set active VFO frequency"); }
}rig_set_VFO(&rig_server);

/*************************************************************************/
/***************************** PTT ***************************************/
/*************************************************************************/
class rig_get_PTT : public XmlRpcServerMethod {
public:
    rig_get_PTT(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_ptt", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params; // shut up the compiler for unused variables
        bool xmit = r->rozy->getXmit();
        result = xmit ? 1 : 0;
    }
    std::string help() { return std::string("get vfo 0=off (RCV) 1=on (XMIT)"); }
}rig_get_ptt(&rig_server);

// vfo 1 = ON (XMIT) 0 = OFF (RCV)
class rig_set_PTT : public XmlRpcServerMethod {
public:
    rig_set_PTT(XmlRpcServer *s) : XmlRpcServerMethod("rig.set_ptt", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)result;
        int xmit = int(params[0]);
        r->rozy->setGuiXmit(xmit ? true : false);
    }
    std::string help() { return std::string("set PTT 0=off, !0=on"); }
}rig_set_PTT(&rig_server);


/*************************************************************************/
/***************************** BANDWIDTH *********************************/
/*************************************************************************/
class rig_set_BW : public XmlRpcServerMethod {
public:
    rig_set_BW(XmlRpcServer *s) : XmlRpcServerMethod("rig.set_bw", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)result;
        int i = int(params[0]);
        SDRMODE_DTTSP m = r->rozy->getMode();
        std::string v(filtersByMode[m][i]);
        r->rozy->setBW(v, 0);
    }
    std::string help() { return std::string("set bandwidth"); }
}rig_set_BW(&rig_server);

// return the current BW
class rig_get_BW : public XmlRpcServerMethod {
public:
    rig_get_BW(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_bw", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params; // shut up the compiler for unused variables
        string curBW = r->rozy->getCurBW();
        result[0] = curBW;
        /*
        SDRMODE_DTTSP m = r->rozy->getMode();
        int fsize = sizeof(filtersByMode[m]) / sizeof(filtersByMode[m][0]);
        int i=0;
        for (; i<fsize; i++)
            if (strstr(filtersByMode[m][i], curBW.c_str()))
               break;
        if (i<fsize)
            result[0] = i;
        else
            result[0] = 0;
        result[1] = 0;
        */
    }
    std::string help() { return std::string("get BW"); }
}rig_get_BW(&rig_server);

// return all available bandwidths (for the running mode)
class rig_getBWs : public XmlRpcServerMethod {
public:
    rig_getBWs(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_bws", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params;
        XmlRpcValue bws;
        bws[0][0] = "Bandwidth";
        int m = r->rozy->getMode();
        for (int i=0; i < 10; i++) {
            bws[0][i+1] = filtersByMode[m][i];
        }
        result = bws;
    }
    std::string help() { return std::string("get list of available bandwidths"); }
}rig_getBWs(&rig_server);


/*************************************************************************/
/******************************* MODE ************************************/
/*************************************************************************/
// rig set mode
class rig_set_mode : public XmlRpcServerMethod {
public:
    rig_set_mode(XmlRpcServer *s) : XmlRpcServerMethod("rig.set_mode", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)result;
        string nmode = string(params[0]);
        SDRMODE_DTTSP m = r->rozy->getModeType(nmode);
        r->rozy->setMode(m, 0);
    }
    std::string help() { return std::string("set mode"); }
}rig_set_mode(&rig_server);

// return the current mode
class rig_getMode : public XmlRpcServerMethod {
public:
    rig_getMode(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_mode", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params; // shut up the compiler for unused variables
        string rs = r->rozy->getModeStr(r->rozy->getMode());
        result = rs;
    }
    std::string help() { return std::string("get mode"); }
}rig_getMode(&rig_server);

// return all available modes
class rig_getModes : public XmlRpcServerMethod {
public:
    rig_getModes(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_modes", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params;
        XmlRpcValue modes;
            int numModes = r->rozy->getNumModes();
            for (int i=0; i<numModes; i++) {
                modes[i] = r->rozy->getModeStr((SDRMODE_DTTSP)i);
                result = modes;
            }
    }
    std::string help() { return std::string("get list of available modulation modes"); }
}rig_getModes(&rig_server);


/*************************************************************************/
/******************************* XCVR ************************************/
/*************************************************************************/
class rig_getXcvr : public XmlRpcServerMethod {
public:
    rig_getXcvr(XmlRpcServer *s) : XmlRpcServerMethod("rig.get_xcvr", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params;
        string v = " rsdk "; // TODO FIXME use argv[0]
        result = v;
    }
    std::string help() { return std::string("get xcvr string"); }
}rig_getXcvr(&rig_server);


/*************************************************************************/
/************************** RSDK METHODS *********************************/
/*************************************************************************/
static const struct MLIST {
    string name; string signature; string help;
} mlist[] = {
    { "rig.get_AB",       "s:n", "returns vfo in use A or B" },
    { "rig.get_bw",       "s:n", "return BW of current VFO" },
    { "rig.get_bws",      "s:n", "return table of BW values" },
    { "rig.get_info",     "s:n", "return an info string" },
    { "rig.get_mode",     "s:n", "return MODE of current VFO" },
    { "rig.get_modes",    "s:n", "return table of MODE values" },
    { "rig.get_sideband", "s:n", "return sideband (U/L)" },
    { "rig.get_notch",    "s:n", "return notch value" },
    { "rig.get_ptt",      "s:n", "return PTT state" },
    { "rig.get_pwrmeter", "s:n", "return PWR out" },
    { "rig.get_smeter",   "s:n", "return Smeter" },
    { "rig.get_update",   "s:n", "return update to info" },
    { "rig.get_vfo",      "s:n", "return current VFO in Hz" },
    { "rig.get_xcvr",     "s:n", "returns name of transceiver" },
    { "rig.set_AB",       "s:s", "set VFO A/B" },
    { "rig.set_bw",       "i:i", "set BW iaw BW table" },
    { "rig.set_BW",       "i:i", "set L/U pair" },
    { "rig.set_mode",     "i:i", "set MODE iaw MODE table" },
    { "rig.set_notch",    "d:d", "set NOTCH value in Hz" },
    { "rig.set_ptt",      "i:i", "set PTT 1/0 (on/off)" },
    { "rig.set_vfo",      "d:d", "set current VFO in Hz" },
    { "main.set_frequency",      "d:d", "set current VFO in Hz" }
};

class system_listMethods : public XmlRpcServerMethod {
public:
    system_listMethods(XmlRpcServer *s) : XmlRpcServerMethod("system.listMethods", s) {}
    void execute(XmlRpcValue& params, XmlRpcValue& result) {
        (void)params;
        vector<XmlRpcValue> methods;
        size_t x = sizeof(mlist) / sizeof(*mlist);
        for (size_t n = 0; n < x; ++n) {
            XmlRpcValue::ValueStruct item;
            item["name"]      = mlist[n].name;
            item["signature"] = mlist[n].signature;
            item["help"]      = mlist[n].help;
            methods.push_back(item);
        }
        result = methods;
    }
    std::string help() { return std::string("get r->rozy->methods"); }
}system_listMethods(&rig_server);

/*************************************************************************/
/*************************** CLIENT LOOP *********************************/
/*************************************************************************/
// the client command loop
void *xml_cli_loop(void *arg)
{
    (void)arg;
    //flxr *f = (flxr*)arg;
    while(true) {
        rig_server.work(-1.0);
    }
    // never reached
    return NULL;
}

/*************************************************************************/
/****************************** FLXR *************************************/
/*************************************************************************/
flxr::flxr()
{
    // set up the server and get it running
    start_server(XML_SERVER_PORT);
}

flxr::~flxr()
{
    stop_server();
}

int flxr::start_server(int port)
{
    XmlRpc::setVerbosity(1);
    int rc = -1;
    if(rig_server.bindAndListen(port))
    {
        rig_server.enableIntrospection(true);

        // create a thread to listen for client connections and commands
        if ((rc = pthread_create(&xml_thread, NULL, &xml_cli_loop, this))!=0)
        {
            // TODO FIXME
            fprintf(stderr,"%s: flxr pthread_create failed: rc=%d\n", __func__, rc);
        }
        pthread_setname_np(xml_thread, "rsdk xml");
    }

    return rc;
}

void flxr::stop_server()
{
    rig_server.exit();
    rig_server.shutdown();
}
