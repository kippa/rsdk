/******************************************************************************
 * Project: hpsdr
 *
 * File: buffer.h 
 *
 * Description: implement a simple buffer pool manager to avoid allocs/deallocs
 *				during operations. Alos used to minimize blocking IO time for USB
 *
 * Date: Mon 25 Feb 2013
 *
 * $Revision:$
 *
 * $Header:$
 *
 * Copyright (C) 2013 Kipp A. Aldrich <kippaldirch@gmail.com>
 ******************************************************************************
 *
 * $Log:$
 *
 ******************************************************************************
 */

#ifndef _OZYBUFIO_H
#define _OZYBUFIO_H

#include </usr/include/libusb-1.0/libusb.h>
// stole the kernel's linked lists
#include "list.h"
#include <stddef.h> // size_t
#include <pthread.h>

#define OZY_BUFFER_SIZE 512

// maintain c linkage for libusb
#ifdef __cplusplus
extern "C" {
#endif

struct usbBuf
{
	struct list_head list; // stolen kernel linked list for elegance and familitarity
    unsigned char *buf;
    int id;
    int ep; // usb end point
	size_t size;
	int busy; // in flight
    struct libusb_transfer *xfr;
    void *ud; // maybe an object?
};
typedef  struct usbBuf usbBuf_t;

struct usbBufPool
{
	pthread_mutex_t lock;
    struct list_head in_flight;
    usbBuf_t *ob;
    const char *name;
    int ep;
};
typedef struct usbBufPool usbBufPool_t;

extern void  usb_destroy_buffer_pool(usbBufPool_t *list);
extern usbBufPool_t *usb_create_buffer_pool(const int num_bufs, size_t size, const char *name, const unsigned int ep);
extern int usb_init_buffer_pools(void);
extern usbBuf_t *usb_create_buffer(size_t size, const unsigned int ep);
extern usbBuf_t *usb_get_buffer(usbBufPool_t *list, void *user_data);
extern void usb_put_buffer(usbBufPool_t *list, usbBuf_t *entry);
extern int usb_clear_in_flight(usbBufPool_t *list); // remove all entries in a given in_flight list, intended to remove all in-flight txBufs
extern int usb_count_buffers(usbBufPool_t *list);
extern int usb_buffer_take_lock(usbBufPool_t *list); // allow external callers to block usb buffer access
extern int usb_buffer_release_lock(usbBufPool_t *list); // allow external callers to unblock usb buffer access

#ifdef __cplusplus
}
#endif

#endif /* _OZYBUFIO_H */

