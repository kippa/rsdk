#include <string>
#ifndef STRINGCONVERT_H
#define STRINGCONVERT_H

// convert std::string like "2.4K" to actual value 2400
// or convert actual value like 5000 to "5.0K"
class stringConvert
{
public:
    explicit stringConvert() {};
    ~stringConvert(void) {};

    double convertToValues(const std::string &input);
    std::string convertToUnits(double nvalue);

    // no copy, no assign
    stringConvert(const stringConvert &NoCopy);
    stringConvert &operator=(const stringConvert &NoAssign);
};

#endif // STRINGCONVERT_H


