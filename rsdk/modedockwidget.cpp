#include <QTimer>
#include <stdio.h>

#include "rsdk.h"
#include "modedockwidget.h"
#include "ui_modedockwidget.h"

ModeDockWidget::ModeDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::ModeDockWidget),
    m_mode(USB),
    m_autoMode(true)
{
    ui->setupUi(this);
    connect(ui->ModeButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(modeKeyPress(QAbstractButton*)));
}

ModeDockWidget::~ModeDockWidget()
{
    delete ui;
}

void ModeDockWidget::start()
{
    setAutoMode(m_autoMode);

    // and start by syncing to the theRsdk->rozy
    setMode(theRsdk->rozy->getMode());
}

void ModeDockWidget::updateMode()
{
    // get theRsdk->rozy's idea of mode and make it so
    int mode = theRsdk->rozy->getMode();
    int curMode = whichMode();
    if (curMode != mode)
        setMode(mode, true);
}

int ModeDockWidget::getMode() const
{
    return m_mode;
}

int ModeDockWidget::getModeType(QString mode)
{
    return theRsdk->rozy->getModeType(mode.toUtf8().constData());
}

QString ModeDockWidget::getModeStr() const
{
    return QString::fromStdString(theRsdk->rozy->getModeStr(theRsdk->rozy->getMode()));
}

int ModeDockWidget::whichMode()
{
    int m = (int)USB; // default is USB
        if(ui->ModeLSB->isChecked()) {
            m = (int)LSB;
        } else if(ui->ModeUSB->isChecked()) {
            m = (int)USB;
        } else if(ui->ModeDSB->isChecked()) {
            m = (int)DSB;
        } else if(ui->ModeCW->isChecked()) {
            m = (int)CWL;
        } else if(ui->ModeFMN->isChecked()) {
            m = (int)FMN;
        } else if(ui->ModeAM->isChecked()) {
            m = (int)AM;
        } else if(ui->ModeDIGU->isChecked()) {
            m = (int)DIGU;
        } else if(ui->ModeDIGL->isChecked()) {
            m = (int)DIGL;
        } else if(ui->ModeSPC->isChecked()) {
            m = (int)SPEC;
        } else if(ui->ModeSAM->isChecked()) {
            m = (int)SAM;
        } else if(ui->ModeDRM->isChecked()) {
            m = (int)DRM;
        }
        return m;
}

void ModeDockWidget::setMode(int m, bool sendSignal)
{
    (void)sendSignal; // not used here right now
    m_mode = m;
    if(m_mode == LSB)
        ui->ModeLSB->setChecked(true);
    else if(m_mode == USB)
        ui->ModeUSB->setChecked(true);
    else if(m_mode == DSB)
        ui->ModeDSB->setChecked(true);
    else if(m_mode == CWL)
        ui->ModeCW->setChecked(true);
    else if(m_mode == CWU)
        ui->ModeCW->setChecked(true);
    else if(m_mode == FMN)
        ui->ModeFMN->setChecked(true);
    else if(m_mode == AM)
        ui->ModeAM->setChecked(true);
    else if(m_mode == DIGU)
        ui->ModeDIGU->setChecked(true);
    else if(m_mode == DIGL)
        ui->ModeDIGL->setChecked(true);
    else if(m_mode == SPEC)
        ui->ModeSPC->setChecked(true);
    else if(m_mode == SAM)
        ui->ModeSAM->setChecked(true);
    else if(m_mode == DRM)
        ui->ModeDRM->setChecked(true);

    if (sendSignal)
    {
        theRsdk->setRsdkMode((SDRMODE_DTTSP)m_mode);
        //emit modeChanged((SDRMODE_DTTSP)m);
    }
}

// by QString
void ModeDockWidget::setMode(QString mode, bool sendSignal)
{
    setMode(theRsdk->rozy->getModeType(mode.toUtf8().constData()), sendSignal);
}

// by const char *
void ModeDockWidget::setMode(const char *m, bool sendSignal)
{
    std::string s(m);
    setMode(theRsdk->rozy->getModeType(s), sendSignal);
}

void ModeDockWidget::setMode(int m)
{
    setMode(m, true);
}

//  button press comes here
void ModeDockWidget::modeKeyPress(QAbstractButton *btn)
{
    QString btnName=btn->text();
    setMode(btnName, true);
}

void ModeDockWidget::on_ModeAuto_toggled(bool checked)
{
    setAutoMode(checked);
}

bool ModeDockWidget::getAutoMode() const
{
    return m_autoMode;
}

void ModeDockWidget::setAutoMode(bool autoMode)
{
    m_autoMode = autoMode;
    bool pstate = ui->ModeAuto->blockSignals(true);
    ui->ModeAuto->setChecked(autoMode);
    ui->ModeAuto->blockSignals(pstate);
}
