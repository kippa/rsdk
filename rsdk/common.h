#ifndef COMMON_H
#define COMMON_H

// careful
#include <string>
#include <stdint.h>
#include <dttsp.h>

// some defines for pipes used for IPC and signal data
#define RXAUDOUTNAME "/tmp/RSDK_AUD_TO_SYS_AUD_FIFO"

// some typedefs
typedef long long int64;

#define NUM_ELEMS(d) (sizeof(d)/sizeof(d[0]))

// banddockwidget shares with the database bands table via this struct in a QList
struct band {
    int count; // track the number of same-band entries, = 0 - N entries in band list, allows cycling through all same-band band segments
    double start; // retain fractional frequencies
    double end;
    double lastFreq; // remember this band's last frequency
    std::string desc;
    std::string prefMode; // this is DttSP mode, like CWU, DIGU, USB, LSB, AM, etc.
    std::string hamClass;
    std::string bandType;
    std::string notes; // cursor hover information
    std::string color; // standard color names
    std::string subMode; // mostly for digital submode (DIGU) zones, like RTTY
};


#ifdef __cplusplus
extern "C" {
#endif
    extern void my_usleep(uint32_t usec);
    extern long long timeval_diff(struct timeval *start_time, struct timeval *end_time);
    extern void dump_buffer(unsigned char * b, unsigned int size);
    extern void printHex(const uint8_t *buf, int len);
    extern int toBCD(const uint64_t v, void *buf, unsigned int len);
    extern int fromBCD(const uint8_t *buf, int len);
    extern double convertToValues(const char *input);

#ifdef __cplusplus
}
#endif

#endif // COMMON_H
