#ifndef EXCEPTION_H
#define EXCEPTION_H
#include <stdarg.h> //variable argument va_ utilities
#include <string.h>
#include <stdio.h>


#include <iostream>
#include <exception>
using namespace std;

#define CRITICAL_LEVEL	0
#define ERROR_LEVEL 	1
#define WARNING_LEVEL 	2
#define DEBUG_LEVEL 	3
#define TRACE_LEVEL 	4

#define MAXEXCBUFSIZE 2048
class rsdkException : public exception
{
public:
    int RetCode;
    virtual const char* what() const throw() {return ErrStr;};
    virtual const char* what_line() const throw() {return LineModule;};
    virtual int what_return_code() const throw() {return RetCode;};
    rsdkException(int Level_prm, int ErrCode_prm, const char *FileName_prm, int LineNumber_prm, const char *FunctionName_prm,const char *Format_prm,...) throw();
    virtual ~rsdkException() throw() {} ; //don't throw from the destructor!
private:
    char LineModule[MAXEXCBUFSIZE];
    char ErrStr[MAXEXCBUFSIZE];
    //	rsdkException(const rsdkException &NoCopy); //This is probably because you are throwing the rsdkException rather than than rsdkException&
    rsdkException &operator=(const rsdkException &NoAssign);
};

static inline void THROW_EXCEPTION(int Level_prm,int ErrorCode_prm,const char *Format_prm,...)
{
    char buf[MAXEXCBUFSIZE];
    va_list valist;
    va_start(valist,Format_prm);
    vsnprintf(buf,MAXEXCBUFSIZE-2,Format_prm,valist);
    va_end(valist);
    throw rsdkException(Level_prm,ErrorCode_prm,__FILE__, __LINE__, __FUNCTION__, buf);
}

#endif // EXCEPTION_H
