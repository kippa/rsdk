#include <assert.h>

#include "rangecontrol.h"
#include "agcparmsdockwidget.h"
#include "ui_agcparmsdockwidget.h"

#include "agcdockwidget.h" // friend?, used so we can know the enum and resuse and existing signal with that parm, which we don't need

#include "common.h" // DttSP interface
#include "rsdk.h"

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

AgcParmsDockWidget::AgcParmsDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::AgcParmsDockWidget)
{
    ui->setupUi(this);

    ui->gridLayout->setSpacing(0);
    ui->gridLayout->setContentsMargins(1, 1, 1, 1);

    QString name = "Attack";
    attackRC = new RangeControl(ui->dockWidgetContents, -10.0, 10.0, 2.0, name, true); // in msec?
    ui->gridLayout->addWidget(attackRC, 0, 0, 1, 1);

    name = "Decay ";
    decayRC = new RangeControl(ui->dockWidgetContents, 0.0, 2500.0, 500.0, name, true);
    ui->gridLayout->addWidget(decayRC, 0, 1, 1, 1);

    name = " Hang ";
    hangRC = new RangeControl(ui->dockWidgetContents, -3.0, 3.0, 0.75, name, true);
    ui->gridLayout->addWidget(hangRC, 0, 2, 1, 1);

    /*
    name = "Thresh";
    hangThresholdRC = new RangeControl(ui->dockWidgetContents, -12.0, 12.0, 5.0, name, true);
    ui->gridLayout->addWidget(hangThresholdRC, 0, 3, 1, 1);
    */

    name = "Slope ";
    slopeRC = new RangeControl(ui->dockWidgetContents, -12.0, 12.0, 5.0, name, true);
    ui->gridLayout->addWidget(slopeRC, 0, 4, 1, 1);

}

AgcParmsDockWidget::~AgcParmsDockWidget()
{
    delete ui;
}

void AgcParmsDockWidget::start()
{
    //if (r) {
        connect (attackRC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRsdkAgcAttack(double)));
        connect (decayRC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRsdkAgcDecay(double)));
        connect (hangRC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRsdkAgcHang(double)));
        connect (slopeRC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRsdkAgcSlope(double)));
        //connect (hangThresholdRC, SIGNAL(rangeChanged(double)), r, SLOT(setRsdkAgcHangThreshold(double)));
    //}

    //emit agcChanged(m_mode);
    //emit dockChanged(this, Qt::NoDockWidgetArea);
}

void AgcParmsDockWidget::updateValues()
{
    // this widget just reflects the state of the DttSPAGC
    // keep the sliders/values up to date with possible changes
    // the Attack time
    double val = (double) GetRXAGCAttack(theRsdk->rozy->curMercury, 0);
    attackRC->setM_Value(val);

    // the decay time
    val = (double) GetRXAGCDecay(theRsdk->rozy->curMercury, 0);
    decayRC->setM_Value(val);

    // the hang time
    val = (double) GetRXAGCHang(theRsdk->rozy->curMercury, 0);
    hangRC->setM_Value(val);

    // the hangthreshold
    /*
    val = (double) GetRXAGCHangThreshold(theRsdk->rozy->curMercury, 0);
    hangThresholdRC->setM_Value(val);
    */

    // the agc slope
    val = (double) GetRXAGCSlope(theRsdk->rozy->curMercury, 0);
    slopeRC->setM_Value(val);
}
