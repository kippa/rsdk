#include <QEvent>
#include <QPainter>
#include <QScrollBar>
#include <QMouseEvent>

#include "common.h" // string conversion routines for kilo, mega, etc.
#include "rsdk.h"

#include "mercuryselectdockwidget.h"
#include "ui_mercuryselectdockwidget.h"

LocalLabel::LocalLabel(QWidget *parent) : QLabel(parent),
    m_curP(QPoint(0, 0))
{
}

void LocalLabel::mousePressEvent(QMouseEvent *e)
{
    doSignals(e);
}

void LocalLabel::mouseMoveEvent(QMouseEvent *e)
{
    doSignals(e);
}

void LocalLabel::mouseReleaseEvent(QMouseEvent *e)
{
    doSignals(e);
}

void LocalLabel::doSignals(QMouseEvent *e)
{
    // clip to visible pixmap
    int lx = e->localPos().x();
    int ly = e->localPos().y();
    int fw = frameWidth()*4;
    int px = pixmap()->width();
    int py = pixmap()->height();
    int w = this->width() - fw;
    int h = this->height() - fw;

    int x = lx > px ? px : lx < 0 ? 0 : lx;
    int y = ly > py ? py : ly < 0 ? 0 : ly;

    double dx = (double)x / (double)px;
    double dy = (double)y / (double)py;
    m_curP.setX(dx);
    m_curP.setY(dy);

    makePixmap(w, h);

    double i = ((m_curP.x() * w) - (w/2)) / (w/2);
    double q = ((m_curP.y() * h) - (h/2)) / (h/2);
    q = q > 1.0 ? 1.0 : q < -1.0 ? -1.0 : q;
    i = i > 1.0 ? 1.0 : i < -1.0 ? -1.0 : i;

    emit sendNormI(i);
    emit sendNormQ(q);
}

void LocalLabel::resizeEvent(QResizeEvent *e)
{
    // both sides, plus 2 extra to allow sizing down; if filled completely qlabel won't resize smaller
    int fw = frameWidth()*4;
    int w = e->size().width() - fw;
    int h = e->size().height() - fw;

    makePixmap(w, h);

    QLabel::resizeEvent(e);
}

void LocalLabel::makeArcs(int w, int h)
{
    // redraw a center cross, don't accumulate points, QOBJECT will handle "lost" items
    m_grat = QPainterPath();
    m_grat.moveTo(w/2, h);
    m_grat.lineTo(w/2, 0.0);
    m_grat.moveTo(0.0, h/2);
    m_grat.lineTo(w, h/2);

    // draw the curP current point as a small white cross
    m_cross = QPainterPath();
    int x = w * m_curP.x();
    int y = h * m_curP.y();

    m_cross.moveTo(x, y-6);
    m_cross.lineTo(x, y+6);
    m_cross.moveTo(x-6, y);
    m_cross.lineTo(x+6, y);
}

void LocalLabel::makePixmap(int w, int h)
{
    QPixmap pmap(w, h);
    pmap.fill(QColor("black"));

    makeArcs(w, h);

    QPainter painter(&pmap);
    //painter.setRenderHint(QPainter::Antialiasing, true);
    //painter.setRenderHint(QPainter::TextAntialiasing, true);
    //painter.setRenderHint(QPainter::SmoothPixmapTransform, true);

    QPen pen;
    pen.setWidth(1);

    pen.setColor(QColor("red"));
    painter.setPen(pen);
    painter.drawPath(m_grat);

    pen.setColor(QColor("white"));
    painter.setPen(pen);
    painter.drawPath(m_cross);

    // old pixmap is QOBJECT handled, don't free previous pmap
    setPixmap(pmap);
}

MercurySelectDockWidget::MercurySelectDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::MercurySelectDockWidget),
    m_mode(MERC1)
{
    ui->setupUi(this);

    connect (ui->mercury1ToolButton, SIGNAL(toggled(bool)), this, SLOT(m1Toggled(bool)));
    connect (ui->mercury2ToolButton, SIGNAL(toggled(bool)), this, SLOT(m2Toggled(bool)));
    connect (ui->mercuryBothToolButton, SIGNAL(toggled(bool)), this, SLOT(mBothToggled(bool)));

    connect (ui->iqLabel, SIGNAL(sendNormI(double)), this, SLOT(doNormI(double)));
    connect (ui->iqLabel, SIGNAL(sendNormQ(double)), this, SLOT(doNormQ(double)));

    //installEventFilter(this);
    setMode(MERC1);
}

MercurySelectDockWidget::~MercurySelectDockWidget()
{
    delete ui;
}

MercurySelectDockWidget::MERCMODE MercurySelectDockWidget::mode() const
{
    return m_mode;
}

void MercurySelectDockWidget::setMode(const MERCMODE &mode)
{
    m_mode = mode;
    emit mercSelChanged(m_mode);
}

void MercurySelectDockWidget::start()
{
}

void MercurySelectDockWidget::m1Toggled(bool)
{
    setMode(MERC1);
}

void MercurySelectDockWidget::m2Toggled(bool)
{
    setMode(MERC2);
}

void MercurySelectDockWidget::mBothToggled(bool)
{
    setMode(MERCBOTH);
}

void MercurySelectDockWidget::doNormI(double value)
{
    emit sendNormI(value);
}

void MercurySelectDockWidget::doNormQ(double value)
{
    emit sendNormQ(value);
}

void MercurySelectDockWidget::on_gainScrollBar_valueChanged(int value)
{
   // slider range os -100 -> +100, convert to -1.0 -> +1.0
    float fval = (float)value / 100.0f;
    emit sendGain(fval);
}
