#ifndef PLOTDRAWOPTIONSDOCKWIDGET_H
#define PLOTDRAWOPTIONSDOCKWIDGET_H

#include <QDockWidget>

#include <dttsp.h>

//#include "serializationcontext.h"
#include "rsdkdockwidget.h"

namespace Ui {
class PlotDrawOptionsDockWidget;
}

class PlotDrawOptionsDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(bool m_phaseOn READ getPhaseOn WRITE setPhaseOn)
    Q_PROPERTY(bool m_spectrumOn READ getSpectrumOn WRITE setSpectrumOn)
    Q_PROPERTY(bool m_waterfallOn READ getWaterfallOn WRITE setWaterfallOn)
    Q_PROPERTY(bool m_scopeOn READ getScopeOn WRITE setScopeOn)
    Q_PROPERTY(bool m_yMaxAutoScale READ getYMaxAutoScale WRITE on_yMaxAutoScaleTB_toggled)
    Q_PROPERTY(bool m_yMinAutoScale READ getYMinAutoScale WRITE on_yMinAutoScaleTB_toggled)
    Q_PROPERTY(int m_yMin READ yMin WRITE setYMin)
    Q_PROPERTY(int m_yMax READ yMax WRITE setYMax)
    Q_PROPERTY(int m_numCoeffs READ getNumCoeffs WRITE setNumCoeffs)
    Q_PROPERTY(double m_snapRate READ getDrawRate WRITE setDrawRate);
    Q_PROPERTY(bool m_sGridEnabled READ getSGridEnabled WRITE setSGridEnabled)
    Q_PROPERTY(bool m_dbGridEnabled READ getDbGridEnabled WRITE setDbGridEnabled)
    Q_PROPERTY(bool m_fGridEnabled READ getFreqGridEnabled WRITE setFreqGridEnabled)
    Q_PROPERTY(bool m_titleEnabled READ getTitleEnabled WRITE setTitleEnabled)
    Q_PROPERTY(bool m_fillTrace READ fillTrace WRITE setFillTrace)
    Q_PROPERTY(bool m_bandScopeEnabled READ bandScopeEnabled WRITE setBandScopeEnabled)
    Q_PROPERTY(bool m_bandEnabled READ bandEnabled WRITE setBandEnabled)

public:
    explicit PlotDrawOptionsDockWidget(QWidget *parent = 0);
    ~PlotDrawOptionsDockWidget();

    bool getPhaseOn() const;
    void setPhaseOn(bool value);

    bool getSpectrumOn() const;
    void setSpectrumOn(bool value);

    bool getWaterfallOn() const;
    void setWaterfallOn(bool value);

    bool getScopeOn() const;
    void setScopeOn(bool value);

    int yMin() const;
    int yMax() const;

    int getNumCoeffs() const;
    bool getYMinAutoScale() const;
    bool getYMaxAutoScale() const;
    double getDrawRate() const;

    bool getSGridEnabled() const;
    void setSGridEnabled(bool value);

    bool getDbGridEnabled() const;
    void setDbGridEnabled(bool value);

    bool getFreqGridEnabled() const;
    void setFreqGridEnabled(bool value);

    bool getTitleEnabled() const;
    void setTitleEnabled(bool value);

    bool fillTrace() const;
    bool bandScopeEnabled() const;

    bool bandEnabled() const;

    double getSnapRate() const;
    void setSnapRate(double snapRate);

    int getWfInterval() const;
    int getSpInterval() const;

public slots:
    void start();
    void setYMin(int val);
    void setYMax(int val);

    void setNumCoeffs(int value);
    void setDrawRate(double value);
    void spectrumColor(QColor color);
    void sGridColor(QColor color);
    void dbGridColor(QColor color);
    void fGridColor(QColor color);
    void titleColor(QColor color);
    void bandScopeColor(QColor color);
    void bandColor(QColor color);

    void scopeColor(QColor color);
    void scopeColor(QColor color, bool sendSignal);
    void scopePkColor(QColor color);
    void scopePkColor(QColor color, bool sendSignal);
    void scopeGridColor(QColor color);
    void scopeGridColor(QColor color, bool sendSignal);

    void setWfInterval(int wfInterval);
    void setWfInterval(int wfInterval, bool sendSig);
    void setSpInterval(int spInterval);
    void setSpInterval(int spInterval, bool sendSig);

    // if using the spectrum and waterfall linear gradient, return the color a pct 0.0 - 1.0
    QColor getSpxGradientColorAtPct(const QLinearGradient &lg, const double pct);
    QLinearGradient getSpxGradient();

private slots:
    void on_waterfallTB_toggled(bool checked);
    void on_spectrumTB_toggled(bool checked);
    void on_scopeTB_toggled(bool checked);
    void on_phaseTB_toggled(bool checked);
    void on_yMinAutoScaleTB_toggled(bool checked);
    void on_yMaxAutoScaleTB_toggled(bool checked);
    void on_titleTB_toggled(bool checked);

    void on_dbGridTB_toggled(bool checked);
    void on_fGridTB_toggled(bool checked);
    void on_sGridTB_toggled(bool checked);
    void on_bandTB_toggled(bool checked);
    void on_fillTraceTB_toggled(bool checked);
    void on_bandScopeTB_toggled(bool checked);

    void rateChanged(int val);

    void yMinSpinBoxChanged(QString min);
    void yMaxSpinBoxChanged(QString max);

    void setFillTrace(bool fillTrace);
    void setBandScopeEnabled(bool enabled);
    void setBandEnabled(bool bandEnabled);
    void setSpxLGradient(const QLinearGradient &lg);

signals:
    void waterfallToggled(bool);

    void yMinChanged(int);
    void yMaxChanged(int);
    void rebuildBands(bool);

    void scopeToggled(bool);
    void scopeColorChanged(QColor);
    void scopePkColorChanged(QColor);
    void scopeGridColorChanged(QColor);

    void spectrumToggled(bool);
    void spectrumColorChanged(QColor);

    void sGridEnabled(bool);
    void sGridColorChanged(QColor);

    void phaseToggled(bool);
    void phaseColorChanged(QColor);

    void dbGridEnabled(bool);
    void dbGridColorChanged(QColor);

    void titleEnabled(bool);
    void titleColorChanged(QColor);

    void numCoeffsChanged(int);

    void fillTraceChanged(bool);

    void bandScopeChanged(bool);
    void bandScopeColorChanged(QColor);

    void fGridEnabled(bool);
    void fGridColorChanged(QColor);

    void bandChanged(bool);
    void bandColorChanged(QColor);

    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

    void waterfallIntervalChanged(int);
    void spectrumIntervalChanged(int);

    void spxLGradientChanged(const QLinearGradient &);

private:
    Ui::PlotDrawOptionsDockWidget *ui;

    bool m_phaseOn;
    bool m_spectrumOn;
    bool m_waterfallOn;
    bool m_scopeOn;
    int m_yMin;
    int m_yMax;
    bool m_yMinAutoScale;
    bool m_yMaxAutoScale;
    bool m_initialized;
    int m_numCoeffs;
    int m_snapRate;
    int m_wfInterval;
    int m_spInterval;
    bool m_sGridEnabled;
    bool m_dbGridEnabled;
    bool m_fGridEnabled;
    bool m_titleEnabled;
    bool m_fillTrace;
    bool m_bandScopeEnabled;
    bool m_bandEnabled;
};

//Q_DECLARE_METATYPE(PlotDrawOptionsDockWidget);

#endif // PLOTDRAWOPTIONSDOCKWIDGET_H
