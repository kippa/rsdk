#ifndef AUDIODOCKWIDGET_H
#define AUDIODOCKWIDGET_H

#include <QWidget>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QDockWidget>
#else
#include <QtGui/QDockWidget>
#endif

#include "rsdkdockwidget.h"

namespace Ui {
class AudioDockWidget;
}

class AudioDockWidget : public rsdkDockWidget
{
    Q_OBJECT

    Q_PROPERTY(float m_rxVol READ getVol WRITE setVol)
    Q_PROPERTY(float m_rxBal READ getBal WRITE setBal)
    Q_PROPERTY(float m_subRxVol READ getSubRxVol WRITE setSubRxVol)
    Q_PROPERTY(float m_subRxBal READ getSubRxBal WRITE setSubRxBal)
    Q_PROPERTY(float m_txLeft READ txLeft WRITE setTxLeft)
    Q_PROPERTY(bool m_jackOutEnable READ getJackOutEnable WRITE setJackOutEnable)
    Q_PROPERTY(bool m_jackInEnable READ getJackInEnable WRITE setJackInEnable)
    Q_PROPERTY(float m_jackOutLevel READ getJackOutLevel WRITE setJackOutLevelF)
    Q_PROPERTY(float m_jackInLevel READ getJackInLevel WRITE setJackInLevelF)

public:
    explicit AudioDockWidget(QWidget *parent = nullptr);
    ~AudioDockWidget();


    float prevVol() const;
    void setPrevVol(float prevVol);

    float prevBal() const;
    void setPrevBal(float prevBal);

    bool getJackOutEnable() const;
    bool getJackInEnable() const;

    float getJackOutLevel() const;
    float getJackInLevel() const;

public slots:
    void start();

    void setJackOutLevel(int value);
    void setJackOutLevelF(float value);
    void setJackInLevel(int value);
    void setJackInLevelF(float value);

    void setJackOutEnable(bool jackOutEnable);
    void setJackInEnable(bool jackInEnable);

    float getVol() const;
    void setVol(float value);
    void setIntVol(int value);

    float getBal() const;
    void setBal(float value);
    void setIntBal(int value);

    float getSubRxBal() const;
    void setSubRxBal(float value);
    void setIntSubRxBal(int value);

    float getSubRxVol() const;
    void setSubRxVol(float value);
    void setIntSubRxVol(int value);

    float txLeft() const;
    void setTxLeft(float value);
    void setIntTxLeft(int value);

    void enableSubRx(bool showit);

signals:
    void rxVolChanged(float);
    void rxBalChanged(float);
    void subRxVolChanged(float);
    void subRxBalChanged(float);
    void txLeftChanged(float);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);
    void jackOutChanged(float);
    void jackInChanged(float);

private slots:

private:
    Ui::AudioDockWidget *ui;

    float m_rxVol;
    float m_prevVol;
    float m_rxBal;
    float m_subRxVol;
    float m_subRxBal;
    float m_txLeft;
    float m_jackOutLevel;
    float m_jackInLevel;

    bool m_jackOutEnable;
    bool m_jackInEnable;

};

#endif // AUDIODOCKWIDGET_H
