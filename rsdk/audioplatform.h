#ifndef AUDIOPLATFORM_H
#define AUDIOPLATFORM_H

/*
 * the goal here is to open system audio pipes
 * then play selected receiver audio channels
 * to local machine audio pipes
 * OR
 * use system audio pipes to send to transmit
 * and/or monitor
 */

#include <math.h>

#include <QFile>
#include <QTimer>
#include <QThread>
#include <QIODevice>
#include <QByteArray>
//#include <QAudioInput>
//#include <QAudioOutput>

class audioPlatform : public QObject
{
    Q_OBJECT
public:
    explicit audioPlatform();
    ~audioPlatform();

signals:

public slots:
    void handleInputStateChange(QAudio::State state);
    void handleOutputStateChange(QAudio::State state);
    void readSome();
    void writeSome();
    void rrfin();
    void initAudioIn();
    void initAudioOut(bool doit);
    void handleBytesWritten(qint64 bytes);
    void handleChannelBytesWritten(int channel, qint64 bytes);
    void handleChannelReadyRead(int channel);
    void handleReadChannelFinished();
    void handleReadyRead();
    void handleAboutToClose();

private:

    //void createAudioIn();
    //void createAudioOut();
    qint64 writeData(const char *buf, unsigned int len);
    int readData(char *buf, unsigned int len);
    qint64 m_lastElapsed;

    int m_bufferSize;
    int m_sampleRate;

    void *m_p;
    QTimer *m_pushTimer; // push to the audio output this often

    QString m_deviceNickName;
    QString m_recFileName;
    QString m_playFileName;

    // gazindas and gazouttas
    // the gazindas accept audio data from some source
    bool m_inPull; // true for pull, false for push mode
    QIODevice *m_din;
    QAudioInput *m_ain;
    QAudioFormat m_inFormat;
    QAudioDeviceInfo m_inDevice;

    // the gazouttas send audio data to some destination
    bool m_outPull; // true for pull, false for push mode
    QFile *m_pipeOut; // pipe to receive data to send to system audio
    QIODevice *m_dout;
    QAudioOutput *m_aout;
    QAudioFormat m_outFormat;
    QAudioDeviceInfo m_outDevice;

    QThread collectThread;

    // no copies, no assignments
    audioPlatform(const audioPlatform &NoCopy);
    audioPlatform &operator=(const audioPlatform &NoAssign);
};

#endif // AUDIOPLATFORM_H
