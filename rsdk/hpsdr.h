#ifndef HPSDR_H
#define HPSDR_H

#include <stdio.h>

// handle HPSDR USB protocol defs and actions
// USB is from radiousb

#include </usr/include/libusb-1.0/libusb.h>
//#include <libusb-1.0/libusb.h>

#include <QObject>
#include <QMetaType>

#include "radiousb.h"

#define BIT(n) (1<<n)

#define OZY_SYNC 			0x7F

// bit field masks
/******************************************************************************/
// HPSDR to PC
/******************************************************************************/
// C0 always
#define OZY_C0_RECV_MASK		~(BIT(0)|BIT(1)|BIT(2)); // C0=00000xxx
#define OZY_BIT_PTT_MASK		BIT(0)
#define OZY_BIT_DASH_MASK		BIT(1)
#define OZY_BIT_DOT_MASK		BIT(2)

// C0=00000xxx C1
#define OZY_C0MASK_1XXX			BIT(3)
#define OZY_BIT_LT2208_OVFLW	BIT(0) // 0=inactive, 1=active
#define OZY_BIT_HERMES_I01		BIT(1) // 0=active, 1=inactive
#define OZY_BIT_HERMES_I02		BIT(2) // 0=active, 1=inactive
#define OZY_BIT_HERMES_I03		BIT(3) // 0=active, 1=inactive

#define OZY_MAX_SUBRX	2
#define OZY_MAX_MERCURY 2
#define OZY_MAX_THREADS	2 // 0 for rx merc[0], 1 for xmit

/******************************************************************************/
// PC to HPSDR packets definitions
/******************************************************************************/
// C0
#define HPSDR_C0_CONTROL			0x00
#define HPSDR_MOX_BIT_0				BIT(0)
#define HPSDR_MOX_BITS				HPSDR_MOX_BIT_0
#define HPSDR_MOX_OFF				0
#define HPSDR_MOX_ON				HPSDR_MOX_BIT_0

// C1
#define HPSDR_SAMPLE_RATE_BIT_0 	BIT(0)
#define HPSDR_SAMPLE_RATE_BIT_1 	BIT(1)
#define HPSDR_SAMPLE_RATE_BITS		(HPSDR_SAMPLE_RATE_BIT_0 | HPSDR_SAMPLE_RATE_BIT_1)
#define HPSDR_SAMPLE_RATE_48000 	0
#define HPSDR_SAMPLE_RATE_96000 	HPSDR_SAMPLE_RATE_BIT_0
#define HPSDR_SAMPLE_RATE_192000 	HPSDR_SAMPLE_RATE_BIT_1
#define HPSDR_SAMPLE_RATE_384000 	(HPSDR_SAMPLE_RATE_BIT_0 | HPSDR_SAMPLE_RATE_BIT_1)

#define HPSDR_10MHZ_REF_BIT_0		BIT(2)
#define HPSDR_10MHZ_REF_BIT_1		BIT(3)
#define HPSDR_10MHZ_REF_BITS	 	(HPSDR_10MHZ_REF_BIT_0 | HPSDR_10MHZ_REF_BIT_1)
#define HPSDR_10MHZ_REF_ATLAS 		0
#define HPSDR_10MHZ_REF_PENELOPE 	HPSDR_10MHZ_REF_BIT_0
#define HPSDR_10MHZ_REF_MERCURY 	HPSDR_10MHZ_REF_BIT_1

#define HPSDR_122_88_SRC_BIT_0		BIT(4)
#define HPSDR_122_88_SRC_BITS	 	HPSDR_122_88_SRC_BIT_0
#define HPSDR_122_88_SRC_PENELOPE 	0
#define HPSDR_122_88_SRC_MERCURY 	HPSDR_122_88_SRC_BIT_0


#define HPSDR_CONFIG_BIT_0			BIT(5)
#define HPSDR_CONFIG_BIT_1			BIT(6)
#define HPSDR_CONFIG_BITS	 		(HPSDR_CONFIG_BIT_0 | HPSDR_CONFIG_BIT_1)
#define HPSDR_CONFIG_NIL			0
#define HPSDR_CONFIG_PENELOPE 		HPSDR_CONFIG_BIT_0
#define HPSDR_CONFIG_MERCURY 		HPSDR_CONFIG_BIT_1
#define HPSDR_CONFIG_BOTH 			(HPSDR_CONFIG_BIT_0 | HPSDR_CONFIG_BIT_1)

#define HPSDR_MIC_SRC_BIT_0			BIT(7)
#define HPSDR_MIC_SRC_BITS	 		HPSDR_MIC_SRC_BIT_0
#define HPSDR_MIC_SRC_JANUS 		0
#define HPSDR_MIC_SRC_PENELOPE 		HPSDR_MIC_SRC_BIT_0
// C2
#define HPSDR_MODE_BIT_0 			BIT(0)
#define HPSDR_MODE_BITS				HPSDR_MODE_BIT_0
#define HPSDR_MODE_REGULAR			0
#define HPSDR_MODE_CLASS_E			HPSDR_MODE_BIT_0

#define HPSDR_OC0_BIT 				BIT(1)
#define HPSDR_OC0_OFF 				0
#define HPSDR_OC0_ON 				HPSDR_OC0_BIT
#define HPSDR_OC1_BIT 				BIT(2)
#define HPSDR_OC1_OFF 				0
#define HPSDR_OC1_ON 				HPSDR_OC1_BIT
#define HPSDR_OC2_BIT 				BIT(3)
#define HPSDR_OC2_OFF 				0
#define HPSDR_OC2_ON 				HPSDR_OC2_BIT
#define HPSDR_OC3_BIT 				BIT(4)
#define HPSDR_OC3_OFF 				0
#define HPSDR_OC3_ON 				HPSDR_OC3_BIT
#define HPSDR_OC4_BIT 				BIT(5)
#define HPSDR_OC4_OFF 				0
#define HPSDR_OC4_ON 				HPSDR_OC4_BIT
#define HPSDR_OC5_BIT 				BIT(6)
#define HPSDR_OC5_OFF 				0
#define HPSDR_OC5_ON 				HPSDR_OC5_BIT
#define HPSDR_OC6_BIT 				BIT(7)
#define HPSDR_OC6_OFF 				0
#define HPSDR_OC6_ON 				HPSDR_OC6_BIT
#define HPSDR_OC_BITS			    (HPSDR_OC0_BIT | HPSDR_OC1_BIT | HPSDR_OC2_BIT | HPSDR_OC3_BIT | HPSDR_OC4_BIT | HPSDR_OC5_BIT)

// C3
#define HPSDR_ALEX_ATT_BIT_0		BIT(0)
#define HPSDR_ALEX_ATT_BIT_1		BIT(1)
#define HPSDR_ALEX_ATT_BITS	 		(HPSDR_ALEX_ATT_BIT_0 | HPSDR_ALEX_ATT_BIT_1)
#define HPSDR_ALEX_ATT_0DB			0
#define HPSDR_ALEX_ATT_10DB 		HPSDR_ALEX_ATT_BIT_0
#define HPSDR_ALEX_ATT_20DB 		HPSDR_ALEX_ATT_BIT_1
#define HPSDR_ALEX_ATT_30DB 		(HPSDR_ALEX_ATT_BIT_0 | HPSDR_ALEX_ATT_BIT_1)

#define HPSDR_PREAMP_BIT_0			BIT(2)
#define HPSDR_PREAMP_BITS	 		HPSDR_PREAMP_BIT_0
#define HPSDR_PREAMP_OFF    		0
#define HPSDR_PREAMP_ON     		HPSDR_PREAMP_BIT_0

#define HPSDR_LT2208_DITHER_BIT_0	BIT(3)
#define HPSDR_LT2208_DITHER_BITS	HPSDR_LT2208_DITHER_BIT_0
#define HPSDR_LT2208_DITHER_OFF   	0
#define HPSDR_LT2208_DITHER_ON     	HPSDR_LT2208_DITHER_BIT_0

#define HPSDR_LT2208_RANDOM_BIT_0	BIT(4)
#define HPSDR_LT2208_RANDOM_BITS	HPSDR_LT2208_RANDOM_BIT_0
#define HPSDR_LT2208_RANDOM_OFF   	0
#define HPSDR_LT2208_RANDOM_ON     	HPSDR_LT2208_RANDOM_BIT_0

#define HPSDR_ALEX_RX_ANT_BIT_0		BIT(5)
#define HPSDR_ALEX_RX_ANT_BIT_1		BIT(6)
#define HPSDR_ALEX_RX_ANT_BITS	 	(HPSDR_ALEX_RX_ANT_BIT_0 | HPSDR_ALEX_RX_ANT_BIT_1)
#define HPSDR_ALEX_RX_ANT_NONE		0
#define HPSDR_ALEX_RX_ANT_RX1  		HPSDR_ALEX_RX_ANT_BIT_0
#define HPSDR_ALEX_RX_ANT_RX2  		HPSDR_ALEX_RX_ANT_BIT_1
#define HPSDR_ALEX_RX_ANT_XV   		(HPSDR_ALEX_RX_ANT_BIT_0 | HPSDR_ALEX_RX_ANT_BIT_1)

#define HPSDR_ALEX_RX_OUT_BIT_0		BIT(7)
#define HPSDR_ALEX_RX_OUT_BITS		HPSDR_ALEX_RX_OUT_BIT_0
#define HPSDR_ALEX_RX_OUT_OFF   	0
#define HPSDR_ALEX_RX_OUT_ON     	HPSDR_ALEX_RX_OUT_BIT_0

// C4
#define HPSDR_ALEX_TX_RELAY_BIT_0	BIT(0)
#define HPSDR_ALEX_TX_RELAY_BIT_1	BIT(1)
#define HPSDR_ALEX_TX_RELAY_BITS	(HPSDR_ALEX_TX_RELAY_BIT_0 | HPSDR_ALEX_TX_RELAY_BIT_1)
#define HPSDR_ALEX_TX_RELAY_TX1 	0
#define HPSDR_ALEX_TX_RELAY_TX2  	HPSDR_ALEX_TX_RELAY_BIT_0
#define HPSDR_ALEX_TX_RELAY_TX3  	HPSDR_ALEX_TX_RELAY_BIT_1

#define HPSDR_DUPLEX_BIT_0			BIT(2)
#define HPSDR_DUPLEX_BITS			HPSDR_DUPLEX_BIT_0
#define HPSDR_DUPLEX_OFF   			0
#define HPSDR_DUPLEX_ON     		HPSDR_DUPLEX_BIT_0

#define HPSDR_TOTAL_RX_BIT_0		BIT(3)
#define HPSDR_TOTAL_RX_BIT_1		BIT(4)
#define HPSDR_TOTAL_RX_BIT_2		BIT(5)
#define HPSDR_TOTAL_RX_BITS			(HPSDR_TOTAL_RX_BIT_0 | HPSDR_TOTAL_RX_BIT_1 | HPSDR_TOTAL_RX_BIT_2)
#define HPSDR_TOTAL_RX_1RX 			0
#define HPSDR_TOTAL_RX_2RX  		HPSDR_TOTAL_RX_BIT_0
#define HPSDR_TOTAL_RX_3RX  		HPSDR_TOTAL_RX_BIT_1
#define HPSDR_TOTAL_RX_4RX  		(HPSDR_TOTAL_RX_BIT_1 | HPSDR_TOTAL_RX_BIT_0)
#define HPSDR_TOTAL_RX_5RX  		HPSDR_TOTAL_RX_BIT_2
#define HPSDR_TOTAL_RX_6RX  		(HPSDR_TOTAL_RX_BIT_2 | HPSDR_TOTAL_RX_BIT_0)
#define HPSDR_TOTAL_RX_7RX  		(HPSDR_TOTAL_RX_BIT_2 | HPSDR_TOTAL_RX_BIT_1)
#define HPSDR_TOTAL_RX_8RX  		(HPSDR_TOTAL_RX_BIT_2 | HPSDR_TOTAL_RX_BIT_1 | HPSDR_TOTAL_RX_BIT_0)

#define HPSDR_TIMESTAMP_BIT_0		BIT(6)
#define HPSDR_TIMESTAMP_BITS		HPSDR_TIMESTAMP_BIT_0
#define HPSDR_TIMESTAMP_OFF   		0
#define HPSDR_TIMESTAMP_ON     		HPSDR_TIMESTAMP_BIT_0

#define HPSDR_MERCURY_COMMON_BIT_0	BIT(7)
#define HPSDR_MERCURY_COMMON_BITS	HPSDR_MERCURY_COMMON_BIT_0
#define HPSDR_MERCURY_COMMON_OFF   	0
#define HPSDR_MERCURY_COMMON_ON     HPSDR_MERCURY_COMMON_BIT_0

/******************************************************************************/
// C0
// C0 as Frequency command for TX, and up to 7 RX
#define HPSDR_C0_FREQUENCY_BITS		(BIT(1) | BIT(2) | BIT(3) | BIT(4))
#define HPSDR_C0_FREQUENCY_TX   	BIT(1)
#define HPSDR_C0_FREQUENCY_RX0		BIT(2)
#define HPSDR_C0_FREQUENCY_RX1		(BIT(2) | BIT(1))
#define HPSDR_C0_FREQUENCY_RX2		BIT(3)
#define HPSDR_C0_FREQUENCY_RX3		(BIT(3) | BIT(1))
#define HPSDR_C0_FREQUENCY_RX4		(BIT(3) | BIT(2))
#define HPSDR_C0_FREQUENCY_RX5		(BIT(3) | BIT(2) | BIT(1))
#define HPSDR_C0_FREQUENCY_RX6		BIT(4)
// C1 32-bit MSB frequency
// C2
// C3
// C4 32-bit LSB frequency

/******************************************************************************/
// C0
// C0 as Hermes Alex Control
#define HPSDR_C0_EXT_CMD_12		 	(BIT(4) | BIT(1)) // 0x12

// C1
// Hermes/PennyLane Drive Level (0-255)

// C2
#define HPSDR_MIC_BOOST_BIT			BIT(0)
#define HPSDR_MIC_BOOST_ODB			0
#define HPSDR_MIC_BOOST_20DB		HPSDR_MIC_BOOST_BIT

#define HPSDR_MIC_LINE_BIT			BIT(1)
#define HPSDR_MIC_IN				0
#define HPSDR_LINE_IN				HPSDR_MIC_LINE_BIT

#define HPSDR_APOLLO_FILTER_BIT		BIT(2)
#define HPSDR_APOLLO_FILTER_DISABLE	0
#define HPSDR_APOLLO_FILTER_ENABLE	HPSDR_APOLLO_FILTER_BIT

#define HPSDR_APOLLO_TUNER_BIT		BIT(3)
#define HPSDR_APOLLO_TUNER_DISABLE	0
#define HPSDR_APOLLO_TUNER_ENABLE	HPSDR_APOLLO_TUNER_BIT

#define HPSDR_APOLLO_AT_BIT			BIT(4)
#define HPSDR_APOLLO_AT_END			0
#define HPSDR_APOLLO_AT_START		HPSDR_APOLLO_AT_BIT

#define HPSDR_FLTBD_BIT				BIT(5)
#define HPSDR_FLTBD_ALEX			0
#define HPSDR_FLTBD_APOLLO			HPSDR_FLTBD_BIT

#define HPSDR_MANFLT_BIT			BIT(6)
#define HPSDR_MANFLT_DISABLE		0
#define HPSDR_MANFLT_ENABLE			HPSDR_MANFLT_BIT

#define HPSDR_VNAMODE_BIT			BIT(7)
#define HPSDR_VNAMODE_OFF 			0
#define HPSDR_VNAMODE_ON			HPSDR_VNAMODE_BIT

// C3
#define HPSDR_ALEX_13HPF_BIT		BIT(0)
#define HPSDR_ALEX_13HPF_DISABLE	0
#define HPSDR_ALEX_13HPF_ENABLE		HPSDR_ALEX_13HPF_BIT

#define HPSDR_ALEX_20HPF_BIT		BIT(1)
#define HPSDR_ALEX_20HPF_DISABLE	0
#define HPSDR_ALEX_20HPF_ENABLE		HPSDR_ALEX_20HPF_BIT

#define HPSDR_ALEX_9_5HPF_BIT		BIT(2)
#define HPSDR_ALEX_9_5HPF_DISABLE	0
#define HPSDR_ALEX_9_5HPF_ENABLE	HPSDR_ALEX_9_5HPF_BIT

#define HPSDR_ALEX_6_5HPF_BIT		BIT(3)
#define HPSDR_ALEX_6_5HPF_DISABLE	0
#define HPSDR_ALEX_6_5HPF_ENABLE	HPSDR_ALEX_6_5HPF_BIT

#define HPSDR_ALEX_1_5HPF_BIT		BIT(4)
#define HPSDR_ALEX_1_5HPF_DISABLE	0
#define HPSDR_ALEX_1_5HPF_ENABLE	HPSDR_ALEX_1_5HPF_BIT

#define HPSDR_ALEX_BYPASS_HPF_BIT		BIT(5)
#define HPSDR_ALEX_BYPASS_HPF_DISABLE	0
#define HPSDR_ALEX_BYPASS_HPF_ENABLE	HPSDR_ALEX_BYPASS_HPF_BIT

#define HPSDR_ALEX_6MLNA_BIT		BIT(6)
#define HPSDR_ALEX_6MLNA_DISABLE	0
#define HPSDR_ALEX_6MLNA_ENABLE		HPSDR_ALEX_6MLNA_BIT

//#define HPSDR_UNKNONW_BIT		BIT(7)
//#define HPSDR_UNKNOWN_DISABLE	0
//#define HPSDR_UNKNOWN_ENABLE	HPSDR_UNKNOWN_BIT

// C4
#define HPSDR_ALEX_30_20MLPF_BIT		BIT(0)
#define HPSDR_ALEX_30_20MLPF_DISABLE	0
#define HPSDR_ALEX_30_20MLPF_ENABLE		HPSDR_ALEX_30_20MLPF_BIT

#define HPSDR_ALEX_60_40MLPF_BIT		BIT(1)
#define HPSDR_ALEX_60_40MLPF_DISABLE	0
#define HPSDR_ALEX_60_40MLPF_ENABLE		HPSDR_ALEX_60_40MLPF_BIT

#define HPSDR_ALEX_80MLPF_BIT			BIT(2)
#define HPSDR_ALEX_80MLPF_DISABLE		0
#define HPSDR_ALEX_80MLPF_ENABLE		HPSDR_ALEX_80MLPF_BIT

#define HPSDR_ALEX_160MLPF_BIT			BIT(3)
#define HPSDR_ALEX_160MLPF_DISABLE		0
#define HPSDR_ALEX_160MLPF_ENABLE		HPSDR_ALEX_160MLPF_BIT

#define HPSDR_ALEX_6MLPF_BIT			BIT(4)
#define HPSDR_ALEX_6MLPF_DISABLE		0
#define HPSDR_ALEX_6MLPF_ENABLE			HPSDR_ALEX_6MLPF_BIT

#define HPSDR_ALEX_12_10MLPF_BIT		BIT(5)
#define HPSDR_ALEX_12_10MLPF_DISABLE	0
#define HPSDR_ALEX_12_10MLPF_ENABLE		HPSDR_ALEX_12_10MLPF_BIT

#define HPSDR_ALEX_17_15MLPF_BIT		BIT(6)
#define HPSDR_ALEX_17_15MLPF_DISABLE	0
#define HPSDR_ALEX_17_15MLPF_ENABLE		HPSDR_ALEX_17_15MLPF_BIT

/******************************************************************************/
// C0
// C0 Control
#define HPSDR_C0_EXT_CMD_14		 	(BIT(4) | BIT(2)) // 0x14

// C1
#define HPSDR_RX1_PRE_AMP_BIT           BIT(0)
#define HPSDR_RX1_PRE_AMP_OFF       	0
#define HPSDR_RX1_PRE_AMP_ON    		HPSDR_RX1_PRE_AMP_BIT

#define HPSDR_RX2_PRE_AMP_BIT           BIT(1)
#define HPSDR_RX2_PRE_AMP_OFF       	0
#define HPSDR_RX2_PRE_AMP_ON    		HPSDR_RX2_PRE_AMP_BIT

#define HPSDR_RX3_PRE_AMP_BIT           BIT(2)
#define HPSDR_RX3_PRE_AMP_OFF       	0
#define HPSDR_RX3_PRE_AMP_ON    		HPSDR_RX3_PRE_AMP_BIT

#define HPSDR_RX4_PRE_AMP_BIT           BIT(3)
#define HPSDR_RX4_PRE_AMP_OFF       	0
#define HPSDR_RX4_PRE_AMP_ON    		HPSDR_RX4_PRE_AMP_BIT

#define HPSDR_ORION_TR_BIT              BIT(4)
#define HPSDR_ORION_TR_TIP             	0
#define HPSDR_ORION_TR_RING     		HPSDR_ORION_TR_BIT

#define HPSDR_ORION_MIC_BIAS_BIT        BIT(5)
#define HPSDR_ORION_MIC_BIAS_OFF      	0
#define HPSDR_ORION_MIC_BIAS_ON    		HPSDR_ORION_MIC_BIAS_BIT

#define HPSDR_ORION_MIC_PTT_BIT         BIT(6)
#define HPSDR_ORION_MIC_PTT_OFF      	0
#define HPSDR_ORION_MIC_PTT_ON    		HPSDR_ORION_MIC_PTT_BIT

// C2
#define HPSDR_TLV320_LINE_GAIN          (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4))

#define HPSDR_C2_ATT_RX_ON_TX_BIT       BIT(5)
#define HPSDR_C2_ATT_RX_ON_TX_DISABLE   0
#define HPSDR_C2_ATT_RX_ON_TX_ENABLE    HPSDR_C2_ATT_RX_ON_TX_BIT

#define HPSDR_PURESIGNAL_BIT            BIT(6)
#define HPSDR_PURESIGNAL_DISABLE        0
#define HPSDR_PURESIGNAL_ENABLE         HPSDR_PURESIGNAL_BIT

#define HPSDR_PEN_CW_BIT                BIT(7)
#define HPSDR_PEN_CW_DISABLE            0
#define HPSDR_PEN_CW_ENABLE             HPSDR_PEN_CW_BIT

// C3
#define HPSDR_METIS_DB9_PIN1_BIT        BIT(0)
#define HPSDR_METIS_DB9_PIN1_OFF        0
#define HPSDR_METIS_DB9_PIN1_ON         HPSDR_METIS_DB9_PIN1_BIT

#define HPSDR_METIS_DB9_PIN2_BIT        BIT(1)
#define HPSDR_METIS_DB9_PIN2_OFF        0
#define HPSDR_METIS_DB9_PIN2_ON         HPSDR_METIS_DB9_PIN2_BIT

#define HPSDR_METIS_DB9_PIN3_BIT        BIT(2)
#define HPSDR_METIS_DB9_PIN3_OFF        0
#define HPSDR_METIS_DB9_PIN3_ON         HPSDR_METIS_DB9_PIN3_BIT

#define HPSDR_METIS_DB9_PIN4_BIT        BIT(3)
#define HPSDR_METIS_DB9_PIN4_OFF        0
#define HPSDR_METIS_DB9_PIN4_ON         HPSDR_METIS_DB9_PIN4_BIT

#define HPSDR_C3_ATT_RX_ON_TX_BIT       BIT(4)
#define HPSDR_C3_ATT_RX_ON_TX_DISABLE   0
#define HPSDR_C3_ATT_RX_ON_TX_ENABLE    HPSDR_C3_ATT_RX_ON_TX_BIT

// C4
#define HPSDR_ADC1_ATT_BITS             (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4))

#define HPSDR_ADC1_ATT_BIT              BIT(5)
#define HPSDR_ADC1_ATT_DISABLE          0
#define HPSDR_ADC1_ATT_ENABLE           HPSDR_ADC1_ATT_BIT

/******************************************************************************/
// C0
#define HPSDR_C0_EXT_CMD_16		 	(BIT(4) | BIT(2) | BIT(1)) // 0x16

// C1
#define HPSDR_ADC2_ATT_BITS             (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4))

#define HPSDR_ADC2_ATT_BIT              BIT(5)
#define HPSDR_ADC2_ATT_DISABLE          0
#define HPSDR_ADC2_ATT_ENABLE           HPSDR_ADC2_ATT_BIT

// C2
#define HPSDR_ADC3_ATT_BITS             (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4))

#define HPSDR_ADC3_ATT_BIT              BIT(5)
#define HPSDR_ADC3_ATT_DISABLE          0
#define HPSDR_ADC3_ATT_ENABLE           HPSDR_ADC3_ATT_BIT

#define HPSDR_REVERSE_CW_KEYS_BIT       BIT(6)
#define HPSDR_REVERSE_CW_KEYS_DISABLE   0
#define HPSDR_REVERSE_CW_KEYS_ENABLE    HPSDR_REVERSE_CW_KEYS_BIT

// C3
#define HPSDR_CW_KEYER_SPEED_BITS       (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4) | BIT(5)) // 1 - 60 WPM
#define HPSDR_CW_KEYER_MODE_BITS        (BIT(6) | BIT(7)) // 0x00 = straight key, 0x01 = mode A, 0x10 = mode B

// C4
#define HPSDR_CW_KEYER_WEIGHT_BITS      (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4) | BIT(5) | BIT(6)) // 0 - 100
#define HPSDR_CW_KEYER_SPACING_BIT      BIT(7) // keyer spacing
#define HPSDR_CW_KEYER_SPACING_OFF      0
#define HPSDR_CW_KEYER_SPACING_ON       HPSDR_CW_KEYER_SPACING_BIT

/******************************************************************************/
// C0
#define HPSDR_C0_EXT_CMD_18		 	(BIT(4) | BIT(3)) // 0x18, reserved for additional Mercury boards

/******************************************************************************/
// C0
#define HPSDR_C0_EXT_CMD_1A		 	(BIT(4) | BIT(3) | BIT(1)) // 0x1A, reserved for additional Mercury boards

/******************************************************************************/
// C0
#define HPSDR_C0_EXT_CMD_1C		 	BIT(4) | BIT(3) | BIT(2) // 0x1C, ADC / rcvr assignments

// C1
#define HPSDR_ADC_RX1				BIT(0) | BIT(1)
#define HPSDR_ADC_RX2				BIT(2) | BIT(3)
#define HPSDR_ADC_RX3				BIT(4) | BIT(5)
#define HPSDR_ADC_RX4				BIT(6) | BIT(7)
// assign these to the above C1
#define HPSDR_ADC1					0
#define HPSDR_ADC2					BIT(0)
#define HPSDR_ADC3					BIT(1)
// C2
#define HPSDR_ADC_RX5				BIT(0) | BIT(1)
#define HPSDR_ADC_RX6				BIT(2) | BIT(3)
#define HPSDR_ADC_RX7				BIT(4) | BIT(5)
// C3
#define HPSDR_ADC_ATT				BIT(4) | BIT(3) | BIT(2) | BIT(1) | BIT(0)
// C4 UNUSED

/******************************************************************************/
// C0
#define HPSDR_C0_EXT_CMD_1E		 	BIT(4) | BIT(3) | BIT(2) | BIT(1) // 0x1E, CW keyer stuff

// C1
#define HPSDR_CW_EXT_INT_BIT        BIT(0)
#define HPSDR_CW_EXT                0
#define HPSDR_CW_INT                HPSDR_CW_EXT_INT_BIT

// C2
// Sidetone volume (0-127)

// C3
// CW PTT delay mS (0-255)

// C4
// currently reserved for raised cosine profile time

/******************************************************************************/
// C0
#define HPSDR_C0_EXT_CMD_20		 	BIT(5) // 0x20, more CW keyer stuff

// C1
#define HPSDR_CW_HANG_TIME_MSB      (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4) | BIT(5) | BIT(6) | BIT(7))

// C2
#define HPSDR_CW_HANG_TIME_LSB      (BIT(0) | BIT(1))

// C3
#define HPSDR_CW_SIDETONE_FREQ_MSB  (BIT(0) | BIT(1) | BIT(2) | BIT(3) | BIT(4) | BIT(5) | BIT(6) | BIT(7))

// C4
#define HPSDR_CW_SIDETONE_FREQ_LSB  (BIT(0) | BIT(1) | BIT(2) | BIT(3))




/*******************************************************************************/

// USB end points
// ozy uses these end points
#define OZY_EP_TX_DATA		(2 | LIBUSB_ENDPOINT_OUT) // 0x02, to ozy/mercury
#define OZY_EP_RX_BS_DATA	(4 | LIBUSB_ENDPOINT_IN)  // 0x84, bandscope data
#define OZY_EP_RX_DATA		(6 | LIBUSB_ENDPOINT_IN)  // 0x86, from ozy/mercury
// and this timeout is common
#define OZY_IO_TIMEOUT 		50

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

#define MAX_HF_FREQ 61400000LL

// template for overlay on ozy 512 byte packets for single receiver
typedef struct OzyRx1Packet
{
    uint8_t sync[3]; // sync is 0x7f 0x7f 0x7f
    uint8_t cmd[5]; // command and control bytes

    // data
    uint8_t d[504];
}OzyRx1Packet_t;


const uint8_t tx_C0_cmd_list[] = {
    HPSDR_C0_CONTROL,
    HPSDR_C0_FREQUENCY_TX,
    HPSDR_C0_FREQUENCY_RX0,
    //HPSDR_C0_FREQUENCY_RX1,
    HPSDR_C0_EXT_CMD_12,
    HPSDR_C0_EXT_CMD_14,
    HPSDR_C0_EXT_CMD_16, // partial CW keyer, speed, mode, weight, spacing
    HPSDR_C0_EXT_CMD_1C,
    HPSDR_C0_EXT_CMD_1E, //
    HPSDR_C0_EXT_CMD_20
};

extern int libusbOzyXferDev(int ep, void* buffer, int buffersize, void *user_data);
extern void libusbOzyHandleCb(struct libusb_transfer *ux);
extern void libusbOzyHandleCtl(struct libusb_transfer *usb_xfer); // a return for ctl packets that don't use the usbBuf_t, but still use the async libusb api
extern int libusbOzyClearQueue(usbBufPool_t *q);

// forward declre RadioOzy
class RadioOzy;

extern int libusbOzyResync(RadioOzy *rf); // search for 0x7f three times in data stream, resync with 509 byte read following
extern int libusbOzyDosync(RadioOzy *rf, uint8_t *d, int len); // actually fetch the single byte data

class hpsdr : public QObject
{
    Q_OBJECT

public:
    explicit hpsdr(QWidget *parent = 0);
    ~hpsdr();

public:
    enum HpsdrMoxEnum { Mox_Off=HPSDR_MOX_OFF,
                        Mox_On=HPSDR_MOX_ON };
    Q_ENUM (HpsdrMoxEnum)


    enum HpsdrSampleRateEnum { SampleRate48000=HPSDR_SAMPLE_RATE_48000,
                               SampleRate96000=HPSDR_SAMPLE_RATE_96000,
                               SampleRate192000=HPSDR_SAMPLE_RATE_192000,
                               SampleRate384000=HPSDR_SAMPLE_RATE_384000 };
    Q_ENUM (HpsdrSampleRateEnum)

    enum HpsdrRef10MEnum { Ref10M_Atlas=HPSDR_10MHZ_REF_ATLAS,
                           Ref10M_Penelope=HPSDR_10MHZ_REF_PENELOPE,
                           Ref10M_Mercury=HPSDR_10MHZ_REF_MERCURY };
    Q_ENUM (HpsdrRef10MEnum)

    enum HpsdrRef122MEnum { Ref122M_Mercury=HPSDR_122_88_SRC_PENELOPE,
                            Ref122M_Penelope=HPSDR_122_88_SRC_MERCURY };
    Q_ENUM (HpsdrRef122MEnum)

    enum HpsdrConfigEnum { Config_Penelope=HPSDR_CONFIG_PENELOPE,
                           Config_Mercury=HPSDR_CONFIG_MERCURY,
                           Config_Both=HPSDR_CONFIG_BOTH };
    Q_ENUM (HpsdrConfigEnum)

    enum HpsdrMicSrcEnum { Mic_Janus=HPSDR_MIC_SRC_JANUS,
                           Mic_Penelope=HPSDR_MIC_SRC_PENELOPE };
    Q_ENUM (HpsdrMicSrcEnum)

    enum HpsdrClassEEnum { Class_Regular=HPSDR_MODE_REGULAR,
                           Class_E=HPSDR_MODE_CLASS_E };
    Q_ENUM (HpsdrClassEEnum)

    enum HpsdrAttEnum { Att_0dB=HPSDR_ALEX_ATT_0DB,
                        Att_10dB=HPSDR_ALEX_ATT_10DB,
                        Att_20dB=HPSDR_ALEX_ATT_20DB,
                        Att_30dB=HPSDR_ALEX_ATT_30DB };
    Q_ENUM (HpsdrAttEnum)

    enum HpsdrPreAmpEnum { Preamp_Off=HPSDR_PREAMP_OFF,
                           Preamp_On=HPSDR_PREAMP_ON };
    Q_ENUM (HpsdrPreAmpEnum)

    enum HpsdrDitherEnum { dither_Off=HPSDR_LT2208_DITHER_OFF,
                           dither_On=HPSDR_LT2208_DITHER_ON };
    Q_ENUM (HpsdrDitherEnum)

    enum HpsdrRandomEnum { random_Off=HPSDR_LT2208_RANDOM_OFF,
                                 random_On=HPSDR_LT2208_RANDOM_ON };
    Q_ENUM (HpsdrRandomEnum)

    enum HpsdrRxAntEnum { rxAnt_None=HPSDR_ALEX_RX_ANT_NONE,
                          rxAnt_RX1=HPSDR_ALEX_RX_ANT_RX1,
                          rxAnt_RX2=HPSDR_ALEX_RX_ANT_RX2,
                          rxAnt_XV=HPSDR_ALEX_RX_ANT_XV };
    Q_ENUM (HpsdrRxAntEnum)

    enum HpsdrRxOutEnum { rxOut_Off=HPSDR_ALEX_RX_OUT_OFF,
                          rxOut_On=HPSDR_ALEX_RX_OUT_ON };
    Q_ENUM (HpsdrRxOutEnum)

    enum HpsdrTxAntEnum { txAnt_1=HPSDR_ALEX_TX_RELAY_TX1,
                          txAnt_2=HPSDR_ALEX_TX_RELAY_TX2,
                          txAnt_3=HPSDR_ALEX_TX_RELAY_TX3 };
    Q_ENUM (HpsdrTxAntEnum)

    enum HpsdrDuplexEnum { duplex_Full=HPSDR_DUPLEX_OFF,
                           duplex_Half=HPSDR_DUPLEX_ON };
    Q_ENUM (HpsdrDuplexEnum)

    enum HpsdrMicGainEnum { MicGain_0dB=HPSDR_MIC_BOOST_ODB,
                            MicGain_20dB=HPSDR_MIC_BOOST_20DB };
    Q_ENUM (HpsdrMicGainEnum)

    enum HpsdrMicLineEnum { MicIn=HPSDR_MIC_IN,
                            LineIn=HPSDR_LINE_IN};
    Q_ENUM (HpsdrMicLineEnum)

    enum HpsdrXmitAttEnum { XmitAttOff=HPSDR_C2_ATT_RX_ON_TX_DISABLE,
                            XmitAttOn=HPSDR_C2_ATT_RX_ON_TX_ENABLE};
    Q_ENUM (HpsdrXmitAttEnum)

private:

};

#endif // HPSDR_
