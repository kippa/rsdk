#include "rsdk.h"

#include "rangecontrol.h"
#include "rxparmsdockwidget.h"
#include "ui_rxparmsdockwidget.h"

RxParmsDockWidget::RxParmsDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::RxParmsDockWidget)
{
    int cnt = 0;

    ui->setupUi(this);

    QString xname = "Squelch";
    squelchRC = new RangeControl(ui->dockWidgetContents, -175.0, 50.0, -150.0, xname, true);
    rcList.append(squelchRC);
    ui->gridLayout->addWidget(squelchRC, 0, cnt++, 1, 1);

    /*
    xname = "DCB";
    dcbRC = new RangeControl(ui->dockWidgetContents, -1.0, 0.5, 1.0, xname, true);
    rcList.append(dcbRC);
    ui->gridLayout->addWidget(dcbRC, 0, cnt++, 1, 1);
    */

    xname = "NB1";
    nb1RC = new RangeControl(ui->dockWidgetContents, 0.0, 20.0, 9.0, xname, true);
    rcList.append(nb1RC);
    ui->gridLayout->addWidget(nb1RC, 0, cnt++, 1, 1);

    xname = "NB1Hang";
    nb1HTRC = new RangeControl(ui->dockWidgetContents, 0.0, 32.0, 7.0, xname, true);
    rcList.append(nb1HTRC);
    ui->gridLayout->addWidget(nb1HTRC, 0, cnt++, 1, 1);

    xname = "NB2";
    nb2RC = new RangeControl(ui->dockWidgetContents, 0.0, 20.0, 5.0, xname, true);
    rcList.append(nb2RC);
    ui->gridLayout->addWidget(nb2RC, 0, cnt++, 1, 1);

    xname = "Compander";
    compander = new RangeControl(ui->dockWidgetContents, -10.0, 10.0, -0.1, xname, true);
    rcList.append(compander);
    ui->gridLayout->addWidget(compander, 0, cnt++, 1, 1);
}

RxParmsDockWidget::~RxParmsDockWidget()
{
    delete ui;
}

void RxParmsDockWidget::start()
{

    //connect thru rsdk
    connect (squelchRC, SIGNAL(rangeEnabled(bool)), theRsdk, SLOT(setSquelch(bool)));
    connect (squelchRC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setSquelchVal(double)));

    //connect (theRsdk, SIGNAL(squelchStateChanged(bool, bool)), squelchRC, SLOT(setEnabled(bool, false)));
    //connect (theRsdk, SIGNAL(squelchGainChanged(double)), squelchRC, SLOT(setValue(double)));

    // initialize the nb
    nb1RC->setEnabled(theRsdk->getRsdkNB());
    nb1RC->setM_Value(theRsdk->getRsdkNBVals());
    connect (nb1RC, SIGNAL(rangeEnabled(bool)), theRsdk, SLOT(setRsdkNB(bool)));
    connect (nb1RC, SIGNAL(rangeEnabled(bool)), theRsdk->DspDock, SLOT(setDspNB1(bool)));
    connect (nb1RC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRsdkNBVals(double)));

    connect (nb1HTRC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRsdkNBHangtime(double)));


    nb2RC->setEnabled(theRsdk->getRsdkSDROM());
    nb2RC->setM_Value(theRsdk->getRsdkSDROMVals());
    connect (nb2RC, SIGNAL(rangeEnabled(bool)), theRsdk, SLOT(setRsdkSDROM(bool)));
    connect (nb2RC, SIGNAL(rangeEnabled(bool)), theRsdk->DspDock, SLOT(setDspNB2(bool)));
    connect (nb2RC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRsdkSDROMVals(double)));

    //connect (dcbRC, SIGNAL(rangeEnabled(bool)), theRsdk, SLOT(setRXDCBlock(bool)));
    //connect (dcbRC, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRXDCBlockGain(double)));

    connect (compander, SIGNAL(rangeEnabled(bool)), theRsdk, SLOT(setRXCompander(bool)));
    connect (compander, SIGNAL(rangeChanged(double)), theRsdk, SLOT(setRXCompanderLevel(double)));
}

void RxParmsDockWidget::setnb1(bool setit)
{
    nb1RC->setEnabled(setit, false);
}

void RxParmsDockWidget::setnb2(bool setit)
{
    nb2RC->setEnabled(setit, false);
}
