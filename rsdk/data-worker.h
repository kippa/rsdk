#ifndef DATA_WORKER_H
#define DATA_WORKER_H

#include <QObject>
#include <QThread>
#include <QTimer>

class Worker2 : public QObject
{
    Q_OBJECT

    Q_PROPERTY(unsigned int m_rate READ getRate WRITE setRate);

public:
    //  meant to call Process_Panadatper or Process_Scope from libDttSP.a
    Worker2(void (*fn)(int, float *), int curRx, void *buf);
    ~Worker2();

    unsigned int getRate() const;
    bool isRunning() { return m_interval.isActive(); }

public slots:
    void stopTimer();
    void startTimer();
    void sampleData();
    void setRate(unsigned int msecs);

signals:
    void sampleDone();
    void finished();

private:
    int m_rx;
    void *m_buf;
    QTimer m_interval;
    void (*m_fn)(int, float *);

    unsigned int m_rate;

    // no copy, no assign
    Worker2(const Worker2 &NoCopy);
    Worker2 &operator=(const Worker2 &NoAssign);
};

class Worker3 : public QObject
{
    Q_OBJECT

    Q_PROPERTY(double m_rate READ getRate WRITE setRate);

public:
    //  meant to call Process_Panadatper or Process_Scope from libDttSP.a
    Worker3(void (*fn)(int, float *, int), int curRx, void *buf, unsigned int size);
    ~Worker3();

    double getRate() const;

public slots:
    void stopTimer();
    void startTimer();
    void sampleData();
    void setRate(unsigned int msec);

signals:
    void sampleDone();

private:
    int m_rx;
    int m_size;
    QTimer m_interval;
    unsigned int m_numSamps;
    void *m_buf;
    void (*m_fn)(int, float *, int);

    unsigned int m_rate;

    // no copy, no assign
    Worker3(const Worker3 &NoCopy);
    Worker3 &operator=(const Worker3 &NoAssign);
};

#endif // DATA_WORKER_H
