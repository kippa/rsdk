#ifndef RADIOFILE_H
#define RADIOFILE_H

#ifdef DO_JACK
#include <jack_rsdk.h>
#endif

#include "common.h" // for int64

// the callback for async simulation
typedef int (*ASYNCCB)(void *cbdata_prm, unsigned char *buf_prm, int int_prm);

class RadioFile
{
public:
    RadioFile(const char * const name_prm, ASYNCCB cb, void *cbdata);
    virtual ~RadioFile();

    int Read(unsigned char *buf_prm, int64 len_prm, bool wait_prm=true);
    int Write(unsigned char *buf_prm, int64 len_prm, bool wait_prm=true);

    //virtual int SetFreq(float f_prm);
    virtual int SetSampRate(int samprate_prm);

    bool getjackInEnabled() const;
    int setjackInEnabled(bool value);

    bool getJackOutEnabled() const;
    void setJackOutEnabled(bool enable);

    int sampRate;

    ASYNCCB cb;
    void *cbdata;

#ifdef DO_JACK
    int jackStart();
    int jackStop();
    jack *jackConn;
    bool jackInEnabled;
    bool jackOutEnabled;
    float jackInLevel;
    float jackOutLevel;
#endif

    float getJackInLevel() const;
    void setJackInLevel(float value);

    float getJackOutLevel() const;
    void setJackOutLevel(float value);

protected:

private:
    int fd;
    // no copy, no assign
    RadioFile(const RadioFile &NoCopy);
    RadioFile &operator=(const RadioFile &NoAssign);

};

#endif // RADIOFILE_H
