#include <stdexcept>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/fs.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <errno.h>
#include <string.h> // strerror

#include "common.h" // int64
#include "exception.h"
#include "radiofile.h"

// create RadioFiles with a method to callback to when a buffer has been filled, or sent
// this is async IO to/from the radios.
RadioFile::RadioFile(const char * const name_prm, ASYNCCB cb_prm, void *cbdata_prm) :
    sampRate(0),
    cb(cb_prm),
    cbdata(cbdata_prm),
    jackConn(nullptr),
    jackInEnabled(false),
    jackOutEnabled(false),
    fd(-1),
    jackInLevel(0.0),
    jackOutLevel(0.0)
{

    if (!name_prm) {
        THROW_EXCEPTION( CRITICAL_LEVEL, 0, "No name_prm\n");
    }

    fd=open(name_prm, O_RDWR|O_LARGEFILE|O_CREAT|O_APPEND, S_IRWXU|S_IRWXG|S_IRWXO);
    if (fd==-1) {
        THROW_EXCEPTION( CRITICAL_LEVEL, 0, "Unable to open %s (%s)\n", name_prm, strerror(errno));
    }

    jackStart();
}

RadioFile::~RadioFile()
{
#ifdef DO_JACK
    jackStop();
#endif

    if (fd>0)
        close(fd);
}

int RadioFile::Read(unsigned char *buf_prm, int64 len_prm, bool wait_prm)
{
    int ret=read(fd, buf_prm, len_prm);
    if (wait_prm==false)
    {
        cb(cbdata, buf_prm, 0);
    }
    return ret;
}

int RadioFile::Write(unsigned char *buf_prm, int64 len_prm, bool wait_prm)
{
    int ret=write(fd, buf_prm, len_prm);
    if(ret<0)
    {
        fprintf(stderr, "%s: Write %lld bytes returned %d (%s)\n", __func__, len_prm, ret, strerror(ret));
        if(wait_prm==false)
        {
            cb(cbdata, buf_prm, -1);
        }
    }
    else if (wait_prm==false)
    {
        //if(wait_prm==false)
        {
            cb(cbdata, buf_prm, 0);
        }
    }
    return ret;
}

int RadioFile::SetSampRate(int s_prm)
{
    // TODO sanitize
    sampRate = s_prm;
    return 0;
}

#ifdef DO_JACK
int RadioFile::jackStart()
{
    if (jackConn && jackConn->isRunning())
        jackStop();

    delete jackConn;

    jackConn = new jack("rsdk");
    int rc = jackConn->startJack();
    return rc;
}

int RadioFile::jackStop()
{
    if (jackConn->isRunning()) {
        jackConn->stopJack();
        // TODO FIXME this is just wrong here, jackStop() will delete jackConn
        //delete jackConn;
    }
    //jackConn = nullptr;
    return 0;
}

float RadioFile::getJackOutLevel() const
{
    return jackOutLevel;
}

void RadioFile::setJackOutLevel(float value)
{
    jackOutLevel = value;
}

float RadioFile::getJackInLevel() const
{
    return jackInLevel;
}

void RadioFile::setJackInLevel(float value)
{
    jackInLevel = value;
}

bool RadioFile::getJackOutEnabled() const
{
    return jackOutEnabled;
}

void RadioFile::setJackOutEnabled(bool enable)
{
    jackOutEnabled = false;
    if (enable) {
        int ret = 0;
        if (!jackConn->isRunning())
            ret = jackStart();
        if (ret) {
            fprintf(stderr, "%s: jackStart failed, cannot enable jack out!\n", __func__);
        } else {
            jackOutEnabled = true;
        }
    } else {
        static float out_fbl[1024];
        static float out_fbr[1024];
        unsigned int fullSize = 1024 * sizeof(float);
        memset((void*)&out_fbl, 0, fullSize);
        memset((void*)&out_fbr, 0, fullSize);
        if (jackConn->isRunning() && jackOutEnabled) {
            jackConn->fillRingbuffer((const char *)out_fbl, fullSize, jackConn->rb_outL);
            jackConn->fillRingbuffer((const char *)out_fbr, fullSize, jackConn->rb_outR);
        }
    }
}

bool RadioFile::getjackInEnabled() const
{
    return jackInEnabled;
}

// caution races for JackIn and JackOut?
int RadioFile::setjackInEnabled(bool enable)
{
    jackInEnabled = false;
    if (enable) {
        int ret = 0;
        if (!jackConn->isRunning())
            ret = jackStart();
        if (ret) {
            fprintf(stderr, "%s: jackStart failed, cannot enable jack in!\n", __func__);
        } else {
            jackInEnabled = true;
        }
    }
    // don't stop
    //
    //else {
    //    if (!jackOutEnabled) // the other side is not using the jack connection, disable it
    //        jackStop();
    //}
    return 0;
}
#endif
