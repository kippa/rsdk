#include <iostream>
#include <stdio.h>
#include <QTextStream>

#include "rsdk.h"
#include "hpsdrdockwidget.h"
#include "ui_hpsdrdockwidget.h"


HpsdrDockWidget::HpsdrDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::HpsdrDockWidget),
    m_SampleRate(hpsdr::SampleRate384000),
    m_Ref10M(hpsdr::Ref10M_Atlas),
    m_Ref122M(hpsdr::Ref122M_Mercury),
    m_Dither(hpsdr::dither_Off),
    m_Random(hpsdr::random_Off),
    m_ClassE(hpsdr::Class_Regular),
    m_Duplex(hpsdr::duplex_Full),
    m_MicSrc(hpsdr::Mic_Penelope),
    m_PreAmp(hpsdr::Preamp_Off),
    m_MicLine(hpsdr::MicIn),
    m_MicGain(hpsdr::MicGain_0dB),
    m_XmitAtten(hpsdr::XmitAttOff)
{
    // register the metatype enums from hpsdr.h so we can serialize
    qRegisterMetaType<hpsdr::HpsdrMoxEnum>();
    qRegisterMetaType<hpsdr::HpsdrSampleRateEnum>();
    qRegisterMetaType<hpsdr::HpsdrRef10MEnum>();
    qRegisterMetaType<hpsdr::HpsdrRef122MEnum>();
    qRegisterMetaType<hpsdr::HpsdrConfigEnum>();
    qRegisterMetaType<hpsdr::HpsdrMicSrcEnum>();
    qRegisterMetaType<hpsdr::HpsdrClassEEnum>();
    qRegisterMetaType<hpsdr::HpsdrAttEnum>();
    qRegisterMetaType<hpsdr::HpsdrPreAmpEnum>();
    qRegisterMetaType<hpsdr::HpsdrDitherEnum>();
    qRegisterMetaType<hpsdr::HpsdrRandomEnum>();
    qRegisterMetaType<hpsdr::HpsdrRxAntEnum>();
    qRegisterMetaType<hpsdr::HpsdrRxOutEnum>();
    qRegisterMetaType<hpsdr::HpsdrTxAntEnum>();
    qRegisterMetaType<hpsdr::HpsdrDuplexEnum>();
    qRegisterMetaType<hpsdr::HpsdrMicGainEnum>();
    qRegisterMetaType<hpsdr::HpsdrMicLineEnum>();
    qRegisterMetaType<hpsdr::HpsdrXmitAttEnum>();

    ui->setupUi(this);

    connect(ui->classEBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(classEBtnGrpClick(QAbstractButton*)));
    connect(ui->duplexBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(duplexBtnGrpClick(QAbstractButton*)));
    connect(ui->clk10SelBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(clk10SelBtnGrpClick(QAbstractButton*)));
    connect(ui->clk122SelBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(clk122SelBtnGrpClick(QAbstractButton*)));
    connect(ui->micSelBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(micSelBtnGrpClick(QAbstractButton*)));
    connect(ui->sampRateBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(sampRateBtnGrpClick(QAbstractButton*)));
    connect(ui->micGainBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(micGainBtnGrpClick(QAbstractButton*)));
    connect(ui->micLineBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(micLineBtnGrpClick(QAbstractButton*)));

    connect(ui->preAmpTB, SIGNAL(toggled(bool)), this, SLOT(preAmpBtnToggled(bool)));
    connect(ui->ditherTB, SIGNAL(toggled(bool)), this, SLOT(ditherBtnToggled(bool)));
    connect(ui->randomTB, SIGNAL(toggled(bool)), this, SLOT(randomBtnToggled(bool)));
    connect(ui->xmitAttTB, SIGNAL(toggled(bool)), this, SLOT(xmitAttBtnToggled(bool)));

    // initial defaults overriden later if so
    setSampleRate(m_SampleRate);
    setRef10M(m_Ref10M);
    setRef122M(m_Ref122M);
    setConfig(m_Config);
    setClassE(m_ClassE);
    setMicSrc(m_MicSrc);
    setDither(m_Dither);
    setRandom(m_Random);
    setDuplex(m_Duplex);
    setPreAmp(m_PreAmp);
    setMicGain(m_MicGain);
    setMicLine(m_MicLine);
    setXmitAtten(m_XmitAtten);
}

HpsdrDockWidget::~HpsdrDockWidget()
{
    delete ui;
}

void HpsdrDockWidget::start()
{
    emit sampleRateChanged(m_SampleRate);
    emit ref10MChanged(m_Ref10M);
    emit ref122MChanged(m_Ref122M);
    emit ditherChanged(m_Dither);
    emit randomChanged(m_Random);
    emit classEChanged(m_ClassE);
    emit duplexChanged(m_Duplex);
    emit micSrcChanged(m_MicSrc);
    emit preAmpChanged(m_PreAmp);
    emit configChanged(m_Config);
    emit micLineChanged(m_MicLine);
    emit micGainChanged(m_MicGain);
}

void HpsdrDockWidget::setOpacity(bool b)
{
    if (b)
        setWindowOpacity(0.80);
}

void HpsdrDockWidget::setSampleRate(hpsdr::HpsdrSampleRateEnum sampleRate)
{
    m_SampleRate = sampleRate;

    if(sampleRate == hpsdr::SampleRate384000)
        ui->rate384KRB->setChecked(true);
    else if(sampleRate == hpsdr::SampleRate192000)
        ui->rate192KRB->setChecked(true);
    else if(sampleRate == hpsdr::SampleRate96000)
        ui->rate96KRB->setChecked(true);
    else // if(sampleRate == hpsdr::SampleRate48000)
        ui->rate48KRB->setChecked(true);

    emit sampleRateChanged(m_SampleRate);
}

hpsdr::HpsdrSampleRateEnum HpsdrDockWidget::getSampleRate()
{
    return m_SampleRate;
}

int HpsdrDockWidget::getSampleRateVal()
{
    int val;
    if(m_SampleRate == hpsdr::SampleRate384000)
        val = 384000;
    else if(m_SampleRate == hpsdr::SampleRate192000)
        val = 192000;
    else if(m_SampleRate == hpsdr::SampleRate96000)
        val = 96000;
    else // if(m_SampleRate == hpsdr::SampleRate48000)
        val = 48000;

    return val;
}

void HpsdrDockWidget::setRef10M(hpsdr::HpsdrRef10MEnum ref10M)
{
   m_Ref10M = ref10M;

   if(ref10M == hpsdr::Ref10M_Atlas)
       ui->atlas10RB->setChecked(true);
   else if(ref10M == hpsdr::Ref10M_Penelope)
       ui->penelope10RB->setChecked(true);
   else // if(ref10M == hpsdr::Ref10M_Mercuryy)
       ui->mercury10RB->setChecked(true);

   emit ref10MChanged(m_Ref10M);
}

hpsdr::HpsdrRef10MEnum HpsdrDockWidget::getRef10M()
{
   return m_Ref10M;
}

void HpsdrDockWidget::setRef122M(hpsdr::HpsdrRef122MEnum ref122M)
{
   m_Ref122M = ref122M;

   if(ref122M == hpsdr::Ref122M_Penelope)
       ui->penelope122RB->setChecked(true);
   else // if(ref122M == hpsdr::Ref122M_Mercury)
       ui->mercury122RB->setChecked(true);

   emit ref122MChanged(m_Ref122M);
}

hpsdr::HpsdrRef122MEnum HpsdrDockWidget::getRef122M()
{
   return m_Ref122M;
}

void HpsdrDockWidget::setDither(hpsdr::HpsdrDitherEnum dither)
{
  m_Dither = dither;

  if(m_Dither == hpsdr::dither_Off)
      ui->ditherTB->setChecked(false);
  else if(m_Dither == hpsdr::dither_On)
      ui->ditherTB->setChecked(true);

  emit ditherChanged(m_Dither);
}

hpsdr::HpsdrDitherEnum HpsdrDockWidget::getDither()
{
    return m_Dither;
}

void HpsdrDockWidget::setRandom(hpsdr::HpsdrRandomEnum random)
{
   m_Random = random;

   if(m_Random == hpsdr::random_Off)
       ui->randomTB->setChecked(false);
   else
       ui->randomTB->setChecked(true);

   emit randomChanged(m_Random);
}

hpsdr::HpsdrRandomEnum HpsdrDockWidget::getRandom()
{
    return m_Random;
}

void HpsdrDockWidget::setConfig(hpsdr::HpsdrConfigEnum Config)
{
    m_Config = Config;
    emit configChanged(Config);
}

hpsdr::HpsdrConfigEnum HpsdrDockWidget::getConfig()
{
    return m_Config;
}

void HpsdrDockWidget::setClassE(hpsdr::HpsdrClassEEnum ClassE)
{
    m_ClassE = ClassE;

    if (ClassE == hpsdr::Class_E)
        ui->classERB->setChecked(true);
    else // if (ClassE == hpsdr::Class_Regular)
        ui->otherRB->setChecked(true);

    emit classEChanged(m_ClassE);
}

hpsdr::HpsdrClassEEnum HpsdrDockWidget::getClassE()
{
    return m_ClassE;
}

void HpsdrDockWidget::setDuplex(hpsdr::HpsdrDuplexEnum duplex)
{
    m_Duplex = duplex;

    if (duplex == hpsdr::duplex_Half)
        ui->halfDuplexRB->setChecked(true);
    else // if (duplex == hpsdr::duplex_Half)
        ui->fullDuplexRB->setChecked(true);

    emit duplexChanged(m_Duplex);
}

hpsdr::HpsdrDuplexEnum HpsdrDockWidget::getDuplex()
{
    return m_Duplex;
}

void HpsdrDockWidget::setMicSrc(hpsdr::HpsdrMicSrcEnum MicSrc)
{
   m_MicSrc = MicSrc;

   if (MicSrc == hpsdr::Mic_Janus)
       ui->janusMicRB->setChecked(true);
   else if (MicSrc == hpsdr::Mic_Penelope)
       ui->penelopeMicRB->setChecked(true);

   emit micSrcChanged(m_MicSrc);
}

hpsdr::HpsdrMicSrcEnum HpsdrDockWidget::getMicSrc()
{
    return m_MicSrc;
}

void HpsdrDockWidget::setPreAmp(hpsdr::HpsdrPreAmpEnum PreAmp)
{
    m_PreAmp = PreAmp;

    if(PreAmp == hpsdr::Preamp_Off)
        ui->preAmpTB->setChecked(false);
    else // if(PreAmp == hpsdr::Preamp_Off)
        ui->preAmpTB->setChecked(true);

    emit preAmpChanged(m_PreAmp);
}

hpsdr::HpsdrPreAmpEnum HpsdrDockWidget::getPreAmp()
{
   return m_PreAmp;
}

void HpsdrDockWidget::preAmpBtnToggled(bool checked)
{
    //hpsdr::HpsdrPreAmpEnum prev_PreAmp = m_PreAmp;
    if (checked)
        m_PreAmp = hpsdr::Preamp_On;
    else
        m_PreAmp = hpsdr::Preamp_Off;

    emit preAmpChanged(m_PreAmp);
}

void HpsdrDockWidget::classEBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    //hpsdr::HpsdrClassEEnum prev_classE = m_ClassE;

    if (btnName.contains("Class E"))
        m_ClassE = hpsdr::Class_E;
    else // if (btnName.contains("Other")) // default
        m_ClassE = hpsdr::Class_Regular;

    emit classEChanged(m_ClassE);

}

void HpsdrDockWidget::duplexBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    //hpsdr::HpsdrDuplexEnum prev_duplex = m_Duplex;

    if (btnName.contains("Half"))
        m_Duplex = hpsdr::duplex_Half;
    else // if (btnName.contains("Full")) // default
        m_Duplex = hpsdr::duplex_Full;

    //if (m_Duplex != prev_duplex)
    emit duplexChanged(m_Duplex);
}

void HpsdrDockWidget::ditherBtnToggled(bool checked)
{
    //hpsdr::HpsdrDitherEnum prev_dither = m_Dither;
    if (checked)
        m_Dither = hpsdr::dither_On;
    else
        m_Dither = hpsdr::dither_Off;
    emit ditherChanged(m_Dither);
}

void HpsdrDockWidget::randomBtnToggled(bool checked)
{
    //hpsdr::HpsdrRandomEnum prev_random = m_Random;
    if (checked)
        m_Random = hpsdr::random_On;
    else
        m_Random = hpsdr::random_Off;
    emit randomChanged(m_Random);
}

void HpsdrDockWidget::xmitAttBtnToggled(bool checked)
{
    if (checked) {
        m_XmitAtten = hpsdr::XmitAttOn;
        theRsdk->rozy->setXmit20dBAtt(true);
    } else {
        m_XmitAtten = hpsdr::XmitAttOff;
        theRsdk->rozy->setXmit20dBAtt(false);
    }
}

hpsdr::HpsdrXmitAttEnum HpsdrDockWidget::getXmitAtten() const
{
    return m_XmitAtten;
}

void HpsdrDockWidget::setXmitAtten(const hpsdr::HpsdrXmitAttEnum &XmitAtten)
{
    m_XmitAtten = XmitAtten;
}

// we recklessly us m_micGain as a line in gain too, lazy
void HpsdrDockWidget::micGainBtnGrpClick(QAbstractButton *btn)
{
    // we dynamically change the button text, so don't use that to select
    // instead use the objectName, which is invariant
    if (btn->objectName().contains("micGain20RB"))
        m_MicGain = hpsdr::MicGain_20dB;
    else  // defaults to 0db gain
        m_MicGain = hpsdr::MicGain_0dB;

    emit micGainChanged(m_MicGain);
}

void HpsdrDockWidget::micLineBtnGrpClick(QAbstractButton *btn)
{
    // we dynamically change the button text, so don't use that to select
    // instead use the objectName, which is invariant
    if (btn->objectName().contains("lineInRB"))
    {
        m_MicLine = hpsdr::LineIn;
        ui->micGain0RB->setText(QString("-18 dB"));
        ui->micGain20RB->setText(QString("0 dB"));
    }
    else
    {
        m_MicLine = hpsdr::MicIn;
        ui->micGain0RB->setText(QString("0 dB"));
        ui->micGain20RB->setText(QString("+20 dB"));
    }

    emit micLineChanged(m_MicLine);
}

hpsdr::HpsdrMicGainEnum HpsdrDockWidget::getMicGain() const
{
    return m_MicGain;
}

void HpsdrDockWidget::setMicGain(const hpsdr::HpsdrMicGainEnum &MicGain)
{
    m_MicGain = MicGain;
}

hpsdr::HpsdrMicLineEnum HpsdrDockWidget::getMicLine() const
{
    return m_MicLine;
}

void HpsdrDockWidget::setMicLine(const hpsdr::HpsdrMicLineEnum &MicLine)
{
    m_MicLine = MicLine;
}

void HpsdrDockWidget::clk10SelBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    //hpsdr::HpsdrRef10MEnum prev_ref10M = m_Ref10M;

    if (btnName.contains("Atlas"))
        m_Ref10M = hpsdr::Ref10M_Atlas;
    else if (btnName.contains("Penelope"))
        m_Ref10M= hpsdr::Ref10M_Penelope;
    else //if (btnName.contains("Mercury")) // default
        m_Ref10M = hpsdr::Ref10M_Mercury;

    emit ref10MChanged(m_Ref10M);
}

void HpsdrDockWidget::clk122SelBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    //hpsdr::HpsdrRef122MEnum prev_ref122M = m_Ref122M;

    if (btnName.contains("Penny"))
        m_Ref122M= hpsdr::Ref122M_Penelope;
    else //if (btnName.contains("Mercury")) // default
        m_Ref122M = hpsdr::Ref122M_Mercury;

    emit ref122MChanged(m_Ref122M);
}

void HpsdrDockWidget::micSelBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    //hpsdr::HpsdrMicSrcEnum prev_MicSrc = m_MicSrc;

    if (btnName.contains("Penny"))
        m_MicSrc= hpsdr::Mic_Penelope;
    else //if (btnName.contains("Janus")) // default
        m_MicSrc = hpsdr::Mic_Janus;

    emit micSrcChanged(m_MicSrc);
}

void HpsdrDockWidget::sampRateBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    if (btnName.contains("96K"))
        m_SampleRate = hpsdr::SampleRate96000;
    else if (btnName.contains("192K"))
        m_SampleRate = hpsdr::SampleRate192000;
    else if (btnName.contains("384K"))
        m_SampleRate = hpsdr::SampleRate384000;
    else // if (btnName.contains("48K")) // default
        m_SampleRate = hpsdr::SampleRate48000;

    setSampleRate(m_SampleRate);
}
