/**
  * this is convenience for dock widget lists that want to access a start() function
  * like dockList in rsdk.cpp. I can't call someDockWidget->start() as QDockWidget knows
  * nothing of ->start(), which I have built into each and every DockWidget to allow for
  * a convenient place to ensure all things are ready before starting each DockWidget.
  * So we subclass to rsdkDockWidget which has the included start() method and the list
  * is now of type rsdkDockWidget, not DockWidget
  */

#include <QDockWidget>
#include <QBuffer>
#include <QTimer>

#include <QMetaProperty>
#include "rsdk.h"
#include "rsdkdockwidget.h"

/**
 * @brief rsdkDockWidget::rsdkDockWidget
 * @param parent
 * common handling for dock widgets
 */
rsdkDockWidget::rsdkDockWidget(QWidget *parent) : QDockWidget(parent),
  theRsdk(qobject_cast<rsdk*>(parent)),
  theParent(parent)
{
    // handle visibility changes here
    connect (this, SIGNAL(visibilityChanged(bool)), this, SLOT(stateChange(bool)));
    connect (this, SIGNAL(topLevelChanged(bool)), this, SLOT(stateChange(bool)));
    connect (this, SIGNAL(dockLocationChanged(Qt::DockWidgetArea)), this, SLOT(setTitlePos(Qt::DockWidgetArea)));

    // all docks in this project can go anywhere
    setAllowedAreas(Qt::AllDockWidgetAreas);

    // and start with all features enabled
    setFeatures(QDockWidget::AllDockWidgetFeatures);

    //! derived classes are free to adjust the interval
    //! the base class will just got to updateHandler() and
    //! stop the timer with the first event
    updateTimer.stop();
    updateTimer.setInterval(100);
    updateTimer.setTimerType(Qt::PreciseTimer);
    connect(&updateTimer, SIGNAL(timeout()), this, SLOT(updateHandler()));
    updateTimer.start(100);
}

rsdkDockWidget::~rsdkDockWidget()
{
    updateTimer.stop();
}

/**
 * @brief rsdkDockWidget::updateHandler
 * The updateTimer in this base class calls here once
 * and stops. No harm no foul. Derived classes will
 * overload updateHandler() to their own advantage
 * and the timer will not come here and stop
 */
void rsdkDockWidget::updateHandler()
{
    updateTimer.stop();
}

/**
 * @brief DockWidet::stateChange We would like to change the rsdk menuDocks checked state
 * in the menu
 * @param visible
 */
void rsdkDockWidget::stateChange(bool visible)
{
    Q_ASSERT(theParent);

    rsdk *r = qobject_cast<rsdk*>(theParent);
    if (!r)
        return;

    if(visible)
        r->setDockMenuChecked(this, true);
    else
        r->setDockMenuChecked(this, false);

    QDockWidget::DockWidgetFeatures f = this->features();
    if (isFloating())
    {
        f &= ~(QDockWidget::DockWidgetVerticalTitleBar);
        setFeatures(f);
    }
}

// adjust title position to left edge or top edge depending upon top or side dock areas
void rsdkDockWidget::setTitlePos(Qt::DockWidgetArea area)
{
    Q_ASSERT(theParent);

    bool x = isFloating();
    QDockWidget::DockWidgetFeatures f = QDockWidget::AllDockWidgetFeatures;

    if ( x || (area == Qt::LeftDockWidgetArea || area == Qt::RightDockWidgetArea)) {
        // top title bar
        f &= ~QDockWidget::DockWidgetVerticalTitleBar;
    } else if (area == Qt::TopDockWidgetArea || area == Qt::BottomDockWidgetArea) {
        // side title bar
        f |= QDockWidget::DockWidgetVerticalTitleBar;
    }
    setFeatures(f);
}
