#include <iostream>
#include <stdio.h>
#include <QTextStream>

#include "alexdockwidget.h"
#include "ui_alexdockwidget.h"

AlexDockWidget::AlexDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::AlexDockWidget),
    m_att(hpsdr::Att_0dB),
    m_rxant(hpsdr::rxAnt_None),
    m_txant(hpsdr::txAnt_1),
    m_rxout(hpsdr::rxOut_Off)
{
    ui->setupUi(this);

    connect(ui->txAntBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(txAntBtnGrpClick(QAbstractButton*)));
    connect(ui->rxAntBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(rxAntBtnGrpClick(QAbstractButton*)));
    connect(ui->attBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(attBtnGrpClick(QAbstractButton*)));
    connect(ui->rxOutTB, SIGNAL(toggled(bool)), this, SLOT(rxOutToggled(bool)));

    setAlexAtt(m_att);
    setAlexRxAnt(m_rxant);
    setAlexRxOut(m_rxout);
    setAlexTxAnt(m_txant);
}

AlexDockWidget::~AlexDockWidget()
{
    delete ui;
}

void AlexDockWidget::start()
{
    emit attChanged(m_att);
    emit rxAntChanged(m_rxant);
    emit txAntChanged(m_txant);
    emit rxOutChanged(m_rxout);
}

hpsdr::HpsdrAttEnum AlexDockWidget::getAlexAtt() const
{
    return m_att;
}

void AlexDockWidget::setAlexAtt(hpsdr::HpsdrAttEnum att)
{
    m_att = att;

    if(m_att == hpsdr::Att_0dB)
        ui->att0dBRB->setChecked(true);
    else if(m_att == hpsdr::Att_10dB)
        ui->att10dBRB->setChecked(true);
    else if(m_att == hpsdr::Att_20dB)
        ui->att20dBRB->setChecked(true);
    else if(m_att == hpsdr::Att_30dB)
        ui->att30dBRB->setChecked(true);

    emit attChanged(m_att);
}

hpsdr::HpsdrRxAntEnum AlexDockWidget::getAlexRxAnt() const
{
    return m_rxant;
}

void AlexDockWidget::setAlexRxAnt(hpsdr::HpsdrRxAntEnum rxant)
{
    m_rxant = rxant;

    if (m_rxant == hpsdr::rxAnt_None)
        ui->rxNoneRB->setChecked(true);
    else if (m_rxant == hpsdr::rxAnt_RX1)
        ui->rxAnt1RB->setChecked(true);
    else if (m_rxant == hpsdr::rxAnt_RX2)
        ui->rxAnt2RB->setChecked(true);
    else if (m_rxant == hpsdr::rxAnt_XV)
        ui->rxXVRB->setChecked(true);

    emit rxAntChanged(m_rxant);
}

hpsdr::HpsdrTxAntEnum AlexDockWidget::getAlexTxAnt() const
{
    return m_txant;
}

void AlexDockWidget::setAlexTxAnt(hpsdr::HpsdrTxAntEnum txant)
{
    m_txant = txant;

    if (m_txant == hpsdr::txAnt_1)
        ui->txAnt1RB->setChecked(true);
    else if (m_txant == hpsdr::txAnt_2)
        ui->txAnt2RB->setChecked(true);
    else if (m_txant == hpsdr::txAnt_3)
        ui->txAnt3RB->setChecked(true);

    emit txAntChanged(m_txant);
}

hpsdr::HpsdrRxOutEnum AlexDockWidget::getAlexRxOut() const
{
    return m_rxout;
}

void AlexDockWidget::setAlexRxOut(hpsdr::HpsdrRxOutEnum rxout)
{
    m_rxout = rxout;

    if (m_rxout == true)
        ui->rxOutTB->setChecked(true);
    else
        ui->rxOutTB->setChecked(false);

    emit rxOutChanged(m_rxout);
}

void AlexDockWidget::txAntBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    if (btnName.contains("Ant 1"))
        m_txant = hpsdr::txAnt_1;
    else if (btnName.contains("Ant 2"))
        m_txant = hpsdr::txAnt_2;
    else if (btnName.contains("Ant 3"))
        m_txant = hpsdr::txAnt_3;

    emit txAntChanged(m_txant);
}

void AlexDockWidget::rxAntBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    if (btnName.contains("None"))
        m_rxant = hpsdr::rxAnt_None;
    else if (btnName.contains("Rx1 In"))
        m_rxant = hpsdr::rxAnt_RX1;
    else if (btnName.contains("Rx2 In"))
        m_rxant = hpsdr::rxAnt_RX2;
    else if (btnName.contains("Rx XV In"))
        m_rxant = hpsdr::rxAnt_XV;

    emit rxAntChanged(m_rxant);
}

void AlexDockWidget::attBtnGrpClick(QAbstractButton *btn)
{
    QString btnName=btn->text();

    if (btnName.contains("-10 dB"))
        m_att = hpsdr::Att_10dB;
    else if (btnName.contains("-20 dB"))
        m_att = hpsdr::Att_20dB;
    else if (btnName.contains("-30 dB"))
        m_att = hpsdr::Att_30dB;
    else // if (btnName.constains("0 dB") default
        m_att = hpsdr::Att_0dB;

    emit attChanged(m_att);
}

void AlexDockWidget::rxOutToggled(bool checked)
{
    hpsdr::HpsdrRxOutEnum prev_rxout = m_rxout;
    if (checked)
        m_rxout = hpsdr::rxOut_On;
    else
        m_rxout = hpsdr::rxOut_Off;

    if (m_rxout != prev_rxout)
        emit rxOutChanged(m_rxout);
}
