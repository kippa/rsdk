#ifndef AGCDOCKWIDGET_H
#define AGCDOCKWIDGET_H

#include <QWidget>
#include <QAbstractButton>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QDockWidget>
#else
#include <QtGui/QDockWidget>
#endif

#include "dttsp.h"

#include "rangecontrol.h"
#include "rsdkdockwidget.h"

namespace Ui {
class AgcDockWidget;
}

class AgcDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(AGCMODE m_mode READ mode WRITE setMode)
    Q_PROPERTY(bool m_txDCB READ txDCB WRITE setTxDCB)
    Q_PROPERTY(bool m_rxDCB READ rxDCB WRITE setRxDCB)

public:
    explicit AgcDockWidget(QWidget *parent = 0);
    ~AgcDockWidget();

    enum AGCMODE { agcOFF=0, agcLONG, agcSLOW, agcMED, agcFAST };
    Q_ENUM(AGCMODE)

    bool txDCB() const;
    bool rxDCB() const;

public slots:
    AGCMODE mode() const;
    void setMode(const AGCMODE &mode);
    void start();
    void setTxDCB(bool txDCB);
    void setRxDCB(bool rxDCB);
    // separate sets that don't emit a signal, to avoid feedback
    void setTxDCBState();
    void setRxDCBState();

private slots:
    void agcBtnGrpKeyPress(QAbstractButton *btn);

signals:
    void agcChanged(AGCMODE mode);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);
    void rxDcbChanged(bool); // receiver DSP dcblocking filter
    void txDcbChanged(bool); // transmitter DSP dcblocking filter

private:
    Ui::AgcDockWidget *ui;

    AGCMODE m_mode;
    bool m_txDCB;
    bool m_rxDCB;
};

#endif // AGCDOCKWIDGET_H
