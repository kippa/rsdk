#ifndef BANDDOCKWIDGET_H
#define BANDDOCKWIDGET_H

#include <list>

#include <QWidget>
#include <QAbstractButton>
#include <QToolButton>
#include <QMap>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QDockWidget>
#else
#include <QtGui/QDockWidget>
#endif

#include "dttsp.h"
#include "common.h" // struct band

#include "rsdkdockwidget.h"

namespace Ui {
class BandDockWidget;
}

class BandDockWidget : public rsdkDockWidget
{
    Q_OBJECT

public:
    explicit BandDockWidget(QWidget *parent = 0);
    ~BandDockWidget();

    void start();
    int fillBands();

    // for a given range of frequencies from start_prm to end_prm
    // return a std::list of struct band *
    std::list<struct band *> bandsForRange(double start_prm, double end_prm);

public slots:
    // select the band button for a given frequency
    void bandBtnForFreq(double f);
    void bandBtnForFreq(qlonglong f);
    void updateBands(const int dummy);
    void bandSetLastFreq(double f);

signals:
    void bandBtnChanged(QAbstractButton *btn);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private slots:
    void bandBtnGrpKeyPress(QAbstractButton *btn);
    void updateHandler(); // overload of rsdkdockwidget::updateHandller();

private:
    Ui::BandDockWidget *ui;
    QAbstractButton *getBtnForFreq(double f);
    qlonglong lastFreq; // spare the updateHandler if not needed

    // use std::list not QList as interface to database
    // will be simpler w/o including Qt stuff there
    std::list<struct band *> bands;
    // map the band buttons to a band on the bands list
    QMap <QToolButton *, struct band *> bandKeyMap;
    pthread_mutex_t bandModifyLock;

    QToolButton *m_lastPb;
    int m_count;
    double m_lastFreq;
    void removeBandsList();
};

#endif // BANDDOCKWIDGET_H
