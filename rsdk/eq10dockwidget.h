#ifndef EQ10DOCKWIDGET_H
#define EQ10DOCKWIDGET_H

#include <QList>
#include <QToolButton>
#include <QDockWidget>

#include "EqSlider.h"
#include "rsdkdockwidget.h"

namespace Ui {
class EQ10DockWidget;
}

class EQ10DockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(int* m_vals READ values WRITE setValues)

public:
    //explicit EQ10DockWidget(QWidget *parent = 0);
    explicit EQ10DockWidget(QWidget *parent = 0, const QString &name = "EQ10");
    ~EQ10DockWidget();
    void start();

    QString name() const;
    void setName(const QString &name);

    // pull all or just one of the ISO band db gain levels
    int *values(void);
    int value(int ndx);

    // set all or just one of the ISO band db gain levels
    void setValues(int *vals);
    void setValueByNdx(int ndx, int val);

    bool enable() const;

signals:

public slots:
    void setValue(double val);
    void setEnable(bool enable);
    void resetZero(); // set all sliders back to zero

private:
    QString m_name;
    bool m_enable;

    QList <EqSlider *>sliders;

    Ui::EQ10DockWidget *ui;

    // we keep the values here as DttSP has no concept of storing these anywhere
    // DttSP just creates the filters and then abondons these values to the wind
    // so we'll track them here, it's a one way push to DttSP, no way to read current values
    int m_vals[12]; // 0 is preamp gain, 1-11 are 10 bands of eq centered on ISO bands

    QToolButton *enableTB;
    QToolButton *resetTB;

    // the eq band widgets
    EqSlider *preAmpGain; // or band0, get it?
    EqSlider *band1;
    EqSlider *band2;
    EqSlider *band3;
    EqSlider *band4;
    EqSlider *band5;
    EqSlider *band6;
    EqSlider *band7;
    EqSlider *band8;
    EqSlider *band9;
    EqSlider *band10;

};

#endif // EQ10DOCKWIDGET_H
