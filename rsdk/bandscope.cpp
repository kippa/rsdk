#include <asm/byteorder.h>

#include <QPointF>
#include <QMouseEvent>
#include <QWheelEvent>

#include <qwt_plot_canvas.h>

#include "rsdk.h"
#include "apex_memmove.h"

#include "bandscope.h"
#include "ui_bandscope.h"

// TODO FIXME
extern rsdk *r;

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

// implement a Blackman-Harris filter object
// count is the number of samples
class bhfilter {

public:

    bhfilter (const unsigned int count) {

        // make space in the filter buffer
        //f = new double[count];

        const double coeff0 = 0.35875;
        const double coeff1 = 0.48829;
        const double coeff2 = 0.14128;
        const double coeff3 = 0.01168;
        const double twopi = 2.0 * M_PI;
        const double fourpi = 4.0 * M_PI;
        const double sixpi = 6.0 * M_PI;

        unsigned int ndx=0;
        for (unsigned int i=0; i<count; i++)
        {
            double val = coeff0
                    - (coeff1 * cos(twopi * (i+0.5) / count))
                    + (coeff2 * cos(fourpi * (i+0.5)/ count))
                    - (coeff3 * cos(sixpi * (i+0.5) / count));
            f[ndx] = val;
            ndx++;
        }
    };

    ~bhfilter() {};
    //~bhfilter() { delete[] f; }

    double f[BANDSCOPE_NUM_SAMPLES];

private:

};

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

class BScopeGrid: public QwtPlotGrid
{
public:

    BScopeGrid() :
        m_color("grey50")
    {
        enableY(false);
        enableYMin(false);
        enableX(false);
        enableXMin(false);
        setZ(300);
    }

    /*
    void draw(QPainter *p, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &rect) const
    {
        double x1 = rect.left();
        //double x2 = rect.right()-1.0;
        //const double w = x2 - x1;
        const double y1 = rect.top();
        //const double y2 = rect.bottom()-1.0;
        //const double hzPerStep = MAX_BANDSCOPE_FREQ / ((BANDSCOPE_NUM_SAMPLES)+1);
        //const double hzPerStep = MAX_BANDSCOPE_FREQ / w;

        p->save();

        QColor labelColor("white");

        if (xEnabled())
        {
            QwtScaleDiv xScale = xScaleDiv();
            const QList<double>values = xScale.ticks(QwtScaleDiv::MajorTick);
            for (int i=0; i<values.count(); i++)
            {
                double value = xMap.transform(values[i]);
                QString label = QLocale(QLocale::English).toString(values[i]/1e6, 'f', 0);
                const QFont f("Arial", 12, QFont::Light);
                p->setFont(f);
                QRect r = p->boundingRect(value, y1, 0, 0, Qt::AlignHCenter, label);
                p->setPen(labelColor.darker(256 - (m_color.value()/2)));
                p->drawText(r, Qt::AlignCenter, label);
                //p->setPen(m_color);
                //p->drawLine(value, r.height()+2, value, y2);
                //p->drawRoundedRect(r, 1.0, 1.0, Qt::AbsoluteSize);
            }
        }

        if (yEnabled())
        {
            QwtScaleDiv yScale = yScaleDiv();
            const QList<double>values = yScale.ticks(QwtScaleDiv::MajorTick);
            for (int i=0; i<values.count(); i++)
            {
                double value = yMap.transform(values[i]);
                QString label = QLocale(QLocale::English).toString(value, 'f', 0);
                const QFont f("Arial", 12, QFont::Light);
                p->setFont(f);
                QRect r = p->boundingRect(x1, value, 0, 0, Qt::AlignVCenter, label);
                p->setPen(labelColor.darker(256 - (m_color.value()/2)));
                p->drawText(r, Qt::AlignCenter, label);
                //p->setPen(m_color);
                //p->drawLine(value, r.height()+2, value, y2);
                //p->drawRoundedRect(r, 1.0, 1.0, Qt::AbsoluteSize);
            }

        }

        QwtPlotGrid::draw(p, xMap, yMap, rect);

        p->restore();
    }
    */

    QColor getColor() { return m_color; }
    void setColor(const QColor &color) {
        m_color =color;
        setMajorPen(m_color, (qreal)1.00, Qt::SolidLine);
        setMinorPen(m_color.darker(), (qreal)1.00, Qt::DotLine);
    }

private:
    QColor m_color;
};

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

BandScope::BandScope(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BandScope),
    m_traceColor(QColor("navajowhite")),
    m_gridColor(QColor("navajowhite").darker()),
    m_bandScopeEnabled(false),
    yMin(0.0),
    yMax(0.0),
    avgInit(false),
    m_activeDragXPos(0),
    m_smooth(17)
{
    ui->setupUi(this);

    // set up dsp dft
    bhf = new bhfilter(BANDSCOPE_NUM_SAMPLES);
    time_buf = (double *) fftw_malloc(sizeof(double) * BANDSCOPE_NUM_SAMPLES);
    freq_buf = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * BANDSCOPE_NUM_SAMPLES);
    plan = fftw_plan_dft_r2c_1d(BANDSCOPE_NUM_SAMPLES, time_buf, freq_buf, FFTW_PATIENT);

    // setup the bandScope display QwtPlot
    ui->BandScopePlot->setAxisAutoScale(QwtPlot::xTop, false);
    ui->BandScopePlot->setAxisAutoScale(QwtPlot::xBottom, false);
    ui->BandScopePlot->setAxisAutoScale(QwtPlot::yLeft, false);
    ui->BandScopePlot->setAxisAutoScale(QwtPlot::yRight, true);

    ui->BandScopePlot->enableAxis(QwtPlot::xTop, false);
    ui->BandScopePlot->enableAxis(QwtPlot::xBottom, true);
    ui->BandScopePlot->enableAxis(QwtPlot::yLeft, false);
    ui->BandScopePlot->enableAxis(QwtPlot::yRight, true);

    ui->BandScopePlot->setAxisScale(QwtPlot::xTop, 0, BANDSCOPE_NUM_SAMPLES/2, 0);
    ui->BandScopePlot->setAxisScale(QwtPlot::xBottom, 0, MAX_BANDSCOPE_FREQ, 0);
    ui->BandScopePlot->setAxisScale(QwtPlot::yRight, yMin, yMax, 0);

    // pick up mouse over filter regions
    ui->BandScopePlot->setMouseTracking(true);
    ui->BandScopePlot->setCanvasBackground(* new QBrush(Qt::black));
    ui->BandScopePlot->canvas()->setMouseTracking(true);
    this->setMouseTracking(true);

    grid = new BScopeGrid;
    grid->enableX(true);
    grid->enableXMin(true);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->setXAxis(QwtPlot::xBottom);
    grid->setColor(m_gridColor);
    //grid->setMajorPen(m_gridColor, 1.0, Qt::SolidLine);
    //grid->setMinorPen(m_gridColor.darker(), 1.0, Qt::DotLine);
    grid->attach(ui->BandScopePlot);
    grid->setZ(300.0);

    // settings for the drawing canvas
    QwtPlotCanvas *cnvs = (QwtPlotCanvas *)ui->BandScopePlot->canvas();
    cnvs->setPaintAttribute(QwtPlotCanvas::BackingStore, false);
    cnvs->setPaintAttribute(QwtPlotCanvas::Opaque, false);
    cnvs->setPaintAttribute(QwtPlotCanvas::ImmediatePaint, true);
    if(cnvs->testPaintAttribute(QwtPlotCanvas::BackingStore))
    {
        cnvs->setAttribute(Qt::WA_PaintOnScreen, true);
        cnvs->setAttribute(Qt::WA_NoSystemBackground, true);
    }

    memset(xes, 0, BANDSCOPE_NUM_SAMPLES*sizeof(double));
    memset(yes, 0, BANDSCOPE_NUM_SAMPLES*sizeof(double));
    memset(lastSamps, 0, BANDSCOPE_NUM_SAMPLES*sizeof(double)*64);

    bandScopeCurve.setStyle(QwtPlotCurve::Lines);
    /*
    bandScopeCurve.setCurveAttribute(QwtPlotCurve::Fitted, false);
    bandScopeCurve.setPen(m_traceColor, 1.0, Qt::SolidLine);
    bandScopeCurve.setItemAttribute(QwtPlotItem::Margins, false);
    bandScopeCurve.setRenderHint(QwtPlotItem::RenderAntialiased);
    bandScopeCurve.setPaintAttribute(QwtPlotCurve::ClipPolygons, true);
    bandScopeCurve.setPaintAttribute(QwtPlotCurve::FilterPoints, false);
    bandScopeCurve.setPaintAttribute(QwtPlotCurve::MinimizeMemory, false);
    bandScopeCurve.setPaintAttribute(QwtPlotCurve::ImageBuffer, false);
    */
    bandScopeCurve.setOrientation(Qt::Horizontal);
    bandScopeCurve.setRawSamples(xes, yes, BANDSCOPE_NUM_SAMPLES/2);
    bandScopeCurve.setItemAttribute(QwtPlotItem::AutoScale, false);
    bandScopeCurve.attach(ui->BandScopePlot);
    bandScopeCurve.setZ(0.0);
    //bandScopeCurve.setBaseline(-180);
    bandScopeCurve.setAxes(QwtPlot::xTop, QwtPlot::yRight);

    // marker for current frequency
    freqMarker.setLineStyle(QwtPlotMarker::VLine);
    freqMarker.attach(ui->BandScopePlot);
    freqMarker.setLinePen(Qt::darkCyan, (qreal)2.5, Qt::SolidLine);
    freqMarker.setYValue(0);
    freqMarker.setLabelAlignment(Qt::AlignBottom|Qt::AlignRight);
    freqMarker.setZ(-100.0);

    IFZone.setZ(-100.0);
    IFZone.setInterval(-200, 200); // dummy
    IFZone.setPen(Qt::darkRed, 1.0, Qt::SolidLine);
    IFZone.attach(ui->BandScopePlot);

    // use a timer instead of snapper
    sampTimer.stop();
    sampTimer.setInterval(0); // asap
    sampTimer.setTimerType(Qt::PreciseTimer);
    connect(&sampTimer, SIGNAL(timeout()), this, SLOT(plotBandScope()));
    // timer is started/stopped in events show, hide
    //sampTimer.start();
}

BandScope::~BandScope()
{
    fftw_free(time_buf);
    fftw_free(freq_buf);
    fftw_destroy_plan(plan);

    delete grid;
    delete bhf;
    delete ui;
}

const float expLog = 0.30103001; // log10(2), used for float log10 lut

// 4k uint16_t buffer (8k bytes)
// continuously update in RadioOzy w/ bandscope data
// on usb endpoint 84 from Mercury
void BandScope::plotBandScope()
{
    if (!r || !r->rozy)
        return;

    if (r->PlotDrawOptionsDock->bandScopeEnabled())
    {
        int16_t*p = r->rozy->bsBuf;

        yMin = 1000;
        yMax = -1000;

        // load the time buffer
        for (int i=0; i<BANDSCOPE_NUM_SAMPLES; i++) {
            time_buf[i] = (double)*p++ * bhf->f[i]; // endianess
        }

        // run the dft
        fftw_execute(plan);

        int numKElems = r->DisplayDock->getFilterCoeffs();

        if (numKElems) {
            for (int x=numKElems; x>0; x--)
                apex_memcpy(lastSamps[x], lastSamps[x-1], BANDSCOPE_NUM_SAMPLES * sizeof(double));
        }

        //double preAmpOffset = (r->HpsdrDock->getPreAmp() == hpsdr::Preamp_Off) ? 0.0 : 20.0;

        for(int i=0; i<BANDSCOPE_NUM_SAMPLES/2; i++) {
            // power of frequencies
            float val = (10.0 * log10 ( sqrt((freq_buf[i][0] * freq_buf[i][0]) + (freq_buf[i][1] * freq_buf[i][1])) ));
            for (int x=0; x<numKElems; x++)
                val = val + lastSamps[x][i];
            val = val / (numKElems+1);

            xes[i] = (double)i;
            yes[i] = val;
            lastSamps[0][i] = yes[i];

            if (i>20) { // skip leading filter skirt where misleading values are likely
                if (yes[i]>yMax)
                    yMax=yes[i];
                if (yes[i]<yMin)
                    yMin=yes[i];
            }
        }

        qlonglong freq = r->VfoDock->getFreq();
        //qlonglong freq = rozy->getRxFreq(rozy->curMercury);
        int sampRate = r->getRsdkSampleRate();
        double loIFFreq = (freq - (sampRate/2));
        double hiIFFreq = (freq + (sampRate/2));
        IFZone.setInterval(loIFFreq, hiIFFreq);

        //freqMarker.setLabel(label);
        freqMarker.setXValue(freq);

        bandScopeCurve.setStyle(QwtPlotCurve::Lines);
        bandScopeCurve.setPen(QPen(m_traceColor));
        ui->BandScopePlot->setAxisScale(QwtPlot::yRight, 45, 70, 0);
        //ui->BandScopePlot->setAxisScale(QwtPlot::yLeft, yMin, yMax, 0);

        //zoom->blockSignals(true);
    }
    else
    {
        bandScopeCurve.setStyle(QwtPlotCurve::NoCurve);
    }

    // replot
    ui->BandScopePlot->replot();
}

double BandScope::smooth() const
{
    return m_smooth;
}

void BandScope::setSmooth(int smooth)
{
    setSmooth((double)smooth);
}

void BandScope::setSmooth(double smooth)
{
    m_smooth = smooth;


    ui->BandScopePlot->setAxisScale(QwtPlot::xTop, 0, BANDSCOPE_NUM_SAMPLES/2, 0);
    bandScopeCurve.setRawSamples(xes, yes, BANDSCOPE_NUM_SAMPLES/2);
}

QColor BandScope::traceColor() const
{
    return m_traceColor;
}

void BandScope::setTraceColor(const QColor &traceColor)
{
    m_traceColor = traceColor;
    //grid->setColor(m_traceColor);
    yMax = -200.0;
    yMin =  200.0;
    //emit bandScopeColorChanged(m_traceColor);
}

bool BandScope::bandScopeEnabled() const
{
    return m_bandScopeEnabled;
}

void BandScope::setBandScopeEnabled(bool bandScopeEnabled)
{
    m_bandScopeEnabled = bandScopeEnabled;
    yMax = -200.0;
    yMin =  200.0;

    if (m_bandScopeEnabled)
        ui->BandScopePlot->setVisible(true);
    else
        ui->BandScopePlot->setVisible(false);
}

bool BandScope::event(QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        QPoint pos = dynamic_cast<QMouseEvent*>(event)->pos();
        m_activeDragXPos = pos.x();
        QwtScaleMap xmap = ui->BandScopePlot->canvasMap(QwtPlot::xBottom);
        double v1 = xmap.invTransform(pos.x());
        double tr = (double)r->getRsdkTuneRate();
        v1 -= fmod(v1, tr);
        qlonglong f = (qlonglong)v1;
        emit freqChanged(f);
    }
    else if (event->type() == QEvent::MouseButtonRelease)
    {
        m_activeDragXPos = 0.0;
    }
    else if (event->type() == QEvent::MouseMove)
    {
        QPoint pos = dynamic_cast<QMouseEvent*>(event)->pos();
        if (m_activeDragXPos)
        {
            m_activeDragXPos = pos.x();
            QwtScaleMap xmap = ui->BandScopePlot->canvasMap(QwtPlot::xBottom);
            double v1 = xmap.invTransform(pos.x());
            double tr = (double)r->getRsdkTuneRate();
            v1 -= fmod(v1, tr);
            qlonglong f = (qlonglong)v1;
            //r->VfoDock->setFreq(f, true);
            emit freqChanged(f);
        }
    }
    else if (event->type() == QEvent::Show)
    {
        sampTimer.start();

    }
    else if (event->type() == QEvent::Hide)
    {
        sampTimer.stop();
    }
    else if (event->type() == QEvent::Resize)
    {
        // convenient place to reset the traces
        memset(xes, 0, BANDSCOPE_NUM_SAMPLES*sizeof(double));
        memset(yes, 0, BANDSCOPE_NUM_SAMPLES*sizeof(double));
        memset(lastSamps, 0, BANDSCOPE_NUM_SAMPLES*sizeof(double)*64);
    }

    return QWidget::event(event);
}
