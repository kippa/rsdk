#include <QAbstractButton>

#include "cwdockwidget.h"
#include "ui_cwdockwidget.h"

CwDockWidget::CwDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::CwDockWidget),
    m_freq(880),
    m_speed(23),
    m_keysReversed(false),
    m_mode(1), // Mode A TODO this should have defines for modes
    m_weight(55),
    m_spacing(true),
    m_sideToneVolume(30),
    m_pttDelay(30), // nominal, careful in "Page" mode, too short and keydown briefly on wrong frequency
    m_hangTime(350), // milliseconds
    m_keyerEnabled(false)
{
    ui->setupUi(this);

    connect(ui->keyerModeBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(selectKeyerMode(QAbstractButton*)));
    connect(ui->hangTimeSpinBox, SIGNAL(editingFinished()), this, SLOT(hangTimeEditingFinished()));

    setFreq(m_freq);
    setSpeed(m_speed);
    setKeysReversed(m_keysReversed);
    setMode(m_mode);
    setWeight(m_weight);
    setSpacing(m_spacing);
    setSideToneVolume(m_sideToneVolume);
    setPttDelay(m_pttDelay);
    setHangTime(m_hangTime);
    setKeyerEnabled(m_keyerEnabled);
}

CwDockWidget::~CwDockWidget()
{
    delete ui;
}

void CwDockWidget::start()
{
    emit cwFreqChanged(m_freq);
    emit cwSpeedChanged(m_speed);
    emit cwKeysReversedChanged(m_keysReversed);
    emit cwModeChanged(m_mode);
    emit cwWeightChanged(m_weight);
    emit cwSpacingChanged(m_spacing);
    emit cwSideToneVolumeChanged(m_sideToneVolume);
    emit cwPttDelayChanged(m_pttDelay);
    emit cwHangTimeChanged(m_hangTime);
    emit cwKeyerEnabledChanged(m_keyerEnabled);
    emit dockChanged(this, Qt::BottomDockWidgetArea);
}

void CwDockWidget::setOpacity(bool b)
{
    if (b)
        setWindowOpacity(0.80);
}

double CwDockWidget::freq() const
{
    return m_freq;
}

void CwDockWidget::setFreq(const double &freq)
{
    m_freq = freq;

    if (m_freq != ui->pitchSpinBox->value())
    {
        bool pstate = ui->pitchSpinBox->blockSignals(true);
        ui->pitchSpinBox->setValue(m_freq);
        ui->pitchSpinBox->blockSignals(pstate);
    }

    emit cwFreqChanged(m_freq);
}

void CwDockWidget::on_hangTimeSpinBox_valueChanged(int arg)
{
    setHangTime((unsigned int)arg);
}

void CwDockWidget::on_pitchSpinBox_valueChanged(int arg)
{
    setFreq((const qlonglong) arg);
}

unsigned int CwDockWidget::speed() const
{
    return m_speed;
}

void CwDockWidget::setSpeed(const unsigned int &speed)
{
    m_speed = speed;

    if (m_speed != (unsigned int)ui->speedSpinBox->value())
    {
        bool pstate = ui->speedSpinBox->blockSignals(true);
        ui->speedSpinBox->setValue(m_speed);
        ui->speedSpinBox->blockSignals(pstate);
    }

    emit cwSpeedChanged(m_speed);
}

void CwDockWidget::on_speedSpinBox_valueChanged(int arg)
{
    setSpeed((unsigned int) arg);
}

void CwDockWidget::selectKeyerMode(QAbstractButton *btn)
{
    QToolButton *pb = qobject_cast<QToolButton *>(btn);
    if (pb == ui->modeAPB)
    {
        m_mode = 0x01;
    }
    else if(pb == ui->modeBPB)
    {
        m_mode = 0x02;
    }
    else if (pb == ui->modeSPB) // straight key
    {
        m_mode = 0x00;
    }
    emit cwModeChanged(m_mode);
}

void CwDockWidget::on_weightSpinBox_valueChanged(int arg)
{
   setWeight((unsigned int)arg);
}

void CwDockWidget::on_pttDelaySpinBox_valueChanged(int arg)
{
    setPttDelay((unsigned int )arg);
}

void CwDockWidget::hangTimeEditingFinished()
{
    QString hangTime = ui->hangTimeSpinBox->text();

    double cval = convStringToVal(hangTime);
    if (cval)
    {
        setHangTime((unsigned int)cval);
    }
}

double CwDockWidget::convStringToVal(QString &str)
{
    double retval = 0.0;

    if (str.size())
    {
        // remove prefix, suffix
        QRegExp rx("[a-zA-Z]|,");
        str.remove(rx);

        bool ok = false;
        double val = QLocale(QLocale::English).toDouble(str, &ok);

        if (ok)
        {
            retval = val;
        }
    }

    return retval;
}

unsigned short CwDockWidget::hangTime() const
{
    return m_hangTime;
}

void CwDockWidget::setHangTime(unsigned int hangTime)
{
    m_hangTime = hangTime % 1024; // range limit keyer hangTime to 0 - 1024 milliseconds
    if (m_hangTime != ui->hangTimeSpinBox->value())
    {
        bool pstate = ui->hangTimeSpinBox->blockSignals(true);
        ui->hangTimeSpinBox->setValue(m_hangTime);
        ui->hangTimeSpinBox->blockSignals(pstate);
    }
    emit cwHangTimeChanged(m_hangTime);
}

unsigned int CwDockWidget::pttDelay() const
{
    return m_pttDelay;
}

void CwDockWidget::setPttDelay(unsigned int pttDelay)
{
    m_pttDelay = pttDelay;
    if (m_pttDelay != (uint)ui->pttDelaySpinBox->value())
    {
        bool pstate = ui->pttDelaySpinBox->blockSignals(true);
        ui->pttDelaySpinBox->setValue(m_pttDelay);
        ui->pttDelaySpinBox->blockSignals(pstate);
    }
    emit cwPttDelayChanged(m_pttDelay);
}

unsigned int CwDockWidget::sideToneVolume() const
{
    return m_sideToneVolume;
}

void CwDockWidget::setSideToneVolume(unsigned int sideToneVolume)
{
    m_sideToneVolume = sideToneVolume % 128; // range limit the side tone volume to 0 - 127
    //if (m_sideToneVolume != ui->sideToneVolSlider->value())
    {
        bool pstate = ui->sideToneVolSlider->blockSignals(true);
        ui->sideToneVolSlider->setValue(m_sideToneVolume);
        ui->sideToneVolSlider->blockSignals(pstate);
    }
    emit cwSideToneVolumeChanged(m_sideToneVolume);
}

bool CwDockWidget::spacing() const
{
    return m_spacing;
}

void CwDockWidget::setSpacing(bool spacing)
{
    m_spacing = spacing;
    bool pstate = ui->spacingPB->blockSignals(true);
    if (m_spacing)
        ui->spacingPB->setChecked(true);
    else
        ui->spacingPB->setChecked(false);
    ui->spacingPB->blockSignals(pstate);
    emit cwSpacingChanged(m_spacing);
}

unsigned int CwDockWidget::getWeight() const
{
    return m_weight;
}

void CwDockWidget::setWeight(unsigned int weight)
{
    m_weight = weight % 100; // range limit keyer weight to 0 - 100 percent
    if (m_weight != (uint)ui->weightSpinBox->value())
    {
        bool pstate = ui->weightSpinBox->blockSignals(true);
        ui->weightSpinBox->setValue(m_weight);
        ui->weightSpinBox->blockSignals(pstate);
    }
    emit cwWeightChanged(m_weight);
}

unsigned int CwDockWidget::mode() const
{
    return m_mode;
}

void CwDockWidget::setMode(unsigned int mode)
{
    m_mode = mode % 3; // range limit modes are 0 = straight key, 1 = Mode A, 2 = Mode B

    // prevent endless loop
    bool pstate = ui->keyerModeBtnGrp->blockSignals(true);

    if (m_mode == 0)
        ui->modeSPB->setChecked(true);
    else if (m_mode == 2)
        ui->modeBPB->setChecked(true);
    else // if (m_mode == 1) default to Mode A
        ui->modeAPB->setChecked(true);

    // and unblock for use
    ui->keyerModeBtnGrp->blockSignals(pstate);
    emit cwModeChanged(m_mode);
}

bool CwDockWidget::keysReversed() const
{
    return m_keysReversed;
}

void CwDockWidget::setKeysReversed(bool keysReversed)
{
    m_keysReversed = keysReversed;
    bool pstate = ui->reversePB->blockSignals(true);
    if (m_keysReversed)
        ui->reversePB->setChecked(true);
    else
        ui->reversePB->setChecked(false);
    ui->reversePB->blockSignals(pstate);
    emit cwKeysReversedChanged(m_keysReversed);
}

void CwDockWidget::on_spacingPB_clicked(bool checked)
{
    setSpacing(checked);
}

void CwDockWidget::on_reversePB_clicked(bool checked)
{
    setKeysReversed(checked);
}

void CwDockWidget::on_sideToneVolSlider_valueChanged(int value)
{
    setSideToneVolume((unsigned int)value);
}

void CwDockWidget::on_enableTB_clicked(bool checked)
{
    setKeyerEnabled(checked);
}

bool CwDockWidget::keyerEnabled() const
{
    return m_keyerEnabled;
}

void CwDockWidget::setKeyerEnabled(bool keyerEnabled)
{
    m_keyerEnabled = keyerEnabled;
    bool pstate = ui->enableTB->blockSignals(true);
    ui->enableTB->setChecked(m_keyerEnabled);
    ui->enableTB->blockSignals(pstate);
    emit cwKeyerEnabledChanged(m_keyerEnabled);
}
