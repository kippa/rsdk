#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include </usr/include/libusb-1.0/libusb.h>
#include <errno.h>

#include "common.h"
#include "exception.h"
#include "radiofile.h"
#include "radiousb.h"
#include "hpsdr.h"

RadioUsb::RadioUsb(const char * const name_prm, ASYNCCB cb_prm, void *cbdata_prm, int VID, int PID) : RadioFile(name_prm, cb_prm, cbdata_prm),
    rxBufs(), txBufs(), bsBufs(), ctlBufs(),
    devHandle(NULL), usbContext(NULL), usbCtl(NULL), usbXfr(NULL), bufcnt(0),
    usbInitialized(false), event_handler_thread_id()
{
    int rc;
	if ((rc = libusb_init(&usbContext))<0)
	{
        THROW_EXCEPTION (ERROR_LEVEL, rc, "libusb_init failed rc %d\n", rc);
    }

    //libusb_set_debug(usbContext, LIBUSB_LOG_LEVEL_NONE);
    libusb_set_debug(usbContext, LIBUSB_LOG_LEVEL_INFO);

    if ((rc = libusbOpenDev(VID, PID))<0)
	{
        if (rc == -2)
        {
            fprintf(stderr, "Open USB device VID=0x%x PID=0x%x failed, Device not found!\n", VID, PID);
            fprintf(stderr, "Try to initialize Ozy at VID 0x%x PID 0x%x\n", VID, PID);

        }
        //else
        {
            THROW_EXCEPTION (ERROR_LEVEL, rc, "libusbOpenDev failed rc %d\n", rc);
        }
    }

    //if (!rc)
    //    usbInitialized=true;
    event_handler_thread_request_kill=0;
    event_handler_thread_request_suspend=0;
    pthread_mutex_init(&event_handler_thread_lock, NULL);

    // for testing only
    libusb_set_debug(usbContext, LIBUSB_LOG_LEVEL_DEBUG);

    // some debug
    //libusb_device *dev = findDev(usbContext, VID, PID);
    //int speed = libusb_get_device_speed(dev);
    //fprintf(stderr, "%s: speed %d\n", __func__, speed);
    //int max_packet_size = libusb_get_max_packet_size(dev, 0x86);
    //fprintf(stderr, "%s: max packet size %d\n", __func__, max_packet_size);

    if ((rc=pthread_create(&event_handler_thread_id,NULL,libusbThreadEntry,this))!=0)
	{
        fprintf(stderr, "%s: Unable to start thread handler (%d)\n", __func__, rc);
    }
    pthread_setname_np(event_handler_thread_id, "rsdk libusbEvent");

    // synchronize the pthread start?
    usbInitialized = true;
}

RadioUsb::~RadioUsb()
{
    int rc;

    /*
    if (jclient)
        delete jclient;
    */
    pthread_mutex_lock(&event_handler_thread_lock);
    event_handler_thread_request_suspend = 1;
    pthread_mutex_unlock(&event_handler_thread_lock);


    pthread_mutex_lock(&event_handler_thread_lock);
    event_handler_thread_request_kill = 1;
    pthread_mutex_unlock(&event_handler_thread_lock);

    pthread_join(event_handler_thread_id, NULL);

    libusbCloseDev();

}

void *RadioUsb::libusbThreadEntry(void *this_prm)
{
	RadioUsb *rbu=(RadioUsb *)this_prm;
    int kill = 0;
    int suspend = 0;
    struct timeval to = {0, 1};
    int immRet = 0;

    while(!kill)
    {
        if(rbu->usbInitialized)
        {
            // non-blocking w/ to = {0, 0}
            int rc=libusb_handle_events_timeout_completed(rbu->usbContext, &to, &immRet);
            if (rc)
            {
                //request_exit(rc);
                fprintf(stderr, "%s: pthread return error %s\n", __func__, libusb_error_name(rc));
            }
        }
        my_usleep(10);

        pthread_mutex_lock(&rbu->event_handler_thread_lock);
        kill = rbu->event_handler_thread_request_kill;
        suspend = rbu->event_handler_thread_request_suspend;
        pthread_mutex_unlock(&rbu->event_handler_thread_lock);
	}

    return NULL;
}

int RadioUsb::Read(unsigned char *buf_prm, int64 len_prm, bool wait_prm)
{
    fprintf(stderr, "%s: buf_prm %p len_prm %lld wait_prm %d\n", __func__, buf_prm, len_prm, wait_prm);
    RadioFile::Read(buf_prm, len_prm, wait_prm);
    return 0;
}

int RadioUsb::Write(unsigned char *buf_prm, int64 len_prm, bool wait_prm)
{
    //fprintf(stderr, "%s: buf_prm %p len_prm %lld wait_prm %d\n", __func__, buf_prm, len_prm, wait_prm);
    RadioFile::Write(buf_prm, len_prm, wait_prm);
    return 0;
}

int RadioUsb::isDev(libusb_device *dev, int vid, int pid)
{
    struct libusb_device_descriptor d;
    libusb_get_device_descriptor(dev, &d);
    if(d.idVendor == vid && d.idProduct == pid)
        return 1;
    else
        return 0;
}

libusb_device *RadioUsb::findDev(libusb_context *c, int vid, int pid)
{
    int i, cnt;
    libusb_device **list;
    libusb_device *dev=NULL;

    cnt = libusb_get_device_list(c, &list);
    if(cnt <= 0)
    {
        fprintf(stderr, "%s: No USB device VID 0x%x PID 0x%x found!\n", __func__, vid, pid);
    }
    else
    {
        for (i=0; i<cnt; i++)
        {
            dev = list[i];
            if (isDev(dev, vid, pid))
            {
                fprintf(stderr, "USB device VID 0x%x PID 0x%x found.\n", vid, pid);
                break;
            }
        }
    }
    return dev;
}

int RadioUsb::libusbOpenDev(int VID, int PID)
{
    int rc;

    rxBufs = usb_create_buffer_pool(NUM_RX_BUFS, BUF_SIZE, "rxBufs", OZY_EP_RX_DATA);
    if (!rxBufs)
    {
        THROW_EXCEPTION(ERROR_LEVEL, 0, "rxBufs buffer allocation failed\n");
    }

    txBufs = usb_create_buffer_pool(NUM_TX_BUFS, BUF_SIZE, "txBufs", OZY_EP_TX_DATA);
    if (!txBufs)
    {
        THROW_EXCEPTION(ERROR_LEVEL, 0, "txBufs buffer allocation failed\n");
    }

    bsBufs = usb_create_buffer_pool(NUM_BS_BUFS, BS_BUF_SIZE, "bsBufs", OZY_EP_RX_BS_DATA);
    if (!bsBufs)
    {
        THROW_EXCEPTION(ERROR_LEVEL, 0, "bsBufs buffer allocation failed\n");
    }

    ctlBufs = usb_create_buffer_pool(NUM_CTL_BUFS, BUF_SIZE, "ctlBufs", 0); // control packets use these
    if (!ctlBufs)
    {
        THROW_EXCEPTION(ERROR_LEVEL, 0, "ctlBufs buffer allocation failed\n");
    }

	if ((rc = libusb_init(&usbContext))<0)
	{
        THROW_EXCEPTION(ERROR_LEVEL, 0, "libusb initialization failed, rc %d\n", rc);
    }

    // please install 30-ozy.rules into /etc/udev/rules.d/ so premissions are not a problem
    fprintf(stderr, "%s: Opening USB device VID 0x%4.4x PID 0x%4.4x\n", __func__, VID, PID);
	devHandle=libusb_open_device_with_vid_pid(usbContext, VID, PID); 
	
    if (devHandle==NULL)
	{
        //THROW_EXCEPTION(ERROR_LEVEL, 0, "libusb libusb_open_device_with_vid_pid failed\n");
        return 0;
    }

	if ((rc = libusb_claim_interface(devHandle,0))<0)
	{
        libusb_close(devHandle);
        fprintf(stderr, "%s:  libusb_claim_interface failed! rc %d\n", __func__, rc);
        return -5;
	}

	if (!(usbCtl = libusb_alloc_transfer(0)))
	{
		fprintf(stderr, "%s: libusb_alloc_transfer usb_ctl failed!\n", __func__);
		libusbCloseDev();
        return -6;
	}

    if (!(usbXfr = libusb_alloc_transfer(1)))
	{
		fprintf(stderr, "%s: libusb_alloc_transfer usb_xfr failed!\n", __func__);
		libusbCloseDev();
        return -7;
	}

	return 0;
}

void RadioUsb::libusbCloseDev(void)
{
    //fprintf(stderr, "%s:\n", __func__);

    usbInitialized=false;

    if (usbCtl)
		libusb_free_transfer(usbCtl);
	
	if (devHandle)
		libusb_close(devHandle);
	
	if (usbContext)
        libusb_exit(usbContext);

    usb_destroy_buffer_pool(rxBufs);
    usb_destroy_buffer_pool(txBufs);
    usb_destroy_buffer_pool(bsBufs);
    usb_destroy_buffer_pool(ctlBufs);

	//request_exit(); shutdown
	//pthread_mutex_lock(&exit_cond_lock);
	//while(!do_exit)
	//	pthread_cond_wait(&exit_cond, &exit_cond_lock);
	//pthread_mutex_unlock(&exit_cond_lock);
    //pthread_join(usb_event_reaper_thread_id, NULL);
}
