#include <assert.h>

#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include </usr/include/complex.h>

#include <iostream>

// DttSP exports
#include "dttsp.h"

#include "apex_memmove.h"

#include "buffer.h"
#include "radioozy.h"
#include "hpsdr.h"
#include "rsdk.h"

// TODO FIXME
extern rsdk *r;

// track transmit state
static bool xmit = false;
static bool lastXmit = false;

struct timeval ozy_start_time, ozy_end_time;

static int output_buffer_ndx=0;
static struct timeval thisTime, lastTime;

unsigned long long packetCount;

inline
long elapsedTime()
{
    gettimeofday(&thisTime, 0);
    long diff = ((thisTime.tv_sec-lastTime.tv_sec)*100000) + (thisTime.tv_usec - lastTime.tv_usec);

    lastTime.tv_sec = thisTime.tv_sec;
    lastTime.tv_usec = thisTime.tv_usec;

    return diff;
}

//extern int libusbOzyXferDev(int ep, void* buffer, int buffersize, void *user_data);
int hpsdrProcessBuffer(void *cbdata_prm, unsigned char *buf_prm, int int_prm)
{
    RadioOzy *rf=(RadioOzy *)cbdata_prm;
    OzyRx1Packet_t *p=(OzyRx1Packet_t *)buf_prm;

    if(int_prm == 0x86)
    {
        if(p->sync[0]==OZY_SYNC&&p->sync[1]==OZY_SYNC&&p->sync[2]==OZY_SYNC)
        {
            /*
           if (0)
           {
               unsigned long long diff;
               gettimeofday(&ozy_end_time, nullptr);
               diff = timeval_diff(&ozy_start_time, &ozy_end_time);
               gettimeofday(&ozy_start_time, nullptr);
               fprintf(stderr,"OZY_SYNC delta_t %7lld packetCount %d sampleCountTotal %d\n",
                       diff, packetCount, rf->sampleCountTotal);
               packetCount++;
           }
           */

            //commandprocessingC0...C4

            // the command
            unsigned char cmd = p->cmd[0]&OZY_C0_RECV_MASK;

            rf->ptt= (p->cmd[0]&OZY_BIT_PTT_MASK)?1:0;
            rf->dot= (p->cmd[0]&OZY_BIT_DOT_MASK)?1:0;
            rf->dash=(p->cmd[0]&OZY_BIT_DASH_MASK)?1:0;

            if (rf->ptt || rf->dot || rf->dash) {
                rf->setPacketXmit(true);
            } else {
                rf->setPacketXmit(false);
            }

            switch(cmd)
            {
            case 0x00:
            {
                rf->mercurySN=p->cmd[2];
                rf->penelopeSN=p->cmd[3];
                rf->ozySN=p->cmd[4];
            }
                break;
            case 0x08:
            {
                // 12 bits
                 uint16_t ain5 =(uint16_t)((p->cmd[1]<<8)+p->cmd[2]); // AIN5
                rf->ain5=ain5;
                rf->penelopeFwdPower=ain5;

                 uint16_t ain1 =(uint16_t)((p->cmd[3]<<8)+p->cmd[4]); // AIN1
                rf->ain1=ain1;
                rf->alexFwdPower=ain1;
                break;
            }
            case 0x10:
            {
                // 12 bits
                if(rf->getTuneMode() || rf->getPacketXmit() || rf->getGuiXmit())
                {
                     uint16_t alxr = (int16_t)((p->cmd[1]<<8)+p->cmd[2]);
                    rf->ain2 = alxr;
                    rf->alexRevPower = alxr;
                     uint16_t ain3 = (int16_t)((p->cmd[3]<<8)+p->cmd[4]);
                    rf->ain3 = ain3;
                }
                else
                {
                    rf->ain2=(int16_t)((p->cmd[1]<<8)+p->cmd[2]);
                    rf->ain3=(int16_t)((p->cmd[3]<<8)+p->cmd[4]);
                }
                break;
            }
            case 0x18:
            {
                // 12 bits
                rf->ain4=(int16_t)((p->cmd[1]<<8)+p->cmd[2]);
                rf->ain6=(int16_t)((p->cmd[3]<<8)+p->cmd[4]);
                break;
            }
            case 0x20:
            {
                rf->lt2208Overflow[0]=p->cmd[1]&OZY_BIT_LT2208_OVFLW;
                rf->mercurySoftVer[0]=p->cmd[1]>>1;
                rf->lt2208Overflow[1]=p->cmd[2]&OZY_BIT_LT2208_OVFLW;
                rf->mercurySoftVer[1]=p->cmd[2]>>1;
                /*
                rf->lt2208Overflow[2]=p->cmd[3]&OZY_BIT_LT2208_OVFLW;
                rf->mercurySoftVer[2]=p->cmd[3]>>1;
                rf->lt2208Overflow[3]=p->cmd[4]&OZY_BIT_LT2208_OVFLW;
                rf->mercurySoftVer[3]=p->cmd[4]>>1;
                */
                break;
            }
            default:
                fprintf(stderr, "%s: Unknown protocol C0 0x%2.2x\n", __func__, p->cmd[0]);
            }

            if(rf->getTuneMode() || rf->getPacketXmit() || rf->getGuiXmit())
            {
                // calculate the forward and reflected power from alex
                double fwd = rf->alexFwdPower;
                double rev = rf->alexRevPower;
                //double r = rev/fwd;
                double r = sqrt(rev/fwd);

                // check for NAN, don't use -ffast-math, g++ screws this up
                // we want to prevent NAN values from being used
                // yeah, this is the way to check for NaN
                if (r == r)
                {
                    double vswr = (1.0+r)/(1.0-r);
                    if (vswr == vswr)
                        rf->vswr = vswr;
                }
            } else {
                // don't leave hanging values
                rf->vswr = 1.0;
            }

            // retrieve bipolar 24/16-bit samples samples to floating point buffer
            int i, q, m;
            float re, im;

            for (int x=0; x<504;)
            {
                for (int z=0; z<rf->numRx; z++)
                {
                    // I/Q float conversion
                    i =(int)(  (signed char)p->d[x++])<<16;
                    i+=(int)((unsigned char)p->d[x++])<<8;
                    i+=(int)((unsigned char)p->d[x++]);

                    q =(int)(  (signed char)p->d[x++])<<16;
                    q+=(int)((unsigned char)p->d[x++])<<8;
                    q+=(int)((unsigned char)p->d[x++]);

                    // convert to float -1.0 -> 0.0 -> 1.0
                    re = ((float)i)/8388607.0; // 24 bit samples
                    im = ((float)q)/8388607.0;

                    rf->I_in[z][rf->sampleCount] = re;
                    rf->Q_in[z][rf->sampleCount] = im;
                }

                // theMIC samples are converted and stored in the Mic buffers
                // TODO make this selectable for routing purposes
                // possible to route other sources into the mic feed
                // must always count x++, so don't hide this in the rf->mode check
                m =(int)(  (signed char)p->d[x++])<<8;
                m+=(int)((unsigned char)p->d[x++]);
                //m*=rf->micBoost;

                if ((rf->mode == CWU) || (rf->mode == CWL))
                    m = 0.0;

                // mic splits to both channels for the time being
                // gain adjust via the front panel L and R gain knobs
                rf->Mic_L_in[rf->sampleCount] = ((float)m/32767.0)*rf->txLeftGain; // 16 bit samples
                rf->Mic_R_in[rf->sampleCount] = ((float)m/32767.0)*rf->txLeftGain; // 16 bit samples

                rf->sampleCount++;
                rf->sampleCountTotal++;

                /*
                if(rf->sampleCountTotal>=rf->sampleRate)
                {
                    unsigned long long diff;
                    gettimeofday(&ozy_end_time, nullptr);
                    diff = timeval_diff(&ozy_start_time, &ozy_end_time);
                    gettimeofday(&ozy_start_time, nullptr);
                    fprintf(stderr,"delta_t %7lld packetCount %d sampleCount %d sampleCountTotal %d\n",
                            diff, packetCount, rf->sampleCount, rf->sampleCountTotal);
                    rf->sampleCountTotal=0;
                    packetCount  = 0;
                }
                */

                /*
                if(rf->recFd)
                {
                    rf->rfloats[rf->recSampleCount++]=ifloat;
                    rf->rfloats[rf->recSampleCount++]=qfloat;
                    if(rf->recSampleCount>=(BUF_SIZE*2))
                    {
                        intnum=write(fd,ufloats,recSampleCount);
                        rf->recSampleCount=0;
                    }
                }
                if(rf->udpFd)
                {
                    rf->ufloats[rf->udpSampleCount++]=ifloat;
                    rf->ufloats[rf->udpSampleCount++]=qfloat;
                    if(rf->udpSampleCount>=(BUF_SIZE*2))
                    {
                        intnum=send_udp(udp_buffer,udpSampleCount*sizeof(float));
                        rf->udpSampleCount=0;
                    }
                }
                */

                if(rf->sampleCount >= rf->numSamples)
                {
                     int sri = rf->sampleRate/48000;

                    rf->sampleCount = 0;

                    // this is an attempt to reduce turn-around latency by clearing the outbound USB queue
                    if(rf->getTuneMode() || rf->getPacketXmit() || rf->getGuiXmit()) {
                        xmit = true;
                        if (lastXmit != xmit)
                            libusbOzyClearQueue(rf->txBufs);
                    } else {
                        xmit = false;
                        if (lastXmit != xmit)
                            libusbOzyClearQueue(rf->rxBufs);
                    }

                    if (lastXmit != xmit)
                        lastXmit = xmit;

                    // Run all receivers
                    //for (int z=0; z<rf->numRx; z++)
                        Audio_Callback(rf->I_in[0], rf->Q_in[0], rf->Aud_L_out[0], rf->Aud_R_out[0], rf->numSamples, 0); // rx1, Mercury card #1

                    // Transmitter
                    // load the MIC buffer with audio coming from jack
                    if (rf->jackInEnabled) {
                        static float fbl[1024];
                        static float fbr[1024];

                        const int sampSize = sizeof(float);
                        int numSampsToFetch = rf->numSamples / sri;
                        int numBytesToFetch = numSampsToFetch * sampSize;

                        rf->jackConn->fetchRingbuffer((char *)&fbl, numBytesToFetch, rf->jackConn->rb_inL);
                        rf->jackConn->fetchRingbuffer((char *)&fbr, numBytesToFetch, rf->jackConn->rb_inR);

                        // convert the sample rate and
                        // run the gain controls
                        for(int j=0, z=0; j<rf->numSamples; j++) {
                            rf->Mic_L_in[j] = fbl[z] * rf->jackInLevel * rf->txLeftGain;
                            rf->Mic_R_in[j] = fbr[z] * rf->jackInLevel * rf->txLeftGain;
                            //rf->Mic_L_in[j] = fbl[z] * rf->txLeftGain;
                            //rf->Mic_R_in[j] = fbr[z] * rf->txLeftGain;
                            if (!(j%sri))
                                z++;
                        }
                    }

                    // generate the I/Q for the transmitter
                    Audio_Callback(rf->Mic_L_in, rf->Mic_R_in, rf->I_out, rf->Q_out, rf->numSamples, 1); // tx1, Penelope card #1

                    // xmit gains
                    rf->gain=1.0;

                    // grab the side tone level and scale the local audio and/or the MIC to the hpsdr
                    float sideToneLevel = (float)rf->keyerStvolume / 256.0;

                    // play the audio via jack output if so desired
                    if (rf->jackConn) {
                        // don't realloc these
                        static float out_fbl[1024];
                        static float out_fbr[1024];
                        static bool soutInit;
                        int rcnt=0;

                        if (rf->jackOutEnabled) {
                            soutInit = false;
                            // now convert to/from 48khz using sri
                            for(int j=0; j<rf->numSamples; j+=sri) {
                                if(rf->getTuneMode() || rf->getPacketXmit() || rf->getGuiXmit()) {
                                    // if we're transmitting, we want to hear our  mic audio w/ level control
                                    out_fbl[rcnt] = rf->Aud_L_out[rf->curMercury][j] * sideToneLevel;
                                    out_fbr[rcnt] = rf->Aud_R_out[rf->curMercury][j] * sideToneLevel;
                                } else {
                                    // else, if we're receiving
                                    // and jackOut is enabled, send our demodulated audio to jack
                                    out_fbl[rcnt] = rf->Aud_L_out[rf->curMercury][j] * rf->getJackOutLevel();
                                    out_fbr[rcnt] = rf->Aud_R_out[rf->curMercury][j] * rf->getJackOutLevel();
                                }
                                rcnt++;
                            }
                            int len = rcnt * sizeof(float);
                            rf->jackConn->fillRingbuffer((const char *)out_fbl, len, rf->jackConn->rb_outL);
                            rf->jackConn->fillRingbuffer((const char *)out_fbr, len, rf->jackConn->rb_outR);

                        } else {
                            // run this only once when jackOutEnable goes false
                            // to quiet the jack out audio with silence (zeroes)
                            if (!soutInit) {
                                static const unsigned int fullSize = 1024 * sizeof(float);

                                memset((void*)&out_fbl[rcnt], 0, fullSize);
                                memset((void*)&out_fbr[rcnt], 0, fullSize);

                                rf->jackConn->fillRingbuffer((const char *)out_fbl, fullSize, rf->jackConn->rb_outL);
                                rf->jackConn->fillRingbuffer((const char *)out_fbr, fullSize, rf->jackConn->rb_outR);

                                soutInit = true;
                            }
                        }
                    }

                    // a debug timer
                    /*
                    {
                        long diff = elapsedTime();
                        qInfo() << "ACB diff " << diff/1000.0 << "mSec.";
                    }
                    */

                    // this can go into a seperate thread?
                    // need to lock it in case of overrun?a
                    // it's possible to take too long sending this 1K samples back?
                    // careful for rf-> values as they may change, keep a local copy
                    // to run them without being affected by further operation
                    // copy constructor for radioozy? nooo, way too big for that, nocopy noassign
                    // buffers are a problem, they're large, can't deep copy
                    // maybe use a local buffer insterad of radioozy for I/Qout, MicLR
                    // so how to use the values of rf-> ?

                    static int16_t left_rx_sample, right_rx_sample;
                    static int16_t left_tx_sample, right_tx_sample;

                    for(int j=0; j<rf->numSamples; j+=sri)
                    {
                        //left_rx_sample = right_rx_sample = 0;
                        //left_tx_sample = right_tx_sample = 0;

                        if(rf->getTuneMode() || rf->getPacketXmit() || rf->getGuiXmit())
                        {
                            // xmit
                            // MIC is returned for DAC
                            left_rx_sample=(int16_t)((rf->Mic_L_in[j]*32767.0) * sideToneLevel);
                            right_rx_sample=(int16_t)((rf->Mic_R_in[j]*32767.0) * sideToneLevel);

                            // xmit the DSP return I/Q samples
                            left_tx_sample=(int16_t)(rf->I_out[j]*32767.0*rf->gain);
                            right_tx_sample=(int16_t)(rf->Q_out[j]*32767.0*rf->gain);

                        } else {

                            // NOT xmit
                            // processed audio out to hpsdr
                            left_rx_sample=(int16_t)(rf->Aud_L_out[rf->curMercury][j]*32767.0*r->AudioDock->getVol());
                            right_rx_sample=(int16_t)(rf->Aud_R_out[rf->curMercury][j]*32767.0*r->AudioDock->getVol());

                            // nothing to xmit, but it is possible to do so, hmmm, full duplex TODO FEATURE
                            left_tx_sample=0;
                            right_tx_sample=0;

                        }

                        // grab a new return USB buffer headed to the radio
                        if(rf->txBuf==nullptr)
                        {
                            rf->txBuf = usb_get_buffer(rf->txBufs, rf);
                            if(!rf->txBuf)
                            {
                                std::cout << __func__ << ": No TX buffer " << endl << flush;
                                return -1;
                            }
                        }

                        OzyRx1Packet_t *t=(OzyRx1Packet_t *)rf->txBuf->buf;

                        t->d[output_buffer_ndx++]=left_rx_sample>>8;
                        t->d[output_buffer_ndx++]=left_rx_sample;
                        t->d[output_buffer_ndx++]=right_rx_sample>>8;
                        t->d[output_buffer_ndx++]=right_rx_sample;

                        t->d[output_buffer_ndx++]=left_tx_sample>>8;
                        t->d[output_buffer_ndx++]=left_tx_sample;
                        t->d[output_buffer_ndx++]=right_tx_sample>>8;
                        t->d[output_buffer_ndx++]=right_tx_sample;

                        if (output_buffer_ndx>=504)
                        {
                            //prepare and post for transfer an hpsdr frame
                            t->sync[0] = OZY_SYNC;
                            t->sync[1] = OZY_SYNC;
                            t->sync[2] = OZY_SYNC;

                            // simple state mechanism to continuously run through the HPSDR C0 commands we want to transmit
                            rf->txC0ndx%=rf->numCmdListEntries;
                            t->cmd[0] = 0;
                            switch (tx_C0_cmd_list[rf->txC0ndx++])
                            {
                            case HPSDR_C0_CONTROL:
                            {
                                // C0, nothing to do here, MOX (BIT(0)) is set later
                                t->cmd[0] = 0;
                                // C1
                                // sample rate
                                t->cmd[1] &= ~(HPSDR_SAMPLE_RATE_BITS);
                                if (rf->sampleRate == 384000)
                                    t->cmd[1] |= HPSDR_SAMPLE_RATE_384000;
                                else if (rf->sampleRate == 192000)
                                    t->cmd[1] |= HPSDR_SAMPLE_RATE_192000;
                                else if (rf->sampleRate == 96000)
                                    t->cmd[1] |= HPSDR_SAMPLE_RATE_96000;
                                else // default if (rf->sampleRate == 48000)
                                    t->cmd[1] |= HPSDR_SAMPLE_RATE_48000;

                                // 10 MHz ref.
                                t->cmd[1] &= ~(HPSDR_10MHZ_REF_BITS);
                                if (rf->ref10M == hpsdr::Ref10M_Atlas)
                                    t->cmd[1] |=  HPSDR_10MHZ_REF_ATLAS;
                                else  if (rf->ref10M == hpsdr::Ref10M_Penelope)
                                    t->cmd[1] |= HPSDR_10MHZ_REF_PENELOPE;
                                else // if(rf->ref10M == hpsdr::Ref10M_Mercury) default
                                    t->cmd[1] |= HPSDR_10MHZ_REF_MERCURY;

                                // 122.88 MHz source
                                t->cmd[1] &= ~(HPSDR_122_88_SRC_BITS);
                                if (rf->ref122M == hpsdr::Ref122M_Penelope)
                                    t->cmd[1] |=  HPSDR_122_88_SRC_PENELOPE;
                                else // if(rf->ref122M == hpsdr::Ref122M_Mercury) default
                                    t->cmd[1] |=  HPSDR_122_88_SRC_MERCURY;

                                // config
                                t->cmd[1] &= ~(HPSDR_CONFIG_BITS);
                                if (rf->config == hpsdr::Config_Penelope)
                                    t->cmd[1] |=  HPSDR_CONFIG_PENELOPE;
                                if (rf->config == hpsdr::Config_Both)
                                    t->cmd[1] |=  HPSDR_CONFIG_BOTH;
                                else // if (rf->config == hpsdr::Config_Mercury) default
                                    //t->cmd[1] |=  HPSDR_CONFIG_MERCURY;
                                    t->cmd[1] |=  HPSDR_CONFIG_BOTH;

                                // mic source
                                t->cmd[1] &= ~(HPSDR_MIC_SRC_BITS);
                                if (rf->micSrc == hpsdr::Mic_Janus)
                                    t->cmd[1] |= HPSDR_MIC_SRC_JANUS;
                                else //if (rf->micSrc == hpsdr::Mic_Penelope) default
                                    t->cmd[1] |= HPSDR_MIC_SRC_PENELOPE;

                                // C2
                                // modes
                                t->cmd[2] = 0x00;
                                //t->cmd[2] &= ~(HPSDR_MODE_BITS);
                                if (rf->modeClassE == hpsdr::Class_E)
                                    t->cmd[2] |= HPSDR_MODE_CLASS_E;
                                else // if (rf->modeClassE == hpsdr::Class_Regular) default
                                    t->cmd[2] |= HPSDR_MODE_REGULAR;

                                // turn OFF all open collector outputs on Penelope or Hermes
                                t->cmd[2] &= ~(HPSDR_OC_BITS);

                                // C3
                                // Alex attenuator
                                t->cmd[3] = 0x00;
                                //t->cmd[3] &= ~(HPSDR_ALEX_ATT_BITS);
                                if(rf->getTuneMode() || rf->getPacketXmit() || rf->getGuiXmit())
                                {
                                    t->cmd[3] |=  HPSDR_ALEX_ATT_30DB;
                                } else {
                                    if (rf->alexAtt == hpsdr::Att_30dB)
                                        t->cmd[3] |=  HPSDR_ALEX_ATT_30DB;
                                    else if (rf->alexAtt == hpsdr::Att_20dB)
                                        t->cmd[3] |=  HPSDR_ALEX_ATT_20DB;
                                    else if (rf->alexAtt == hpsdr::Att_10dB)
                                        t->cmd[3] |=  HPSDR_ALEX_ATT_10DB;
                                    else // if (rf->alexAtt == hpsdr::Att_0dB) default
                                        t->cmd[3] |=  HPSDR_ALEX_ATT_0DB;
                                }

                                // preamp, which is on?
                                t->cmd[3] &= ~(HPSDR_PREAMP_BITS);
                                if(rf->getTuneMode() || rf->getPacketXmit() || rf->getGuiXmit())
                                {
                                        t->cmd[3] |=  HPSDR_PREAMP_OFF;
                                } else {
                                    if (rf->preampOn == hpsdr::Preamp_Off)
                                        t->cmd[3] |=  HPSDR_PREAMP_OFF;
                                    else // if (rf->preampOn == hpsdr::Preamp_On)
                                        t->cmd[3] |=  HPSDR_PREAMP_ON;
                                }
                                // LT2208 Dither
                                t->cmd[3] &= ~(HPSDR_LT2208_DITHER_BITS);
                                if (rf->lt2208Dither == hpsdr::dither_Off)
                                    t->cmd[3] |= HPSDR_LT2208_DITHER_OFF;
                                else //if (rf->lt2208Dither == hpsdr::dither_On)
                                    t->cmd[3] |= HPSDR_LT2208_DITHER_ON;

                                // LT2208 Randomization
                                t->cmd[3] &= ~(HPSDR_LT2208_RANDOM_BITS);
                                if (rf->lt2208Random == hpsdr::random_Off)
                                    t->cmd[3] |=  HPSDR_LT2208_RANDOM_OFF;
                                else //if (rf->lt2208Dither == hpsdr::random_On)
                                    t->cmd[3] |=  HPSDR_LT2208_RANDOM_ON;

                                // Alex RX Antenna
                                t->cmd[3] &= ~(HPSDR_ALEX_RX_ANT_BITS);
                                if (rf->rxAnt == hpsdr::rxAnt_RX1)
                                    t->cmd[3] |=  HPSDR_ALEX_RX_ANT_RX1;
                                else if (rf->rxAnt == hpsdr::rxAnt_RX2)
                                    t->cmd[3] |=  HPSDR_ALEX_RX_ANT_RX2;
                                else if (rf->rxAnt == hpsdr::rxAnt_XV)
                                    t->cmd[3] |=  HPSDR_ALEX_RX_ANT_XV;
                                else // if (rf->rxAnt == hpsdr::rxAnt_None) // default rxAnt_None
                                    t->cmd[3] |=  HPSDR_ALEX_RX_ANT_NONE; // already cleared

                                // Alex RX Out
                                t->cmd[3] &= ~(HPSDR_ALEX_RX_OUT_BITS);
                                if (rf->rxOut == hpsdr::rxOut_On)
                                    t->cmd[3] |=  HPSDR_ALEX_RX_OUT_ON;
                                else // if (rf->txAnt == hpsdr::rxOut_Off) default rxOut off
                                    t->cmd[3] |=  HPSDR_ALEX_RX_OUT_OFF;

                                // C4
                                // Alex TX relay
                                t->cmd[4] = 0x00;
                                //t->cmd[4] &= ~(HPSDR_ALEX_TX_RELAY_BITS);
                                if (rf->txAnt == hpsdr::txAnt_2)
                                    t->cmd[4] |=  HPSDR_ALEX_TX_RELAY_TX2;
                                if (rf->txAnt == hpsdr::txAnt_3)
                                    t->cmd[4] |=  HPSDR_ALEX_TX_RELAY_TX3;
                                else // if (rf->txAnt == hpsdr::txAnt_1) default txAnt_1
                                    t->cmd[4] |=  HPSDR_ALEX_TX_RELAY_TX1;

                                // Duplex
                                t->cmd[4] &= ~(HPSDR_DUPLEX_BITS);
                                if (rf->duplex == hpsdr::duplex_Half)
                                    t->cmd[4] |=  HPSDR_DUPLEX_OFF;
                                else // if (rf->duplex == hpsdr::duplex_Full)
                                    t->cmd[4] |=  HPSDR_DUPLEX_ON;

                                // RX bits
                                t->cmd[4] &= ~(HPSDR_TOTAL_RX_BITS);

                                if (rf->numRx == 1)
                                    t->cmd[4] |=  HPSDR_TOTAL_RX_1RX;
                                else if (rf->numRx == 2)
                                    t->cmd[4] |=  HPSDR_TOTAL_RX_2RX;

                                // time stamp for lsb of mic data
                                t->cmd[4] &= ~(HPSDR_TIMESTAMP_BITS);
                                t->cmd[4] |=  HPSDR_TIMESTAMP_OFF;

                                // Common Mercury Frequency
                                t->cmd[4] &= ~(HPSDR_MERCURY_COMMON_BITS);
                                if (rf->curMercuryMode == 2)
                                    t->cmd[4] |=  HPSDR_MERCURY_COMMON_ON;
                                else
                                    t->cmd[4] |=  HPSDR_MERCURY_COMMON_OFF;
                            }

                                break;
                            case HPSDR_C0_FREQUENCY_TX:
                            {

                                //t->cmd[0] &= ~(HPSDR_C0_FREQUENCY_BITS);
                                t->cmd[0] |= HPSDR_C0_FREQUENCY_TX;

                                t->cmd[1]=rf->txFreq[rf->curMercury]>>24;
                                t->cmd[2]=rf->txFreq[rf->curMercury]>>16;
                                t->cmd[3]=rf->txFreq[rf->curMercury]>>8;
                                t->cmd[4]=rf->txFreq[rf->curMercury];
                                //}
                            }
                                break;
                            case HPSDR_C0_FREQUENCY_RX0:
                            {
                                //t->cmd[0] &= ~(HPSDR_C0_FREQUENCY_BITS);
                                t->cmd[0] |= HPSDR_C0_FREQUENCY_RX0;
                                t->cmd[1]=rf->rxFreq[rf->curMercury][0]>>24;
                                t->cmd[2]=rf->rxFreq[rf->curMercury][0]>>16;
                                t->cmd[3]=rf->rxFreq[rf->curMercury][0]>>8;
                                t->cmd[4]=rf->rxFreq[rf->curMercury][0];
                            }
                                break;
                            case HPSDR_C0_FREQUENCY_RX1:
                            {
                                //t->cmd[0] &= ~(HPSDR_C0_FREQUENCY_BITS);
                                t->cmd[0] |= HPSDR_C0_FREQUENCY_RX1;
                                t->cmd[1]=rf->rxFreq[rf->curMercury][1]>>24;
                                t->cmd[2]=rf->rxFreq[rf->curMercury][1]>>16;
                                t->cmd[3]=rf->rxFreq[rf->curMercury][1]>>8;
                                t->cmd[4]=rf->rxFreq[rf->curMercury][1];
                            }
                                break;
                            case HPSDR_C0_EXT_CMD_12: // 0x12
                            {
                                t->cmd[0] = HPSDR_C0_EXT_CMD_12;
                                t->cmd[1] = rf->drive; // drive level 0 - 255
                                t->cmd[2] = 0x00; // +20Mic Boost, Mic in, all hermes/metis/alex zeroes

                                t->cmd[2] &= ~(HPSDR_MIC_BOOST_BIT);
                                if (rf->micGain == hpsdr::MicGain_20dB)
                                    t->cmd[2] |=  HPSDR_MIC_BOOST_20DB;
                                else //if (rf->micGain == hpsdr::MicGain_0dB)
                                    t->cmd[2] |=  HPSDR_MIC_BOOST_ODB;

                                t->cmd[2] &= ~(HPSDR_MIC_LINE_BIT);
                                if (rf->micLine == hpsdr::LineIn)
                                    t->cmd[2] |=  HPSDR_LINE_IN;
                                else //if (rf->micLine == hpsdr::MicIn)
                                    t->cmd[2] |=  HPSDR_MIC_IN;

                                t->cmd[3] = 0x00; // disable alex T/R relay, alex HPF filter selections, invalid unless cmd[2].6 is set
                                t->cmd[4] = 00; // alex LPF filter selections
                            }
                                break;

                            case HPSDR_C0_EXT_CMD_14: // part of CW
                            {
                                t->cmd[0] = HPSDR_C0_EXT_CMD_14;
                                t->cmd[1] = 0x00;
                                t->cmd[2] = 0xE0; // penelope selected, pure signal, enable -20dB atten on xmit
                                //t->cmd[2] = 0xFF; // full line-in gain[4:0], penelope selected, pure signal, enable -20dB atten on xmit
                                //t->cmd[2] = 0x20; // enable -20 db on xmit
                                //t->cmd[2] = 0x00; // enable -20 db on xmit
                                t->cmd[3] = 0x00;
                                if (rf->xmit20dBAtt)
                                {
                                    t->cmd[2] |= HPSDR_C2_ATT_RX_ON_TX_BIT;
                                    t->cmd[3] |= HPSDR_C3_ATT_RX_ON_TX_BIT; // -20dB on Mercury xmit
                                } else {
                                    t->cmd[2] &= ~(HPSDR_C2_ATT_RX_ON_TX_BIT);
                                    t->cmd[3] &= ~(HPSDR_C3_ATT_RX_ON_TX_BIT);
                                }
                                t->cmd[4] = 0x00;
                            }
                                break;
                            case HPSDR_C0_EXT_CMD_16: // part of CW
                            {
                                t->cmd[0] = HPSDR_C0_EXT_CMD_16;
                                t->cmd[1] = 0x00; // ADC2 input atten TODO
                                t->cmd[2] = 0x00; // ADC3 input atten TODO
                                if (rf->keyerReversed)
                                    t->cmd[2] |= HPSDR_REVERSE_CW_KEYS_ENABLE;
                                t->cmd[3] = rf->keyerSpeed & HPSDR_CW_KEYER_SPEED_BITS; // speed 1 - 60 WPM
                                t->cmd[3] |= (rf->keyerMode << 6) & HPSDR_CW_KEYER_MODE_BITS; // modes: 0x00 = straight key, 0x01 = Mode A, 0x02 = Mode B
                                t->cmd[4] = rf->keyerWeight & HPSDR_CW_KEYER_WEIGHT_BITS; // 0 - 100
                                if (rf->keyerSpacing)
                                {
                                    t->cmd[4] |= HPSDR_CW_KEYER_SPACING_BIT;
                                }
                                else
                                {
                                    t->cmd[4] &= ~(HPSDR_CW_KEYER_SPACING_BIT);
                                }
                            }
                                break;
                            case HPSDR_C0_EXT_CMD_1C: // adc assignments
                            {
                                t->cmd[0] = HPSDR_C0_EXT_CMD_1C;
                                t->cmd[1] = 0x00;
                                t->cmd[2] = 0x00;
                                t->cmd[3] = 0x00;
                                t->cmd[4] = 0x00;
                            }
                                break;
                            case HPSDR_C0_EXT_CMD_1E: // CW
                            {
                                t->cmd[0] = HPSDR_C0_EXT_CMD_1E;
                                //t->cmd[1] &= ~(HPSDR_CW_EXT_INT_BIT);
                                //if (rf->keyerEnabled && (rf->mode == CWU || rf->mode == CWL))
                                if (rf->keyerEnabled)
                                    t->cmd[1] = HPSDR_CW_INT; // only the BIT(0) is in use, so safe to set cmd[1] directly
                                else
                                    t->cmd[1] = HPSDR_CW_EXT;
                                t->cmd[2] = rf->keyerStvolume;
                                t->cmd[3] = rf->keyerPttdelay;
                                t->cmd[4] = 0x7f; // unused, possible raised cosine tapering
                            }
                                break;

                            case HPSDR_C0_EXT_CMD_20: // CW keyer
                            {
                                t->cmd[0] = HPSDR_C0_EXT_CMD_20;
                                t->cmd[1] = (unsigned char)(rf->keyerHangtime >> 2);
                                t->cmd[2] = (unsigned char)(rf->keyerHangtime & HPSDR_CW_HANG_TIME_LSB);
                                t->cmd[3] = (unsigned char)(rf->keyerFreq >> 4);
                                t->cmd[4] = (unsigned char)(rf->keyerFreq & HPSDR_CW_SIDETONE_FREQ_LSB);
                            }
                                break;

                            default:
                                break;
                            }

                            // set the MOX bit if xmitting
                            if(rf->getTuneMode() || rf->getPacketXmit() || rf->getGuiXmit())
                            {
                                // set the MOX bit
                                t->cmd[0] |= HPSDR_MOX_ON;
                            }
                            else
                            {
                                // clear the MOX bit for RX
                                t->cmd[0] &= ~(HPSDR_MOX_BIT_0);
                            }
/*
                long diff = elapsedTime();
                qInfo() << "EP_TX_DATA USB packet diff " << diff/1000.0 << "mSec.";
*/

                            if((libusbOzyXferDev(OZY_EP_TX_DATA, rf->txBuf->buf, OZY_BUFFER_SIZE, rf->txBuf))<0)
                            {
                                fprintf(stderr, "%s: libusbOzyXferDev failed\n", __func__);
                                //return -1;
                            }

                            rf->txBuf = nullptr;
                            output_buffer_ndx=0;
                        }
                    } // end j loop
                } // end rf->samplecount => rf->numSamples
              // here is  end 504 count

            }
        }
        else
        { // no sync !!
            // reset the libusb device ? perhaps
            // from the callback? sheesh
            // raise the stop flags? do something, please

            int error = 2; //
            (void)error;
            libusbOzyResync(rf);

            // we're in the callback for usb handler. careful here. need to signal out?
            //rf->killThreads();
            //rf->libusbCloseDev();
            //rf->initOzyUSBDevice();
        }
    }
    else if (int_prm==0x84) // bandscope data arrives on EP4
    {
        // just load the data into a bridge-register
        apex_memcpy(rf->bsBuf, buf_prm, 8192);
    }

    return 0;
}

int libusbOzyClearQueue(usbBufPool_t *q)
{
    if (!q)
        return 0;

    return usb_clear_in_flight(q);
}

int libusbOzyDosync(RadioOzy *rf, uint8_t *d, int len)
{

    if (!rf)
        return -1;

    int num = 0;
    int ret = libusb_interrupt_transfer(rf->devHandle, OZY_EP_RX_DATA, d, len, &num, 250);
    if (ret < 0) {
        fprintf(stderr, "%s: err %d (%s)\n", __func__, ret, libusb_error_name(ret));
        return ret;
    } else if (num != len) {
        fprintf(stderr, "%s: received length %d != requested length %d\n", __func__, num, len);
        return -1;
    }

    return num;
}


int libusbOzyResync(RadioOzy *rf)
{
    if (!rf)
        return -1;

    // use sync io to  search for 0x7f three times
    // then read 509 bytes to sync back up

    // do we need to stop the reader threads?
    rf->suspendThreads();

    libusbOzyClearQueue(rf->rxBufs);
    libusbOzyClearQueue(rf->txBufs);


    static uint8_t d[512]; // extra space JIC??

    int r = 0;
    int cnt = 3; // there should be three 0x7f in a row for sync
    while(cnt) {
        d[0] = 0;

        r = libusbOzyDosync(rf, d, 1);
        if (r<0) {
            break;
        }
        if (r) {
            if (d[0] == 0x7f) {
                --cnt;
            } else {
                cnt = 3; // reset and try again
            }
        }
    }

    if (!cnt) { // count = zero if three 0x7f received
        // now read 509 bytes and throw them away
        // then data stream should be back "in sync"
        // it's probably better to know why it gets out of sync in the first place
        // I suspect a short read error somewhere in libusb-1.0???
        libusbOzyDosync(rf, d, 509);

    }

        // hmm this is trouble waiting to happen
        // start it back up again
        rf->goThreads();

    return r;
}

// user_data = usbBuf_t please
int  libusbOzyXferDev(int ep, void* buffer, int buffersize, void *user_data)
{
    int rc=0;

    if (!user_data)
    {
        fprintf(stderr,"%s: No user_data!\n", __func__);
        return -1;
    }

    usbBuf_t *ob = (usbBuf_t *)user_data;
    RadioOzy *r = (RadioOzy *)ob->ud;

    if (!r)
    {
        fprintf(stderr,"%s: No RadioOzy!\n", __func__);
        return -1;
    }

    if (!r->devHandle)
    {
        fprintf(stderr,"%s: No devHandle!\n", __func__);
        return -1;
    }

tryagain:

    libusb_fill_bulk_transfer(ob->xfr,
                              r->devHandle,
                              (unsigned char)ep,
                              (unsigned char *)buffer,
                              buffersize,
                              libusbOzyHandleCb,
                              user_data,
                              1000); // milliseconds

    rc=libusb_submit_transfer(ob->xfr);
    if(rc<0)
    {
        fprintf(stderr,"%s: libusb_submit_transfer failed: buf %d rc %d\n", __func__, ob->id, rc);
        if (rc==-6) // busy, try again
        {
            struct timeval tv = {0, 0};
            libusb_handle_events_timeout_completed(r->usbContext, &tv, nullptr);
            goto tryagain;
        }
    }

    return rc;
}

// a callback for usb transfers that do not use the usbBuf_t pools
void libusbOzyHandleCtl(struct libusb_transfer *usb_xfer)
{
    RadioUsb *r = nullptr;
    usbBuf_t *ob = nullptr;

    if (!usb_xfer)
    {
        fprintf(stderr, "%s: NO usb_xfer\n", __func__);
        return;
    }

    if (usb_xfer->user_data)
    {
        ob = (usbBuf_t *)usb_xfer->user_data;
        if (ob && ob->ud)
            r = (RadioUsb *)ob->ud;
    }

    if (r)
        if (!ob->ep)
            usb_put_buffer(r->ctlBufs, ob);

    return;
}

void libusbOzyHandleCb(struct libusb_transfer *ux)
{
    RadioOzy *r = nullptr;
    usbBuf_t *ob = nullptr;

    if (!ux)
    {
        fprintf(stderr, "%s: NO ux pid %d\n", __func__, getpid());
        return;
    }

    if (ux->user_data)
    {
        ob = (usbBuf_t *)ux->user_data;
        if (ob && ob->ud)
            r = (RadioOzy *)ob->ud;
    }

    switch(ux->status)
    {
    case LIBUSB_TRANSFER_COMPLETED:
    {
        if (r)
        {
            r->running = true; // FIXME TODO, currently unused
            r->cb(r, ob->buf, ux->endpoint);
        }
        break;
    }
    case LIBUSB_TRANSFER_ERROR:
    {
        if (r)
        {
            r->running = false; // currently unused
        }
        // this is the infamous -71 error, something is wrong, and no explanation for it
        fprintf(stderr, "%s: LIBUSB_TRANSFER_ERROR pid %d ID %d flags 0x%2.2X ep 0x%2.2x type 0x%2.2x timeout %d status 0x%2.2x length %d\n",
                __func__,  getpid(), ((usbBuf_t *)ux->user_data)->id, ux->flags, ux->endpoint, ux->type, ux->timeout, ux->status, ux->length);
        break;
    }
    case LIBUSB_TRANSFER_TIMED_OUT:
    {
        fprintf(stderr, "%s: LIBUSB_TRANSFER_TIMED_OUT pid %d ID %d flags 0x%2.2X ep 0x%2.2x type 0x%2.2x timeout %d status 0x%2.2x length %d\n",
                __func__,  getpid(), ((usbBuf_t *)ux->user_data)->id, ux->flags, ux->endpoint, ux->type, ux->timeout, ux->status, ux->length);
        break;
    }
    case LIBUSB_TRANSFER_CANCELLED:
    {
        fprintf(stderr, "%s: LIBUSB_TRANSFER_CANCELLED pid %d ID %d flags 0x%2.2X ep 0x%2.2x type 0x%2.2x timeout %d status 0x%2.2x length %d\n",
                __func__,  getpid(), ((usbBuf_t *)ux->user_data)->id, ux->flags, ux->endpoint, ux->type, ux->timeout, ux->status, ux->length);
        break;
    }
    case LIBUSB_TRANSFER_STALL:
    {
        fprintf(stderr, "%s: LIBUSB_TRANSFER_STALL pid %d ID %d flags 0x%2.2X ep 0x%2.2x type 0x%2.2x timeout %d status 0x%2.2x length %d\n",
                __func__,  getpid(), ((usbBuf_t *)ux->user_data)->id, ux->flags, ux->endpoint, ux->type, ux->timeout, ux->status, ux->length);
        break;
    }
    case LIBUSB_TRANSFER_NO_DEVICE:
    {
        if (r)
            r->running = false; // currently unused
        fprintf(stderr, "%s: LIBUSB_TRANSFER_NO_DEVICE pid %d ID %d flags 0x%2.2X ep 0x%2.2x type 0x%2.2x timeout %d status 0x%2.2x length %d\n",
                __func__,  getpid(), ((usbBuf_t *)ux->user_data)->id, ux->flags, ux->endpoint, ux->type, ux->timeout, ux->status, ux->length);
        break;
    }
    case LIBUSB_TRANSFER_OVERFLOW:
    {
        fprintf(stderr, "%s: LIBUSB_TRANSFER_OVERFLOW pid %d ID %d flags 0x%2.2X ep 0x%2.2x type 0x%2.2x timeout %d status 0x%2.2x length %d\n",
                __func__,  getpid(), ((usbBuf_t *)ux->user_data)->id, ux->flags, ux->endpoint, ux->type, ux->timeout, ux->status, ux->length);
        break;
    }
    default:
    {
        fprintf(stderr, "%s: UNKNOWN STATUS pid %d ID %d flags 0x%2.2X ep 0x%2.2x type 0x%2.2x timeout %d status 0x%2.2x length %d\n",
                __func__,  getpid(), ((usbBuf_t *)ux->user_data)->id, ux->flags, ux->endpoint, ux->type, ux->timeout, ux->status, ux->length);
    }
    }

    switch(ux->endpoint)
    {
    case 0x00: /* control endpoint completed */
    {
        break;
    }
    case 0x86: /* 512 byte data read completed */
    {
        usb_put_buffer(r->rxBufs, ob);
        break;
    }
    case 0x02: /* write to dev endpoint completed */
    {
        usb_put_buffer(r->txBufs, ob);
        break;
    }
    case 0x84: /* band scope 8192 bytes read */
    {
        usb_put_buffer(r->bsBufs, ob);
        break;
    }
    default:
    {
        fprintf(stderr, "%s: Unknown ep 0x%x\n", __func__, ux->endpoint);
        break;
    }
    } // end switch(endpoint)
}

hpsdr::hpsdr(QWidget *parent)
{
    (void)parent;
    // register the metatype enums so we can serialize
    qRegisterMetaType<HpsdrMoxEnum>();
    qRegisterMetaType<HpsdrSampleRateEnum>();
    qRegisterMetaType<HpsdrRef10MEnum>();
    qRegisterMetaType<HpsdrRef122MEnum>();
    qRegisterMetaType<HpsdrConfigEnum>();
    qRegisterMetaType<HpsdrMicSrcEnum>();
    qRegisterMetaType<HpsdrClassEEnum>();
    qRegisterMetaType<HpsdrAttEnum>();
    qRegisterMetaType<HpsdrPreAmpEnum>();
    qRegisterMetaType<HpsdrDitherEnum>();
    qRegisterMetaType<HpsdrRandomEnum>();
    qRegisterMetaType<HpsdrRxAntEnum>();
    qRegisterMetaType<HpsdrRxOutEnum>();
    qRegisterMetaType<HpsdrTxAntEnum>();
    qRegisterMetaType<HpsdrDuplexEnum>();
    qRegisterMetaType<HpsdrMicGainEnum>();
    qRegisterMetaType<HpsdrMicLineEnum>();

    packetCount = 0LL;
}

hpsdr::~hpsdr()
{
}
