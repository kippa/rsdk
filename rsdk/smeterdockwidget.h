#ifndef SMETERDOCKWIDGET_H
#define SMETERDOCKWIDGET_H

#include <QFont>
#include <QTimer>
#include <QThread>
#include <QDockWidget>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QAbstractButton>
#include <QLinearGradient>
#include <QMutex>

#include <dttsp.h>
#include "rsdkdockwidget.h"

namespace Ui {
    class SmeterDockWidget;
}

class SmeterDockWidget;

// smeter peaks each with a slightly longer timeout
// leave small colored dots for each on the s-meter arc
// and they should "decay" to zero, hang, etc.
class SMeterPeak : public QObject
{
    Q_OBJECT

public:
    SMeterPeak(QWidget *parent, int msecs);
    ~SMeterPeak();

    double peakVal() const;
    void setPeakVal(double peakVal);

    QColor color() const;
    void setColor(const QColor &color);

private slots:
    // restart timer with low peak value
    void peakTimeout();

private:
    QTimer *m_t;
    QColor m_color;
    unsigned int m_decay;
    double m_peakVal;

};

class SMeterWorker : public QObject
{
    friend class SmeterDockWidget;

    Q_OBJECT

public:
    SMeterWorker(SmeterDockWidget *d);
    ~SMeterWorker();
    QTimer grabTimer;

public slots:
    void grabMeter();

signals:
    void collectDone();

private:
    SmeterDockWidget *dock;
    unsigned int fps;
    double rate;
    struct timeval start_time, end_time;

    // no copy, no assign
    SMeterWorker(const SMeterWorker &NoCopy);
    SMeterWorker &operator=(const SMeterWorker &NoAssign);
};

class SmeterDockWidget : public rsdkDockWidget
{
    friend class SMeterWorker;

    Q_OBJECT

    Q_ENUMS(METERTYPE)

    Q_PROPERTY(METERTYPE rxMeterType READ getRxMeterType WRITE setRxMeterType)
    Q_PROPERTY(METERTYPE txMeterType READ getTxMeterType WRITE setTxMeterType)
    Q_PROPERTY(QColor brightLight READ getBrightLight WRITE setBrightLight)
    Q_PROPERTY(QColor darkLight READ getDarkLight WRITE setDarkLight)
    Q_PROPERTY(QColor arcColor READ getArcColor WRITE setArcColor)
    Q_PROPERTY(QColor traceColor READ getTraceColor WRITE setTraceColor)
    Q_PROPERTY(QColor needleColor READ getNeedleColor WRITE setNeedleColor)
    Q_PROPERTY(bool arcOn READ getArcOn WRITE setArcOn)
    Q_PROPERTY(bool traceOn READ getTraceOn WRITE setTraceOn)
    Q_PROPERTY(bool needleOn READ getNeedleOn WRITE setNeedleOn)
    Q_PROPERTY(bool peaksOn READ getPeaksOn WRITE setPeaksOn)

public:
    explicit SmeterDockWidget(QWidget *parent = 0);
    ~SmeterDockWidget();
    void start();

    enum METERTYPE {
        // RX
        SIGNAL_STRENGTH=0, AVG_SIGNAL_STRENGTH, ADC_REAL, ADC_IMAG, AGC_GAIN,
        // TX
        MIC, PWR, ALC, EQtap, LEVELER, COMP, CPDR, ALC_G, LVL_G, MIC_PK,
        ALC_PK, EQ_PK, LEVELER_PK, COMP_PK, CPDR_PK, VSWR,
    };
    Q_ENUM(METERTYPE)

    METERTYPE getRxMeterType() const;
    METERTYPE getTxMeterType() const;

    double meter[3]; // MAX_THREADS 0 rx, 1 rx, 2 tx

    QThread *grabThread;
    SMeterWorker *mgrabber;

    int getYMin() const;
    int getYMax() const;


public slots:
    QColor getBrightLight() const;
    QColor getDarkLight() const;
    QColor getArcColor() const;
    QColor getTraceColor() const;
    QColor getNeedleColor() const;

    bool getTraceOn() const;
    bool getArcOn() const;
    bool getNeedleOn() const;
    bool getPeaksOn() const;

    void setYMin(int value);
    void setYMax(int value);
    void setTxMeterType(const METERTYPE &value);
    void setDarkLight(const QColor &value);
    void setBrightLight(const QColor &value);
    void setArcColor(const QColor &value);
    void setTraceColor(const QColor &value);
    void setNeedleColor(const QColor &value);
    void setNeedleOn(bool value);
    void setArcOn(bool value);
    void setTraceOn(bool value);
    void setPeaksOn(bool value);

signals:
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private slots:
    void drawMeter();
    void rxMtrBtnGrpClicked(QAbstractButton *b);
    void txMtrBtnGrpClicked(QAbstractButton *b);
    void redrawScales(QRectF rf);
    void setRxMeterType(const METERTYPE &value);
    void setRxMeterType(const int &value);
    void makeGradient();
    void setLGrad(const QLinearGradient &lg);
    void splitterMoved(int pos, int ndx);

private:
    Ui::SmeterDockWidget *ui;
    bool event(QEvent *event);
    int fetchReqFontSize();
    void makeArcs(int w, int h);
    void makePixmaps();

    // the meter
    QGraphicsScene *scene;
    QGraphicsPixmapItem *needlePixmap;
    QGraphicsPixmapItem *scalePixmap;

    METERTYPE rxMeterType;
    METERTYPE txMeterType;

    QPainterPath sUnitsArcPath, sUnits2ArcPath, sArcLabels;
    QPainterPath plusDbArcPath, plusDb2ArcPath, plusDbArcLabels;
    QPainterPath totSUnitsArcPath; // a dummy arc from which we can determine position, used
                                   // because to draw SUnits in two colors, we must have two
                                   // separate arcs, one in red for SUnits > S9
    QPainterPath vswrArcPath, vswr2ArcPath, vswrArcLabels;
    QPainterPath vswrRedArcPath, vswrRed2ArcPath, vswrRedArcLabels;
    QPainterPath totVswrArcPath; // another dummy arc to take percentages from

    // a general 0 - 100 % scale for anything that needs it
    QPainterPath pctArcPath, pct2ArcPath, pctArcLabels;
    QPainterPath pctRedArcPath, pctRed2ArcPath, pctRedArcLabels;
    QPainterPath totpctArcPath; // another dummy arc to take percentages from

    QPainterPath debugPath;
    QPainterPath titlePath, titlePath2, redTitlePath;

    double startAngle, plusDbArc, sunitsArc;

    QString S9str; // initialized in ctor init list
    QColor plusDbArcColor;
    QColor sunitsColor;

    QColor brightLight;
    QColor darkLight;
    QColor arcColor;
    QColor traceColor;
    QColor needleColor;

    bool traceOn;
    bool arcOn;
    bool needleOn;
    bool peaksOn;

    double arcOffset, vOffset;
    QString dbmLabel;

    QList <SMeterPeak *> peaks;

    int yMin, yMax;
    bool initialized;

    double sArcWidth;
    QPointF sArcFirstPoint;
    QPointF sArcLastPoint;

    QLinearGradient sTraceLinearGradient;
    double s9Pt; // percentage of smeter scale where S9 is

    // TODO FIXME for variable size too
    QFont m_largeFont;
    QFontMetrics m_largeFontMetrics;

    QFont m_meterFont;
    QFontMetrics m_meterFontMetrics;

    QTimer drawTimer;
    int m_drawInterval;
    bool m_drawEnable;

    // debug use only, get rid of this when satisfied
    struct timeval start_time, end_time;

    QMutex drawLock;
};

#endif // SMETERDOCKWIDGET_H
