#include <assert.h>
#include <string.h> // we use strcasestr

#include <jack/jack.h>
#include <jack/types.h>

#include "jack_rsdk.h"
#include "exception.h"
#include "rsdk.h"

// TODO FIXME
extern rsdk *r;

/*
 * use the jack ring buffers to stow rsdk data
 * out ringbuffers are single channel like inL, inR
 * this allows routing individual channels to/from rsdk
 */

static int process(jack_nframes_t nframes, void *arg)
{
    (void)nframes;
    jack *jp = (jack *)arg;
    assert(jp);

    // handle inbound data, rb_in <- in
    char *buf;
    size_t byteSize = jp->size * sizeof(float);

    // handle inbound data, in -> rb_in
    // we need this much space in the ringbuffer to copy  from the
    // inbound buffer into the ringbuffer where rsdk will access it
    size_t bytesAvail = jack_ringbuffer_write_space(jp->rb_inL);
    if (bytesAvail >= byteSize) {
        buf = (char *) jack_port_get_buffer(jp->inL, jp->size);
        jack_ringbuffer_write(jp->rb_inL, buf, byteSize);
    }

    bytesAvail = jack_ringbuffer_write_space(jp->rb_inR);
    if (bytesAvail >= byteSize) {
        buf = (char *) jack_port_get_buffer(jp->inR, jp->size);
        jack_ringbuffer_write(jp->rb_inR, buf, byteSize);
    }

    // handle outbound data, rb_out -> out
    bytesAvail = jack_ringbuffer_read_space(jp->rb_outL);
    if (bytesAvail >= byteSize) {
        buf = (char *)jack_port_get_buffer(jp->outL, jp->size);
        jack_ringbuffer_read(jp->rb_outL, buf, byteSize);
    }

    bytesAvail = jack_ringbuffer_read_space(jp->rb_outR);
    if (bytesAvail >= byteSize) {
        buf = (char *)jack_port_get_buffer(jp->outR, jp->size);
        jack_ringbuffer_read(jp->rb_outR, buf, byteSize);
    }

return 0;
}

int jack::fillRingbuffer(const char *buf, size_t cnt, jack_ringbuffer_t *rb)
{
    int total = 0;
    size_t ret = 0;
    size_t remain = cnt;
    if (rb) {
        while (remain) {
            // how much space is available to write to?
            size_t avail = jack_ringbuffer_write_space(rb);
            if (!avail)  {
                my_usleep(1);
                continue;
            }  else if (avail <= remain)  {
                ret = jack_ringbuffer_write(rb, buf, avail);
            }  else  {
                ret = jack_ringbuffer_write(rb, buf, remain);
            }
            buf += ret;
            total += ret;
            remain -= ret;
        }
    }
    return total;
}

/* no real checks in here, there should be! */
int jack::fetchRingbuffer(char *buf, size_t cnt, jack_ringbuffer_t *rb)
{
    int total = 0;
    size_t ret = 0;
    size_t remain = cnt;
    if (rb) {
        while (remain) {
            size_t avail = jack_ringbuffer_read_space(rb);
            if (!avail)
                break; // no data, better luck next time!
            else if (avail <= remain)
                ret = jack_ringbuffer_read(rb, buf, avail);
            else
                ret = jack_ringbuffer_read(rb, buf, remain);
            buf += ret;
            total += ret;
            remain -= ret;
        }
    }
    return total;
}

//static void jack_connect_cb(jack_port_id_t a, jack_port_id_t b, int connect, void *arg)
static void connect_cb(jack_port_id_t a, jack_port_id_t b, int connect, void *arg)
{
    (void)a; (void)b; (void)arg;
    //jack *jp = (jack *)arg;
    //jack_port_t *ap = jack_port_by_id(jp->jc, a);
    //jack_port_t *bp = jack_port_by_id(jp->jc, b);
    //const char *an = jack_port_name(ap);
    //const char *bn = jack_port_name(bp);
    //int jpa_flags = jack_port_flags(ap);
    //int jpb_flags = jack_port_flags(bp);

    if (connect)
    {
        /*
        fprintf (stderr, "connect ports %s - %s\n", an, bn);
        rc = jack_connect(jp->jc, an, bn);
        if (rc)
        {
            fprintf (stderr, "cannot connect ports %s - %s rc %d\n", an, bn, rc);
        }
        */
    }
    else
    {
        /*
        fprintf (stderr, "disconnect ports %s - %s\n", an, bn);
        rc = jack_disconnect(jp->jc, an, bn);
        if (rc)
        {
            fprintf (stderr, "cannot disconnect ports %s - %s rc %d\n", an, bn, rc);
        }
        */
    }
}

static void jack_error(const char *msg)
{
    fprintf(stderr, "JACK error! %s\n", msg);
}

static void shutdown_cb(void *arg)
{
    jack *jp = (jack *)arg;
    //fprintf(stderr, "%s:\n", __func__);
    jp->cbs--;
}

static int xrun_cb(void *arg)
{
    jack *jp = (jack *)arg;
    fprintf(stderr, "%s:\n", __func__);
    jp->xcbs++;
    return 0;
}

static int sample_rate_cb(jack_nframes_t nframes, void *arg)
{
    (void)arg;
    //jack *jp = (jack *)arg;
    fprintf(stderr, "%s: %d\n", __func__, nframes);
    // TODO FIXME
    return 0;
}

static int buffer_size_cb(jack_nframes_t nframes, void *arg)
{
    (void)arg;
    //jack *jp = (jack *)arg;
    fprintf(stderr, "%s: %d\n", __func__, nframes);
    // TODO FIXME
    return 0;
}

static void rename_cb(jack_port_id_t port, const char *old_name, const char *new_name, void *arg)
{
    (void)arg;
    //jack *jp = (jack *)arg;
    fprintf(stderr, "%s: port %d old_name %s new_name %s\n", __func__, port, old_name, new_name);
    // TODO FIXME
}

jack::jack(const char * const name_prm) :
    cbs(0),
    xcbs(0),
    inL(0), outL(0),
    inR(0), outR(0),
    rb_inL(0), rb_outL(0),
    rb_inR(0), rb_outR(0),
    size(0), sampleRate(0),
    user_data(0)
{
    name = name_prm;
}

jack::~jack()
{
    m_isRunning = false;

    if (rb_inL)
        jack_ringbuffer_free(rb_inL);
    if (rb_inR)
        jack_ringbuffer_free(rb_inR);
    if (rb_outL)
        jack_ringbuffer_free(rb_outL);
    if (rb_outR)
        jack_ringbuffer_free(rb_outR);

    if (jc) {
        jack_port_unregister(jc, inL);
        jack_port_unregister(jc, inR);
        jack_port_unregister(jc, outL);
        jack_port_unregister(jc, outR);

        jack_client_close(jc);
    }
}

int
jack::init(void *arg, int (*proc_cb)(jack_nframes_t, void *), int (*xrun_cb)(void *))
{
    jack_status_t jstatus;
    jack_options_t jopts = JackNoStartServer;
    jc = jack_client_open(name.c_str(), jopts, &jstatus);
    if(!jc)
    {
        //jack_error_callback("jack::init");
        //THROW_EXCEPTION(ERROR_LEVEL, 0, "jack_client_open %s failed\n", name.c_str());
        return -1; // don't necessarily throw here, just won't be doing jack I/O
    }

    jack_set_error_function(jack_error);
    jack_set_process_callback(jc, proc_cb, arg);
    jack_set_port_connect_callback(jc, connect_cb, arg);
    jack_set_buffer_size_callback(jc, buffer_size_cb, arg);
    jack_set_sample_rate_callback(jc, sample_rate_cb, arg);
    jack_set_port_rename_callback(jc, rename_cb, arg);
    jack_on_shutdown(jc, shutdown_cb, arg);
    jack_set_xrun_callback(jc, xrun_cb, arg);

    // buffer size
    size = jack_get_buffer_size(jc);
    fprintf(stderr, "%s: size %d\n", __func__, size);

    sampleRate = jack_get_sample_rate(jc);
    fprintf(stderr, "%s: sampleRate %d\n", __func__, sampleRate);

    // jack ringbuffer "collectors", into which are stored arriving data in/out
    // oversize these a bit (*4) so we don't bump into things
    rb_inL = jack_ringbuffer_create(size * sizeof(float) * 4);
    rb_inR = jack_ringbuffer_create(size * sizeof(float) * 4);
    rb_outL = jack_ringbuffer_create(size * sizeof(float) * 4);
    rb_outR = jack_ringbuffer_create(size * sizeof(float) * 4);

    // jack ports
    string title = "Left Out";
    outL = jack_port_register(jc, title.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput | JackPortIsPhysical | JackPortIsTerminal, 0);

    title.clear(); title = "Left In";
    inL = jack_port_register(jc, title.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);

    title.clear(); title = "Right Out";
    outR = jack_port_register(jc, title.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput | JackPortIsPhysical | JackPortIsTerminal , 0);

    title.clear(); title = "Right In";
    inR = jack_port_register(jc, title.c_str(), JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);

    jack_ringbuffer_reset(rb_inL);
    jack_ringbuffer_reset(rb_inR);
    jack_ringbuffer_reset(rb_outL);
    jack_ringbuffer_reset(rb_outR);

    m_isRunning = true;

    return 0;
}

bool jack::isRunning() const
{
    return m_isRunning;
}

int
jack::startJack(void)
{
    int rc = 0;
    jc = nullptr;

    rc = init(this, process, xrun_cb);

    if (jc) {
        rc = jack_activate(jc);
    }

    return rc;
}

int jack::stopJack()
{
    m_isRunning = false;

    if (jc) {
        jack_deactivate(jc);
    }
    return 0;
}
