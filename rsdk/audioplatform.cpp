#include <assert.h>
#include <unistd.h>

#include <QBuffer>
#include <QFile>
#include <QDebug>
#include <QIODevice>
#include <QTimer>

#include "audioplatform.h"
#include "rsdk.h"
#include "common.h"

extern rsdk *r;

audioPlatform::audioPlatform() :
        m_lastElapsed(0),
        m_sampleRate(48000),
        m_pushTimer(new QTimer(this)),
        m_deviceNickName("rsdk local"),
        m_din(0),
        m_ain(0),
        m_dout(0),
        m_aout(0)
{
        m_bufferSize = ROZY_XFER_SAMPS_SIZE * sizeof(int16_t) * 2;
        //m_bufferSize = 14800; // 14800 tune this latency vs. dropout

        // open a FIFO for output, rsdk writes to the pipe, and this reads it
        // must have permission to write in the dir, and
        // must open in ReadWrite mode to create file
        m_pipeOut = new QFile(RXAUDOUTNAME); // from common.h
        if (!m_pipeOut->open(QIODevice::ReadWrite | QIODevice::Unbuffered))
            qDebug() << "m_pipeOut err: " << m_pipeOut->errorString();

        initAudioIn();
        initAudioOut(true);
}

audioPlatform::~audioPlatform()
{
        m_pushTimer->stop();
        m_aout->stop();
        m_ain->stop();

        m_pipeOut->close();
        delete m_pipeOut;
        unlink(RXAUDOUTNAME);

        delete m_ain;
        delete m_aout;
        delete m_pushTimer;
}

// audio in comes from system, just get current values
// and set sample rate to 48k
void audioPlatform::initAudioIn()
{
        m_inPull = true;

        foreach (const QAudioDeviceInfo &deviceInfo, QAudioDeviceInfo::availableDevices(QAudio::AudioInput))
                qDebug() << "Input device name: " << deviceInfo.deviceName();

        m_inDevice = QAudioDeviceInfo::defaultInputDevice();

        m_inFormat = m_inDevice.preferredFormat();
        m_inFormat.setSampleRate(m_sampleRate); // color under? 1000/1001 = 47952

        if (!m_inDevice.isFormatSupported(m_inFormat))
        {
                qWarning() << __func__ << "Format not supported: " << m_inFormat;
                return;
        }

        qDebug() << "Using m_inFormat" << m_inFormat;

        if (m_ain) {
                m_ain->stop();
                delete m_ain;
        }

        m_ain = new QAudioInput(m_inFormat, this);
        m_ain->setBufferSize(m_bufferSize);

        // debug timer
        m_lastElapsed = m_ain->elapsedUSecs();
        connect(m_ain, SIGNAL(stateChanged(QAudio::State)), this, SLOT(handleInputStateChange(QAudio::State)));

        m_din = m_ain->start();
        connect(m_din, SIGNAL(readyRead()), this, SLOT(readSome()));
}

void audioPlatform::initAudioOut(bool doit)
{
        // handle signals that send a bool, false means rx is off
        if (!doit)
            return;

        m_outPull = false; // push

        qDebug() << "************************************************************";
        qDebug() << "OUTPUT DEVICES";
        qDebug() << "************************************************************";

        foreach (const QAudioDeviceInfo &deviceInfo, QAudioDeviceInfo::availableDevices(QAudio::AudioOutput))
                qDebug() << "Output device name: " << deviceInfo.deviceName();

        qDebug() << "************************************************************";
        qDebug() << "************************************************************";

        m_outFormat.setSampleRate(m_sampleRate); // color under? 1000/1001 = 47952
        m_outFormat.setSampleSize(16);
        m_outFormat.setChannelCount(2);
        m_outFormat.setCodec("audio/pcm");
        m_outFormat.setByteOrder(QAudioFormat::LittleEndian);
        m_outFormat.setSampleType(QAudioFormat::SignedInt);
        m_outDevice = QAudioDeviceInfo::defaultOutputDevice();
        if (!m_outDevice.isFormatSupported(m_outFormat))
        {
                qWarning() << __func__ << "Format not supported: " << m_outFormat;
                return;
        }

        qDebug() << "Using m_outFormat" << m_outFormat;

        if (m_aout) {
                m_aout->stop();
                delete m_aout;
        }

        m_aout = new QAudioOutput(m_outFormat, this);
        m_aout->setCategory(QString("rsdk Out"));
        m_aout->setBufferSize(1024);
        //m_aout->setNotifyInterval(30);

        //connect(m_aout, SIGNAL(notify()), this, SLOT(rrfin()));
        connect(m_aout, SIGNAL(stateChanged(QAudio::State)), this, SLOT(handleOutputStateChange(QAudio::State)));

        m_aout->start(m_pipeOut);
        //this->moveToThread(&collectThread);
        //m_dout = m_aout->start();

        connect(m_pipeOut, SIGNAL(bytesWritten(qint64)), this, SLOT(handleBytesWritten(qint64)));
        connect(m_pipeOut, SIGNAL(channelBytesWritten(int,qint64)), this, SLOT(handleChannelBytesWritten(int,qint64)));
        connect(m_pipeOut, SIGNAL(channelReadyRead(int)), this, SLOT(handleChannelReadyRead(int)));
        connect(m_pipeOut, SIGNAL(readChannelFinished()), this, SLOT(handleReadChannelFinished()));
        connect(m_pipeOut, SIGNAL(readyRead()), this, SLOT(handleReadyRead()));

        //QTimer::singleShot(0, this, SLOT(writeSome()));
}

void audioPlatform::handleBytesWritten(qint64 bytes)
{
    qDebug() << __func__ << "bytes " << bytes;
}

void audioPlatform::handleChannelBytesWritten(int channel, qint64 bytes)
{
    qDebug() << __func__ << "channel " << channel << " bytes " << bytes;
}

void audioPlatform::handleChannelReadyRead(int channel)
{
    qDebug() << __func__ << "channel " << channel ;
}

void audioPlatform::handleReadChannelFinished()
{
    qDebug() << __func__ ;
}

void audioPlatform::handleReadyRead()
{
    qDebug() << __func__ ;
}

void audioPlatform::handleAboutToClose()
{
    qDebug() << __func__ ;
}

void audioPlatform::handleInputStateChange(QAudio::State state)
{
        if (m_ain->error() != QAudio::NoError)  {
                // handle error here
                ////qDebug() << __func__ << " Error " << m_ain->error();
        }
        switch (state) {
        case QAudio::ActiveState:
                // started
                //qDebug() << __func__ << " ActiveState";
                break;
        case QAudio::SuspendedState:
                // suspended
                //qDebug() << __func__ << " SuspendedState";
                break;
        case QAudio::StoppedState:
                // stopped
                //qDebug() << __func__ << " StoppedState";
                break;
        case QAudio::IdleState:
                // idle
                //qDebug() << __func__ << " IdleState";
                break;
        default:
                // ?
                break;
        }
}

void audioPlatform::handleOutputStateChange(QAudio::State state)
{
        if (m_aout->error() != QAudio::NoError)  {
                qDebug() << __func__ << " Error " << m_aout->error();
        }
        switch (state) {
        case QAudio::ActiveState:
                qDebug() << __func__ << " ActiveState";
                //m_pushTimer->stop();
                break;
        case QAudio::SuspendedState:
                qDebug() << __func__ << " SuspendedState";
                break;
        case QAudio::StoppedState:
                qDebug() << __func__ << " StoppedState";
                break;
        case QAudio::IdleState:
                qDebug() << __func__ << " IdleState";
                //m_pushTimer->start(5);
                break;
        default:
                qDebug() << __func__ << " UnknownState";
                break;
        }
}

void audioPlatform::readSome()
{
        assert(m_din);
        assert(r);

        // don't run audio if usb buffers aren't running
        if (r->rozy->running == false)
                return;

        quint64 numBytesPerSample = m_inFormat.sampleSize() / 8;

        // qt in bytes, not samples
        // period size is minimum required to maintain the stream sync
        quint64 qtPeriodSize = m_ain->periodSize();
        //quint64 qtPeriodSamples = qtPeriodSize / numBytesPerSample;
        quint64 qtBytesAvail = m_ain->bytesReady();
        quint64 qtSampsAvail = qtBytesAvail / numBytesPerSample;

        quint64 rozyBufSize = r->rozy->getRozyXferBufSize();
         quint64 rozySampsAvail = r->rozy->ainAvail();
        //quint64 rozySampleSize = r->rozy->getRozyXferSampleSize();
         quint64 rozyBufSamps = r->rozy->getRozyXferSampleSize();

        if (qtBytesAvail < qtPeriodSize) {
                return;
        }

        quint64 sampsToRecv = qtSampsAvail > rozyBufSamps ? rozyBufSamps : qtSampsAvail;

        if (sampsToRecv > rozySampsAvail) {
                sampsToRecv = rozySampsAvail;
        }

        if (!sampsToRecv) {
                sampsToRecv = qtSampsAvail < rozyBufSamps ? qtSampsAvail : rozyBufSamps;
        }

#ifdef AP_TIMING
        // timing, debug, testing
        qint64 curElapsed = m_ain->elapsedUSecs();
        qint64 tdiff = curElapsed - m_lastElapsed;
        qDebug() << __func__ << "read tdiff: " << tdiff;
        m_lastElapsed = curElapsed;
#endif

        int ndx = r->rozy->ainHead % rozyBufSamps;

        // wrap the buffer ndx if necessary
        if (((quint64)(ndx + sampsToRecv)) > rozyBufSize) {
                int num = rozyBufSamps - ndx;
                int remain = sampsToRecv - num;
                int len = readData((char *)&r->rozy->ainXfer[ndx], num * numBytesPerSample);
                if (len>0)
                        r->rozy->ainHead += num;
                len = readData((char *)&r->rozy->ainXfer[0], remain * numBytesPerSample);
                if (len>0)
                        r->rozy->ainHead += remain;
        } else {
                int len = readData((char *)&r->rozy->ainXfer[ndx], sampsToRecv * numBytesPerSample);
                if (len>0)
                        r->rozy->ainHead += sampsToRecv;
        }
}

void audioPlatform::writeSome()
{
        assert(m_dout);
        assert(r);

        // don't play audio if there are no usb buffers running
        if (r->rozy->running == false)
                return;

        quint64 numBytesPerSample = m_outFormat.sampleSize() / 8;

        // qt in bytes, not samples
         //quint64 qtPeriodSize = m_aout->periodSize();
         quint64 qtBytesFree = m_aout->bytesFree();
         quint64 qtSampsFree = qtBytesFree / numBytesPerSample; // total samples for both L & R
         //qint64 qtBytesToWrite = m_dout->bytesToWrite();

        // rozy in samples, not bytes
         quint64 rozySampsAvail = r->rozy->aoutAvail();
        // quint64 rozyBufSize = r->rozy->getRozyXferBufSize();
         quint64 rozyBufSamps = r->rozy->getRozyXferSampleSize();

        int sampsToSend;
        if (qtSampsFree <= rozySampsAvail)
                sampsToSend = qtSampsFree;
        else
                sampsToSend = rozySampsAvail;

        // sampNdx is in samples, not bytes
        // circular access to aoutXfer buffer
         quint64 sampNdx = r->rozy->aoutTail % rozyBufSamps;
        if (((sampNdx + (quint64)sampsToSend)) > rozyBufSamps) {
                int num = rozyBufSamps - sampNdx;
                int remain = sampsToSend - num;
                int len = writeData((const char *)&r->rozy->aoutXfer[sampNdx], num * numBytesPerSample);
                if (len>0)
                        r->rozy->aoutTail += (len / numBytesPerSample);
                len = writeData((const char *)&r->rozy->aoutXfer[0], remain * numBytesPerSample);
                if (len>0)
                        r->rozy->aoutTail += (len / numBytesPerSample);
        } else {
                int len = writeData((const char *)&r->rozy->aoutXfer[sampNdx], sampsToSend * numBytesPerSample); // L+R
                if (len>0)
                        r->rozy->aoutTail += (len / numBytesPerSample);
        }
}

qint64 audioPlatform::writeData(const char *buf, unsigned int len)
{
        qint64 written = 0;
        qint64 bytesToWrite = (qint64) len;
        while (bytesToWrite) {
                // bytes
                written = m_dout->write(buf,bytesToWrite);
                bytesToWrite -= written;
        }
        return written;
}

int audioPlatform::readData(char *buf, unsigned int len)
{
        int numread = 0;
        if (len) {
                // bytes
                numread = m_din->read(buf, (qint64)len);
        }
        return numread;
}

void audioPlatform::rrfin()
{
        //m_pushTimer->stop();
        writeSome();
}
