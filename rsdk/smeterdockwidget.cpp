﻿#include <unistd.h>
//#include <QGLWidget>
#include <QEvent>
#include <QScrollBar>
#include <QResizeEvent>
#include <QPainterPath>
#include <QOpenGLWidget>
//#include <QGLWidget>

#include <dttsp.h>

#include "rsdk.h"

#include "smeterdockwidget.h"
#include "ui_smeterdockwidget.h"

#define TOT_METER_ANGLE_SPAN 120.0
static const double S9 = -73.0; // S9 = -73 dBm switch to red for +dB/S9
static const qreal arcWidth = 3.5;
// make a log scale function 10^i to scale percentages from 0.0 - 1.0 log10
// VSWR pow() calculations use these in draw and arc create routines
static const double b = 10.0;
static const double s = 1.0 / (b - 1);
static const double t = -1.0 / (b - 1);
static const double vswrStartVal = 1.0;
static const double vswrEndVal = 10.0;

static const double txtVOffset = -7;
static const qreal labelSize = 1.0;

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

// keep peaks and a timer to decay them
SMeterPeak::SMeterPeak(QWidget *parent, int msecs) :
    m_color("white"),
    m_decay(0),
    m_peakVal(-10000.0)
{
    m_decay = (unsigned int) msecs;
    m_t = new QTimer(parent);
    m_t->stop();
    m_t->setInterval(m_decay);
    //m_t->setTimerType(Qt::PreciseTimer);
    connect(m_t, SIGNAL(timeout()), this, SLOT(peakTimeout()));
    //m_t->start();

}

SMeterPeak::~SMeterPeak() {
    delete m_t;
}

double SMeterPeak::peakVal() const
{
    return m_peakVal;
}

void SMeterPeak::setPeakVal(double peakVal)
{
    // sustain peak for interval m_decay
    if (peakVal>=m_peakVal)
    {
        m_peakVal = peakVal;
        m_t->start(m_decay);
    }
}

void SMeterPeak::peakTimeout()
{
    m_t->setInterval(m_decay);
    m_peakVal = -10000.0;
    m_t->start();
}

QColor SMeterPeak::color() const
{
    return m_color;
}

void SMeterPeak::setColor(const QColor &color)
{
    m_color = color;
}


/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

// a worker class w/ a timer to call something
SMeterWorker::SMeterWorker(SmeterDockWidget*d) :
    fps(60) // update rate, but never really gets more than 24fps on average
{
    dock = d;
    rate = 1000.0 / fps;
    grabTimer.stop();
    grabTimer.setInterval(0);
    grabTimer.setTimerType(Qt::PreciseTimer);
    grabTimer.start();
    connect(&grabTimer, SIGNAL(timeout()), this, SLOT(grabMeter()));
}

SMeterWorker::~SMeterWorker()
{
    grabTimer.stop();
    disconnect(&grabTimer, SIGNAL(timeout()), this, SLOT(grabMeter()));
}

void SMeterWorker::grabMeter()
{
    // reach out to DttSP and collect smeter and tx meter values

    // rx
    if (dock->theRsdk->VfoDock->subRxOn())
        dock->meter[0] = CalculateRXMeter(dock->theRsdk->rozy->curMercury, 1, dock->getRxMeterType()); // adjust for display
    else
        dock->meter[0] = CalculateRXMeter(dock->theRsdk->rozy->curMercury, 0, dock->getRxMeterType()); // adjust for display

    // tx
    dock->meter[1] = CalculateTXMeter(2, dock->getTxMeterType());

    emit collectDone();
}

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

SmeterDockWidget::SmeterDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::SmeterDockWidget),
    rxMeterType(SIGNAL_STRENGTH),
    txMeterType(PWR),
    startAngle(150.0),
    plusDbArc(30.0),
    sunitsArc(120.0),
    S9str("9"),
    plusDbArcColor("darkRed"),
    sunitsColor("black"),
    brightLight(QColor("Black")),
    darkLight(QColor("Black")),
    arcColor(QColor("NavajoWhite")),
    needleColor(QColor("White")),
    traceColor(QColor("green")),
    traceOn(true),
    arcOn(true),
    needleOn(true),
    peaksOn(true),
    arcOffset(20.0),
    vOffset(20),
    yMin(-125.0),
    yMax(-30.0),
    m_largeFont(QFont("Fixed", 24, QFont::SansSerif)),
    m_largeFontMetrics(m_largeFont),
    m_meterFont(QFont("Fixed", 12, QFont::SansSerif)),
    m_meterFontMetrics(m_meterFont)
{
    // register the metatype enum
    qRegisterMetaType<METERTYPE>();

    dbmLabel.clear();

    ui->setupUi(this);

    SMeterPeak *peak = new SMeterPeak(this, 1000 /*msec*/);
    peak->setColor(QColor("black"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 2000 /*msec*/);
    peak->setColor(QColor("brown"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 3000 /*msec*/);
    peak->setColor(QColor("red"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 4000 /*msec*/);
    peak->setColor(QColor("orange"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 5000 /*msec*/);
    peak->setColor(QColor("yellow"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 6000 /*msec*/);
    peak->setColor(QColor("green"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 7000 /*msec*/);
    peak->setColor(QColor("blue"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 8000 /*msec*/);
    peak->setColor(QColor("violet"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 9000 /*msec*/);
    peak->setColor(QColor("grey"));
    peaks.append(peak);

    peak = new SMeterPeak(this, 10000 /*msec*/);
    peak->setColor(QColor("white"));
    peaks.append(peak);

    scene = new QGraphicsScene(this);

    QPixmap np = QPixmap(1,1);
    np.fill(Qt::transparent);
    QPixmap sp = QPixmap(1,1);
    sp.fill(Qt::transparent);

    // np in front
    needlePixmap = scene->addPixmap(np);
    needlePixmap->setZValue(100.0);
    // sp in back
    scalePixmap = scene->addPixmap(sp);
    scalePixmap->setZValue(0.0);

    ui->graphicsView->setScene(scene);

    // setup the worker thread for periodic collection of DttSP sample data
    grabThread = new QThread(this);
    grabThread->setObjectName(QString("rsdk smeter"));
    mgrabber = new SMeterWorker(this);
    mgrabber->moveToThread(grabThread);
    connect(mgrabber, SIGNAL(collectDone()), this, SLOT(drawMeter()));

    connect (ui->rxMtrButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(rxMtrBtnGrpClicked(QAbstractButton*)));
    connect (ui->txMtrButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(txMtrBtnGrpClicked(QAbstractButton*)));

    connect (ui->arcTB, SIGNAL(clicked(bool)), this, SLOT(setArcOn(bool)));
    connect (ui->needleTB, SIGNAL(clicked(bool)), this, SLOT(setNeedleOn(bool)));
    connect (ui->traceTB, SIGNAL(clicked(bool)), this, SLOT(setTraceOn(bool)));
    connect (ui->peaksTB, SIGNAL(clicked(bool)), this, SLOT(setPeaksOn(bool)));

    //connect (scene, SIGNAL(sceneRectChanged(QRectF)), this, SLOT(redrawScales(QRectF)));

    connect (ui->brightCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(setBrightLight(QColor)));
    connect (ui->darkCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(setDarkLight(QColor)));
    connect (ui->arcCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(setArcColor(QColor)));
    connect (ui->traceCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(setTraceColor(QColor)));
    connect (ui->needleCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(setNeedleColor(QColor)));

    connect (ui->splitter, SIGNAL(splitterMoved(int, int)), this, SLOT(splitterMoved(int, int)));
}

SmeterDockWidget::~SmeterDockWidget()
{
    QList<SMeterPeak *>::iterator i;
    for (i = peaks.begin(); i != peaks.end(); i++)
        delete *i;

    grabThread->requestInterruption();
    grabThread->quit();
    while (grabThread->isRunning())
        grabThread->wait();
    delete grabThread;

    delete needlePixmap;
    delete mgrabber;
    delete scene;
    delete ui;
}

void SmeterDockWidget::start()
{
    // close the bottom smeterdock scroll area
    QList<int> theSizes = ui->splitter->sizes();
    //theSizes[0] = -1;
    theSizes[1] = 0;
    theSizes[2] = 0;
    ui->splitter->setSizes(theSizes);

    setRxMeterType(rxMeterType);
    setTxMeterType(txMeterType);
    setDarkLight(darkLight);
    setBrightLight(brightLight);
    setArcColor(arcColor);
    setNeedleColor(needleColor);
    setTraceColor("green");
    setNeedleOn(needleOn);
    setArcOn(arcOn);
    setTraceOn(traceOn);
    // adjust size
    makePixmaps();
    grabThread->start();

    connect(theRsdk->PlotDrawOptionsDock, SIGNAL(spxLGradientChanged(const QLinearGradient &)), this, SLOT(setLGrad(const QLinearGradient &)));
}

// enlarge font size until a given string fits a pixmap
int SmeterDockWidget::fetchReqFontSize()
{
    static const QString testStr("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789012345678901234567890");
    int fontSize = 8; // start at this fontSize and increase until fm.width is < pixmap width

    if (!needlePixmap->pixmap().height() || !needlePixmap->pixmap().width())
        return fontSize;

    int h = needlePixmap->pixmap().height()/10;
    int w = needlePixmap->pixmap().width();

    QFont f("Fixed", fontSize, QFont::SansSerif);
    while( true )
    {
        f.setPixelSize( fontSize );
        QRect r = QFontMetrics(f).boundingRect(testStr);
        if ((r.height() <= h) && (r.width() <= w))
            fontSize++;
        else
            break;
    }
    m_meterFont = f;
    m_meterFontMetrics = QFontMetrics(m_meterFont);
    return fontSize;
}

void SmeterDockWidget::drawMeter()
{

    /*
    gettimeofday(&end_time, NULL);
    unsigned long long diff = timeval_diff(&start_time, &end_time);
    gettimeofday(&start_time, NULL);
    qDebug() << __func__ << " diff " << diff/1000.0 << " mSec" <<  " tid " << QThread::currentThreadId();
    */

    //drawLock.lock();

    if (isVisible())
    {
        QString sstr("");
        QString sstr2("");
        QPainterPathStroker pps;

        // yeah, this is a mess while I figure meter scales out
        QPointF pf1;

        // update the meter
        const QSize meterSize = ui->graphicsView->size();
        int w = meterSize.width();
        int h = meterSize.height();
        int frame_width = ui->graphicsView->frameWidth();
        int wid = meterSize.width() - frame_width;
        int ht = meterSize.height() - frame_width;

        qreal x0 = (wid/24);
        qreal y0 = (ht/6);
        qreal w0 = wid - (x0*2);
        qreal h0 = ht - y0;

        QPixmap p = needlePixmap->pixmap();
        p.fill(Qt::transparent);

        QPainter pPainter(&p);
        pPainter.setRenderHint(QPainter::Antialiasing, true);
        pPainter.setRenderHint(QPainter::TextAntialiasing, true);
        pPainter.setRenderHint(QPainter::SmoothPixmapTransform, true);
        pPainter.setRenderHint(QPainter::HighQualityAntialiasing, true);

        // make and use a pen
        QPen pen;
        pen.setCapStyle(Qt::FlatCap);
        pen.setColor("white");

                // draw the dynamic data for any scale draw
        double angle;
        double val = 0.0;
        double totalYRange = 0.0;
        double percentRange = 0.0;
        unsigned int mtrRcvNdx = 0;
        double arc = 0.0;;
        if (rxMeterType == SIGNAL_STRENGTH || rxMeterType == AVG_SIGNAL_STRENGTH)
        {
            // S1 = -121 dBm
            // S9 = - 73 dBm
            //enum HpsdrPreAmpEnum { Preamp_Off=HPSDR_PREAMP_OFF,
            //                       Preamp_On=HPSDR_PREAMP_ON };

            double preAmpOffset = (theRsdk->HpsdrDock->getPreAmp() == hpsdr::Preamp_Off) ? 0.0 : 20.0;

            val = meter[mtrRcvNdx] - preAmpOffset;
            double z1 = yMin + fabs(val);
            totalYRange = fabs(yMin) - fabs(yMax);
            percentRange = fabs(z1) / totalYRange; // precentage of totalYRange
            percentRange = std::max(0.0, std::min(percentRange, 1.0));
            arc = TOT_METER_ANGLE_SPAN * percentRange;
            pf1 = totSUnitsArcPath.pointAtPercent(percentRange);

            sstr = "S-Units/S9+dB";

            x0 = (w/24);
            y0 = (h/6);
            w0 = w - (x0*2);
            h0 = h - y0;
        }
        // if xmit? do selected xmit trace
        if (theRsdk->rozy->getXmit())
        {
            // xmit needle draw
            double percentRange = 0.0;

            if (txMeterType == SmeterDockWidget::METERTYPE::VSWR)
            {
                // trace on VSWR scale
                x0 = (w/24)*3;
                y0 = (h/6)*3;
                w0 = w - (x0*2);
                h0 = h - y0;

                // reflected may be larger than forward
                val = theRsdk->rozy->vswr; // we don't care here, just one positivie scale
                //sstr = " Alex VSWR " + QString::number(val, 'f', 2);
                QString str = QString("Alex VSWR %1").arg(val, 4, 'f', 2);
                sstr = str;

                totalYRange = vswrEndVal - vswrStartVal;
                if (val > 1.0)
                {
                    double xx = 1.0 - (s * pow(b, 1.0 - ((val-1.0) / totalYRange)) + t);
                    percentRange = xx;
                }
                percentRange = std::max(0.0, std::min(percentRange, 1.0));
                pf1 = totVswrArcPath.pointAtPercent(percentRange);
            }
            else if(txMeterType ==  SmeterDockWidget::METERTYPE::PWR)
            {

                // trace on PWR scale
                x0 = (w/24)*2;
                y0 = (h/6)*2;
                w0 = w - (x0*2);
                h0 = h - y0;

                val = (double)theRsdk->rozy->penelopeFwdPower;
                percentRange = (val / (BIT(12) - 1.0)); // max value is 12 bit
                sstr = " PWR % " + QString::number(int(percentRange * 100));
                percentRange = std::max(0.0, std::min(percentRange, 1.0));
                pf1 = totpctArcPath.pointAtPercent(percentRange);
            }

            arc = TOT_METER_ANGLE_SPAN * percentRange;
        }

        if (traceOn)
        {

            //QLinearGradient lg= theRsdk->PlotDrawOptionsDock->getSpxGradient();

            QPen pen2(QBrush(sTraceLinearGradient), sArcWidth);
            pen2.setCapStyle(Qt::RoundCap);
            pPainter.setPen(pen2);
            if (arc > TOT_METER_ANGLE_SPAN)
                arc = TOT_METER_ANGLE_SPAN;
            pPainter.drawArc(x0, y0-arcWidth, w0, h0-arcWidth, startAngle*16, -arc*16);
        }

        titlePath = QPainterPath();
        titlePath.addText(wid/2-m_meterFontMetrics.width(sstr)/2, ht-m_meterFontMetrics.height(), m_meterFont, sstr);
        if (!sstr2.isEmpty()) {
            titlePath2 = QPainterPath();
            titlePath2.addText(wid/2-m_meterFontMetrics.width(sstr2)/2, ht-m_meterFontMetrics.height(), m_meterFont, sstr2);
        }
        //titlePath.setFillRule(Qt::FillRule::WindingFill);
        //Qt::FillRule fr = titlePath.fillRule();
        //pps.setCapStyle(Qt::FlatCap);
        pps.setWidth(0.5);
        titlePath = pps.createStroke(titlePath);
        titlePath2 = pps.createStroke(titlePath2);

        pen.setColor(arcColor);
        pen.setWidthF(0.5);
        pPainter.setPen(pen);
        pPainter.drawPath(titlePath);
        if (!sstr2.isEmpty()) {
            pPainter.setPen(pen);
            pPainter.drawPath(titlePath2);
        }

        pPainter.save();

        // draw the rx peaks
        if (peaksOn && !theRsdk->rozy->getXmit())
        {
            QList<SMeterPeak *>::iterator i;
            QBrush ahb;
            ahb.setStyle(Qt::SolidPattern);
            for (i = peaks.begin(); i != peaks.end(); i++)
            {
                (*i)->setPeakVal(val);

                //delete *i;
                double val2 = (*i)->peakVal();
                double z1 = yMin + fabs(val2);
                percentRange = fabs(z1) / totalYRange; // precentage of totalYRange
                percentRange = std::max(0.0, std::min(percentRange, 1.0));
                QPointF pf = totSUnitsArcPath.pointAtPercent(percentRange);

                ahb.setColor((*i)->color());
                pen.setColor((*i)->color());
                pen.setWidthF(0.5);
                pen.setCapStyle(Qt::RoundCap);
                pPainter.setPen(pen);
                pPainter.setBrush(ahb);
                pPainter.drawEllipse(pf.x(), pf.y(), h/40, h/40);
            }
        }

        pPainter.restore();

        // draw the meter needle
        if (needleOn)
        {
            pen.setColor(needleColor);
            pen.setWidthF(1.5);
            pen.setCapStyle(Qt::RoundCap);
            pPainter.setPen(pen);

            QLineF needle (wid/2.0, ht, pf1.x(), pf1.y());
            QPointF as1 = needle.pointAt(0.96);
            pPainter.drawLine(needle);
        }

        needlePixmap->setPixmap(p);

    }

    //drawLock.unlock();
}

void SmeterDockWidget::rxMtrBtnGrpClicked(QAbstractButton *b)
{
    if (b == ui->AvgSigRB)
        setRxMeterType(AVG_SIGNAL_STRENGTH);
    else if (b == ui->ADCRealRB)
        setRxMeterType(ADC_REAL);
    else if (b == ui->ADCImagRB)
        setRxMeterType(ADC_IMAG);
    else if (b == ui->AGCGainRB)
        setRxMeterType(AGC_GAIN);
    else // if (b == ui->SigRB)
        setRxMeterType(SIGNAL_STRENGTH);
}

void SmeterDockWidget::txMtrBtnGrpClicked(QAbstractButton *b)
{
    if (b == ui->TxMtrMICRB)
        setTxMeterType(MIC);
    else if (b == ui->TxMtrALCRB)
        setTxMeterType(ALC);
    else if (b == ui->TxMtrEQRB)
        setTxMeterType(EQtap);
    else if (b == ui->TxMtrLEVELERRB)
        setTxMeterType(LEVELER);
    else if (b == ui->TxMtrCOMPRB)
        setTxMeterType(COMP);
    else if (b == ui->TxMtrCPDRRB)
        setTxMeterType(CPDR);
    else if (b == ui->TxMtrALCGRB)
        setTxMeterType(ALC_G);
    else if (b == ui->TxMtLVLGRB)
        setTxMeterType(LVL_G);
    else if (b == ui->TxMtrMICPKRB)
        setTxMeterType(MIC_PK);
    else if (b == ui->TxMtrALCPKRB)
        setTxMeterType(ALC_PK);
    else if (b == ui->TxMtrEQPKRB)
        setTxMeterType(EQ_PK);
    else if (b == ui->TxMtrLEVELRPKRB)
        setTxMeterType(LEVELER_PK);
    else if (b == ui->TxMtrCOMPPKRB)
        setTxMeterType(COMP_PK);
    else if (b == ui->TxMtrCPDRPKRB)
        setTxMeterType(CPDR_PK);
    else if (b == ui->TxMtrVSWRRB)
        setTxMeterType(VSWR);
    else //if (b == ui->TxMtrPWRRB) // default
        setTxMeterType(PWR);
}

void SmeterDockWidget::redrawScales(QRectF rf)
{
    (void)rf;
    makePixmaps();
}

void SmeterDockWidget::makeArcs(int w, int h)
{
    if ((h < 0) || (w < 0))
        return;

    //drawLock.lock();

    QString sstr("");
    QString sstr2("");

    QPainterPathStroker pps;

    //debugPath = QPainterPath();
    // make smeter arcs
    //if (rxMeterType == SIGNAL_STRENGTH || rxMeterType == AVG_SIGNAL_STRENGTH)
    {
        // draw the scale arcs
        // in Qt angles are at 0 degrees at 3 o'clock, positive for CCW, negative for CW 1/16 degree increments
        double plusDbArcRange = fabs(S9) + yMax;
        double totalYRange = fabs(yMin) + yMax;
        double plusDbArcPerCent = plusDbArcRange/totalYRange;
        double plusDbArc = TOT_METER_ANGLE_SPAN * plusDbArcPerCent;
        sunitsArc = TOT_METER_ANGLE_SPAN - plusDbArc;

        plusDbArcPath = QPainterPath();
        plusDbArcLabels = QPainterPath();
        plusDb2ArcPath = QPainterPath();

        qreal x0 = (w/24);
        qreal y0 = (h/7.7); // emprically determined value
        qreal w0 = w - (x0*2);
        qreal h0 = h - y0;

        // make a full-scale arc that we use to find percentage from in draw
        totSUnitsArcPath = QPainterPath();
        totSUnitsArcPath.arcMoveTo(x0, y0, w0, h0, startAngle);
        totSUnitsArcPath.arcTo(x0, y0, w0, h0, startAngle, -TOT_METER_ANGLE_SPAN);

        y0 = (h/6);
        h0 = h - y0;

        plusDbArcPath.arcMoveTo(x0, y0, w0, h0, startAngle - sunitsArc);
        plusDbArcPath.arcTo(x0, y0, w0, h0, startAngle - sunitsArc, -plusDbArc);

        // make a dummy arc to anchor text above the plusDbArcPath
        plusDb2ArcPath.arcMoveTo(x0, y0+txtVOffset, w0, h0+txtVOffset, startAngle - sunitsArc);
        plusDb2ArcPath.arcTo(x0, y0+txtVOffset, w0, h0+txtVOffset, startAngle - sunitsArc, -plusDbArc);

        // start at S9 and draw plusDbArcRange tic marks and labels
        int count = 0;
        for (int i=(int)S9; i<=yMax; i+=5)
        {
            double norm = double(i) - S9;
            double percentRange = (norm / plusDbArcRange);
            percentRange = std::max(0.0, std::min(percentRange, 1.0));
            QPointF pap2 = plusDb2ArcPath.pointAtPercent(percentRange);

            QPainterPath p = QPainterPath();
            p.moveTo(w/2, h);
            p.lineTo(pap2.x(), pap2.y());

            QPainterPath l = QPainterPath();
            QPointF pf2 = p.pointAtPercent(0.97);
            // labels skip S9 as it is already marked
            if (count)
            {
                if (count%2) {
                    l.moveTo(pap2);
                    l.lineTo(pf2);
                } else {
                    // only enumerate the tens to keep meter clutter down
                    pf2 = p.pointAtPercent(0.97);
                    l.moveTo(pap2);
                    l.lineTo(pf2);
                    sstr = " +" + QString::number(count*5)+" ";
                    plusDbArcLabels.addText(pap2.x()-m_meterFontMetrics.width(sstr)/2, pap2.y()-m_meterFontMetrics.height()/4, m_meterFont, sstr);
                }
                plusDbArcLabels.addPath(l);
            }
            count++;
        }

        pps.setCapStyle(Qt::RoundCap);
        pps.setJoinStyle(Qt::RoundJoin);
        pps.setWidth(arcWidth);
        plusDbArcPath = pps.createStroke(plusDbArcPath);

        pps.setCapStyle(Qt::FlatCap);
        pps.setWidth(labelSize);
        plusDbArcLabels = pps.createStroke(plusDbArcLabels);

        sUnitsArcPath = QPainterPath();
        sUnits2ArcPath = QPainterPath();
        sArcLabels = QPainterPath();
        //debugPath = QPainterPath();

        sUnitsArcPath.arcMoveTo(x0, y0, w0, h0, startAngle);
        sUnitsArcPath.arcTo(x0, y0, w0, h0, startAngle, -sunitsArc);

        //debugPath.addPath(totSUnitsArcPath);
        //debugPath.translate(0.0, 8.0);
        // make an arc to anchor text above the plusDbArcPath
        sUnits2ArcPath.arcMoveTo(x0, y0+txtVOffset, w0, h0+txtVOffset, startAngle);
        sUnits2ArcPath.arcTo(x0, y0+txtVOffset, w0, h0+txtVOffset, startAngle, -sunitsArc);
        // starting at S9 (a known level at -73dBm) and working backwards
        count = 0;
        for (int i=(int)S9; i>=yMin; i-=6) // at -6dB per step
        {
            double norm = fabs(yMin - double(i));
            double percentRange = norm / fabs(yMin - (int)S9);
            percentRange = std::max(0.0, std::min(percentRange, 1.0));
            QPointF pap2 = sUnits2ArcPath.pointAtPercent(percentRange);

            // dummy line from which we will extract a segment
            QPainterPath p = QPainterPath();
            p.moveTo(w/2, h);
            p.lineTo(pap2.x(), pap2.y());
            //sArcLabels.addPath(p);

            QPointF pf2 = p.pointAtPercent(0.97);
            QPainterPath l = QPainterPath();
            l.moveTo(pap2);
            l.lineTo(pf2);

            sArcWidth = std::max(sArcWidth, l.length());
            sArcLabels.addPath(l);

            sstr = " S" + QString::number(9 - count) + " ";
            sArcLabels.addText(pap2.x()-m_meterFontMetrics.width(sstr)/2, pap2.y()-m_meterFontMetrics.height()/4, m_meterFont, sstr);

            count++;
        }

        pps.setCapStyle(Qt::RoundCap);
        pps.setJoinStyle(Qt::RoundJoin);
        pps.setWidth(arcWidth);
        sArcWidth = pps.width();
        sUnitsArcPath = pps.createStroke(sUnitsArcPath);

        pps.setCapStyle(Qt::FlatCap);
        pps.setWidth(labelSize);
        sArcLabels = pps.createStroke(sArcLabels);
    }
    //else if (rxMeterType == ADC_REAL || rxMeterType == ADC_IMAG)
    {

        // log scale?
        qreal x0 = (w/24)*2;
        qreal y0 = (h/6)*2;
        qreal w0 = w - (x0*2);
        qreal h0 = h - y0;

        //debugPath.addRect(x0, y0, w0, h0);

        totpctArcPath = QPainterPath();
        totpctArcPath.arcMoveTo(x0, y0, w0, h0, startAngle);
        totpctArcPath.arcTo(x0, y0, w0, h0, startAngle, -TOT_METER_ANGLE_SPAN);

        // make a dummy arc to anchor text above the plusDbArcPath
        pct2ArcPath = QPainterPath();
        pct2ArcPath.arcMoveTo(x0, y0-txtVOffset, w0, h0-txtVOffset, startAngle);
        pct2ArcPath.arcTo(x0, y0-txtVOffset, w0, h0-txtVOffset, startAngle, -TOT_METER_ANGLE_SPAN);

        double pctArcDegrees = 0.0;
        double pctRedArcDegrees = 0.0;
        pctArcLabels = QPainterPath();
        for (int i = 0; i <= 100; i+=10)
        {
            QPointF pf = totpctArcPath.pointAtPercent((double)i/100.0);

            // dummy line to find radial line segment
            QPainterPath p = QPainterPath();
            p.moveTo(w/2, h);
            p.lineTo(pf.x(), pf.y());

            QPointF pf2 = p.pointAtPercent(0.95);
            QPainterPath l = QPainterPath();
            l.moveTo(pf);
            l.lineTo(pf2);

            if (i >= 90 && i <= 99)
            {
                pctArcDegrees = (double)i/100.0 * TOT_METER_ANGLE_SPAN;
                pctRedArcDegrees = TOT_METER_ANGLE_SPAN - pctArcDegrees;
            }

            QString sstr = " " + QString::number(i);
            //QString sstr = " " + QString::number(i) + "% ";
            pctArcLabels.addText(pf2.x()-m_meterFontMetrics.width(sstr)/2, pf2.y()+m_meterFontMetrics.height(), m_meterFont, sstr);

            pctArcLabels.addPath(l);
        }

        pctArcPath = QPainterPath(); // 1.0 - 3.0
        pctArcPath.arcMoveTo(x0, y0, w0, h0, startAngle);
        pctArcPath.arcTo(x0, y0, w0, h0, startAngle, -pctArcDegrees);

        pctRedArcPath = QPainterPath(); // 3.0 - 10.0
        pctRedArcPath.arcMoveTo(x0, y0, w0, h0, startAngle - pctArcDegrees);
        pctRedArcPath.arcTo(x0, y0, w0, h0, startAngle - pctArcDegrees, -pctRedArcDegrees);

        pps.setCapStyle(Qt::FlatCap);
        pps.setWidth(arcWidth);
        pctArcPath = pps.createStroke(pctArcPath);
        pctRedArcPath = pps.createStroke(pctRedArcPath);

        pps.setWidth(labelSize);
        pctRedArcLabels = pps.createStroke(pctRedArcLabels);
        pctArcLabels = pps.createStroke(pctArcLabels);
    }
    //else if (txMeterType == VSWR)
    {

        // log scale?
        double totalYRange = vswrEndVal - vswrStartVal;

        qreal x0 = (w/24)*3;
        qreal y0 = (h/6)*3;
        qreal w0 = w - (x0*2);
        qreal h0 = h - y0;

        totVswrArcPath = QPainterPath();
        totVswrArcPath.arcMoveTo(x0, y0, w0, h0, startAngle);
        totVswrArcPath.arcTo(x0, y0, w0, h0, startAngle, -TOT_METER_ANGLE_SPAN);

        // make a dummy arc to anchor text above the plusDbArcPath
        vswr2ArcPath = QPainterPath();
        vswr2ArcPath.arcMoveTo(x0, y0-txtVOffset, w0, h0-txtVOffset, startAngle);
        vswr2ArcPath.arcTo(x0, y0-txtVOffset, w0, h0-txtVOffset, startAngle, -TOT_METER_ANGLE_SPAN);

        double vswrArcDegrees = 0.0;
        double vswrRedArcDegrees = 0.0;
        vswrArcLabels = QPainterPath();
        for (int i = totalYRange; i >= 0.0; i--)
        {
            double xx = s * pow(b, 1.0 - (i / totalYRange)) + t;
            xx = std::max(0.0, std::min(xx, 1.0));
            QPointF pf = totVswrArcPath.pointAtPercent(1.0 - xx);

            // dummy line to find radial line segment
            QPainterPath p = QPainterPath();
            p.moveTo(w/2, h);
            p.lineTo(pf.x(), pf.y());

            QPointF pf2 = p.pointAtPercent(0.95);
            QPainterPath l = QPainterPath();
            l.moveTo(pf);
            l.lineTo(pf2);

            if (i == 2)
            {
                vswrRedArcDegrees = xx * TOT_METER_ANGLE_SPAN;
                vswrArcDegrees = TOT_METER_ANGLE_SPAN - vswrRedArcDegrees;
            }

            QString sstr = QString::number(i+1, 'f', 1);
            vswrArcLabels.addText(pf2.x()-m_meterFontMetrics.width(sstr)/2, pf2.y()+m_meterFontMetrics.height(), m_meterFont, sstr);

            vswrArcLabels.addPath(l);
        }

        vswrArcPath = QPainterPath(); // 1.0 - 3.0
        vswrArcPath.arcMoveTo(x0, y0, w0, h0, startAngle);
        vswrArcPath.arcTo(x0, y0, w0, h0, startAngle, -vswrArcDegrees);

        vswrRedArcPath = QPainterPath(); // 3.0 - 10.0
        vswrRedArcPath.arcMoveTo(x0, y0, w0, h0, startAngle - vswrArcDegrees);
        vswrRedArcPath.arcTo(x0, y0, w0, h0, startAngle - vswrArcDegrees, -vswrRedArcDegrees);

        pps.setCapStyle(Qt::FlatCap);
        pps.setWidth(arcWidth);
        vswrArcPath = pps.createStroke(vswrArcPath);
        vswrRedArcPath = pps.createStroke(vswrRedArcPath);

        pps.setWidth(labelSize);
        vswrRedArcLabels = pps.createStroke(vswrRedArcLabels);
        vswrArcLabels = pps.createStroke(vswrArcLabels);
    }

    QPixmap p = scalePixmap->pixmap();
    QPainter pPainter(&p);

    pPainter.setRenderHint(QPainter::Antialiasing, true);
    pPainter.setRenderHint(QPainter::TextAntialiasing, true);
    pPainter.setRenderHint(QPainter::SmoothPixmapTransform, true);
    pPainter.setRenderHint(QPainter::HighQualityAntialiasing, true);

    // make a lamp-like panel meter background light
    QRadialGradient gradient(w/2.0, h*1.25, w);
    gradient.setColorAt(0, brightLight);
    gradient.setColorAt(1, darkLight);
    QBrush brush(gradient);
    pPainter.setBrush(brush);
    pPainter.fillRect(p.rect(), gradient);

    QPen pen;
    pen.setColor(arcColor);
    pPainter.fillPath(sUnitsArcPath, QBrush(pen.color()));
    pPainter.fillPath(sArcLabels, QBrush(pen.color()));

    //pen.setColor(arcColor.darker());
    //pPainter.setPen(pen);

    pPainter.fillPath(vswrArcPath, QBrush(pen.color()));
    pPainter.fillPath(vswrArcLabels, QBrush(pen.color()));
    pPainter.fillPath(pctArcPath, QBrush(pen.color()));
    pPainter.fillPath(pctArcLabels, QBrush(pen.color()));

    pen.setColor(plusDbArcColor);
    pPainter.setPen(pen);
    pPainter.fillPath(plusDbArcPath, QBrush(pen.color()));
    pPainter.drawPath(plusDbArcLabels);

    //pen.setColor(plusDbArcColor.darker());
    pPainter.setPen(pen);
    pPainter.fillPath(vswrRedArcPath, QBrush(pen.color()));
    pPainter.fillPath(pctRedArcPath, QBrush(pen.color()));
    pPainter.fillPath(pctRedArcLabels, QBrush(pen.color()));

    scalePixmap->setPixmap(p);

    //drawLock.unlock();
}

void SmeterDockWidget::makePixmaps()
{
    // graphicsView is already resized in resizeEvents so we call here and at other times
    // this makes the "arc" pixmap for the smeter, the needle is drawn in drawMeter
    QSize meterSize = ui->graphicsView->size();
    // silently ignore invalid pixmap operations
    // as either size of zero causes invalid pixmap
    if (meterSize.width()<=0)
        meterSize.setWidth(1);
    if (meterSize.height()<=0)
        meterSize.setHeight(1);

    int frame_width = ui->graphicsView->frameWidth();

    int wid = meterSize.width() - frame_width;
    int ht = meterSize.height() - frame_width;

    QRect rect (0, 0, wid, ht);

    QPixmap sp, np;
    if (!needlePixmap) {
        np = QPixmap(wid, ht);
    } else {
        np = needlePixmap->pixmap();
    }
    if (np.rect() != rect)
        np = np.scaled(meterSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    np.fill(Qt::transparent);

    if (!scalePixmap) {
        sp = QPixmap(wid, ht);
    } else {
        sp = scalePixmap->pixmap();
    }
    if (sp.rect() != rect)
        sp = sp.scaled(meterSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    sp.fill(Qt::transparent);

    ui->graphicsView->setSceneRect(0, 0, wid, ht);

    // replace the old ones
    // ordering is important here for display, lowest layers first
    if (scalePixmap)
        scene->removeItem(scalePixmap);
    scalePixmap = scene->addPixmap(sp);
    if (needlePixmap)
        scene->removeItem(needlePixmap);
    needlePixmap = scene->addPixmap(np);

    fetchReqFontSize();
    makeArcs(wid, ht);
    // pre-make the gradient for the strace arc
    makeGradient();

}

void SmeterDockWidget::makeGradient()
{
    // find the sunit and plusdb arc bounds
    QRectF spath = sUnitsArcPath.boundingRect();
    QRectF ppath = plusDbArcPath.boundingRect();

    // short cut the math and just use the x values, widths may overlap
    s9Pt = spath.width() / ((ppath.x() + ppath.width()) - spath.x());
    // clip 0.0 - 1.0
    s9Pt = std::max(0.0, std::min(s9Pt, 1.0));

    QPointF tl(spath.x(), spath.y());
    QPointF br((spath.x() + spath.width()), spath.x() + spath.height());;

    sTraceLinearGradient.setStart(tl);
    sTraceLinearGradient.setFinalStop(br);
}

void SmeterDockWidget::setLGrad(const QLinearGradient &lg)
{
    sTraceLinearGradient = lg;
    makeGradient();
}

void SmeterDockWidget::splitterMoved(int pos, int ndx)
{
    makePixmaps();
}

bool SmeterDockWidget::getPeaksOn() const
{
    return peaksOn;
}

void SmeterDockWidget::setPeaksOn(bool value)
{
    peaksOn = value;
    bool pstate = ui->peaksTB->blockSignals(true);
    ui->peaksTB->setChecked(value);
    ui->peaksTB->blockSignals(pstate);
}

bool SmeterDockWidget::getNeedleOn() const
{
    return needleOn;
}

void SmeterDockWidget::setNeedleOn(bool value)
{
    needleOn = value;
    bool pstate = ui->needleTB->blockSignals(true);
    ui->needleTB->setChecked(value);
    ui->needleTB->blockSignals(pstate);
}

bool SmeterDockWidget::getArcOn() const
{
    return arcOn;
}

void SmeterDockWidget::setArcOn(bool value)
{
    arcOn = value;
    bool pstate = ui->arcTB->blockSignals(true);
    ui->arcTB->setChecked(value);
    ui->arcTB->blockSignals(pstate);
}

bool SmeterDockWidget::getTraceOn() const
{
    return traceOn;
}

void SmeterDockWidget::setTraceOn(bool value)
{
    traceOn = value;
    bool pstate = ui->traceTB->blockSignals(true);
    ui->traceTB->setChecked(value);
    ui->traceTB->blockSignals(pstate);
}

QColor SmeterDockWidget::getArcColor() const
{
    return arcColor;
}

void SmeterDockWidget::setArcColor(const QColor &value)
{
    arcColor = value;
    bool pstate = ui->arcCSelector->blockSignals(true);
    ui->arcCSelector->setColor(value);
    ui->arcCSelector->blockSignals(pstate);
    makePixmaps();
}

QColor SmeterDockWidget::getNeedleColor() const
{
    return needleColor;
}

void SmeterDockWidget::setNeedleColor(const QColor &value)
{
    needleColor = value;
    bool pstate = ui->needleCSelector->blockSignals(true);
    ui->needleCSelector->setColor(value);
    ui->needleCSelector->blockSignals(pstate);
}

QColor SmeterDockWidget::getTraceColor() const
{
    return traceColor;
}

void SmeterDockWidget::setTraceColor(const QColor &value)
{
    traceColor = value;
    bool pstate = ui->traceCSelector->blockSignals(true);
    ui->traceCSelector->setColor(value);
    ui->traceCSelector->blockSignals(pstate);
    makePixmaps();
}

QColor SmeterDockWidget::getDarkLight() const
{
    return darkLight;
}

void SmeterDockWidget::setDarkLight(const QColor &value)
{
    darkLight = value;
    bool pstate = ui->darkCSelector->blockSignals(true);
    ui->darkCSelector->setColor(value);
    ui->darkCSelector->blockSignals(pstate);
    makePixmaps();
}

QColor SmeterDockWidget::getBrightLight() const
{
    return brightLight;
}

void SmeterDockWidget::setBrightLight(const QColor &value)
{
    brightLight = value;
    bool pstate = ui->brightCSelector->blockSignals(true);
    ui->brightCSelector->setColor(value);
    ui->brightCSelector->blockSignals(pstate);
    makePixmaps();
}


int SmeterDockWidget::getYMax() const
{
    return yMax;
}

void SmeterDockWidget::setYMax(int value)
{
    yMax = value;
    makePixmaps();
}

int SmeterDockWidget::getYMin() const
{
    return yMin;
}

void SmeterDockWidget::setYMin(int value)
{
    (void)value;
    // leave yMin at -125 so meter is alwasy S1 to start
    //yMin = value;
    makePixmaps();
}

SmeterDockWidget::METERTYPE SmeterDockWidget::getTxMeterType() const
{
    return txMeterType;
}

void SmeterDockWidget::setTxMeterType(const METERTYPE &value)
{
    txMeterType = value;

    bool pstate = ui->txMtrButtonGroup->blockSignals(true);
    if (value == MIC)
        ui->TxMtrMICRB->setChecked(true);
    else if (value == ALC)
        ui->TxMtrALCRB->setChecked(true);
    else if (value == EQtap)
        ui->TxMtrEQRB->setChecked(true);
    else if (value == LEVELER)
        ui->TxMtrLEVELERRB->setChecked(true);
    else if (value == COMP)
        ui->TxMtrCOMPRB->setChecked(true);
    else if (value == CPDR)
        ui->TxMtrCPDRRB->setChecked(true);
    else if (value == ALC_G)
        ui->TxMtrALCGRB->setChecked(true);
    else if (value == LVL_G)
        ui->TxMtLVLGRB->setChecked(true);
    else if (value == MIC_PK)
        ui->TxMtrMICPKRB->setChecked(true);
    else if (value == ALC_PK)
        ui->TxMtrALCPKRB->setChecked(true);
    else if (value == EQ_PK)
        ui->TxMtrEQPKRB->setChecked(true);
    else if (value == LEVELER_PK)
        ui->TxMtrLEVELRPKRB->setChecked(true);
    else if (value == COMP_PK)
        ui->TxMtrCOMPPKRB->setChecked(true);
    else if (value == CPDR_PK)
        ui->TxMtrCPDRPKRB->setChecked(true);
    else if (value == VSWR)
        ui->TxMtrVSWRRB->setChecked(true);
    else //if (value == PWR) // default
        ui->TxMtrPWRRB->setChecked(true);
    ui->txMtrButtonGroup->blockSignals(pstate);

    makePixmaps();
}

SmeterDockWidget::METERTYPE SmeterDockWidget::getRxMeterType() const
{
    return rxMeterType;
}

void SmeterDockWidget::setRxMeterType(const METERTYPE &value)
{
    rxMeterType = value;

    bool pstate = ui->rxMtrButtonGroup->blockSignals(true);
    if (value == AVG_SIGNAL_STRENGTH)
        ui->AvgSigRB->setChecked(true);
    else if (value == ADC_REAL)
        ui->ADCRealRB->setChecked(true);
    else if (value == ADC_IMAG)
        ui->ADCImagRB->setChecked(true);
    else if (value == AGC_GAIN)
        ui->AGCGainRB->setChecked(true);
    else // if (value == )SIGNAL_STRENGTH // default
        ui->SigRB->setChecked(true);
    ui->rxMtrButtonGroup->blockSignals(pstate);

    makePixmaps();
}

// overlarded
void SmeterDockWidget::setRxMeterType(const int &value)
{
    setRxMeterType((METERTYPE)value);
}

bool SmeterDockWidget::event(QEvent *event)
{
    // DO NOT DRAW ARCS IN PAINT EVENT, USE RESIZE
    if (event->type() == QEvent::Resize) {
        //QResizeEvent *e = dynamic_cast<QResizeEvent*>(event);
        // reset a value used elsewhere
        sArcWidth = 0.0;
        makePixmaps();
    } else if (event->type() == QEvent::Show) {
        mgrabber->grabTimer.start();
        makePixmaps();
    } else if (event->type() == QEvent::Hide) {
        mgrabber->grabTimer.stop();
    }

    return QDockWidget::event(event);
}
