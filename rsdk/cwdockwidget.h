#ifndef CWDOCKWIDGET_H
#define CWDOCKWIDGET_H

#include <QDockWidget>
#include <QAbstractButton>

#include "rsdkdockwidget.h"

namespace Ui {
class CwDockWidget;
}

class CwDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(double m_freq READ freq WRITE setFreq)
    Q_PROPERTY(unsigned int m_speed READ speed WRITE setSpeed)
    Q_PROPERTY(bool m_keysReversed READ keysReversed WRITE setKeysReversed)
    Q_PROPERTY(unsigned int m_mode READ mode WRITE setMode)
    Q_PROPERTY(unsigned int m_weight READ getWeight WRITE setWeight)
    Q_PROPERTY(bool m_spacing READ spacing WRITE setSpacing)
    Q_PROPERTY(unsigned int m_sideToneVolume READ sideToneVolume WRITE setSideToneVolume)
    Q_PROPERTY(unsigned int m_pttDelay READ pttDelay WRITE setPttDelay)
    Q_PROPERTY(unsigned short m_hangTime READ hangTime WRITE setHangTime)
    Q_PROPERTY(bool m_keyerEnabled READ keyerEnabled WRITE setKeyerEnabled)

public:
    explicit CwDockWidget(QWidget *parent = 0);
    ~CwDockWidget();

    void start(void);

    double freq() const;
    void setFreq(const double &freq);

    unsigned int speed() const;
    void setSpeed(const unsigned int &speed);

    bool keysReversed() const;
    void setKeysReversed(bool keysReversed);

    unsigned int mode() const;
    void setMode(unsigned int mode);

    unsigned int getWeight() const;
    void setWeight(unsigned int weight);

    bool spacing() const;
    void setSpacing(bool spacing);

    unsigned int sideToneVolume() const;
    void setSideToneVolume(unsigned int sideToneVolume);

    unsigned int pttDelay() const;
    void setPttDelay(unsigned int pttDelay);

    unsigned short hangTime() const;
    void setHangTime(unsigned int hangTime);

    bool keyerEnabled() const;
    void setKeyerEnabled(bool keyerEnabled);

public slots:
    void setOpacity(bool b);

signals:
    void cwFreqChanged(double freq);
    void cwSpeedChanged(unsigned int wpm);
    void cwModeChanged(unsigned int mode);
    void cwWeightChanged(unsigned int weight);
    void cwPttDelayChanged(unsigned int delay);
    void cwHangTimeChanged(unsigned int hangTime);
    void cwSpacingChanged(bool spacing);
    void cwKeysReversedChanged(bool reversed);
    void cwSideToneVolumeChanged(unsigned int volume);
    void cwKeyerEnabledChanged(bool enabled);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private slots:
    void on_pitchSpinBox_valueChanged(int arg);
    void on_speedSpinBox_valueChanged(int arg);
    void on_weightSpinBox_valueChanged(int arg);
    void on_pttDelaySpinBox_valueChanged(int arg);
    void on_spacingPB_clicked(bool checked);
    void on_reversePB_clicked(bool checked);
    void on_sideToneVolSlider_valueChanged(int value);
    void selectKeyerMode(QAbstractButton *btn);

    void on_enableTB_clicked(bool checked);
    void hangTimeEditingFinished();

    void on_hangTimeSpinBox_valueChanged(int arg);

private:
    Ui::CwDockWidget *ui;
    double convStringToVal(QString &str);

    double m_freq; // center frequency and tone frequency for cw mode, 0 - 4096 HZ 12 bits
    unsigned int m_speed; // 1 - 60 WPM
    bool m_keysReversed;
    unsigned int m_mode; // 0 = straight key, 1 = Mode A, 2 = Mode B
    unsigned int m_weight;
    bool m_spacing; // on or off
    unsigned int m_sideToneVolume; // 0 - 127
    unsigned int m_pttDelay;
    unsigned short m_hangTime; // 0 - 3 mS
    bool m_keyerEnabled;
};

#endif // CWDOCKWIDGET_H
