/******************************************************************************
 * Project: hpsdr
 *
 * File: buffer.c 
 *
 * Description: implement a simple buffer pool manager to avoid allocs/deallocs
 *				during operations. Alos used to minimize blocking IO time for USB
 *
 * Date: Mon 25 Feb 2013
 *
 * $Revision:$
 *
 * $Header:$
 *
 * Copyright (C) 2013 Kipp A. Aldrich <kippaldirch@gmail.com>
 ******************************************************************************
 *
 * $Log:$
 *
 ******************************************************************************
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>

#include "list.h"
#include "buffer.h"

// don't load common.h here! TODO FIXME
extern void my_usleep(uint32_t usec);

static unsigned int SERIAL=0;

//########### usbBufPool_t ##########//
usbBuf_t *usb_create_buffer(size_t size, const unsigned int ep)
{
    usbBuf_t *entry = (usbBuf_t *)malloc(sizeof(usbBuf_t));
	if (!entry)
	{
		fprintf(stderr, "%s: malloc oxyBuf_t failed\n", __func__);
		return NULL;
	}
    memset(entry, 0, sizeof(*entry));

    INIT_LIST_HEAD(&entry->list); // this entry points at nothing initially

	entry->buf = malloc(size);
	if (!entry->buf)
	{
        fprintf(stderr, "%s: malloc buf size %d\n", __func__, (int)size);
		free(entry);
		return NULL;
	}
	memset(entry->buf, 0, size);

    entry->ep = ep;
    entry->size = size;
    entry->id = SERIAL++;
    entry->ud = NULL; // no user data attached

	// and make a libusb_transfer
    entry->xfr = libusb_alloc_transfer(0);
	if (!entry->xfr)
	{
		fprintf(stderr, "%s: libusb_alloc_transfer failed!\n", __func__);
		free(entry->buf);
		free(entry);
		return NULL;
    }

    //fprintf(stderr, "%s:entry %p p %p n %p id %d buf %p \n", __func__, entry, entry->list.prev, entry->list.next, entry->id, entry->buf);

	return entry;
}

void usb_destroy_buffer_pool(usbBufPool_t *list)
{
	struct list_head *pos, *q;
    usbBuf_t *tmp;

	if (list)
	{
		pthread_mutex_lock(&list->lock);

		list_for_each_safe(pos, q, &list->ob->list) 
		{
            tmp = list_entry(pos, usbBuf_t, list);
			if (tmp)
			{
				if(tmp->buf)
					free(tmp->buf);
				if (tmp->xfr)
					libusb_free_transfer(tmp->xfr);
				list_del(pos);
				free(tmp);
			}
		}
		free(list);
		list=NULL;
		// no more list to unlock!
		// pthread_mutex_unlock(&entry->lock);
	}
}

void usbPrintList(usbBufPool_t *list)
{
    struct list_head *pos;
    usbBuf_t *tmp;

	//pthread_mutex_lock(&list->lock);
	list_for_each(pos, &list->ob->list) 
	{
        tmp = list_entry(pos, usbBuf_t, list);
		if (tmp)
		{
			fprintf(stderr, "%s: %p p %p n %p id %d buf %p\n", __func__, pos, pos->prev, pos->next, tmp->id, tmp->buf);
		}
	}
	//pthread_mutex_unlock(&list->lock);
}

	
usbBufPool_t *usb_create_buffer_pool(const int num_bufs, size_t size, const char *name, const unsigned int ep)
{
	int rc, i;
    usbBufPool_t *list;
    usbBuf_t *base;
	
	//fprintf(stderr, "%s: num_bufs %d size %d\n", __func__, num_bufs, size);

    list = (usbBufPool_t *)malloc(sizeof(usbBufPool_t));
	if (!list)
	{
        fprintf(stderr, "%s: malloc usbBufPool_t failed\n", __func__);
		return NULL;
	}
    memset (list, 0, sizeof(usbBufPool_t));
    list->name = name;
    list->ep = ep;

	rc = pthread_mutex_init(&list->lock, NULL); // unlocked
	if (rc<0)
	{
		free(list);
        fprintf(stderr, "%s: pthread_mutex_init usbBufPool_t failed rc=%d\n", __func__, rc);
		return NULL;
	}

    pthread_mutex_lock(&list->lock);

    INIT_LIST_HEAD(&list->in_flight);

	// initial entry
    base = usb_create_buffer(size, ep);
	if (!base)
	{
		free(list);
        fprintf(stderr, "%s: Initial usb_create_buffer(%d) failed\n", __func__, (int)size);
        pthread_mutex_unlock(&list->lock);
        return NULL;
    }
	INIT_LIST_HEAD(&base->list);
    list->ob=base;

	for (i=0; i<num_bufs; i++)
	{
        usbBuf_t *entry = usb_create_buffer(size, ep);
		if (entry)
		{
			INIT_LIST_HEAD(&entry->list);
			list_add_tail(&entry->list, &base->list);
		}
	}

    pthread_mutex_unlock(&list->lock);

	return list;
}

// remove all the entries in a given list->ob->list
int usb_clear_in_flight(usbBufPool_t *list)
{
    int count = 0;
    struct list_head *pos;
    usbBuf_t *tmp;

    while (pthread_mutex_trylock(&list->lock));

    //int c = usb_count_buffers(list);
    int d = 0;

    list_for_each(pos, &list->in_flight)
        d++;

    // if there are buffers
    // and there are some in flight
    // let things settle a bit
    //if (c && d)
     //   usleep(1);

    list_for_each(pos, &list->in_flight)
    {
        tmp = list_entry(pos, usbBuf_t, list);
        if (tmp) {
            count++;

            //int status = (int) tmp->xfr->status;
            libusb_cancel_transfer(tmp->xfr);
        }
    }
    pthread_mutex_unlock(&list->lock);

    return count;
}

usbBuf_t *usb_get_buffer(usbBufPool_t *list, void *user_data)
{
	struct list_head *pos;
    usbBuf_t *tmp;

	if (!list)
		return NULL;

	tmp=NULL;

    while (pthread_mutex_trylock(&list->lock));

    //fprintf(stderr, "%s usb_count_buffers %d\n", list->name, usb_count_buffers(list));

    list_for_each(pos, &list->ob->list)
	{
        tmp = list_entry(pos, usbBuf_t, list);
		break;
	}

    if (tmp)
    {
        list_del_init(&tmp->list);
        tmp->ud=user_data;
        //list_add_tail(&tmp->list, &list->in_flight);
    }
    pthread_mutex_unlock(&list->lock);

    return tmp;
}

void usb_put_buffer(usbBufPool_t *list, usbBuf_t *entry)
{
	if (!list || !entry)
		return;


    //pthread_mutex_lock(&list->lock);
    while (pthread_mutex_trylock(&list->lock));
    // int c = usb_count_buffers(list);
    //int d = 0;
    //struct list_head *pos;
    //list_for_each(pos, &list->in_flight)
    //    d++;
    list_del_init(&entry->list);
	list_add_tail(&entry->list, &list->ob->list);
    pthread_mutex_unlock(&list->lock);
}

int usb_count_buffers(usbBufPool_t *list)
{
	struct list_head *pos;
	int count;

	if (!list)
		return -1;

	count=0;
    //pthread_mutex_lock(&list->lock);
	list_for_each(pos, &list->ob->list) 
	{
		count++;
	}
    //pthread_mutex_unlock(&list->lock);

	return count;
}

int usb_buffer_take_lock(usbBufPool_t *list)
{
    while (pthread_mutex_trylock(&list->lock));
    return 0;
}

int usb_buffer_release_lock(usbBufPool_t *list)
{
    pthread_mutex_unlock(&list->lock);
    return 0;
}
