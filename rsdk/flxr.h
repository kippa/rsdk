#ifndef FLXR_H_
#define FLXR_H_

#include <string>
using namespace std;

#include <XmlRpc.h>
using namespace XmlRpc;

// ugh, not the best design
#ifndef RADIOOZY_H
#include "radioozy.h"
#endif
class RadioOzy;
extern RadioOzy *rozy;

class flxr
{
public:
    flxr();
    ~flxr();

private:
    pthread_t xml_thread;

    int start_server(int port);
    void stop_server();

    flxr(const flxr &NoCopy);
    flxr &operator=(const flxr &NoAssign);
};

#endif // FLXR_H_
