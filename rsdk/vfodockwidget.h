#ifndef VFODOCKWIDGET_H
#define VFODOCKWIDGET_H
#include <iostream>
#include <stdio.h>
#include <QTextStream>
#include <QStack>
#include <QDockWidget>
#include <QDataStream>
#include <QAbstractButton>
#include <QAbstractSlider>
#include <QLineEdit>
#include <QTimer>

class VfoDockWidget;

#include "rsdk.h"

//#include "serializationcontext.h"
#include "rsdkdockwidget.h"

using namespace std;

namespace Ui {
    class VfoDockWidget;
}

class VfoDockWidget : public rsdkDockWidget
{
    Q_OBJECT

    Q_PROPERTY(vfoTuneInc m_tuneInc READ tuneInc WRITE setTuneInc)
    Q_PROPERTY(vfoCurrSel m_currVfo READ currVfo WRITE setCurrVfo)
    Q_PROPERTY(vfoFreqMode m_freqMode READ freqMode() WRITE setFreqMode)
    Q_PROPERTY(double m_cal READ cal WRITE setCal)
    Q_PROPERTY(bool m_calEnable READ calEnable WRITE setCalEnable)
    Q_PROPERTY(qlonglong m_fA READ getFreqA WRITE setFreqA)
    Q_PROPERTY(qlonglong m_fB READ getFreqB WRITE setFreqB)
    Q_PROPERTY(bool m_pageTune READ pageTune WRITE setPageTune)

public:
    explicit VfoDockWidget(QWidget *parent = 0);
    //VfoDockWidget(const VfoDockWidget& other) : m_freqHm_freqA(other.m_freqA) {};
    ~VfoDockWidget();

    static QList <QString> tuneRates;

    enum vfoTuneInc { vfoTuneInc_1Hz = 0,
                      vfoTuneInc_5Hz,
                      vfoTuneInc_10Hz,
                      vfoTuneInc_25Hz,
                      vfoTuneInc_50Hz,
                      vfoTuneInc_100Hz,
                      vfoTuneInc_250Hz,
                      vfoTuneInc_500Hz,
                      vfoTuneInc_1KHz,
                      vfoTuneInc_2_5KHz,
                      vfoTuneInc_5KHz,
                      vfoTuneInc_10KHz,
                      vfoTuneInc_25KHz,
                      vfoTuneInc_50KHz,
                      vfoTuneInc_100KHz,
                      vfoTuneInc_FINAL
                    };
    Q_ENUM(vfoTuneInc)

    enum vfoFreqMode { vfoFreqMode_Hz = 0,
                       vfoFreqMode_KHz,
                       vfoFreqMode_MHz,
                       vfoFreqMode_FINAL
                     };
    Q_ENUM(vfoFreqMode)

    enum vfoCurrSel { vfoSelect_A = 0,
                      vfoSelect_B,
                      vfoSelect_FINAL
                    };
    Q_ENUM(vfoCurrSel)

    qlonglong getFreqA(void) const;
    void setFreqA(qlonglong f);
    void setFreqA(qlonglong f, bool sendSig);

    qlonglong getFreqB(void) const;
    void setFreqB(qlonglong f);
    void setFreqB(qlonglong f, bool sendSig);

    qlonglong getFreq(void) const;
    QString freqLabel() const;

    int tuneRate_asInt() const;
    vfoTuneInc tuneInc() const;
    int tuneIncByNdx(const int ndx); // range limited access to tuneinc values by index, for mouse acceleration
    void setTuneInc(const int tuneRate);
    void setTuneInc(const int tuneRate, bool update);

    QString freqLabelA() const;
    void setFreqLabelA(const QString &freqLabelA);

    QString freqLabelB() const;
    void setFreqLabelB(const QString &freqLabelB);

    vfoFreqMode freqMode() const;
    vfoCurrSel currVfo() const;
    int currRx() const;

    double cal() const;

    // calFreq is 0.0 unless calibration is in progress
    // then it's the calibration start frequency
    // interested parties can check calFreq() == 0.0 for calibration mode
    double calFreq() const;
    void setCalFreq(double calFreq);

    bool calEnable() const;

    QString freqLabelR2() const;
    void setFreqLabelR2(const QString &freqLabelR2);

    bool event(QEvent *event);

    qlonglong freqR2() const;
    void setFreqR2(const qlonglong &freqR2, bool sendSig);

    bool subRxOn() const;
    void setSubRxOn(bool subRxOn);

    bool pageTune() const;

    bool getXmit() const;

public slots:
    void nextTuneInc(void); // advance to the next tuneInc, but stop at entry before FINAL
    int nextTuneIncPeek(void); // get the next tuneInc, but don't change m_tuneInc
    void prevTuneInc(void); // retreat to the previous tuneInc, but stop at the zero entry
    int prevTuneIncPeek(void); // get the previous tuneInc, but don't change m_tuneInc
    void setOpacity(bool b);
    void setCal(double cal);
    void setCalEnable(bool calEnable);
    void start();
    void setFreq(qlonglong f, bool sendSig);
    void modeChanged(SDRMODE_DTTSP mode);
    void setXmit(bool xmit);
    void doFreqLabels(bool sendSig);
    void setPageTune(bool pageTune);

private slots:
    void calLineEdit_editingFinished();
    void vfoALineEdit_editingFinished();
    void vfoBLineEdit_editingFinished();
    void vfoR2LineEdit_editingFinished();
    void vfoMainLineEdit_editingFinished();
    void setFreqMode(const vfoFreqMode &freqMode);
    void setCurrVfo(const vfoCurrSel &currVfo);
    void setTuneInc(const vfoTuneInc &tuneInc);
    void setCurrRx(int currRx);

    void on_A2B_PB_released();
    void on_B2A_PB_released();
    void on_vfoB_PB_toggled(bool checked);
    void on_vfoA_PB_toggled(bool checked);
    void on_vfoR2_PB_toggled(bool checked);

    void on_S2V_PB_released();
    void calTB_toggled(bool checked);
    void on_pageTune_TB_toggled(bool checked);

    void setFreqModeBtn();
    void freqModeKeyPress(QAbstractButton *btn);
    void setTuneRate(QString rate);

    void updateHandler(); //overload the base rsdkdockwidget class

signals:
    void freqChanged(qlonglong newFreq);
    void tuneIncChanged(vfoTuneInc newTuneInc);
    void subRxFreqChanged(qlonglong subRxFreq);
    void subRxChanged(bool val);
    void pageTuneChanged(bool val);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private:
    Ui::VfoDockWidget *ui;
    QString makeFreqLabel(qlonglong freq);
    void setFreqLabelXX(qlonglong *fReg, QString &freqLabel, QLineEdit *le);

    // the current VFO frequency, either VFO A, or VFO B
    // don't use QStack here anymore, use memories from the database
    //QStack <struct fItem *> m_freqH;
    qlonglong m_fA;
    qlonglong m_fB;

    // the current R2 frequency, second receive
    qlonglong m_freqR2;

    QString m_freqLabelA;
    QString m_freqLabelB;
    QString m_freqLabelR2;
    vfoTuneInc m_tuneInc;
    vfoFreqMode m_freqMode;
    vfoCurrSel m_currVfo;
    int m_currRx;
    double m_cal; // calibrate the frequency by this amount in Hertza
    double m_calFreq; // the calibration start frequency used to calculate offset in hertz via zero-beat
    int m_prevTuneRate;
    bool m_calEnable;
    bool m_subRxOn;
    bool m_pageTune;
    bool m_xmit;

    void vfoSelectKeyPress(QAbstractButton *btn);
    double convLabelToValue(QString &label);
};

#endif // VFODOCKWIDGET_H
