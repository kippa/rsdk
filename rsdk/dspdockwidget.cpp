#include <stdio.h>

#include <QTimer>

#include "dttsp.h"

#include "rsdk.h"
#include "dspdockwidget.h"
#include "ui_dspdockwidget.h"

DspDockWidget::DspDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::DspDockWidget),
    m_ANF(false), m_BIN(false), m_MUTE(false),
    m_NB1(false), m_NB2(false), m_NR(false), m_BNR(false)
{
    ui->setupUi(this);
}

void DspDockWidget::start()
{
    emit dspANFChanged(m_ANF);
    emit dspBINChanged(m_BIN);
    emit dspMUTEChanged(m_MUTE);
    emit dspNB1Changed(m_NB1);
    emit dspNB2Changed(m_NB2);
    emit dspNRChanged(m_NR);
    emit dspBNRChanged(m_BNR);
}

void DspDockWidget::setOpacity(bool b)
{
    if (b)
        setWindowOpacity(0.80);
}

DspDockWidget::~DspDockWidget()
{
    delete ui;
}

void DspDockWidget::setDspANF(bool m)
{
    m_ANF = m;
    on_DspToolButtonANF_toggled(m);
}

void DspDockWidget::setDspNR(bool m)
{
    m_NR = m;
    on_DspToolButtonNR_toggled(m);
}

void DspDockWidget::setDspBNR(bool m)
{
    m_BNR = m;
    on_DspToolButtonBNR_toggled(m);
}

void DspDockWidget::setDspNB1(bool m)
{
    m_NB1 = m;
    on_DspToolButtonNB1_toggled(m);
}

void DspDockWidget::setDspNB2(bool m)
{
    m_NB2 = m;
    on_DspToolButtonNB2_toggled(m);
}

void DspDockWidget::setDspMUTE(bool m)
{
    m_MUTE = m;
    on_DspToolButtonMUTE_toggled(m);
}

void DspDockWidget::setDspBIN(bool m)
{
    m_BIN = m;
    on_DspToolButtonBIN_toggled(m);
}

void DspDockWidget::on_DspToolButtonANF_toggled(bool checked)
{
    m_ANF = checked;
    if (m_ANF == true)
        ui->DspToolButtonANF->setChecked(true);
    else
        ui->DspToolButtonANF->setChecked(false);

    emit dspANFChanged(m_ANF);
}

void DspDockWidget::on_DspToolButtonBIN_toggled(bool checked)
{
    m_BIN = checked;
    if (m_BIN == true)
        ui->DspToolButtonBIN->setChecked(true);
    else
        ui->DspToolButtonBIN->setChecked(false);
    emit dspBINChanged(m_BIN);
}

void DspDockWidget::on_DspToolButtonMUTE_toggled(bool checked)
{
    m_MUTE = checked;
    if (m_MUTE == true)
        ui->DspToolButtonMUTE->setChecked(true);
    else
        ui->DspToolButtonMUTE->setChecked(false);
    emit dspMUTEChanged(m_MUTE);
}

void DspDockWidget::on_DspToolButtonNB1_toggled(bool checked)
{
    m_NB1 = checked;
    if (m_NB1 == true)
        ui->DspToolButtonNB1->setChecked(true);
    else
        ui->DspToolButtonNB1->setChecked(false);
    emit dspNB1Changed(m_NB1);
}

void DspDockWidget::on_DspToolButtonNB2_toggled(bool checked)
{
    m_NB2 = checked;
    if (m_NB2 == true)
        ui->DspToolButtonNB2->setChecked(true);
    else
        ui->DspToolButtonNB2->setChecked(false);
    emit dspNB2Changed(m_NB2);
}

void DspDockWidget::on_DspToolButtonNR_toggled(bool checked)
{
    m_NR = checked;
    if (m_NR == true)
        ui->DspToolButtonNR->setChecked(true);
    else
        ui->DspToolButtonNR->setChecked(false);
    emit dspNRChanged(m_NR);
}

void DspDockWidget::on_DspToolButtonBNR_toggled(bool checked)
{
    m_BNR = checked;
    if (m_BNR == true)
        ui->DspToolButtonBNR->setChecked(true);
    else
        ui->DspToolButtonBNR->setChecked(false);
    emit dspBNRChanged(m_BNR);
}

void DspDockWidget::on_dspGainSlider_valueChanged(int value)
{
    // convert to 0.0 - 1.0 range
    setDspGain(static_cast<double>(value)/100.0);
}

void DspDockWidget::setDspGain(double dspGain)
{
    m_dspGain = dspGain;
    // this should be in rsdk? TODO FIXME, or not
    // now the dsp gain is always at 1.0, and we conntrol volume locally
    SetRXOutputGain(theRsdk->rozy->curMercury, 0, m_dspGain);
    SetRXOutputGain(theRsdk->rozy->curMercury, 1, m_dspGain);
}
