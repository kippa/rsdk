#ifndef HPSDRDOCKWIDGET_H
#define HPSDRDOCKWIDGET_H

#include <QDockWidget>
#include <QAbstractButton>

#include "hpsdr.h"
//#include "serializationcontext.h"
#include "rsdkdockwidget.h"

namespace Ui {
class HpsdrDockWidget;
}

class HpsdrDockWidget : public rsdkDockWidget
{
    Q_OBJECT

    Q_PROPERTY(hpsdr::HpsdrSampleRateEnum m_SampleRate READ getSampleRate WRITE setSampleRate)
    Q_PROPERTY(hpsdr::HpsdrRef10MEnum m_Ref10M READ getRef10M WRITE setRef10M)
    Q_PROPERTY(hpsdr::HpsdrRef122MEnum m_Ref122M READ getRef122M WRITE setRef122M)
    Q_PROPERTY(hpsdr::HpsdrDitherEnum m_Dither READ getDither WRITE setDither)
    Q_PROPERTY(hpsdr::HpsdrRandomEnum m_Random READ getRandom WRITE setRandom)
    //Q_PROPERTY(hpsdr::HpsdrConfigEnum m_Config READ getConfig WRITE setConfig)
    Q_PROPERTY(hpsdr::HpsdrClassEEnum m_ClassE READ getClassE WRITE setClassE)
    Q_PROPERTY(hpsdr::HpsdrDuplexEnum m_Duplex READ getDuplex WRITE setDuplex)
    Q_PROPERTY(hpsdr::HpsdrMicSrcEnum m_MicSrc READ getMicSrc WRITE setMicSrc)
    Q_PROPERTY(hpsdr::HpsdrPreAmpEnum m_PreAmp READ getPreAmp WRITE setPreAmp)
    Q_PROPERTY(hpsdr::HpsdrMicGainEnum m_MicGain READ getMicGain WRITE setMicGain)
    Q_PROPERTY(hpsdr::HpsdrXmitAttEnum m_XmitAtten READ getXmitAtten WRITE setXmitAtten)

public:
    explicit HpsdrDockWidget(QWidget *parent = 0);
    ~HpsdrDockWidget();

public slots:
    void setOpacity(bool b);
    void start();

    void setSampleRate(hpsdr::HpsdrSampleRateEnum sampleRate);
    hpsdr::HpsdrSampleRateEnum getSampleRate(void);
    int getSampleRateVal(void);

    void setRef10M(hpsdr::HpsdrRef10MEnum ref10M);
    hpsdr::HpsdrRef10MEnum getRef10M(void);

    void setRef122M(hpsdr::HpsdrRef122MEnum ref122M);
    hpsdr::HpsdrRef122MEnum getRef122M(void);

    void setDither(hpsdr::HpsdrDitherEnum dither);
    hpsdr::HpsdrDitherEnum getDither(void);

    void setRandom(hpsdr::HpsdrRandomEnum random);
    hpsdr::HpsdrRandomEnum getRandom(void);

    void setConfig(hpsdr::HpsdrConfigEnum Config);
    hpsdr::HpsdrConfigEnum getConfig(void);

    void setClassE(hpsdr::HpsdrClassEEnum ClassE);
    hpsdr::HpsdrClassEEnum getClassE(void);

    void setDuplex(hpsdr::HpsdrDuplexEnum duplex);
    hpsdr::HpsdrDuplexEnum getDuplex(void);

    void setMicSrc(hpsdr::HpsdrMicSrcEnum MicSrc);
    hpsdr::HpsdrMicSrcEnum getMicSrc(void);

    void setPreAmp(hpsdr::HpsdrPreAmpEnum PreAmp);
    hpsdr::HpsdrPreAmpEnum getPreAmp(void);

    hpsdr::HpsdrMicLineEnum getMicLine() const;
    void setMicLine(const hpsdr::HpsdrMicLineEnum &MicLine);

    hpsdr::HpsdrMicGainEnum getMicGain() const;
    void setMicGain(const hpsdr::HpsdrMicGainEnum &MicGain);

    hpsdr::HpsdrXmitAttEnum getXmitAtten() const;
    void setXmitAtten(const hpsdr::HpsdrXmitAttEnum &XmitAtten);

private slots:
    void sampRateBtnGrpClick(QAbstractButton *btn);
    void clk10SelBtnGrpClick(QAbstractButton *btn);
    void clk122SelBtnGrpClick(QAbstractButton *btn);
    void classEBtnGrpClick(QAbstractButton *btn);
    void duplexBtnGrpClick(QAbstractButton *btn);
    void micSelBtnGrpClick(QAbstractButton *btn);
    void micGainBtnGrpClick(QAbstractButton *btn);
    void micLineBtnGrpClick(QAbstractButton *btn);

    void preAmpBtnToggled(bool checked);
    void ditherBtnToggled(bool checked);
    void randomBtnToggled(bool checked);
    void xmitAttBtnToggled(bool checked);

signals:
    void sampleRateChanged(hpsdr::HpsdrSampleRateEnum);
    void ref10MChanged(hpsdr::HpsdrRef10MEnum);
    void ref122MChanged(hpsdr::HpsdrRef122MEnum);
    void ditherChanged(hpsdr::HpsdrDitherEnum);
    void randomChanged(hpsdr::HpsdrRandomEnum);
    void classEChanged(hpsdr::HpsdrClassEEnum);
    void duplexChanged(hpsdr::HpsdrDuplexEnum);
    void micSrcChanged(hpsdr::HpsdrMicSrcEnum);
    void preAmpChanged(hpsdr::HpsdrPreAmpEnum);
    void configChanged(hpsdr::HpsdrConfigEnum);
    void micGainChanged(hpsdr::HpsdrMicGainEnum);
    void micLineChanged(hpsdr::HpsdrMicLineEnum);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private:
    Ui::HpsdrDockWidget *ui;

    hpsdr::HpsdrSampleRateEnum m_SampleRate;
    hpsdr::HpsdrRef10MEnum m_Ref10M;
    hpsdr::HpsdrRef122MEnum m_Ref122M;
    hpsdr::HpsdrDitherEnum m_Dither;
    hpsdr::HpsdrRandomEnum m_Random;
    hpsdr::HpsdrConfigEnum m_Config; // penelope, mercury, or both
    hpsdr::HpsdrClassEEnum m_ClassE;
    hpsdr::HpsdrDuplexEnum m_Duplex;
    hpsdr::HpsdrMicSrcEnum m_MicSrc;
    hpsdr::HpsdrPreAmpEnum m_PreAmp;
    hpsdr::HpsdrMicLineEnum m_MicLine;
    hpsdr::HpsdrMicGainEnum m_MicGain;
    hpsdr::HpsdrXmitAttEnum m_XmitAtten;
};

//Q_DECLARE_METATYPE(HpsdrDockWidget);

#endif // HPSDRDOCKWIDGET_H
