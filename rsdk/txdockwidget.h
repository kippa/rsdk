#ifndef TXDOCKWIDGET_H
#define TXDOCKWIDGET_H

#include <QWidget>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QDockWidget>
#else
#include <QtGui/QDockWidget>
#endif

#include "modedockwidget.h"
#include "rsdkdockwidget.h"

namespace Ui {
class TxDockWidget;
}

class TxDockWidget : public rsdkDockWidget
{
    Q_OBJECT

    Q_PROPERTY(unsigned int m_txDriveLevel READ txDriveLevel WRITE setTxDriveLevel)
    Q_PROPERTY(unsigned int m_txTuneDriveLevel READ getTxTuneDriveLevel WRITE setTxTuneDriveLevel)

public:
    explicit TxDockWidget(QWidget *parent = 0);
    ~TxDockWidget();

    void start();

    // TX DRIVE LEVEL 0-255
    unsigned int txDriveLevel() const;

    bool getTuneMode() const;
    unsigned int getTxTuneDriveLevel() const;

signals:
    void txDriveLevelChanged(unsigned int m_txDriveLevel);
    void txTuneDriveLevelChanged(unsigned int m_txTuneDriveLevel);
    void txPressed(bool);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

public slots:
    void setTxTuneDriveLevel(unsigned int level);
    void setTxDriveLevel(unsigned int driveLevel);
    void setTuneMode(bool value);

protected slots:
private slots:
    void on_txDriveLevel_valueChanged(int value);
    void on_txTuneDriveLevel_valueChanged(int value);
    void on_txPB_toggled(bool checked);

private:
    Ui::TxDockWidget *ui;

    unsigned int m_txDriveLevel; // these end up being 8 bit values, but qproperty doesn't like uchar
    unsigned int m_txTuneDriveLevel;
    unsigned int prevDriveLevel;
    SDRMODE_DTTSP prevSdrMode;
    bool tuneMode;
};

#endif // TXDOCKWIDGET_H
