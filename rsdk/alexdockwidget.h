#ifndef AlexDOCKWIDGET_H
#define AlexDOCKWIDGET_H

#include <QDockWidget>
#include <QAbstractButton>

#include "hpsdr.h"
//#include "serializationcontext.h"
#include "rsdkdockwidget.h"

namespace Ui {
    class AlexDockWidget;
}

class AlexDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(hpsdr::HpsdrAttEnum m_att READ getAlexAtt WRITE setAlexAtt)
    Q_PROPERTY(hpsdr::HpsdrRxAntEnum m_rxant READ getAlexRxAnt WRITE setAlexRxAnt)
    Q_PROPERTY(hpsdr::HpsdrTxAntEnum m_txant READ getAlexTxAnt WRITE setAlexTxAnt)
    Q_PROPERTY(hpsdr::HpsdrRxOutEnum m_rxout READ getAlexRxOut WRITE setAlexRxOut)

public:
    explicit AlexDockWidget(QWidget *parent = 0);
    ~AlexDockWidget();

    hpsdr::HpsdrAttEnum getAlexAtt(void) const;
    void setAlexAtt(hpsdr::HpsdrAttEnum att);

    hpsdr::HpsdrRxAntEnum getAlexRxAnt(void) const;
    void setAlexRxAnt(hpsdr::HpsdrRxAntEnum rxant);

    hpsdr::HpsdrTxAntEnum getAlexTxAnt(void) const;
    void setAlexTxAnt(hpsdr::HpsdrTxAntEnum txant);

    hpsdr::HpsdrRxOutEnum getAlexRxOut(void) const;
    void setAlexRxOut(hpsdr::HpsdrRxOutEnum rxout);

public slots:
    void start();

private slots:
    void attBtnGrpClick(QAbstractButton *btn);
    void rxAntBtnGrpClick(QAbstractButton *btn);
    void txAntBtnGrpClick(QAbstractButton *btn);
    void rxOutToggled(bool checked);

signals:
    void attChanged(hpsdr::HpsdrAttEnum att);
    void rxAntChanged(hpsdr::HpsdrRxAntEnum rxAnt);
    void txAntChanged(hpsdr::HpsdrTxAntEnum txAnt);
    void rxOutChanged(hpsdr::HpsdrRxOutEnum rxOut);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private:
    Ui::AlexDockWidget *ui;

    hpsdr::HpsdrAttEnum m_att;
    hpsdr::HpsdrRxAntEnum m_rxant;
    hpsdr::HpsdrTxAntEnum m_txant;
    hpsdr::HpsdrRxOutEnum m_rxout;
};

#endif // AlexDOCKWIDGET_H
