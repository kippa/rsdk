#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h> // basename

#include <QFile>
#include <QObject>
#include <QtWidgets/QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>

#include"sqlite3.h"

//#include <QAudioOutput>

// the DttSP library
#include "dttsp.h"
#include "common.h"
#include "rsdk.h"

rsdk *r;

int main(int argc, char *argv[])
{
    r = nullptr;

    QApplication app(argc, argv);

        //qEnvironmentVariableIsSet("QT_OPENGL_NO_SANITY_CHECK");

    /* does nothing
    QSysInfo sysInfo;
    if (sysInfo.kernelType() == "Linux") {
        //icon for app
        app.setWindowIcon(QIcon("./images/rsdk.ico"));
    }
    */

    QFile file("rsdk.qss");
    if (file.open(QIODevice::ReadOnly))
    {
        app.setStyleSheet(file.readAll());
        file.close();
    }

    //char name[128];
    //memset(name, 0, 128);
    //snprintf(name, 128, "%s-%d", basename(argv[0]), getpid());

    // partially initialize the DttSP library, gotta be ready for radioozy buffers
    Setup_SDR("rsdk-run");

    Release_Update();

    SetThreadProcessingMode(0, RUN_PLAY); // rx1
    SetThreadProcessingMode(1, RUN_PLAY); // tx1
    SetThreadProcessingMode(2, RUN_PASS); // rx2

    SetTRX(0, RX);
    SetTRX(1, TX);
    SetTRX(2, RX);

    r = new rsdk(nullptr, &app);
    QIcon icon;
    r->setWindowIcon(icon);
        r->update();
    r->show();

    app.exec();
    app.quit();

    delete r;

    return(0);
}
