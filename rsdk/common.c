#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include </usr/include/libusb-1.0/libusb.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>
#include <math.h>

// convert uint64_t to BCD, expects a buffer to build in, and a buffer to read from
// conversion is lsb in buf[0]
int
toBCD(const uint64_t v, uint8_t *buf, int buflen)
{
    uint64_t val = v;
    uint8_t g1 = 0x00, m100 = 0x00, m10 = 0x00, m1 = 0x00, k100 = 0x00, k10  = 0x00, k1 = 0x00, h100 = 0x00, h10 = 0x00, h1 = 0x00;

    uint64_t x = val / 1000000000; // 1s giga
    if (x) {
        g1 = x;
        val -= x*1000000000;
    }
    x = val / 100000000; // 100s mega
    if (x) {
        m100 = x;
        val -= x*100000000;
    }
    x = val / 10000000; // 10s mega
    if (x) {
        m10 = x;
        val -= x*10000000;
    }
    x = val / 1000000; // 1s mega
    if (x) {
        m1 = x;
        val -= x*1000000;
    }
    x = val / 100000; // 100s kilo
    if (x) {
        k100 = x;
        val -= x*100000;
    }
    x = val / 10000; // 10s kilo
    if (x) {
        k10 = x;
        val -= x*10000;
    }
    x = val / 1000; // 1s kilo
    if (x) {
        k1 = x;
        val -= x*1000;
    }
    x = val / 100; // 100s
    if (x) {
        h100 = x;
        val -= x*100;
    }
    x = val / 10; // 10s
    if (x) {
        h10 = x;
        val -= x*10;
    }
    // remainder should be ones
    h1 = val;

    uint8_t i = 0;
    buf[i++] = (h10<<4) | h1;
    if (i >= buflen) return v;
    buf[i++] = (k1<<4) | h100;
    if (i >= buflen) return v;
    buf[i++] = (k100<<4) | k10;
    if (i >= buflen) return v;
    buf[i++] = (m10<<4) | m1;
    if (i >= buflen) return v;
    buf[i] = (g1<<4) | m100;
    // there aren't any more

    return v;
}

int
fromBCD(const uint8_t *buf, int len)
{
    assert(buf);
    // len is the number of bcd bytes to decode, each nibble is a power of 10
    // this starts assuming the LSB arrives first in buf[]
    // there is at least one byte to convert
    int result = 0;
    int j = 0;
    for (int i = 0; i < len; i++)
    {
        double m1 = pow(10, (double)j++);
        double m2 = pow(10, (double)j++);
        assert(((buf[i] & 0xF0) >> 4 ) < 10);
        assert((buf[i] & 0x0F) < 10);
        result += (((buf[i] & 0xF0) >> 4) * m2) + ((buf[i] & 0x0F) * m1);
    }
    return result;
}

// print buffer buf to length len in hexadecimal with ascii under
void
printHex(const uint8_t *buf, int len)
{
    // no safety checks, whatever
    // debug print
    const ssize_t s = 16;// number of values to print on one line
    for (int x = 0; x < len;)
    {
        fprintf(stderr, "%2.2x: ", x);
        fflush(stderr);
        for (int y = 0; y < (len>s?s:len); y++)
        {
            fprintf(stderr, "%2.2x ", buf[x+y]);
            fflush(stderr);
        }
        fprintf(stderr, "\n");
        fflush(stderr);
        fprintf(stderr, "%2.2x: ", x);
        fflush(stderr);
        for (int y = 0; y < (len>s?s:len); y++)
        {
            if (isprint(buf[x+y]))
                fprintf(stderr, "%2c ", buf[x+y]);
            else
                fprintf(stderr, "%s ",  "--");
            fflush(stderr);
        }
        x += s;
        fflush(stderr);
        fprintf(stderr, "\n");
        fflush(stderr);
    }
}

// a microsecond sleeper that will complete
void my_usleep(uint32_t usec)
{
    struct timespec timeout0;
    struct timespec timeout1;
    struct timespec* tmp;
    struct timespec* t0 = &timeout0;
    struct timespec* t1 = &timeout1;

    t0->tv_sec = usec / 1000000;
    t0->tv_nsec = (usec % 1000000) * 1000;

    while ((nanosleep(t0, t1) == (-1)) && (errno == EINTR))
    {
        tmp = t0;
        t0 = t1;
        t1 = tmp;
    }
}

// return microsecond difference between two struct timeval
long long timeval_diff(struct timeval *start_time, struct timeval *end_time)
{
    struct timeval difference;
    timersub(end_time, start_time, &difference);
    return (1000000LL * difference.tv_sec) + difference.tv_usec;
}

// primitive debugging of a buffer
void dump_buffer(uint8_t * b, uint32_t size)
{
    uint32_t cnt;
    uint8_t c;
    int x, len = size > 16 ? 16 : size;

    fprintf(stderr, "\n");
    /* make a header line and underscore it */
    //if(size > 16)
    {
        fprintf(stderr,"     : ");
        for(x = 0; x < len; x++) fprintf(stderr,"%02x ", x); fprintf(stderr,"\n");
        fprintf(stderr,"     : ");
        for(x = 0; x < len; x++) fprintf(stderr,"-- "); fprintf(stderr,"\n");
    }

    fprintf(stderr," 0000: ");
    /* print the rows of data */
    for(cnt = 0; cnt < size;)
    {
        c = *b++;
        fprintf(stderr,"%02x",(uint32_t) c);
        cnt++;
        if(!(cnt % 16) && cnt < size)
            fprintf(stderr,"\n %4.4x: ", cnt);
        else
            fprintf(stderr," ");
    }
    fprintf(stderr,"\n");
}


/*
void X_aligned_memcpy_sse2(void* dest, const void* src, const unsigned long size)
{

  __asm
  {
    mov esi, src;    //src pointer
    mov edi, dest;   //dest pointer

    mov ebx, size;   //ebx is our counter
    shr ebx, 7;      //divide by 128 (8 * 128bit registers)


    loop_copy:
      prefetchnta 128[ESI]; //SSE2 prefetch
      prefetchnta 160[ESI];
      prefetchnta 192[ESI];
      prefetchnta 224[ESI];

      movdqa xmm0, 0[ESI]; //move data from src to registers
      movdqa xmm1, 16[ESI];
      movdqa xmm2, 32[ESI];
      movdqa xmm3, 48[ESI];
      movdqa xmm4, 64[ESI];
      movdqa xmm5, 80[ESI];
      movdqa xmm6, 96[ESI];
      movdqa xmm7, 112[ESI];

      movntdq 0[EDI], xmm0; //move data from registers to dest
      movntdq 16[EDI], xmm1;
      movntdq 32[EDI], xmm2;
      movntdq 48[EDI], xmm3;
      movntdq 64[EDI], xmm4;
      movntdq 80[EDI], xmm5;
      movntdq 96[EDI], xmm6;
      movntdq 112[EDI], xmm7;

      add esi, 128;
      add edi, 128;
      dec ebx;

      jnz loop_copy; //loop please
    loop_copy_end:
  }
}
*/

#define UNIT_BUF_LEN 8
#define VALUE_BUF_LEN 8

char *convertToUnits(double nvalue)
{
    char *unit = "";

    // make everything positive
    double value = nvalue < 0.0 ? -nvalue : nvalue;

    if(value>=1000000000000&&value<1000000000000000) {
        value=value/1000000000000;
        unit="P";
    } else if(value>=1000000000&&value<1000000000000) {
        value=value/1000000000;
        unit="G";
    } else if(value>=1000000&&value<1000000000) {
        value=value/1000000;
        unit="M";
    } else if(value>=1000&&value<1000000) {
        value=value/1000;
        unit="K";
    } else if((value>=1&&value<1000)) {
        //value=value*1;
    } else if((value*1000)>=1&&value<1000) {
        value=value*1000;
        unit="m";
    } else if((value*1000000)>=1&&value<1000000) {
        value=value*1000000;
        unit="u";
    } else if((value*1000000000)>=1&&value<1000000000) {
        value=value*1000000000;
        unit="n";
    } else if((value*1000000000000)>=1&&value<1000000000000) {
        value=value*1000000000000;
        unit="p";
    }

    char *retStr = calloc(VALUE_BUF_LEN, sizeof(char));
    snprintf(retStr, VALUE_BUF_LEN, "%f %s", value, unit);

    return retStr;
}

double convertToValues(const char *input)
{
    assert(input);

    char unit[UNIT_BUF_LEN];
    char value[VALUE_BUF_LEN];

    double inValue;

    // clear the accumulation buffers to zeroes
    memset(unit, 0x00, UNIT_BUF_LEN);
    memset(value, 0x00, VALUE_BUF_LEN);

    // parse out unit abbreviations
    // this is very loose, caution
    int j=0;
    int len = (int) strlen(input);
    for(int i=0; i < len; i++)
    {
        if((input[i]>='A'&&input[i]<='Z')||
                (input[i]>='a'&&input[i]<='z'))
        {
            // just use the first one found, caution
            unit[j++]=input[i];
            break;
        }
    }
    for(int k=0; k < len-j ; k++)
    {
        value[k]=input[k];
    }

    inValue = atof(value);

    if(unit[0]=='p') // pico
    {
        return(inValue/1000000000000);
    }
    else if(unit[0]=='n') // nano
    {
        return(inValue/1000000000);
    }
    else if(unit[0]=='u')
    {
        return(inValue/1000000); // micro
    }
    else if(unit[0]=='m')
    {
        return(inValue/1000);
    }
    else if(unit[0]=='K' || unit[0]=='k') // Kilo kilo
    {
        return(inValue*1000);
    }
    else if(unit[0]=='M') // Mega
    {
        return(inValue*1000000);
    }
    else if(unit[0]=='G') // Giga
    {
        return(inValue*1000000000);
    }
    else if(unit[0]=='P') // Peta
    {
        return(inValue*1000000000000);
    }
    else
    {
        return(inValue*1);
    }
}
