#ifndef RSDKDOCKWIDGET_H
#define RSDKDOCKWIDGET_H

#include <QObject>
#include <QWidget>
#include <QDockWidget>
#include <QTextStream>
#include <QTimer>

class rsdk;

class rsdkDockWidget : public QDockWidget
{
    Q_OBJECT
public:
    explicit rsdkDockWidget(QWidget *parent = nullptr);
    ~rsdkDockWidget();

    class rsdk *theRsdk;

signals:
    void dockChanged(Qt::DockWidgetArea);

public slots:
    void stateChange(bool visible);
    virtual void start()=0; // a virtual hanger for all DockWidget start() calls
    void setTitlePos(Qt::DockWidgetArea area);
    virtual void updateHandler(void);

protected:
    /*! this timer allows each derived DockWidget to provide an
     *  overload of the handler to do specific updates from rozy
     */
    QTimer updateTimer;

private:
    QWidget *theParent;

    rsdkDockWidget(const rsdkDockWidget& NoCopy);
    rsdkDockWidget &operator=(const rsdkDockWidget &NoAssign);
};

#endif // RSDKDOCKWIDGET_H
