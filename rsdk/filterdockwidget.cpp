#include <sstream>
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

#include <QTextStream>

#include "common.h" // string conversion routines for kilo, mega, etc.
#include "rsdk.h"
#include "stringConvert.h"

#include "modedockwidget.h"
#include "filterdockwidget.h"
#include "ui_filterdockwidget.h"

#define MAX_FILTER_HZ 24000

FilterDockWidget::FilterDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::FilterDockWidget),
    prevBW("0"),
    started(false)
{
    ui->setupUi(this);

    ui->FilterHiSpinBox->setSuffix(" Hz");
    ui->FilterLoSpinBox->setSuffix(" Hz");

    connect(ui->FilterHiSpinBox, SIGNAL(valueChanged(QString)), this, SLOT(hiSpinBoxChanged(QString)));
    connect(ui->FilterLoSpinBox, SIGNAL(valueChanged(QString)), this, SLOT(loSpinBoxChanged(QString)));

    connect(ui->filterBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(filterBtnGrpClick(QAbstractButton*)));

}

FilterDockWidget::~FilterDockWidget()
{
    started = false;
    delete ui;
}

// derived and overloaded from the base rsdkdockwidget class
void FilterDockWidget::updateHandler()
{
    std::string curBW = theRsdk->rozy->getCurBW();
    //std::string curSelBW = getCurFilterBtnTxt().toUtf8().constData();

    if (started &&
        !curBW.empty() &&
         curBW != prevBW) {

        setFilterBtn();
        prevBW = curBW;

        if (theRsdk->VfoDock->subRxOn()) {
            setHi(theRsdk->rozy->DSPFiltHi[1]);
            setLow(theRsdk->rozy->DSPFiltLo[1]);
        } else {
            setHi(theRsdk->rozy->DSPFiltHi[0]);
            setLow(theRsdk->rozy->DSPFiltLo[0]);
        }
    }
}

void FilterDockWidget::hiSpinBoxChanged(QString hi)
{
    // remove alpha , to get rid of " hz" from string to make an int
    QRegExp rx ( "[a-zA-Z]|,");
    QString labelVal = hi.remove(rx);

    bool ok;
    double val = QLocale(QLocale::English).toDouble(labelVal, &ok);
    val /= 1000;

    if (ok==true)
    {
        //std::string nhi = std::to_string(val) + "K";
        std::ostringstream nhi;
        nhi << std::setprecision(2) << val << "K";
        if (theRsdk->VfoDock->subRxOn())
            theRsdk->rozy->setBW(nhi.str(), 1);
        else
            theRsdk->rozy->setBW(nhi.str(), 0);
    }
}

void FilterDockWidget::setFilterBtn()
{

    std::string curBW = theRsdk->rozy->getCurBW();

    /*
    if (curBW.empty())
        return;
    */

    double absval = scv.convertToValues(curBW);

    QStringConvert conv;
    SDRMODE_DTTSP mode = theRsdk->rozy->getMode();

    QAbstractButton *btn = ui->FilterToolButton_10;

    if (absval >= 0.0 && absval <= conv.convertToValues(filtersByMode[mode][9]))
    {
        // redundant from above, but prevents no btn selection possibility
        btn = ui->FilterToolButton_10;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][9]) && absval <= conv.convertToValues(filtersByMode[mode][8]))
    {
        btn = ui->FilterToolButton_9;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][8]) && absval <= conv.convertToValues(filtersByMode[mode][7]))
    {
        btn = ui->FilterToolButton_8;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][7]) && absval <= conv.convertToValues(filtersByMode[mode][6]))
    {
        btn = ui->FilterToolButton_7;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][6]) && absval <= conv.convertToValues(filtersByMode[mode][5]))
    {
        btn = ui->FilterToolButton_6;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][5]) && absval <= conv.convertToValues(filtersByMode[mode][4]))
    {
        btn = ui->FilterToolButton_5;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][4]) && absval <= conv.convertToValues(filtersByMode[mode][3]))
    {
        btn = ui->FilterToolButton_4;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][3]) && absval <= conv.convertToValues(filtersByMode[mode][2]))
    {
        btn = ui->FilterToolButton_3;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][2]) && absval <= conv.convertToValues(filtersByMode[mode][1]))
    {
        btn = ui->FilterToolButton_2;
    }
    else if( absval > conv.convertToValues(filtersByMode[mode][1]) && absval <= conv.convertToValues(filtersByMode[mode][0]))
    {
        btn = ui->FilterToolButton_1;
    }

    bool pstate = btn->blockSignals(true);
    btn->setChecked(true);
    btn->blockSignals(pstate);

    setFilterBank(theRsdk->rozy->getMode());

    // make low value track hi value?
    //int loval;
    //if (theRsdk->VfoDock->subRxOn())
    //    loval = theRsdk->rozy->DSPFiltLo[1];
    //else
    //    loval = theRsdk->rozy->DSPFiltLo[0];

    //emit filterChanged(loval, val);
}

void FilterDockWidget::loSpinBoxChanged(QString low)
{
    // remove alpha , to get rid of " hz" from string to make an int
    QRegExp rx ( "[a-zA-Z]|,-");
    QString labelVal = low.remove(rx);

    bool ok;
    double val = QLocale(QLocale::English).toDouble(labelVal, &ok);
    //double hval;

    if (ok==true)
    {
        if (theRsdk->VfoDock->subRxOn())
        {
            theRsdk->rozy->DSPFiltLo[1]=val;
            //hval = theRsdk->rozy->DSPFiltHi[1];
        }
        else
        {
            theRsdk->rozy->DSPFiltLo[0]=val;
            //hval = theRsdk->rozy->DSPFiltHi[0];
        }
        //emit filterChanged(val, hval);
    }
}

void FilterDockWidget::start()
{
    //setLow(0);
    //setHi(0);
    started = true;
}

double FilterDockWidget::getHi() const
{
    if (theRsdk->VfoDock->subRxOn())
        return theRsdk->rozy->DSPFiltHi[1];
    else
        return theRsdk->rozy->DSPFiltHi[0];
}

void FilterDockWidget::setHi(double hi)
{
    double lo = theRsdk->rozy->DSPFiltLo[0];
    if (theRsdk->VfoDock->subRxOn())
    {
        theRsdk->rozy->DSPFiltHi[1]=hi;
        lo = theRsdk->rozy->DSPFiltLo[1];
    } else {
        theRsdk->rozy->DSPFiltHi[0]=hi;
    }

    if (static_cast<int>(hi) != ui->FilterHiSpinBox->value())
    {
        bool pstate = ui->FilterHiSpinBox->blockSignals(true);
        ui->FilterHiSpinBox->setValue(static_cast<int>(hi));
        ui->FilterHiSpinBox->blockSignals(pstate);
    }
    double diff = abs(hi - lo); // TODO FIXME cw mode lo and hi may wrap around zero
    std::string str = to_string(diff);
    theRsdk->rozy->setBW(str, true);
    setFilterBtn();
}

double FilterDockWidget::getLow() const
{
    if (theRsdk->VfoDock->subRxOn())
        return theRsdk->rozy->DSPFiltLo[1];
    else
        return theRsdk->rozy->DSPFiltLo[0];
}

void FilterDockWidget::setLow(double low)
{
    double hi = theRsdk->rozy->DSPFiltHi[0];
    if (theRsdk->VfoDock->subRxOn())
    {
        theRsdk->rozy->DSPFiltLo[1]=static_cast<double>(low);
        hi = theRsdk->rozy->DSPFiltHi[1];
    } else {
        theRsdk->rozy->DSPFiltLo[0]=static_cast<double>(low);
    }

    if (static_cast<int>(low) != ui->FilterLoSpinBox->value())
    {
        bool pstate = ui->FilterLoSpinBox->blockSignals(true);
        ui->FilterLoSpinBox->setValue(static_cast<int>(low));
        ui->FilterLoSpinBox->blockSignals(pstate);
    }
    double diff = abs(hi - low); // TODO FIXME cw will possibly wrap around zero
    std::string str = to_string(diff);
    theRsdk->rozy->setBW(str, true);
    setFilterBtn();
}

void FilterDockWidget::setFilterBank(SDRMODE_DTTSP mode)
{
    ui->FilterToolButton_1->setText(filtersByMode[mode][0]);
    ui->FilterToolButton_2->setText(filtersByMode[mode][1]);
    ui->FilterToolButton_3->setText(filtersByMode[mode][2]);
    ui->FilterToolButton_4->setText(filtersByMode[mode][3]);
    ui->FilterToolButton_5->setText(filtersByMode[mode][4]);
    ui->FilterToolButton_6->setText(filtersByMode[mode][5]);
    ui->FilterToolButton_7->setText(filtersByMode[mode][6]);
    ui->FilterToolButton_8->setText(filtersByMode[mode][7]);
    ui->FilterToolButton_9->setText(filtersByMode[mode][8]);
    ui->FilterToolButton_10->setText(filtersByMode[mode][9]);

    // refire the button that is already checked
    // this should re-call the filterBtnGrpClick
    if(ui->FilterToolButton_1->isChecked())
        ui->FilterToolButton_1->click();
    else if(ui->FilterToolButton_2->isChecked())
        ui->FilterToolButton_2->click();
    else if(ui->FilterToolButton_3->isChecked())
        ui->FilterToolButton_3->click();
    else if(ui->FilterToolButton_4->isChecked())
        ui->FilterToolButton_4->click();
    else if(ui->FilterToolButton_5->isChecked())
        ui->FilterToolButton_5->click();
    else if(ui->FilterToolButton_6->isChecked())
        ui->FilterToolButton_6->click();
    else if(ui->FilterToolButton_7->isChecked())
        ui->FilterToolButton_7->click();
    else if(ui->FilterToolButton_8->isChecked())
        ui->FilterToolButton_8->click();
    else if(ui->FilterToolButton_9->isChecked())
        ui->FilterToolButton_9->click();
    else if(ui->FilterToolButton_10->isChecked())
        ui->FilterToolButton_10->click();
}

QString FilterDockWidget::getCurFilterBtnTxt()
{
    QString ret;
    if(ui->FilterToolButton_1->isChecked())
        ret = ui->FilterToolButton_1->text();
    else if(ui->FilterToolButton_2->isChecked())
        ret = ui->FilterToolButton_2->text();
    else if(ui->FilterToolButton_3->isChecked())
        ret = ui->FilterToolButton_3->text();
    else if(ui->FilterToolButton_4->isChecked())
        ret = ui->FilterToolButton_4->text();
    else if(ui->FilterToolButton_5->isChecked())
        ret = ui->FilterToolButton_5->text();
    else if(ui->FilterToolButton_6->isChecked())
        ret = ui->FilterToolButton_6->text();
    else if(ui->FilterToolButton_7->isChecked())
        ret = ui->FilterToolButton_7->text();
    else if(ui->FilterToolButton_8->isChecked())
        ret = ui->FilterToolButton_8->text();
    else if(ui->FilterToolButton_9->isChecked())
        ret = ui->FilterToolButton_9->text();
    else if(ui->FilterToolButton_10->isChecked())
        ret = ui->FilterToolButton_10->text();

    return ret;
}

// f is cw tone frequency
void FilterDockWidget::setCenterFreq(const qlonglong f)
{
    (void)f; // what was I doing with this?
    QString bwStr = getCurFilterBtnTxt();
    if (theRsdk->VfoDock->subRxOn())
        theRsdk->rozy->setBW(bwStr.toUtf8().data(),1);
    else
        theRsdk->rozy->setBW(bwStr.toUtf8().data(),0);
}

void FilterDockWidget::setCenterFreq(const double f)
{
    setCenterFreq((qlonglong)f);
}

void FilterDockWidget::filterBtnGrpClick(QAbstractButton *btn)
{
    if (theRsdk->VfoDock->subRxOn()) {
        theRsdk->rozy->setBW(btn->text().toUtf8().constData(),1);
        //setHi(theRsdk->rozy->DSPFiltHi[1]);
        //setLow(theRsdk->rozy->DSPFiltLo[1]);
    } else {
        theRsdk->rozy->setBW(btn->text().toUtf8().constData(),0);
        //setHi(theRsdk->rozy->DSPFiltHi[0]);
        //setLow(theRsdk->rozy->DSPFiltLo[0]);
    }
}
