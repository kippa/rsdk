#ifndef DISPLAYDOCKWIDGET_H
#define DISPLAYDOCKWIDGET_H

#include <QAbstractButton>
#include <QDockWidget>
#include <QThread>
#include <QTimer>
#include <QFont>
#include <QVector>
#include <QPointF>
#include <QTime>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QScrollArea>

#include <qwt_color_map.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_legenditem.h>
#include <qwt_plot_marker.h>
#include <qwt_plot_rasteritem.h>
#include <qwt_plot_zoneitem.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_shapeitem.h>

#include "scrollzoomer.h"
#include "data-worker.h"
#include "bandscope.h"
#include "sgsmooth.h"
#include "oscope.h"
#include "dttsp.h"
#include "rsdkdockwidget.h"
#include "plotdrawoptionsdockwidget.h"

namespace Ui {
    class DisplayDockWidget;
}

class Zoomer;
class DisplayDockWidget;

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

class FreqGrid: public QwtPlotGrid
{

public:

    FreqGrid() :
        m_color("red")
    {
        enableY(false);
        enableYMin(false);
        enableX(false);
        enableXMin(false);
        setMajorPen(m_color, (qreal)1.00, Qt::SolidLine);
        setMinorPen(m_color, (qreal)0.75, Qt::DotLine);
        setZ(-100);
    }

    void draw(QPainter *p, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &rect) const
    {
        const double x1 = rect.left();
        const double x2 = rect.right()-1.0;
        const double y1 = rect.top();
        const double y2 = rect.bottom()-1.0;

        QwtPlotGrid::draw(p, xMap, yMap, rect);
        QColor labelColor("burlywood");

        //if (xEnabled())
        {
            QwtScaleDiv xScale = xScaleDiv();
            const QList<double>values = xScale.ticks(QwtScaleDiv::MajorTick);

            for (int i=0; i<values.count(); i++)
            {
                double value = xMap.transform(values[i]);

                if (qwtFuzzyGreaterOrEqual(value, x1) && qwtFuzzyLessOrEqual(value, x2))
                {
                    QString label = QLocale(QLocale::English).toString(values[i]/1000000.0, 'f', 2);
                    const QFont f("Arial", 12, QFont::Bold);
                    p->setFont(f);
                    //QFontMetrics m(f);
                    QRect r = p->boundingRect(value, y1, 0, 0, Qt::AlignHCenter, label);
                    p->setPen(labelColor.darker(256 - m_color.value()));
                    p->drawText(r, Qt::AlignCenter, label);
                    p->setPen(m_color);
                    p->drawLine(value, r.height()+2, value, y2);
                    p->drawRoundedRect(r, 1.0, 1.0, Qt::AbsoluteSize);
                }
            }

            const QList<double>othervalues = xScale.ticks(QwtScaleDiv::MinorTick);

            for (int i=0; i<othervalues.count(); i++)
            {
                double value = xMap.transform(othervalues[i]);

                if (qwtFuzzyGreaterOrEqual(value, x1) && qwtFuzzyLessOrEqual(value, x2))
                {
                    QString label = QLocale(QLocale::English).toString(othervalues[i]/1000000.0, 'f', 3);
                    const QFont f("Arial", 8, QFont::Light);
                    p->setFont(f);
                    //QFontMetrics m(f);
                    QRect r = p->boundingRect(value, y1+7, 0, 0, Qt::AlignHCenter, label);
                    p->setPen(labelColor.darker(256 - (m_color.value()/2)));
                    p->setBackground(QColor("black"));
                    p->drawText(r, Qt::AlignCenter, label);
                    p->drawLine(value, 0, value, 7);
                }
            }
        }

        if (yEnabled())
        {
        }
    }

    QColor getColor() { return m_color; }
    void setColor(const QColor &color) {
        m_color =color;
        setMajorPen(m_color, (qreal)1.00, Qt::SolidLine);
        setMinorPen(m_color, (qreal)0.75, Qt::DotLine);
    }

private:
    QColor m_color;
};

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

class dBGrid: public QwtPlotGrid
{
public:

    dBGrid() :
        m_color("red")
    {
        enableY(false);
        enableYMin(false);
        enableX(false);
        enableXMin(false);
        setMajorPen(m_color, (qreal)1.00, Qt::SolidLine);
        setMinorPen(m_color, (qreal)0.75, Qt::DashLine);
        setZ(-100);
    }

    void draw(QPainter *p, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &rect) const
    {
        const double x1 = rect.left();
        const double x2 = rect.right()-1.0;
        const double y1 = rect.top();
        const double y2 = rect.bottom()-1.0;

        QwtPlotGrid::draw(p, xMap, yMap, rect);
        QColor labelColor("burlywood");

        if (xEnabled())
        {
        }

        //if (yEnabled())
        {
            QwtScaleDiv yScale = yScaleDiv();
            const QList<double>values = yScale.ticks(QwtScaleDiv::MajorTick);

            for (int i=0; i<values.count(); i++)
            {
                double value = yMap.transform(values[i]);

                // render into range on screen
                if (value <= (y1 + 15))
                    continue;
                if (value > (y2 - 15))
                    continue;

                if (qwtFuzzyGreaterOrEqual(value, y1) && qwtFuzzyLessOrEqual(value, y2))
                {
                    QString label = QLocale(QLocale::English).toString(values[i], 'f', 0) + " dBm";
                    const QFont f("fixed", 8, -1, true);
                    p->setFont(f);
                    QRect r = p->boundingRect(x1+2, value, 0, 0, Qt::AlignVCenter, label);

                    // remember the original pen
                    QPen oldPen = p->pen();

                    p->setPen(labelColor.darker(256 - (m_color.value()/2)));
                    p->drawText(r, Qt::AlignCenter, label);

                    QPen pen = oldPen;
                    pen.setStyle(Qt::DotLine);
                    pen.setColor(m_color);
                    p->setPen(pen);

                    p->drawLine(r.width()+6, value, x2, value);

                    // restore the original pen
                    p->setPen(oldPen);
                }
            }
        }
    }

    QColor getColor() { return m_color; }
    void setColor(const QColor &color) {
        m_color =color;
        setMajorPen(m_color, (qreal)1.00, Qt::SolidLine);
        setMinorPen(m_color, (qreal)0.75, Qt::DashLine);
    }

private:
    QColor m_color;
};

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

// S- signal markers predefined for convenience
struct a_mark {
    double value;
    char name[16];
};

static const struct a_mark marks[] = {
    { -121.0, "S1" },
    { -109.0, "S3" },
    { -97.0, "S5" },
    { -85.0, "S7" },
    { -73.0, "S9" },
    { -63.0, "S9+10dB" },
    { -53.0, "S9+20dB" },
    { -43.0, "S9+30dB" },
    { -33.0, "S9+40dB" },
    { -23.0, "S9+50dB" },
    { -13.0, "S9+60dB" }
};
#define NUM_S_MARKS (sizeof(marks)/sizeof(marks[0]))

class SGrid: public QwtPlotGrid
{
public:

    SGrid() :
        m_color("red")
    {
        QColor textColor("burlywood");
        enableY(false);
        enableYMin(false);
        enableX(false);
        enableXMin(false);
        setMajorPen(m_color, (qreal)1.00, Qt::SolidLine);
        setMinorPen(m_color, (qreal)0.75, Qt::DashLine);
        setZ(-100);
    }

    void draw(QPainter *p, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &rect) const
    {
        const double x1 = rect.left();
        const double x2 = rect.right()-1.0;
        const double y1 = rect.top();
        const double y2 = rect.bottom()-1.0;

        QwtPlotGrid::draw(p, xMap, yMap, rect);
        QColor labelColor("burlywood");

        if (xEnabled())
        {
        }

        //if (yEnabled())
        {
            QwtScaleDiv yScale = yScaleDiv();
            const QList<double>values = yScale.ticks(QwtScaleDiv::MajorTick);

            for (int i=0; i<(int)NUM_S_MARKS; i++)
            {
                double value = yMap.transform(marks[i].value);

                // render into range on screen
                if (value <= (y1 + 15))
                    continue;
                if (value > (y2 - 15))
                    continue;

                if (qwtFuzzyGreaterOrEqual(value, y1) && qwtFuzzyLessOrEqual(value, y2))
                {
                    QString label(marks[i].name);
                    const QFont f("fixed", 8, -1, true);
                    p->setFont(f);
                    QFontMetrics fm(f);
                    int wide = fm.width(label);
                    QRect r = p->boundingRect(x2-wide, value, 0, 0, Qt::AlignVCenter, label);
                    //QRect r = p->boundingRect(x1+2, value, 0, 0, Qt::AlignVCenter, label);

                    // remember the original pen
                    QPen oldPen = p->pen();

                    p->setPen(labelColor.darker(256 - (m_color.value()/2)));
                    p->drawText(r, Qt::AlignCenter, label);

                    QPen pen = oldPen;
                    pen.setStyle(Qt::DotLine);
                    pen.setColor(m_color);
                    p->setPen(pen);

                    //p->drawLine(r.width()+6, value, x2, value);
                    p->drawLine(x1+2, value, x2-r.width(), value);

                    // restore the original pen
                    p->setPen(oldPen);
                }
            }
        }
    }

    QColor getColor() { return m_color; }
    void setColor(const QColor &color) {
        m_color = color;
        setMajorPen(m_color, (qreal)1.00, Qt::SolidLine);
        setMinorPen(m_color, (qreal)0.75, Qt::DashLine);
    }

private:
    QColor m_color;
};

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

class DisplayDockWidget : public rsdkDockWidget
{
    Q_OBJECT

    Q_PROPERTY(QColor m_spectrumColor READ getSpectrumColor WRITE propSetSpectrumColor)
    Q_PROPERTY(QColor m_sGridColor READ sGridColor WRITE propSetSGridColor)
    Q_PROPERTY(QColor m_dbGridColor READ dbGridColor WRITE propSetDbGridColor)
    Q_PROPERTY(QColor m_fGridColor READ fGridColor WRITE propSetFGridColor)
    Q_PROPERTY(QColor m_titleColor READ titleColor WRITE propSetTitleColor)
    Q_PROPERTY(QColor m_fillColor READ fillColor WRITE propSetFillColor)
    Q_PROPERTY(QColor m_bandColor READ bandColor WRITE propSetBandColor)

    Q_PROPERTY(int yAxisMinY READ getMinY WRITE setMinY)
    Q_PROPERTY(int yAxisMaxY READ getMaxY WRITE setMaxY)
    Q_PROPERTY(bool fillTrace READ getFillTrace WRITE setFillTrace)
    Q_PROPERTY(bool bandScopeOn READ getBandScopeOn WRITE setBandScopeOn())
    Q_PROPERTY(bool oScopeOn READ getOScopeOn WRITE setOScopeOn())
    Q_PROPERTY(bool titleOn READ getTitleOn WRITE setTitleOn())
    Q_PROPERTY(bool fGridOn READ freqGridOn WRITE setFreqGridOn)
    Q_PROPERTY(bool m_bandOn READ bandOn WRITE setBandOn)

    Q_PROPERTY(int m_waterfallInterval READ getWaterfallInterval WRITE setWaterfallInterval)
    Q_PROPERTY(int m_spectrumInterval READ getSpectrumInterval WRITE setSpectrumInterval)

public:
    explicit DisplayDockWidget(QWidget *parent = nullptr);
    ~DisplayDockWidget();

    sgSmooth smooth;

    //QThread collectThread;
    Worker2 *snapper;

    int getMinY() const;
    int getMaxY() const;


    bool event (QEvent *event);

    int getFilterCoeffs() const;

    QColor getSpectrumColor() const;
    QColor fGridColor() const;
    QColor sGridColor() const;
    QColor dbGridColor() const;
    QColor titleColor() const;
    QColor fillColor() const;
    QColor bandColor() const;

    double activeDragXPos() const;
    void setActiveDragXPos(double activeDragXPos);
    QBrush gradBrush() const;
    bool getFillTrace() const;
    // the bandscope display
    BandScope *bandscope;
    bool getBandScopeOn() const;
    OScope *oscope;
    bool getTitleOn() const;
    bool freqGridOn() const;
    bool bandOn() const;
    bool pageTune() const;
    double getXAxisMinX() const;
    void setXAxisMinX(double value);
    void updateXAxis(const qlonglong &freq);
    double getXAxisMaxX() const;
    void setXAxisMaxX(double value);
    bool rebuildBands() const;
    bool getOScopeOn() const;
    int getSpectrumInterval() const;
    int getWaterfallInterval() const;


public slots:
    void start();
    void drawPlot();
    void drawWaterfall();
    void setMinY(int value);
    void setMaxY(int value);
    void setSpectrumColor(const QColor &value);
    void enableSGrid(bool enable);
    void enableFGrid(bool enable);
    void enableDbGrid(bool enable);
    void setSGridColor(const QColor &value);
    void setFGridColor(const QColor &value);
    void setDbGridColor(const QColor &value);
    void setTitleOn(bool value);
    void setTitleColor(const QColor &titleColor);
    void setGradBrush(const QBrush &gradBrush);
    void setFilterCoeffs(int value);
    void setFillTrace(bool value);
    void setFillColor(const QColor &fillColor);
    void setBandScopeOn(bool enable);
    void setOScopeOn(bool enable);
    void setFreqGridOn(bool freqGridOn);
    void setBandOn(bool bandOn);
    void setPageTune(bool pageTune);
    void setBandColor(const QColor &color);
    void setRebuildBands(bool rebuildBands);
    void setSpectrumInterval(int spectrumInterval);
    void setWaterfallInterval(int waterfallInterval);
    bool getWaterfallEnable() const;
    void setWaterfallEnable(bool waterfallEnable);
    bool getSpectrumEnable() const;
    void setSpectrumEnable(bool spectrumEnable);
    QLinearGradient getLgrad() const;
    void setLGrad(const QLinearGradient &lg);
    void resizeLGrad();

private slots:
    void propSetSpectrumColor(const QColor &value);
    void propSetSGridColor(const QColor &value);
    void propSetDbGridColor(const QColor &value);
    void propSetFGridColor(const QColor &value);
    void propSetTitleColor(const QColor &value);
    void propSetFillColor(const QColor &value);
    void propSetBandColor(const QColor &value);

    void on_splitter_splitterMoved(int pos, int index);
    void on_zoomed(const QRectF &rect);

signals:
    void yMaxChanged(int yMax);
    void yMinChanged(int yMin);
    void freqChanged(qlonglong newFreq);
    void spectrumColorChanged(QColor color);
    void sGridColorChanged(QColor color);
    void dbGridColorChanged(QColor color);
    void fGridColorChanged(QColor color);
    void titleColorChanged(QColor color);
    void fillColorChanged(QColor color);
    void bandColorChanged(QColor color);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);
    void spectrumIntervalChanged(int interval, bool sendSig);
    void waterfallIntervalChanged(int interval, bool sendSig);
    void rebuildBandsChanged(bool);

private:
    Ui::DisplayDockWidget *ui;
    void makeSGrid(void);
    void initCmap(void);
    void initZoom();
    void setXAxis(const qlonglong &freq);

    QString title; // the mode sets the title

    // drawing rates
    //QTimer drawSpectrumTimer;
    int m_spectrumInterval;
    bool m_spectrumEnable;

    //QTimer drawWaterfallTimer;
    int m_waterfallInterval;
    bool m_waterfallEnable;

    QwtPlotCurve spectrumCurve;
    QwtPlotCurve phaseCurve;

    // curve fill gradient
    QLinearGradient lgrad;

    bool curveFitted;
    Zoomer *zoom;
    QwtPlotMarker rx1FreqMark, rx2FreqMark;
    QwtPlotZoneItem rx1FilterZone, rx2FilterZone;

    //bands are dynamice, come the database via BandDockWidget internals
    // we cache, and check to see if they've changed to maximize performance
    // and reduce allocation overhead
    std::list<QwtPlotMarker *> bandMarkers;
    std::list<struct band *>lastBandZones;

    FreqGrid *fGrid;
    SGrid *sGrid;
    dBGrid *dbGrid;

    // the waterfall objects
    QGraphicsScene *scene;
    QGraphicsPixmapItem *wfPixmap;

    float avgSmoothVal;
    bool initSpectrum;

    int yAxisMinY; // scale Y min, left axis
    int yAxisMaxY; // scale Y max
    double xAxisMinX;
    double xAxisMaxX;

    static constexpr float m_smoothing = 0.08f; // TODO FIXME
    QTime lastSecond;
    double m_activeDragXPos;
    double m_activeSlideXPos;
    QColor m_spectrumColor;
    QColor m_fillColor;
    QColor m_phaseColor;

    QColor m_dbGridColor;
    QColor m_sGridColor;
    QColor m_titleColor;
    QColor m_fGridColor;
    QColor m_bandColor;

    QBrush m_gradBrush;
    float filterCoeff;
    bool fillTrace;
    bool bandScopeOn;
    bool oScopeOn;
    bool titleOn;
    bool m_freqGridOn;
    QWidget dummyTitleWidget; // dummy title bar widget to make title bar go away
    bool m_bandOn;
    bool m_pageTune; // change SpectrumPlot tune scale only on pages of sample rate
    void removeLastBandZones();
    void removeBandMarkers();
    bool m_rebuildBands;

    int smoothValue;
    int m_scrollDecay; // decay scroll events this many milliseconds
    bool m_scrolling; // currently under wheel scrolling event(s)?
    struct timeval m_scrollDecayStart;

#define NUM_KELEMS 16 // smoothing of the mouse wheel differences to avoid quick events
                     // but not too fast
    unsigned long long lastDiff[NUM_KELEMS];
    double m_scrollLastDirection; // -1 for backwards, 0 for stop, +1 for forward, speeds too?

    QThread *drawThread;
};

#endif // DISPLAYDOCKWIDGET_H
