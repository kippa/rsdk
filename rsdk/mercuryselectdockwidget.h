#ifndef MERCURYSELECTDOCKWIDGET_H
#define MERCURYSELECTDOCKWIDGET_H

#include <QLabel>
#include <QDockWidget>
#include <QAbstractButton>

//#include <dttsp.h>

//#include "serializationcontext.h"
#include "rsdkdockwidget.h"

// this gets instantiated in ui as ui->iqLabel
class LocalLabel : public QLabel
{
    Q_OBJECT
public:
    explicit LocalLabel(QWidget *parent=0);
signals:
    void sendMousePoint(QPointF point);
    void sendNormI(double i); // normalized -1.0 -> 0 -> +1.0
    void sendNormQ(double q); // normalized -1.0 -> 0 -> +1.0
public slots:
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void resizeEvent(QResizeEvent *e);
private:
    QPainterPath m_grat;
    QPainterPath m_cross;
    void makeArcs(int w, int h);
    void makePixmap(int w, int h);
    void doSignals(QMouseEvent *e);
    QPointF m_curP;
};

namespace Ui {
class MercurySelectDockWidget;
}

class MercurySelectDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_ENUMS(MERCMODE)

public:
    explicit MercurySelectDockWidget(QWidget *parent = 0);
    MercurySelectDockWidget(const MercurySelectDockWidget& other) : rsdkDockWidget(), m_mode(other.m_mode) {};
    ~MercurySelectDockWidget();

    enum MERCMODE { MERC1=0, MERC2, MERCBOTH };

    MERCMODE mode() const;
    void setMode(const MERCMODE &mode);

    void start();

signals:
    void mercSelChanged(MercurySelectDockWidget::MERCMODE merc);
    void sendNormI(double i);
    void sendNormQ(double q);
    void sendGain(double g);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private slots:
    void m1Toggled(bool);
    void m2Toggled(bool);
    void mBothToggled(bool);
    void doNormI(double value);
    void doNormQ(double value);

    void on_gainScrollBar_valueChanged(int value);

private:
    Ui::MercurySelectDockWidget *ui;
    MERCMODE m_mode;
};

#endif // MERCURYSELECTDOCKWIDGET_H
