#include <string>
#include <regex>

#include "common.h"
#include "rsdk.h"
#include "banddockwidget.h"
#include "ui_banddockwidget.h"

BandDockWidget::BandDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::BandDockWidget),
    m_lastPb(NULL),
    m_count(0)
{
    pthread_mutex_init(&bandModifyLock, NULL);
    pthread_mutex_lock(&bandModifyLock);
    bands.clear();
    pthread_mutex_unlock(&bandModifyLock);

    bandKeyMap.clear();

    ui->setupUi(this);

    connect (ui->bandBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(bandBtnGrpKeyPress(QAbstractButton*)));
    //connect (this, SIGNAL(bandBtnChanged(QAbstractButton*)), this, SLOT(bandBtnGrpKeyPress(QAbstractButton*)));
}

BandDockWidget::~BandDockWidget()
{
    pthread_mutex_lock(&bandModifyLock);
    removeBandsList();
    pthread_mutex_unlock(&bandModifyLock);
    pthread_mutex_destroy(&bandModifyLock);
}

// call this with the lock already held at a higher level
// this avoids unnecessary complications
// check to ensure this is true, bitch a little and return doing nothing
void BandDockWidget::removeBandsList()
{
    // check for lock held, return if not
    if (pthread_mutex_trylock(&bandModifyLock)==0)
    {
        // we got the lock, so obviously it wasn't locked!
        pthread_mutex_unlock(&bandModifyLock);

        // throw? na, not a punishable offense

        // silently return doing nothing
        return;
    }
    std::list<struct band *>::iterator i = bands.begin();
    std::list<struct band *>::iterator end = bands.end();
    while(i != end)
    {
        bands.erase(i++);
        end = bands.end();
    }
    //pthread_mutex_unlock(&bandModifyLock);
}

void BandDockWidget::updateHandler()
{
    qlonglong f = theRsdk->VfoDock->getFreq();
    //qlonglong f = rozy->getRxFreq(rozy->curMercury);
    if (lastFreq != f)
    {
        bandBtnForFreq(f);
        lastFreq = f;
    }
}

void BandDockWidget::start()
{
    fillBands();
    theRsdk->DisplayDock->setRebuildBands(true);
}

// WARNING! Button names must match band names via regex for this to work
// band selection involves matching the band name to the button name
int BandDockWidget::fillBands()
{
    pthread_mutex_lock(&bandModifyLock);

    // get rid of the previous band data
    removeBandsList();

    // get the new band data
    theRsdk->db->getBands(bands);

    // clear bandKeyMap mappings too
    bandKeyMap.clear();

    // map buttons to the ham bands,
    // i.e. a button may map to several different bands
    // and mode may be used to select which band will be used
    // each with its own lastFreq
    QObjectList children = ui->BanddockWidgetContents->children();
    for(int i = 0; i < children.size(); i++)
    {
        QToolButton * pushButton = qobject_cast<QToolButton *>(children[i]);
        if(pushButton)
        {
            std::list<struct band *>::iterator x = bands.begin();
            int count = 0;
            while(x != bands.end())
            {
                // extract the pushbutton text and regex match that to the bands
                // pushbuttons must have names like "160", "80M", "20m" for ham band names
                // the regex for QRegExp is like "^160[mM]?$" to match 160, 160m, or 160M but nothing else
                // also, the database "bands.desc" field must contain the match or it will be silently ignored
                // make the regex
                QString str(pushButton->text());
                str.prepend("^");
                str.append("[mM]?$");
                QRegExp exp(str);

                // from the database "bands.desc" field
                QString z = (*x)->desc.c_str();
                if (z.contains(exp))
                {
                    // so the button text matches a band description field,
                    // push this band onto the button's list of bands
                    // but first, count the number of band entries for each pushbutton,
                    // allows 1 pushbutton to cycle through several band segments
                    (*x)->count = count++;
                    bandKeyMap.insertMulti(pushButton, *x);
                }
                x++;
            }
        }
    }
    pthread_mutex_unlock(&bandModifyLock);
    return 0;
}

// this returns a list<struct band *> of all bands that are included
std::list<struct band *>
BandDockWidget::bandsForRange(double start_prm , double end_prm)
{
    // bands are kept in KHz, inputs here are in Hz, convert
    double start = start_prm / 1e3;
    double end = end_prm / 1e3;

    // caution FIXME TODO these are not safe for exits
    pthread_mutex_lock(&bandModifyLock);

    std::list<struct band *> selBands; // return this
    std::list<struct band *>::iterator i = bands.begin();
    while(i != bands.end())
    {
        double s = (*i)->start;
        double e = (*i)->end;

        // if the band is in the range of start_prm -> end_prm use it
        if ((end > s) && (start < e))
        {
            selBands.push_back(*i);
        }
        i++;
    }

    pthread_mutex_unlock(&bandModifyLock);

    return selBands;
}

void BandDockWidget::bandBtnForFreq(qlonglong f)
{
    bandBtnForFreq((double)f);
}

void BandDockWidget::updateBands(const int dummy)
{
    if (dummy)
    {
        pthread_mutex_lock(&bandModifyLock);
        removeBandsList();
        pthread_mutex_unlock(&bandModifyLock);
        // race?? no one cares, I don't think
        // maybe DisplayDock tries to access bands whilst it's gone?
        fillBands();
        theRsdk->DisplayDock->setRebuildBands(true);
    }
}

// return the actual button that would be checked for a given frequency.
// Don't check it here, just return the appropriate button
// it's possible to return NULL from here
// check the return
QAbstractButton *BandDockWidget::getBtnForFreq(double f)
{
    QAbstractButton *ret = NULL;

    //if (pthread_mutex_trylock(&bandModifyLock)==0)
    pthread_mutex_lock(&bandModifyLock);
    {
        // got the lock

        QObjectList children = ui->BanddockWidgetContents->children();

        for(int i = 0; i < children.size(); i++)
        {
            QToolButton *tb = qobject_cast<QToolButton *>(children[i]);
            if(tb)
            {
                // check all the subbands (band segments),
                // each tool button will have 1 or more bands associated w/ it
                QList<struct band *>bands = bandKeyMap.values(tb);

                int num = bands.size();
                for (int z=0; z<num; z++)
                {
                    struct band *band = bands[z];
                    if (band)
                    {
                        double s = band->start * 1e3;
                        double e = band->end * 1e3;

                        if ((f >= s) && (f <= e)) // WWV is exactly a single frequency, memories will be too
                        {
                            band->lastFreq = f;
                            ret = (QAbstractButton *)tb;

                            // set the mode too
                            if (theRsdk->ModeDock->getAutoMode())
                                theRsdk->setRsdkMode(band->prefMode.c_str());

                            QString st = band->prefMode.c_str();
                            st += " ";
                            st += band->bandType.c_str();
                            ui->modeLabel->setText(st);

                            break;
                        }
                    }
                }
                // break out of the second loop too
                if (ret)
                    break;
            }
        }
        pthread_mutex_unlock(&bandModifyLock);
    }

    return ret;
}

void BandDockWidget::bandBtnForFreq(double f)
{
    // select the button that matches the f
    // only do so if the f has changed since last time to minimize work
    // or general band button if not in any range
    // also track the WWV/CHU reference frequencies

    if (f == m_lastFreq)
        return;

    m_lastFreq = f;

    QAbstractButton *tb = getBtnForFreq(f);

    if (tb)
    {
        tb->setChecked(true);
        // emit signal bandBtnChanged
    } else {
        ui->bandGEN->setChecked(true);
        if (theRsdk->ModeDock->getAutoMode())
            theRsdk->setRsdkMode("USB");
    }
}

void BandDockWidget::bandBtnGrpKeyPress(QAbstractButton *btn)
{
    QToolButton *pb = qobject_cast<QToolButton *>(btn);

    pthread_mutex_lock(&bandModifyLock);

    struct band *band = bandKeyMap.value(pb);

    if (pb == m_lastPb)
    {
        // cycle to the next band mapped to this band's push button
        QList<struct band *>bands = bandKeyMap.values(pb);
        int numEntries = bands.size();
        if (numEntries > 0) // if bands.size()
        {
            // range limit the index to the number of those available,
            // so it will cycle through all entries
            m_count++;
            m_count = (m_count % numEntries);
            // reassign band
            band = bands[m_count];
        }
    }
    else
    {
        m_lastPb = pb;
        m_count = 0;
    }

    if (band)
    {
        // band start and end are in KHz, convert to Hz before setting frequency
        // for now, use center frequency of band segment as frequency to set
        // TODO use memories to set to the last memory in this band
        double f = (band->start + (fabs(band->end - band->start)*0.5)) * 1e3;

        // use the memory, if it's set
        if (band->lastFreq)
            f = band->lastFreq;

        // set the frequency
        theRsdk->setRsdkFreq(f);
        QString st = band->prefMode.c_str();
        st += " ";
        st += band->bandType.c_str();
        ui->modeLabel->setText(st);
    }
    else
    {
        // handle wwv, gen, xvtr
    }

    pthread_mutex_unlock(&bandModifyLock);
}

// slot for rsdk to call on frequency change
// sets band button if necessary
// sets band->lastFreq
void BandDockWidget::bandSetLastFreq(double f)
{
    (void)f;
    // find the band, click the button, set the band->lastFreq
    //QAbstractButton *tb = getBtnForFreq(f);
}
