#include <unistd.h>

#include <rsdk.h>
#include "data-worker.h"

// this is a bit of a mess
// the workers call out to DttSp which has 2 function-call routines and 3 function-call routines
// a worker class w/ a timer to call replots
Worker2::Worker2(void (*fn)(int, float *), int rx, void *buf) :
    m_rate(0.0)
{
    Q_ASSERT(fn);
    Q_CHECK_PTR(buf);

    m_rx = rx;
    m_buf = buf;
    m_fn = fn;

    m_interval.setTimerType(Qt::PreciseTimer);
    m_interval.start(0);
    connect(&m_interval, SIGNAL(timeout()), this, SLOT(sampleData()));
}

Worker2::~Worker2()
{
    m_interval.stop();
    disconnect(&m_interval, SIGNAL(timeout()), this, SLOT(sampleData()));
}

void Worker2::sampleData()
{
    m_fn(m_rx, (float *)m_buf);
    emit sampleDone();
}

unsigned int Worker2::getRate() const
{
    return m_rate;
}

void Worker2::stopTimer()
{
    m_interval.stop();
}

void Worker2::startTimer()
{
   m_interval.start(m_rate);
}

// set the rate in milliseconds
void Worker2::setRate(unsigned int msecs)
{
    m_rate = msecs;
    m_interval.stop();
    m_interval.setInterval((int)m_rate); // msec
    m_interval.start();
}

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/
/* with 3 args in the passed in the DttSP Process_Scope function call */

Worker3::Worker3(void (*fn)(int, float *, int), int rx, void *buf, unsigned int size) :
    m_rate(5.0)
{
    Q_ASSERT(fn);
    Q_CHECK_PTR(buf);

    m_rx = rx;
    m_buf = buf;
    m_fn = fn;
    m_size = size;

    m_interval.setInterval((int)m_rate); // msec
    m_interval.setTimerType(Qt::PreciseTimer);
    connect(&m_interval, SIGNAL(timeout()), this, SLOT(sampleData()));
}

Worker3::~Worker3()
{
    m_interval.stop();
    disconnect(&m_interval, SIGNAL(timeout()), this, SLOT(sampleData()));
}

void Worker3::sampleData()
{
    int numLoops = m_size / DTTSP_SNAP_SAMPLES;
    for (int z=0; z<numLoops; z++) {
        float *offset = (float *)((uint8_t *)m_buf + (z * DTTSP_SNAP_SAMPLES * sizeof(float) * 2)); // I & Q are packed in there
        m_fn(m_rx, (float *)offset, DTTSP_SNAP_SAMPLES);
    }
    //m_fn(m_rx, (float *)m_buf, DTTSP_SNAP_SAMPLES);
    emit sampleDone();
}

double Worker3::getRate() const
{
    return m_rate;
}

void Worker3::stopTimer()
{
    m_interval.stop();
}

void Worker3::startTimer()
{
    m_interval.start(m_rate);
}

// set the interval rate in milliseconds
void Worker3::setRate(unsigned int msecs)
{
    m_rate = msecs;
    m_interval.stop();
    m_interval.setInterval((int)m_rate); // msec
    m_interval.start();
}
