#ifndef SERIALIZATIONCONTEXT_H
#define SERIALIZATIONCONTEXT_H

#include <iostream>
#include <QObject>
#include <QDebug>
#include <QDataStream>
#include <QMetaProperty>

#include "hpsdr.h"

// handle both pointers and references
template<typename T>
T * ptr(T & obj) { return &obj; } // reference converts to pointer

template<typename T>
T * ptr(T * obj) { return obj; } // already a pointer, no conversion

template<typename T>
QDataStream& operator << (QDataStream& ds, const T& obj)
{
    if (!ptr(obj))
        return ds;

    //qDebug() << __func__ << ptr(obj)->metaObject()->className();
    for(int i=0; i< ptr(obj)->metaObject()->propertyCount(); ++i)
    {
        if( ptr(obj)->metaObject()->property(i).isStored(ptr(obj)))
        {
            /*
            qDebug() << " metaobject " << ptr(obj)->metaObject()->property(i).name()
                     << " val " << ptr(obj)->metaObject()->property(i).read(ptr(obj))
                     << " NOTIFY " << ptr(obj)->metaObject()->property(i).hasNotifySignal()
                     << " ENUMTYPE " << ptr(obj)->metaObject()->property(i).isEnumType()
                     << " isFINAL " << ptr(obj)->metaObject()->property(i).isFinal();
                     */
            ds << ptr(obj)->metaObject()->property(i).read(ptr(obj));
        }
    }
    return ds;
}

template<typename T>
QDataStream& operator >> (QDataStream& ds, T& obj)
{
    QVariant var;

    if (!ptr(obj))
        return ds;

    for(int i=0; i<ptr(obj)->metaObject()->propertyCount(); ++i)
    {
        if( ptr(obj)->metaObject()->property(i).isStored(ptr(obj)))
        {
            ds >> var;
            /*
            qDebug() << ptr(obj)->metaObject()->property(i).name() << var;
            */
            ptr(obj)->metaObject()->property(i).write(ptr(obj), var);
        }
    }
    return ds;
}

#endif // SERIALIZATIONCONTEXT_H
