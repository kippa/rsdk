#ifndef ICOM_SERIAL_H
#define ICOM_SERIAL_H

#include <stdlib.h>

#include "radioozy.h"

// these are from the Icom CI-V protocol manual for command 0x04
// these must be mapped from our SDR_DTTSP_ MODES
typedef enum icom_modes
{
  ICOM_LSB,
  ICOM_USB,
  ICOM_AM,
  ICOM_CWL,
  ICOM_RTTYL,
  ICOM_FMN,
  ICOM_FMW,
  ICOM_CWU,
  ICOM_RTTYU,
  ICOM_SAM,
  ICOM_PSK,
  ICOM_PSKU,
  ICOM_SSB,
} ICOM_MODES;

class icom_serial
{
public:
    icom_serial(RadioOzy *r);
    ~icom_serial();

    int init_pty(const char *device);
    void stop_pty();
    ssize_t read_cmd();
    uint8_t buf[1024];
    char *getSlave_name() const;

private:
    int masterfd, slavefd;
    char *slave_name;
    RadioOzy *rozy;
    void send_FB(uint8_t src, uint8_t dst); // send All Good (0xFB) from src to dst
    void send_NG(uint8_t src, uint8_t dst); // send No Good (0xFDA) from src to dst

    icom_serial(const icom_serial &NoCopy);
    icom_serial &operator=(const icom_serial &NoAssign);
    int process_cmd(const uint8_t *buf, const int len);
    void prepare_buf(const uint8_t *buf, uint8_t *ab, int resp_len, int cmd_len);
    int send_packet(const uint8_t *pkt, int len);

    // map DttSP modes to ICOM_SERIAL modes
    std::map<SDRMODE_DTTSP, ICOM_MODES>modeMap;
};

#endif // ICOM_SERIAL_H
