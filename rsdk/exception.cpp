#include <stdarg.h>
#include <string.h>

#include "exception.h"

rsdkException::rsdkException(int Level_prm,int ErrorCode_prm,const char *ModuleName_prm,int LineNumber_prm,const char *FunctionName_prm,const char *Format_prm,...) throw():
    RetCode(ErrorCode_prm),LineModule(),ErrStr()
{
    va_list valist;
    va_start(valist,Format_prm);
    vsnprintf(ErrStr,MAXEXCBUFSIZE-2,Format_prm,valist);
    va_end(valist);
    //appends a \n if there isn't one..
    if (ErrStr[strlen(ErrStr)-1]!='\n')
    {
        strcat(ErrStr,"\n");
    }
    //call the common output routine (which is global)
    sprintf(LineModule,"%s %s at Line %d %d", ModuleName_prm, FunctionName_prm, LineNumber_prm, Level_prm);
    fprintf(stderr, "%s\n", ErrStr);
}
