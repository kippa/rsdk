#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/ioctl.h>

#include "common.h"
#include "dttsp.h" // modes for translation dttsp <-> icom, in a std::map
#include "icom_serial.h"

//#define SERIAL_DEBUG

icom_serial::icom_serial(RadioOzy *r) :
    slave_name(nullptr)
{
    assert(r);
    rozy = r;
    // init the modeMap to translate SDRMMODE_DTTSP to ICOM_MODES
    modeMap[LSB] = ICOM_LSB;
    modeMap[USB] = ICOM_USB;
    modeMap[DSB] = ICOM_USB; // no double side band?
    modeMap[CWL] = ICOM_CWL;
    modeMap[CWU] = ICOM_CWU;
    modeMap[FMN] = ICOM_FMN;
    modeMap[AM] = ICOM_AM;
    modeMap[DIGU] = ICOM_RTTYU;
    modeMap[SPEC] = ICOM_USB;
    modeMap[DIGL] = ICOM_RTTYL;
    modeMap[SAM] = ICOM_SAM;
    modeMap[DRM] = ICOM_USB;

    // name is unused at this time, possible link to provide constant name to pty
    init_pty("/dev/rsdk");
}

icom_serial::~icom_serial()
{
    stop_pty();
}

int icom_serial::init_pty(const char *device)
{
    (void)device;

    // ripped from online somewhere
    if ((masterfd = posix_openpt (O_RDWR | O_NOCTTY )) == -1
            || grantpt (masterfd) == -1
            || unlockpt (masterfd) == -1
            || (slave_name = ptsname (masterfd)) == nullptr)
        return -1;

    struct termios parms;
    int rc = tcgetattr(masterfd, &parms);
    if (rc) fprintf(stderr, "%s: tcgetattr error %d\n", __func__, rc);
    cfmakeraw(&parms);
    rc = tcsetattr(masterfd, TCSANOW, &parms);
    if (rc) fprintf(stderr, "%s: tcsetattr error %d\n", __func__, rc);
    rc = tcflush(masterfd, TCIOFLUSH);
    if (rc) fprintf(stderr, "%s: tcflush error %d\n", __func__, rc);

    fprintf(stderr, "icom_serial::%s: ptsname %s\n", __func__, slave_name);

    return masterfd;
}

void icom_serial::stop_pty()
{
    if (slavefd >= 0)
        close(slavefd);

    if (masterfd >= 0)
        close (masterfd);

}

void icom_serial::prepare_buf(const uint8_t *civ_cmd, uint8_t *ab, int resp_len, int cmd_len)
{
    if (resp_len > 0)
    {
        // use and twist the inbound src / dst
        // so response packet is returned to sender
        ab[0] = 0xFE;
        ab[1] = 0xFE;
        ab[2] = civ_cmd[3]; //dst
        ab[3] = civ_cmd[2]; // src
        for (int i = 0; i < cmd_len; i++)
            ab[4+i] = civ_cmd[4+i];
        // from here to len-1 is return data
        ab[resp_len-1] = 0xFD;
    }
}

int icom_serial::process_cmd(const uint8_t *cmd, const int len)
{
    if (len <=0) return -1;
    if (!cmd) return -1;
    if (cmd[0] == 0xFE && cmd[1] == 0xFE && cmd[len-1] == 0xFD)
    {
        // valid command frame received, process it
        // check to see if it is for us
        // compare to our default id
        // resend the cmd packet, stupid icom protocol
        // because hardware connections cause "echo" to occur
        // so rigctl expects to read the packet it just transmitted
        // BEFORE reading a response. Jeez

        send_packet(cmd, len);

        switch(cmd[4])
        {
        case 0x03: // read frequency
        {

            static const uint8_t RESP_LEN_0X03 = 11;
            uint8_t resp_0x03[16];

            // TODO FIXME account for page tune mode
            uint64_t f = rozy->getRxFreq(0);
            prepare_buf(cmd, resp_0x03, RESP_LEN_0X03, 1);
            toBCD(f, &resp_0x03[5], 7);
            send_packet(resp_0x03, RESP_LEN_0X03);
            break;
        }
        case 0x04: // read mode
        {

            static const uint8_t RESP_LEN_0X04 = 8;
            uint8_t resp_0x04[16];

            SDRMODE_DTTSP mode = rozy->getMode();
            // make the headers
            prepare_buf(cmd, resp_0x04, RESP_LEN_0X04, 1);

            // map mode from DttSP to Icom CI-V
            // careful, but we always know mode is correct right? we do, right?
            // TODO FIXME
            resp_0x04[5] = modeMap.at(mode);

            // use the index of the current filter to decide amongst wide, medium, and narrow values in CI-V

            // TODO FIXME this is fixed for subrx zero (0), make it for active rx someday
            double lo = rozy->getDSPFiltLo(0);
            double hi = rozy->getDSPFiltHi(0);
            double bw = hi - lo;

            int i = 0;
            for (; i<10; i++)
            {
                double val = convertToValues(filtersByMode[mode][i]);

                if (bw >= val)
                    break;
            }

            // translate rsdk bandwith to icom low, mid, high
            // we emulate ic-781
            if (i>=0 && i<4)
                resp_0x04[6] = 0x01; // wide bandwidth
            else if (i>=4 && i<6)
                resp_0x04[6] = 0x02; // medium bandwidth
            else if (i>=6)
                resp_0x04[6] = 0x03; // narrow bandwidth

            send_packet(resp_0x04, RESP_LEN_0X04);
            break;
        }
        case 0x05: // set frequency
        {
            // decode and set the frequency

            uint64_t f = (unsigned int)fromBCD(&cmd[5], 5);
            rozy->setFreq(f);
            rozy->setTxFreq(f);
            send_FB(cmd[3], cmd[2]);
            break;
        }
        case 0x07: // sub-band
        {
            static const uint8_t RESP_LEN_0x07 = 7;
            uint8_t resp_0x07[RESP_LEN_0x07];
            memset(resp_0x07, 0x00, RESP_LEN_0x07);
            // just echo the setting back
            prepare_buf(cmd, resp_0x07, RESP_LEN_0x07, 2);
            //send_packet(resp_0x07, RESP_LEN_0x07);
            send_FB(cmd[3], cmd[2]);
            break;
        }
        case 0x1a: // read / write stuff
        {
            switch (cmd[5])
            {
            case 0x03: // r/w IF filter settings, not really applicable?
            {
                // let's just report 3.6 KHz (40 bcd)
                static const uint8_t RESP_LEN_0X1a03 = 8;
                uint8_t resp_0x1a03[RESP_LEN_0X1a03];
                memset(resp_0x1a03, 0x00, RESP_LEN_0X1a03);
                prepare_buf(cmd, resp_0x1a03, RESP_LEN_0X1a03, 2);
                toBCD(40, &resp_0x1a03[6], 1);
                send_packet(resp_0x1a03, RESP_LEN_0X1a03);
                break;
            }
            case 0x06: // digital mode and filters
            {
                SDRMODE_DTTSP mode = rozy->getMode();
                static const uint8_t RESP_LEN_0X1a06 = 9;
                uint8_t resp_0x1a06[RESP_LEN_0X1a06];
                memset(resp_0x1a06, 0x00, RESP_LEN_0X1a06);
                prepare_buf(cmd, resp_0x1a06, RESP_LEN_0X1a06, 2);
                if (mode == DIGL || mode == DIGU) {
                    resp_0x1a06[6] = 0x01;
                    resp_0x1a06[7] = 0x01;
                } else {
                    resp_0x1a06[6] = 0x00;
                    resp_0x1a06[7] = 0x00;
                }
                send_packet(resp_0x1a06, RESP_LEN_0X1a06);
                break;
            }

            case 0x08: // memory channel
            {
                static const uint8_t RESP_LEN_0X08 = 8;
                uint8_t resp_0x08[RESP_LEN_0X08];
                memset(resp_0x08, 0x00, RESP_LEN_0X08);
                prepare_buf(cmd, resp_0x08, RESP_LEN_0X08, 1);
                resp_0x08[6] = 0x01; // just memory channel one for now, more to follow TODO FIXME
                send_packet(resp_0x08, RESP_LEN_0X08);
                break;
            }
            default:
                break;
            }
            break; // end of cmd 0x1a
        }
        case 0x1c: // exchange xmit and rcv
        {
            static const uint8_t RESP_LEN_0x1c = 8;
            uint8_t resp_0x1c[RESP_LEN_0x1c];
            if (len == RESP_LEN_0x1c) // a cmd was sent, not just a query of ptt
            {
                if (cmd[6])
                    rozy->setGuiXmit(true);
                else
                    rozy->setGuiXmit(false);
                send_FB(cmd[3], cmd[2]);
            } else {
                memset(resp_0x1c, 0x00, RESP_LEN_0x1c);
                prepare_buf(cmd, resp_0x1c, RESP_LEN_0x1c, 2);
                bool xmit = rozy->getXmit();
                if (xmit)
                    resp_0x1c[6] = 0x01;
                send_packet(resp_0x1c, RESP_LEN_0x1c);
            }
            break;
        }
        default:
            break;
        }

    } else {
        fprintf(stderr, "%s: NO SYNC \n" , __func__);
    }

    return 0;
}

ssize_t icom_serial::read_cmd()
{
    int avail;
    ssize_t bytes_read = 0;
    if (rozy)
    {
        //uint64_t freq = rozy->getRxFreq(0);
        if (masterfd >= 0)
        {
            ioctl(masterfd, FIONREAD, &avail);

            if (avail > 1024)
                avail = 1024;

            if (avail)
            {
                memset(buf, 0x0, 1024);
                bytes_read = read(masterfd, buf, avail);

                if (bytes_read)
                {

#ifdef SERIAL_DEBUG
                    fprintf(stderr, "%s: CMD IN avail %d bytes_read %d\n", __func__, avail, bytes_read);
                    fflush(stderr);
                    printHex(buf, static_cast<int>(bytes_read));
                    fflush(stderr);
                    fprintf(stderr, "\n");
                    fflush(stderr);
#endif // SERIAL_DEBUG

                    int ret = process_cmd(buf, bytes_read);

                } else {

                    fprintf(stderr, "%s: bytes read %d error\n", __func__, bytes_read);

                }
            }
        }
    }
    return bytes_read;
}

char *icom_serial::getSlave_name() const
{
    return slave_name;
}

int icom_serial::send_packet(const uint8_t *pkt, int len)
{
    int bytes_sent=0;
    while (len)
    {
        bytes_sent = write(masterfd, pkt, len);
        if (bytes_sent < 0)
            break;
        len -= static_cast<size_t>(bytes_sent);
    }

#ifdef SERIAL_DEBUG
    // DEBUG
    fprintf(stderr, "%s: RESP OUT bytes_sent %d\n", __func__, bytes_sent);
    fflush(stderr);
    printHex(pkt, static_cast<int>(bytes_sent));
    fflush(stderr);
                    fprintf(stderr, "\n");
                    fflush(stderr);
#endif // SERIAL_DEBUG

    return bytes_sent; // might be the error from write
}

// "Fine Business" all is okay response if there isn't any other response use this
void icom_serial::send_FB(uint8_t src, uint8_t dst)
{
static const uint8_t RESP_LEN_0XFB = 0x06;
static uint8_t resp_0xFB[RESP_LEN_0XFB] = { 0xFE, 0xFE, src, dst, 0xFB, 0xFD };
    send_packet(resp_0xFB, RESP_LEN_0XFB);
}

void icom_serial::send_NG(uint8_t src, uint8_t dst)
{
static const uint8_t RESP_LEN_0XFA = 0x06;
static uint8_t resp_0xFA[RESP_LEN_0XFA] = { 0xFE, 0xFE, src, dst, 0xFA, 0xFD };
    send_packet(resp_0xFA, RESP_LEN_0XFA);
}
