#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include <signal.h>

#include <iostream>
#include <cmath>
#include <algorithm>

#include <QString>
#include <QFile>
#include <QTimer>
#include <QtWidgets>
#include <QRadioButton>
#include <QFileDialog>
#include <QDebug>

#include "exception.h"

#include "serializationcontext.h"
#include "agcparmsdockwidget.h"

#include "rsdk.h"
#include "ui_rsdk.h"

#define TXTHREAD 1 // mercury 0 is thread zero, mercury 1 is thread 2, this is repeated in radioozy.cpp, FIXME TODO

static QCommandLineOption s_opt({"s","skip prev"}, "Skip loading previously saved geometry");

rsdk::rsdk(QWidget *parent, QApplication *app) :
    ModeDock(),
    FilterDock(),
    DspDock(),
    VfoDock(),
    AlexDock(),
    DisplayDock(),
    SmeterDock(),
    BandDock(),
    HpsdrDock(),
    AgcDock(),
    AgcParmsDock(),
    AudioDock(),
    TxDock(),
    PlotDrawOptionsDock(),
    CwDock(),
    TxControlsDock(),
    //mainWin(),
    MercurySelectDock(),
    RXEQDock(),
    TXEQDock(),
    RxParmsDock(),
    m_restartThreads(0),
    m_scopeSnapMode(0)
{
    if (!app)
    {
        THROW_EXCEPTION(CRITICAL_LEVEL, 0, "No QApplication, can't continue\n");
    }
    m_app = app;

    //mainWin = new QMainWindow(parent);
    //mainWin->statusBar()->hide();
    tbar = new QToolBar("rsdk");
    addToolBar(Qt::BottomToolBarArea, tbar);
    sbar = new QStatusBar();
    tbar->addWidget(sbar);

    dataFileName = "rsdk.data";
    confFileName = "rsdk.conf";

    // rsdk level command line options
    mainParser.setApplicationDescription("Description: SDR user interface for openHPSDR");
    // "s", skip loading the previous state of rsdk, allows to restore
    mainParser.addHelpOption();
    mainParser.addVersionOption();
    mainParser.addOption(s_opt);
    mainParser.process(QCoreApplication::arguments());

    rozy = new RadioOzy(dataFileName.toUtf8().constData(), &hpsdrProcessBuffer, nullptr);

    makeDocks(parent);
}

void rsdk::makeDocks(QWidget *parent)
{
    (void)parent;

    dockActMap = nullptr;

    setDocumentMode(false);
    setTabPosition(Qt::AllDockWidgetAreas, QTabWidget::South);
    setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
    setDockOptions(QMainWindow::AnimatedDocks | \
                   QMainWindow::AllowNestedDocks | \
                   QMainWindow::AllowTabbedDocks | \
                   QMainWindow::VerticalTabs);

    // this will THROW if a problem occurs, check stdout or stderr for trouble
    db = new dbStore(); // default name is "rsdk.db"

    // lock any sample rate changes
    //pthread_mutex_init(&sampRateLock, nullptr);

    setupUi(this);

    // make a receiver container and put in containers for each receiver
    BandDock = new BandDockWidget(this);
    addDockWidget(Qt::LeftDockWidgetArea, BandDock);
    dockList += BandDock;

    ModeDock = new ModeDockWidget(this);
    addDockWidget(Qt::LeftDockWidgetArea, ModeDock);
    dockList += ModeDock;

    FilterDock = new FilterDockWidget(this);
    addDockWidget(Qt::LeftDockWidgetArea, FilterDock);
    dockList += FilterDock;

    DspDock = new DspDockWidget(this);
    addDockWidget(Qt::LeftDockWidgetArea, DspDock);
    dockList += DspDock;

    SmeterDock = new SmeterDockWidget(this);
    addDockWidget(Qt::TopDockWidgetArea, SmeterDock);
    dockList += SmeterDock;
    //showProps(SmeterDock);

    VfoDock = new VfoDockWidget(this);
    addDockWidget(Qt::TopDockWidgetArea, VfoDock);
    dockList += VfoDock;

    AgcDock = new AgcDockWidget(this);
    addDockWidget(Qt::LeftDockWidgetArea, AgcDock);
    dockList += AgcDock;

    AgcParmsDock = new AgcParmsDockWidget(this);
    AgcParmsDock->setFloating(true);
    dockList += AgcParmsDock;

    RxParmsDock = new RxParmsDockWidget(this);
    RxParmsDock->setFloating(true);
    dockList += RxParmsDock;

    QString str = "RX EQ";
    RXEQDock = new EQ10DockWidget(this, str);
    RXEQDock->setFloating(true);
    dockList += RXEQDock;

    str = "TX EQ";
    TXEQDock = new EQ10DockWidget(this, str);
    TXEQDock->setFloating(true);
    dockList += TXEQDock;

    // BOTTOM DOCK
    TxDock = new TxDockWidget(this);
    addDockWidget(Qt::BottomDockWidgetArea, TxDock);
    dockList += TxDock;

    AudioDock = new AudioDockWidget(this);
    addDockWidget(Qt::BottomDockWidgetArea, AudioDock);
    dockList += AudioDock;

    CwDock = new CwDockWidget(this);
    addDockWidget(Qt::BottomDockWidgetArea, CwDock);
    dockList += CwDock;

    MercurySelectDock = new MercurySelectDockWidget(this);
    MercurySelectDock->setFloating(true);
    dockList += MercurySelectDock;

    HpsdrDock = new HpsdrDockWidget(this);
    addDockWidget(Qt::RightDockWidgetArea, HpsdrDock);
    dockList += HpsdrDock;

    AlexDock = new AlexDockWidget(this);
    addDockWidget(Qt::RightDockWidgetArea, AlexDock);
    dockList += AlexDock;

    PlotDrawOptionsDock = new PlotDrawOptionsDockWidget(this);
    addDockWidget(Qt::LeftDockWidgetArea, PlotDrawOptionsDock);
    dockList += PlotDrawOptionsDock;

    TxControlsDock = new TXControlDockWidget(this);
    TxControlsDock->setFloating(true);
    dockList += TxControlsDock;

    // Central Dock Area, only one widget allowed so use a splitter if needed
    // this one last (for dependencies)
    DisplayDock = new DisplayDockWidget(this);
    setCentralWidget(DisplayDock);
    dockList += DisplayDock;

    //make sure to not call start before rsdk is fully ctor'd
    QTimer::singleShot(0, this, SLOT(startAll()));

}

void rsdk::startAll()
{
    // deserialize and restore the gui components

    // connect signals from rsdk w to slots in this to call radioozy calls
    connect (this, SIGNAL(rxDCBStateChanged()), AgcDock, SLOT(setRxDCBState()));
    connect (this, SIGNAL(txDCBStateChanged()), AgcDock, SLOT(setTxDCBState()));

    // cross connect frequency settings, and temporarily disconnect where appropriate
    connect (VfoDock, SIGNAL(freqChanged(qlonglong)), this, SLOT(setRsdkFreq(qlonglong)));
    connect (VfoDock, SIGNAL(freqChanged(qlonglong)), BandDock, SLOT(bandBtnForFreq(qlonglong)));
    connect (VfoDock, SIGNAL(pageTuneChanged(bool)), DisplayDock, SLOT(setPageTune(bool)));
    //connect (this, SIGNAL(rsdkFreqChanged(qlonglong)), VfoDock, SLOT(setFreq(qlonglong)));
    connect (VfoDock, SIGNAL(subRxChanged(bool)), AudioDock, SLOT(enableSubRx(bool)));
    connect (VfoDock, SIGNAL(subRxChanged(bool)), this, SLOT(enableSubRx(bool)));


    connect (AlexDock, SIGNAL(attChanged(hpsdr::HpsdrAttEnum)), this, SLOT(setRsdkAtt(hpsdr::HpsdrAttEnum)));
    connect (AlexDock, SIGNAL(txAntChanged(hpsdr::HpsdrTxAntEnum)), this, SLOT(setRsdkTxant(hpsdr::HpsdrTxAntEnum)));
    connect (AlexDock, SIGNAL(rxAntChanged(hpsdr::HpsdrRxAntEnum)), this, SLOT(setRsdkRxant(hpsdr::HpsdrRxAntEnum)));
    connect (AlexDock, SIGNAL(rxOutChanged(hpsdr::HpsdrRxOutEnum)), this, SLOT(setRsdkRxout(hpsdr::HpsdrRxOutEnum)));

    connect (HpsdrDock, SIGNAL(sampleRateChanged(hpsdr::HpsdrSampleRateEnum)), this, SLOT(setRsdkSamplerate(hpsdr::HpsdrSampleRateEnum)));
    connect (HpsdrDock, SIGNAL(preAmpChanged(hpsdr::HpsdrPreAmpEnum)), this, SLOT(setRsdkPreamp(hpsdr::HpsdrPreAmpEnum)));
    connect (HpsdrDock, SIGNAL(ditherChanged(hpsdr::HpsdrDitherEnum)), this, SLOT(setRsdkDither(hpsdr::HpsdrDitherEnum)));
    connect (HpsdrDock, SIGNAL(randomChanged(hpsdr::HpsdrRandomEnum)), this, SLOT(setRsdkRandom(hpsdr::HpsdrRandomEnum)));
    connect (HpsdrDock, SIGNAL(ref10MChanged(hpsdr::HpsdrRef10MEnum)), this, SLOT(setRsdkRef10m(hpsdr::HpsdrRef10MEnum)));
    connect (HpsdrDock, SIGNAL(ref122MChanged(hpsdr::HpsdrRef122MEnum)), this, SLOT(setRsdkRef122m(hpsdr::HpsdrRef122MEnum)));
    connect (HpsdrDock, SIGNAL(duplexChanged(hpsdr::HpsdrDuplexEnum)), this, SLOT(setRsdkDuplex(hpsdr::HpsdrDuplexEnum)));
    connect (HpsdrDock, SIGNAL(micSrcChanged(hpsdr::HpsdrMicSrcEnum)), this, SLOT(setRsdkMicsrc(hpsdr::HpsdrMicSrcEnum)));
    connect (HpsdrDock, SIGNAL(classEChanged(hpsdr::HpsdrClassEEnum)), this, SLOT(setRsdkClasse(hpsdr::HpsdrClassEEnum)));
    connect (HpsdrDock, SIGNAL(configChanged(hpsdr::HpsdrConfigEnum)), this, SLOT(setRsdkConfig(hpsdr::HpsdrConfigEnum)));
    connect (HpsdrDock, SIGNAL(micGainChanged(hpsdr::HpsdrMicGainEnum)), this, SLOT(setRsdkMicGain(hpsdr::HpsdrMicGainEnum)));
    connect (HpsdrDock, SIGNAL(micLineChanged(hpsdr::HpsdrMicLineEnum)), this, SLOT(setRsdkMicLine(hpsdr::HpsdrMicLineEnum)));

    // ordering is important here, set the rozy first
    //connect (ModeDock, SIGNAL(modeChanged(SDRMODE_DTTSP)), this, SLOT(setRsdkMode(SDRMODE_DTTSP)));
    //connect (ModeDock, SIGNAL(modeChanged(SDRMODE_DTTSP)), FilterDock, SLOT(setFilterBank(SDRMODE_DTTSP)));
    //connect (ModeDock, SIGNAL(modeChanged(SDRMODE_DTTSP)), VfoDock, SLOT(modeChanged(SDRMODE_DTTSP)));

    // ordering is important
    connect (AgcDock, SIGNAL(agcChanged(AGCMODE)), this, SLOT(setRsdkAgcMode(AGCMODE)));
    connect (AgcDock, SIGNAL(agcChanged(AGCMODE)), AgcParmsDock, SLOT(updateValues()));
    connect (AgcDock, SIGNAL(rxDcbChanged(bool)), this, SLOT(setRXDCBlock(bool)));
    connect (AgcDock, SIGNAL(txDcbChanged(bool)), this, SLOT(setTXDCBlock(bool)));

    connect (DspDock, SIGNAL(dspANFChanged(bool)), this, SLOT(setRsdkANF(bool)));
    connect (DspDock, SIGNAL(dspMUTEChanged(bool)), this, SLOT(setRsdkMUTE(bool)));
    connect (DspDock, SIGNAL(dspBINChanged(bool)), this, SLOT(setRsdkBIN(bool)));
    connect (DspDock, SIGNAL(dspNB1Changed(bool)), this, SLOT(setRsdkNB(bool)));
    connect (DspDock, SIGNAL(dspNB2Changed(bool)), this, SLOT(setRsdkSDROM(bool)));
    connect (DspDock, SIGNAL(dspNRChanged(bool)), this, SLOT(setRsdkNR(bool)));
    connect (DspDock, SIGNAL(dspBNRChanged(bool)), this, SLOT(setRsdkBNR(bool)));

    // cross connect noise blanker switches in dspdock with rxparmsdock
    connect (DspDock, SIGNAL(dspNB1Changed(bool)), RxParmsDock, SLOT(setnb1(bool)));
    connect (DspDock, SIGNAL(dspNB2Changed(bool)), RxParmsDock, SLOT(setnb2(bool)));

    connect (FilterDock, SIGNAL(filterChanged(double,double)), this, SLOT(setRsdkFilter(double,double)));

    connect (PlotDrawOptionsDock, SIGNAL(yMaxChanged(int)), DisplayDock, SLOT(setMaxY(int)));
    connect (PlotDrawOptionsDock, SIGNAL(yMinChanged(int)), DisplayDock, SLOT(setMinY(int)));
    connect (PlotDrawOptionsDock, SIGNAL(rebuildBands(bool)), DisplayDock, SLOT(setRebuildBands(bool)));
    //connect (PlotDrawOptionsDock, SIGNAL(yMaxChanged(int)), BandDock, SLOT(updateBands(int)));
    //connect (PlotDrawOptionsDock, SIGNAL(yMinChanged(int)), BandDock, SLOT(updateBands(int)));
    connect (PlotDrawOptionsDock, SIGNAL(numCoeffsChanged(int)), DisplayDock, SLOT(setFilterCoeffs(int)));
    connect (PlotDrawOptionsDock, SIGNAL(numCoeffsChanged(int)), DisplayDock->bandscope, SLOT(setSmooth(int)));
    connect (PlotDrawOptionsDock, SIGNAL(numCoeffsChanged(int)), DisplayDock->oscope, SLOT(setSmooth(int)));
    connect (PlotDrawOptionsDock, SIGNAL(titleEnabled(bool)), DisplayDock, SLOT(setTitleOn(bool)));

    connect (PlotDrawOptionsDock, SIGNAL(sGridEnabled(bool)), DisplayDock, SLOT(enableSGrid(bool)));
    connect (PlotDrawOptionsDock, SIGNAL(fGridEnabled(bool)), DisplayDock, SLOT(enableFGrid(bool)));
    connect (PlotDrawOptionsDock, SIGNAL(dbGridEnabled(bool)), DisplayDock, SLOT(enableDbGrid(bool)));

    connect (PlotDrawOptionsDock, SIGNAL(spectrumColorChanged(QColor)), DisplayDock, SLOT(setSpectrumColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(sGridColorChanged(QColor)), DisplayDock, SLOT(setSGridColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(fGridColorChanged(QColor)), DisplayDock, SLOT(setFGridColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(dbGridColorChanged(QColor)), DisplayDock, SLOT(setDbGridColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(titleColorChanged(QColor)), DisplayDock, SLOT(setTitleColor(QColor)));
    //connect (PlotDrawOptionsDock, SIGNAL(fillColorChanged(QColor)), DisplayDock, SLOT(setFillColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(bandScopeColorChanged(QColor)), DisplayDock->bandscope, SLOT(setTraceColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(bandColorChanged(QColor)), DisplayDock, SLOT(setBandColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(scopeColorChanged(QColor)), DisplayDock->oscope, SLOT(setTraceColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(scopePkColorChanged(QColor)), DisplayDock->oscope, SLOT(setPkColor(QColor)));
    connect (PlotDrawOptionsDock, SIGNAL(scopeGridColorChanged(QColor)), DisplayDock->oscope, SLOT(setGridColor(QColor)));

    connect (PlotDrawOptionsDock, SIGNAL(bandScopeChanged(bool)), DisplayDock->bandscope, SLOT(setBandScopeEnabled(bool)));
    connect (PlotDrawOptionsDock, SIGNAL(bandScopeChanged(bool)), DisplayDock, SLOT(setBandScopeOn(bool)));

    connect (PlotDrawOptionsDock, SIGNAL(scopeToggled(bool)), DisplayDock->oscope, SLOT(setOScopeEnabled(bool)));
    connect (PlotDrawOptionsDock, SIGNAL(scopeToggled(bool)), DisplayDock, SLOT(setOScopeOn(bool)));

    connect (PlotDrawOptionsDock, SIGNAL(fillTraceChanged(bool)), DisplayDock, SLOT(setFillTrace(bool)));
    connect (PlotDrawOptionsDock, SIGNAL(bandChanged(bool)), DisplayDock, SLOT(setBandOn(bool)));

    connect (PlotDrawOptionsDock, SIGNAL(yMaxChanged(int)), SmeterDock, SLOT(setYMax(int)));
    connect (PlotDrawOptionsDock, SIGNAL(yMinChanged(int)), SmeterDock, SLOT(setYMin(int)));

    //connect (PlotDrawOptionsDock, SIGNAL(yMaxChanged(int)), DisplayDock2, SLOT(setMaxY(int)));
    //connect (PlotDrawOptionsDock, SIGNAL(yMinChanged(int)), DisplayDock2, SLOT(setMinY(int)));

    connect (AudioDock, SIGNAL(rxVolChanged(float)), this, SLOT(setRsdkVol(float)));
    connect (AudioDock, SIGNAL(rxBalChanged(float)), this, SLOT(setRsdkBal(float)));
    connect (AudioDock, SIGNAL(subRxVolChanged(float)), this, SLOT(setRsdkSubRxVol(float)));
    connect (AudioDock, SIGNAL(subRxBalChanged(float)), this, SLOT(setRsdkSubRxBal(float)));
    connect (AudioDock, SIGNAL(txLeftChanged(float)), this, SLOT(setTxLeftGain(float)));

    connect (CwDock, SIGNAL(cwFreqChanged(double)), this, SLOT(setCwFreq(double)));
    connect (CwDock, SIGNAL(cwSpeedChanged(unsigned int)), this, SLOT(setCwSpeed(unsigned int)));
    connect (CwDock, SIGNAL(cwModeChanged(unsigned int)), this, SLOT(setCwMode(unsigned int)));
    connect (CwDock, SIGNAL(cwWeightChanged(unsigned int)), this, SLOT(setCwWeight(unsigned int)));
    connect (CwDock, SIGNAL(cwPttDelayChanged(unsigned int)), this, SLOT(setCwPttDelay(unsigned int)));
    connect (CwDock, SIGNAL(cwHangTimeChanged(unsigned int)), this, SLOT(setCwHangTime(unsigned int)));
    connect (CwDock, SIGNAL(cwSpacingChanged(bool)), this, SLOT(setCwSpacing(bool)));
    connect (CwDock, SIGNAL(cwKeysReversedChanged(bool)), this, SLOT(setCwKeysReversed(bool)));
    connect (CwDock, SIGNAL(cwSideToneVolumeChanged(unsigned int)), this, SLOT(setCwSideToneVolume(unsigned int)));
    connect (CwDock, SIGNAL(cwKeyerEnabledChanged(bool)), this, SLOT(setCWInternalKeyer(bool)));

    connect (DisplayDock, SIGNAL(freqChanged(qlonglong)), this, SLOT(setRsdkFreq(qlonglong)));
    connect (DisplayDock, SIGNAL(freqChanged(qlonglong)), BandDock, SLOT(bandBtnForFreq(qlonglong)));
    //connect (DisplayDock, SIGNAL(freqChanged(qlonglong)), VfoDock, SLOT(setFreq(qlonglong)));
    connect (DisplayDock->bandscope, SIGNAL(freqChanged(qlonglong)), this, SLOT(setRsdkFreq(qlonglong)));
    connect (DisplayDock->bandscope, SIGNAL(freqChanged(qlonglong)), BandDock, SLOT(bandBtnForFreq(qlonglong)));
    //connect (DisplayDock->bandscope, SIGNAL(freqChanged(qlonglong)), VfoDock, SLOT(setFreq(qlonglong)));

    connect (DisplayDock, SIGNAL(spectrumColorChanged(QColor)), PlotDrawOptionsDock, SLOT(spectrumColor(QColor)));

    connect (DisplayDock, SIGNAL(sGridColorChanged(QColor)), PlotDrawOptionsDock, SLOT(sGridColor(QColor)));
    connect (DisplayDock, SIGNAL(dbGridColorChanged(QColor)), PlotDrawOptionsDock, SLOT(dbGridColor(QColor)));
    connect (DisplayDock, SIGNAL(fGridColorChanged(QColor)), PlotDrawOptionsDock, SLOT(fGridColor(QColor)));

    connect (DisplayDock, SIGNAL(titleColorChanged(QColor)), PlotDrawOptionsDock, SLOT(titleColor(QColor)));
    //connect (DisplayDock, SIGNAL(fillColorChanged(QColor)), PlotDrawOptionsDock, SLOT(fillColor(QColor)));
    connect (DisplayDock->bandscope, SIGNAL(bandScopeColorChanged(QColor)), PlotDrawOptionsDock, SLOT(bandScopeColor(QColor)));
    connect (DisplayDock, SIGNAL(bandColorChanged(QColor)), PlotDrawOptionsDock, SLOT(bandColor(QColor)));

    connect (DisplayDock->oscope, SIGNAL(colorChanged(QColor)), PlotDrawOptionsDock, SLOT(scopeColor(QColor)));
    connect (DisplayDock->oscope, SIGNAL(gridColorChanged(QColor)), PlotDrawOptionsDock, SLOT(scopeGridColor(QColor)));
    connect (DisplayDock->oscope, SIGNAL(pkColorChanged(QColor)), PlotDrawOptionsDock, SLOT(scopePkColor(QColor)));
    connect (DisplayDock->oscope, SIGNAL(snapChanged(QString)), this, SLOT(setScopeSnap(QString)));

    connect (TxDock, SIGNAL(txDriveLevelChanged(unsigned int)), this, SLOT(setRsdkTxDriveLevel(unsigned int)));
    connect (TxDock, SIGNAL(txTuneDriveLevelChanged(unsigned int)), this, SLOT(setRsdkTxDriveLevel(unsigned int)));
    connect (TxDock, SIGNAL(txPressed(bool)), this, SLOT(setXmit(bool)));

    connect (MercurySelectDock, SIGNAL(mercSelChanged(MercurySelectDockWidget::MERCMODE)), this, SLOT(setCurMerc(MercurySelectDockWidget::MERCMODE)));
    connect (MercurySelectDock, SIGNAL(sendNormI(double)), this, SLOT(setNormI(double)));
    connect (MercurySelectDock, SIGNAL(sendNormQ(double)), this, SLOT(setNormQ(double)));
    connect (MercurySelectDock, SIGNAL(sendGain(double)), this, SLOT(setGain(double)));

    connect (TxControlsDock, SIGNAL(AMCarrierLevelChanged(double)), this, SLOT(setAMCarrierLevel(double)));
    connect (TxControlsDock, SIGNAL(CompanderLevelChanged(double)), this, SLOT(setTXCompanderLevel(double)));
    connect (TxControlsDock, SIGNAL(CompanderEnable(bool)), this, SLOT(setTXCompander(bool)));

    connect (PlotDrawOptionsDock, SIGNAL(spectrumIntervalChanged(int)), DisplayDock, SLOT(setSpectrumInterval(int)));
    connect (DisplayDock, SIGNAL(spectrumIntervalChanged(int, bool)), PlotDrawOptionsDock, SLOT(setSpInterval(int, bool)));

    connect (PlotDrawOptionsDock, SIGNAL(waterfallToggled(bool)), DisplayDock, SLOT(setWaterfallEnable(bool)));
    connect (PlotDrawOptionsDock, SIGNAL(spectrumToggled(bool)), DisplayDock, SLOT(setSpectrumEnable(bool)));

    connect (PlotDrawOptionsDock, SIGNAL(waterfallIntervalChanged(int)), DisplayDock, SLOT(setWaterfallInterval(int)));
    connect (DisplayDock, SIGNAL(waterfallIntervalChanged(int, bool)), PlotDrawOptionsDock, SLOT(setWfInterval(int, bool)));

    // make the main window "Docks" menu
    // a checked/checkable menu of the dock widgets for show/hide
    // do this last so we know if a given dock widget is visible or not
    createDockActions();

    // more prelims
    AgcParmsDock->updateValues();

    // wait until connections are made, then start the radioozy threads
    m_restartThreads = rozy->startThreads();

    // these don't need to be on screen until asked for via the Docks menu
    TXEQDock->hide();
    RXEQDock->hide();
    RxParmsDock->hide();
    AgcParmsDock->hide();
    TxControlsDock->hide();
    MercurySelectDock->hide();

    // start all the docks up
    foreach(rsdkDockWidget *w, dockList)
        w->start();

    // restore this rsdk mainWindow to last known size and pos
    if (!mainParser.isSet(s_opt))
    {
        //readPos();
        if (propFileName.isEmpty())
            propFileName = "rsdk.prop";

        loadProps(false);
    }

    /*******************************************************************************/

    int cnt=0;
    rozy->mercurySN = 0;
    rozy->ozySN = 0;
    rozy->penelopeSN = 0;

    while (!rozy->mercurySN && cnt < 1000)
    {
        cnt++;
        my_usleep(1000);
    }
    if (cnt >= 1000)
    {
        QString str(tr("No HPSDR Mercury connected"));
        sbar->showMessage(str);
        //raise(SIGQUIT);
        // testing 07/07/2015 KAA
        //exit(1);
    }

    setTitle();
    // report the icom serial pty slave name, use this name to connect via hamlib
    sbar->showMessage(rozy->icsr->getSlave_name());

    // debugging
    /*
    foreach(QDockWidget *w, dockList) {
        QObject *obj = w;
        qDebug() << obj->objectName();
        if (obj->objectName().contains("Smeter"))
            showProps(obj);
    }
    */
}

void rsdk::on_actionQuit_triggered()
{
    stopAll();
}

void rsdk::stopAll()
{
    QApplication::sendEvent(this,new QCloseEvent());
    if (m_app)
        m_app->quit();
}

void rsdk::showProps(QObject *obj)
{
    const char *name;
    QVariant val;

    if (!obj)
        return;

    const QMetaObject *mo = obj->metaObject();
    int count = mo->propertyCount();
    for (int i=0; i<count; ++i) {
        QMetaProperty mp = mo->property(i);
        name = mp.name();
        val = obj->property(name);
        qDebug() << name << " " << val;
    }
}

rsdk::~rsdk()
{
    // remember size and pos for next time
    writePos();

    // don't delete rozy here, she was made new in main.cpp, deleted there too
    // but we do want to stop buffer processing first
    rozy->killThreads();

    foreach(rsdkDockWidget *dock, dockList)
    {
        delete dock;
    }

    delete sbar;
    delete tbar;

    //delete mainWin;
    delete db;
    //delete ap;
}

// use closeEvent to serialize the qtwidgets BEFORE they go visible(false)
// so we don't have to set visibility on deserialization
void rsdk::closeEvent(QCloseEvent *e)
{
    (void)e;

    storeProps(false); // no gui for the save, use propFileName
}

void rsdk::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);

    if (e->type() == QEvent::WindowStateChange)
    {
        if (isMinimized())
        {
            m_minimized = true;
        } else {
            m_minimized = false;
        }
    }
}

// use closeEvent to serialize the qtwidgets BEFORE they go visible(false)
// so we don't have to set visibility on deserialization

// store all Dock Widget children Q_PROPERTYs
// this is the start of memories
void rsdk::storeProps(bool doDialog)
{
    QStringList filenames;

    if (doDialog)
    {
        QFileDialog qfd(this);
        qfd.setNameFilter("rsdk prop files (*.prop);;Any file (*.*)");
        qfd.setAcceptMode(QFileDialog::AcceptSave);
        qfd.setViewMode(QFileDialog::Detail);
        qfd.setDefaultSuffix(".prop"); // the "." is removed, of course
        qfd.setOption(QFileDialog::DontUseNativeDialog, true);

        if (qfd.exec())
            filenames = qfd.selectedFiles();
    } else {
        filenames.append(propFileName);
    }

    qDebug() << __func__ << "filenames:" << filenames;

    // open one time for all children DockWidgets
    // and call each child's save method
    QString filename;
    foreach (filename, filenames) {
        qDebug() << __func__ << "open:" << filename;
        QFile file(filename);
        if(file.open(QIODevice::WriteOnly))
        {
            QTextStream out(&file);
            saveProps(this, out);
            foreach(rsdkDockWidget *w, dockList)
                saveProps(w, out);

            file.flush();
            file.close();
        }
        else
        {
            cout << "Could not open serialization file " << filename.toUtf8().constData() << endl;
        }
    }
}

void rsdk::loadProps(bool doGui)
{

    QStringList filenames;

    if (doGui) {
        QFileDialog qfd(this);
        qfd.setNameFilter("rsdk prop files (*.prop);;Any file (*.*)");
        qfd.setAcceptMode(QFileDialog::AcceptOpen);
        qfd.setViewMode(QFileDialog::Detail);
        qfd.setDefaultSuffix(".prop"); // the "." is removed
        qfd.setOption(QFileDialog::DontUseNativeDialog, true);
        if (qfd.exec())
            filenames = qfd.selectedFiles();
    } else {
        filenames.append(propFileName);
    }

    QString filename;
    foreach (filename, filenames)
    {
        QFile file(filename);
        if(file.open(QIODevice::ReadOnly))
        {
            QTextStream in(&file);
            readProps(this, in);
            foreach(rsdkDockWidget *w, dockList)
            {
                readProps(w, in);
                in.seek(0); // next object starts at beginning again
            }
            file.close();
        }
        else
        {
            cout << "Could not open serialization file " << filename.toUtf8().constData() << endl;
        }
    }
}

void rsdk::onRsdkDestroyed(QObject *arg1)
{
   fprintf(stderr, "%s: %p\n", __func__, dynamic_cast<void *>(arg1));
}

int rsdk::getRsdkSampleRate() const
{
    return HpsdrDock->getSampleRateVal();
}

// convert from hpsdr::HpsdrSampleRateEnum to int and store it that way
void rsdk::setRsdkSamplerate(hpsdr::HpsdrSampleRateEnum sampleRate_prm)
{
    if (sampleRate_prm == hpsdr::SampleRate96000)
    {
        setRsdkSamplerate(96000);
    }
    else if(sampleRate_prm == hpsdr::SampleRate192000)
    {
        setRsdkSamplerate(192000);
    }
    else if(sampleRate_prm == hpsdr::SampleRate384000)
    {
        setRsdkSamplerate(384000);
    }
    else // if(sampleRate_prm == hpsdr::SampleRate48000)
    {
        setRsdkSamplerate(48000);
    }
}

void rsdk::setCwFreq(double f)
{
    FilterDock->setCenterFreq(f);
    rozy->setKeyerFreq(f);
}

void rsdk::setCwSpeed(unsigned int wpm)
{
    // range limit to HPSDR protocol
    unsigned int val = wpm < 1 ? 1 : wpm > 60 ? 60 : wpm;
    rozy->setKeyerSpeed(static_cast<unsigned char>(val));
}

void rsdk::setCwMode(unsigned int mode)
{
    // range limit to HPSDR protocol
    // modes are 0 = straight key, 1 = Mode A, 2 = Mode B
    unsigned int val = mode > 2 ? 2 : mode;
    rozy->setKeyerMode(static_cast<unsigned char>(val));
}

void rsdk::setCwWeight(unsigned int weight)
{
    // range limit to HPSDR protocol
    unsigned int val = weight > 100 ? 100 : weight;
    rozy->setKeyerWeight(static_cast<unsigned char>(val));
}

void rsdk::setCwPttDelay(unsigned int delay)
{
    rozy->setKeyerPttdelay(static_cast<unsigned char>(delay));
}

void rsdk::setCwHangTime(unsigned int hangTime)
{
    // range limit to HPSDR protocol
    unsigned short val = hangTime % 1024;
    rozy->setKeyerHangtime(val);
}

void rsdk::setCwSpacing(bool spacing)
{
    rozy->setKeyerSpacing(spacing);
}

void rsdk::setCwKeysReversed(bool reversed)
{
    rozy->setKeyerReversed(reversed);
}

void rsdk::setCwSideToneVolume(unsigned int volume)
{
    // range limit to the HPSDR protocol
    unsigned int val = volume > 127 ? 127 : volume;
    rozy->setKeyerStvolume(static_cast<unsigned char>(val));
}

void rsdk::setCWInternalKeyer(bool enable)
{
    rozy->setKeyerEnabled(enable);
}

void rsdk::setTxRightGain(float gain)
{
    // range limit gain
    if (gain > 1.0f)
        gain = 1.0f;
    else if (gain < 0.0f)
        gain = 0.0f;

    //AudioDock->setTxRight(gain);
    rozy->setTxRightGain(gain);
}

void rsdk::setTxLeftGain(float gain)
{
    // range limit gain
    if (gain > 1.0f)
        gain = 1.0f;
    else if (gain < 0.0f)
        gain = 0.0f;

    //AudioDock->setTxLeft(gain);
    rozy->setTxLeftGain(gain);
}

qlonglong rsdk::getRsdkFreq() const
{
    qlonglong ret;

    ret = VfoDock->getFreq();
    return ret;
}

void rsdk::setRsdkFreq(const double value)
{
    setRsdkFreq(static_cast<qlonglong>(value));
}

void rsdk::setRsdkFreq(const qlonglong value)
{
    qlonglong reqFreq = value;
    // range limit the requests
    if (reqFreq < 1) {
        reqFreq = 1;
    } else if (reqFreq > MAX_HF_FREQ)  {
        reqFreq = MAX_HF_FREQ;
    }

    double cal = 0.0;
    cal = VfoDock->cal();
    double cwoffset = rozy->keyerFreq;
    double calOffset = -(static_cast<double>(reqFreq/1e6)) * cal;

    // the current "center" frequency
    qlonglong f = static_cast<qlonglong>(rozy->getRxFreq(0));

    double diff = (static_cast<double>(f - reqFreq)) + calOffset;

    qlonglong x = (qlonglong)DisplayDock->getXAxisMaxX();
    qlonglong y = (qlonglong)DisplayDock->getXAxisMinX();

    if (VfoDock->subRxOn()) {
        if (reqFreq > x) {
            reqFreq = x;
        } else if (reqFreq < y) {
            reqFreq = y;
        }

        // recalc diff 'cause reqFreq may have limited
        diff = ((double)(f - reqFreq)) + calOffset;

        // SUBRX 1
        if (rozy->mode == CWU)
            SetRXOsc(rozy->curMercury, 1, diff + cwoffset);
        else if (rozy->mode == CWL)
            SetRXOsc(rozy->curMercury, 1, diff - cwoffset);
        else
            SetRXOsc(rozy->curMercury, 1, diff);

    } else if (VfoDock->pageTune()) {
        if (reqFreq > x) {
            // move to the next "page" center frequency
            qlonglong ncf = f + (qlonglong)rozy->sampleRate;
            rozy->setFreq(ncf);
            DisplayDock->updateXAxis(ncf);
            setRsdkFreq(reqFreq);
        } else if (reqFreq < y) {
            // move to the prior "page" center frequency
            qlonglong ncf = f - (qlonglong)rozy->sampleRate;
            rozy->setFreq(ncf);
            DisplayDock->updateXAxis(ncf);
            setRsdkFreq(reqFreq);
        }

        // RX 0
        if (rozy->mode == CWU)
            SetRXOsc(rozy->curMercury, 0, diff + cwoffset);
        else if (rozy->mode == CWL)
            SetRXOsc(rozy->curMercury, 0, diff - cwoffset);
        else
            SetRXOsc(rozy->curMercury, 0, diff);

    } else {

        // RX 0
        if (rozy->mode == CWU)
            SetRXOsc(rozy->curMercury, 0, calOffset + cwoffset);
        else if (rozy->mode == CWL)
            SetRXOsc(rozy->curMercury, 0, calOffset - cwoffset);
        else
            SetRXOsc(rozy->curMercury, 0, calOffset);

        // not page tune mode, just always set the center frequency
        rozy->setFreq(reqFreq, 0);
    }

    // caution with true, disable the signals or we'll be here infinitely
    bool pstate = VfoDock->blockSignals(true);
    VfoDock->setFreq(reqFreq, true);
    VfoDock->blockSignals(pstate);

    // update the tx
    rozy->setTxFreq(reqFreq);
    SetTXOsc(TXTHREAD, -calOffset); // I & Q are reversed in xmit path to hpsdr
}

qlonglong rsdk::getRsdkSubRxFreq() const
{
    return VfoDock->freqR2();
}

void rsdk::setRsdkSubRxFreq(const qlonglong &value)
{
    double cal = VfoDock->cal();
    double cwoffset = CwDock->freq();
    double calOffset = -((double)value/1e6) * cal;

    // SUBRX 1
    if (rozy->mode == CWU)
        SetRXOsc(rozy->curMercury, 1, calOffset + cwoffset);
    else if (rozy->mode == CWL)
        SetRXOsc(rozy->curMercury, 1, calOffset - cwoffset);
    else
        SetRXOsc(rozy->curMercury, 1, calOffset);
}

int rsdk::getRsdkTuneRate() const
{
    return VfoDock->tuneRate_asInt();
}

void rsdk::setRsdkTuneRate(const int tuneRate)
{
    VfoDock->setTuneInc(tuneRate);
}

unsigned char rsdk::getRsdkTxDriveLevel() const
{
    return TxDock->txDriveLevel();
}

void rsdk::setRsdkTxDriveLevel(const unsigned int level)
{
    //TxDock->setTxDriveLevel((int)level);
    rozy->setDrive(level);
}

bool rsdk::getRsdkTuneMode() const
{
    return rozy->getTuneMode();
}

void rsdk::setRsdkTuneMode(bool enableTune)
{
    rozy->setTuneMode(enableTune);
}

hpsdr::HpsdrAttEnum rsdk::getRsdkAtt() const
{
    return AlexDock->getAlexAtt();
}

void rsdk::setRsdkAtt(const hpsdr::HpsdrAttEnum &value)
{
    //rozy->setAtt(AlexDock->getAlexAtt());
    rozy->setAtt(value);
}

hpsdr::HpsdrRxAntEnum rsdk::getRsdkRxant() const
{
    return AlexDock->getAlexRxAnt();
}

void rsdk::setRsdkRxant(const hpsdr::HpsdrRxAntEnum &value)
{
    //rozy->setRxAnt(AlexDock->getAlexRxAnt());
    rozy->setRxAnt(value);
}

hpsdr::HpsdrTxAntEnum rsdk::getRsdkTxant() const
{
    return AlexDock->getAlexTxAnt();
}

void rsdk::setRsdkTxant(const hpsdr::HpsdrTxAntEnum &value)
{
    //rozy->setTxAnt(AlexDock->getAlexTxAnt());
    rozy->setTxAnt(value);
}

hpsdr::HpsdrRxOutEnum rsdk::getRsdkRxout() const
{
    return AlexDock->getAlexRxOut();
}

void rsdk::setRsdkRxout(const hpsdr::HpsdrRxOutEnum &value)
{
    //rozy->setRxOut(AlexDock->getAlexRxOut());
    rozy->setRxOut(value);
}

hpsdr::HpsdrPreAmpEnum rsdk::getRsdkPreamp() const
{
    return HpsdrDock->getPreAmp();
}

void rsdk::setRsdkPreamp(const hpsdr::HpsdrPreAmpEnum &value)
{
    rozy->setPreAmp(value);
}

hpsdr::HpsdrDitherEnum rsdk::getRsdkDither() const
{
    return HpsdrDock->getDither();
}

void rsdk::setRsdkDither(const hpsdr::HpsdrDitherEnum &value)
{
    rozy->setDither(value);
}

hpsdr::HpsdrRandomEnum rsdk::getRsdkRandom() const
{
    return HpsdrDock->getRandom();
}

void rsdk::setRsdkRandom(const hpsdr::HpsdrRandomEnum &value)
{
    rozy->setRandom(value);
}

hpsdr::HpsdrRef10MEnum rsdk::getRsdkRef10m() const
{
    return HpsdrDock->getRef10M();
}

void rsdk::setRsdkRef10m(const hpsdr::HpsdrRef10MEnum &value)
{
    rozy->setRef10M(value);
}

hpsdr::HpsdrRef122MEnum rsdk::getRsdkRef122m() const
{
    return HpsdrDock->getRef122M();
}

void rsdk::setRsdkRef122m(const hpsdr::HpsdrRef122MEnum &value)
{
    rozy->setRef122M(value);
}

hpsdr::HpsdrMicSrcEnum rsdk::getRsdkMicsrc() const
{
    return HpsdrDock->getMicSrc();
}

void rsdk::setRsdkMicsrc(const hpsdr::HpsdrMicSrcEnum &value)
{
    rozy->setMicSrc(value);
}

hpsdr::HpsdrDuplexEnum rsdk::getRsdkDuplex() const
{
    return HpsdrDock->getDuplex();
}

void rsdk::setRsdkDuplex(const hpsdr::HpsdrDuplexEnum &value)
{
    rozy->setDuplex(value);
}

hpsdr::HpsdrClassEEnum rsdk::getRsdkClasse() const
{
    return HpsdrDock->getClassE();
}

void rsdk::setRsdkClasse(const hpsdr::HpsdrClassEEnum &value)
{
    rozy->setClassE(value);
}

hpsdr::HpsdrConfigEnum rsdk::getRsdkConfig() const
{
    return HpsdrDock->getConfig();
}

void rsdk::setRsdkConfig(const hpsdr::HpsdrConfigEnum &value)
{
    bool pstate = HpsdrDock->blockSignals(true);
    HpsdrDock->setConfig(value);
    HpsdrDock->blockSignals(pstate);
    rozy->setConfig(value);
}

hpsdr::HpsdrMicGainEnum rsdk::getRsdkMicGain() const
{
    return HpsdrDock->getMicGain();
}

void rsdk::setRsdkMicGain(const hpsdr::HpsdrMicGainEnum &value)
{
    bool pstate = HpsdrDock->blockSignals(true);
    HpsdrDock->setMicGain(value);
    HpsdrDock->blockSignals(pstate);
    rozy->setMicGain(value);
}

hpsdr::HpsdrMicLineEnum rsdk::getRsdkMicLine() const
{
    return HpsdrDock->getMicLine();
}

void rsdk::setRsdkMicLine(const hpsdr::HpsdrMicLineEnum &value)
{
    bool pstate = HpsdrDock->blockSignals(true);
    HpsdrDock->setMicLine(value);
    HpsdrDock->blockSignals(pstate);
    if (value == hpsdr::MicIn)
    {
        // change the M/L Gain to appropriate values for MicIn

    }
    else if (value == hpsdr::LineIn)
    {

    }
    rozy->setMicLine(value);
}

SDRMODE_DTTSP rsdk::getRsdkMode() const
{
    return rozy->getMode();
}

void rsdk::setRsdkMode(const SDRMODE_DTTSP &value)
{
    if (value == LSB || value == USB || value == DSB ||
            value == CWL || value == CWU || value == FMN ||
            value == AM || value == DIGU || value == DIGL ||
            value == SAM || value == DRM )
    {
        // DttSP callouts

        // TX mode get set too
        rozy->setTXMode(value);

        // and rozy callout
        if (VfoDock->subRxOn())
            rozy->setMode(value, 1);
        else
            rozy->setMode(value, 0);

        // and the gui callouts
        ModeDock->setMode(value, false);
        FilterDock->setFilterBank(value);
        VfoDock->modeChanged(value);

        //setRsdkFreq(VfoDock->getFreq());
        //setRsdkFreq(rozy->getRxFreq(rozy->curMercury));
        if (VfoDock->subRxOn())
            setRsdkFilter(rozy->getDSPFiltLo(1), rozy->getDSPFiltHi(1));
        else
            setRsdkFilter(rozy->getDSPFiltLo(0), rozy->getDSPFiltHi(0));

        // if in CW mode, enable internal keyer
        if (value == CWU || value == CWL)
            CwDock->setKeyerEnabled(true);
        else
            CwDock->setKeyerEnabled(false);
    }
}

void rsdk::setRsdkMode(const char *m)
{
    SDRMODE_DTTSP mode = (SDRMODE_DTTSP)ModeDock->getModeType(QString(m));
    setRsdkMode(mode);
}

void rsdk::setRsdkMode(const QString &m)
{
    SDRMODE_DTTSP mode = (SDRMODE_DTTSP)ModeDock->getModeType(m);
    setRsdkMode(mode);
}

AGCMODE rsdk::getRsdkAgcMode() const
{
    return (AGCMODE)AgcDock->mode();
}

void rsdk::setRsdkAgcMode(const AGCMODE &value)
{
    SetRXAGC(rozy->curMercury, 0, (AGCMODE)value);
    SetRXAGC(rozy->curMercury, 1, (AGCMODE)value);
}

void rsdk::setRsdkAgcAttack(double val)
{
    SetRXAGCAttack(rozy->curMercury, 0, (float)val);
    SetRXAGCAttack(rozy->curMercury, 1, (float)val);
}

void rsdk::setRsdkAgcDecay(double val)
{
    SetRXAGCDecay(rozy->curMercury, 0, (float)val);
    SetRXAGCDecay(rozy->curMercury, 1, (float)val);
}

void rsdk::setRsdkAgcHang(double val)
{
    SetRXAGCHang(rozy->curMercury, 0, (float)val);
    SetRXAGCHang(rozy->curMercury, 1, (float)val);
}

void rsdk::setRsdkAgcHangThreshold(double val)
{
    SetRXAGCHangThreshold(rozy->curMercury, 0, (float)val);
    SetRXAGCHangThreshold(rozy->curMercury, 1, (float)val);
}

void rsdk::setRsdkAgcSlope(double val)
{
    SetRXAGCSlope(rozy->curMercury, 0, (float)val);
    SetRXAGCSlope(rozy->curMercury, 1, (float)val);
}

bool rsdk::getRsdkANF() const
{
    return DspDock->getDspANF();
}

void rsdk::setRsdkANF(bool value)
{
    SetANF(rozy->curMercury, 0, value);
    SetANF(rozy->curMercury, 1, value);
}

bool rsdk::getRsdkBIN() const
{
    return DspDock->getDspBIN();
}

void rsdk::setRsdkBIN(bool value)
{
    SetBIN(rozy->curMercury, 0, value);
    SetBIN(rozy->curMercury, 1, value);
}

bool rsdk::getRsdkMUTE() const
{
    return DspDock->getDspMUTE();
}

void rsdk::setRsdkMUTE(bool value)
{
    if (VfoDock->subRxOn())
        SetMute(rozy->curMercury, 1, value);
    else
        SetMute(rozy->curMercury, 0, value);
}

bool rsdk::getRsdkBNR() const
{
    return DspDock->getDspBNR();
}

bool rsdk::getRsdkNR() const
{
    return DspDock->getDspNR();
}

void rsdk::setRsdkNR(bool value)
{
    if (VfoDock->subRxOn())
        SetNR(rozy->curMercury, 1, value);
    else
        SetNR(rozy->curMercury, 0, value);
}

void rsdk::setRsdkBNR(bool value)
{
    if (VfoDock->subRxOn())
        SetBNR(rozy->curMercury, 1, value);
    else
        SetBNR(rozy->curMercury, 0, value);
}

bool rsdk::getRsdkNB() const
{
    return DspDock->getDspNB1();
}

void rsdk::setRsdkNB(bool value)
{
    SetNB(rozy->curMercury, 0, value);
    SetNB(rozy->curMercury, 1, value);
}

void rsdk::setRsdkNBVals(double threshold)
{
    SetNBvals(rozy->curMercury, 0, threshold);
    SetNBvals(rozy->curMercury, 1, threshold);
}

double rsdk::getRsdkNBVals()
{
    // should be always the same
    return GetNBvals(rozy->curMercury, 0);
}

void rsdk::setRsdkNBHangtime(double hang)
{
    SetNBHangtime(rozy->curMercury, 0, hang);
    SetNBHangtime(rozy->curMercury, 1, hang);
}

double rsdk::getRsdkNBHangtime(int subrx)
{
    return GetNBHangtime(rozy->curMercury, subrx);
}

bool rsdk::getRsdkSDROM() const
{
    return DspDock->getDspNB2();
}

void rsdk::setRsdkSDROM(bool value)
{
    SetSDROM(rozy->curMercury, 0, value);
    SetSDROM(rozy->curMercury, 1, value);
}

void rsdk::setRsdkSDROMVals(double threshold)
{
    SetSDROMvals(rozy->curMercury, 0, threshold);
    SetSDROMvals(rozy->curMercury, 1, threshold);
}

double rsdk::getRsdkSDROMVals()
{
    // should be always the same
    return GetSDROMvals(rozy->curMercury, 0);
}

float rsdk::getRsdkBal() const
{
    return AudioDock->getBal();
}

void rsdk::setRsdkBal(float value)
{
    SetRXPan(rozy->curMercury, 0, value);
}

bool rsdk::getXmit()
{
    return (rozy->getTuneMode() || rozy->getXmit() || rozy->getGuiXmit());
}

void rsdk::setXmit(bool xmit)
{
    rozy->setGuiXmit(xmit);
    VfoDock->setXmit(xmit);
}

void rsdk::enableSubRx(bool enable)
{
    SetSubRXSt(rozy->curMercury, 1, enable);
}

/*
void rsdk::enableRx(int rx)
{
    SetSubRXSt(rozy->curMercury, rx, enable);
}
*/

void rsdk::setCurMerc(MercurySelectDockWidget::MERCMODE merc)
{
    // set DttSP diversity mode
    //if (merc == MercurySelectDockWidget::MERCBOTH)
    //SetDiversity(true);
    //else
    //SetDiversity(false);

    rozy->setCurMercury((int)merc);
    // KAA 07-09-2016
    // TODO FIXME, this one works best with the Page Mode
    //setRsdkFreq(VfoDock->getFreq());
    // as this one hasn't actually been set correctly yet if changing MODEs. Royal mess
    //setRsdkFreq(rozy->rxFreq[rozy->curMercury][rozy->curRx]);
}

void rsdk::setNormI(double value)
{
    // sanitize for normalized IQ range
    //value = value < -1.0 ? -1.0 : value > 1.0 ? 1.0 : value;
    rozy->setNcReal(value);
    //SetDiversityScalar(value, rozy->getNcImag());
}

void rsdk::setNormQ(double value)
{
    // sanitize for normalized IQ range
    //value = value < -1.0 ? -1.0 : value > 1.0 ? 1.0 : value;
    rozy->setNcImag(value);
    //SetDiversityScalar(rozy->getNcReal(), value);
}

void rsdk::setGain(double value)
{
    // sanitize
    //value = value < -1.0 ? -1.0 : value > 1.0 ? 1.0 : value;
    rozy->setNcGain(value);
    //SetDiversityGain(value);
}

void rsdk::setScopeSnap(int snapMode)
{
    // ugh, TODO FIXME, make DttSP play well with others
    if (snapMode < 0 /* SNAP_SEMI_RAW */ || snapMode > 4 /* SNAP_PREMOD */)
        return;
    m_scopeSnapMode = snapMode;
    if (VfoDock->subRxOn())
        SetScopeType(rozy->curMercury, 1, snapMode);
    else
        SetScopeType(rozy->curMercury, 0, snapMode);
}

int rsdk::getScopeSnap()
{
    return m_scopeSnapMode;
}

// set the DttSP snap mode for scope snaps
// these are hardcoded because of difficulty in exporting #defines from DttSP/spectrum.h
void rsdk::setScopeSnap(QString snapMode)
{
    if (snapMode.contains(QRegExp("[Pp][Rr][Ee].*[Ff][Ii][lL][Tt]"))) {
        setScopeSnap(1);
    } else if (snapMode.contains(QRegExp("[Pp][Oo][Ss][Tt].*[Ff][Ii][lL][Tt]"))) {
        setScopeSnap(2);
    } else if (snapMode.contains(QRegExp("[Pp][Oo][Ss][Tt].*[Aa][Gg][Cc]"))) {
        setScopeSnap(3);
    } else if (snapMode.contains(QRegExp("[Pp][Oo][Ss][Tt].*[Dd][Ee][Tt]"))) {
        setScopeSnap(4);
    } else /* if (snapMode.contains(QRegExp("^.*[Rr][Aa][Ww].*$"))) */ {
        setScopeSnap(0);
    }
}

void rsdk::setPhaseSnap(QString snapMode)
{
    (void)snapMode;
}

void rsdk::setPanadapterSnap(QString snapMode)
{
    (void)snapMode;
}

void rsdk::setSpectrumSnap(QString snapMode)
{
    (void)snapMode;
}

void rsdk::setTxSnap(QString snapMode)
{
    (void)snapMode;
}

void rsdk::setTitle()
{
    char buf[128];
    memset(buf, 0, 128);
    snprintf(buf, 64, "rsdk Mercury v%u", rozy->mercurySN);
    if (rozy->penelopeSN)
    {
        char buf2[32];
        snprintf(buf2, 32,  " Penny v%u", rozy->penelopeSN);
        strncat(buf, buf2, strlen(buf2));
    }
    if (rozy->ozySN)
    {
        char buf2[32];
        snprintf(buf2, 32,  " Ozy v%u", rozy->ozySN);
        strncat(buf, buf2, strlen(buf2));
    }
    QString title(buf);
    setWindowTitle(title);
}

void rsdk::setAMCarrierLevel(double level)
{
    rozy->setAmCarrierLevel(level);
}

double rsdk::getAMCarrierLevel()
{
    return rozy->getAmCarrierLevel();
}

void rsdk::setTXCompanderLevel(double level)
{
    rozy->setTxCompanderLevel(level);
}

void rsdk::setTXCompander(bool enable)
{
    rozy->setTxCompanderEnable(enable);
}

double rsdk::getTXCompanderLevel()
{
    return rozy->getTxCompanderEnable();
}

bool rsdk::getTXCompander()
{
    return rozy->getTxCompanderEnable();
}

void rsdk::setRXCompanderLevel(double level)
{
    rozy->setRxCompanderLevel(level);
}

void rsdk::setRXCompander(bool enable)
{
    rozy->setRxCompanderEnable(enable);
}

double rsdk::getRXCompanderLevel()
{
    return rozy->getRxCompanderEnable();
}

bool rsdk::getRXCompander()
{
    return rozy->getRxCompanderEnable();
}

void rsdk::setTXDCBlock(bool on)
{
    SetTXDCBlock(TXTHREAD, on);
}

void rsdk::setTXAGCFF(bool setit)
{
    SetTXAGCFF (TXTHREAD, setit);
}

void rsdk::setTXAGCFFCompression(double txcompression)
{
    SetTXAGCFFCompression (TXTHREAD, txcompression);
}

void rsdk::setTXFMDeviation(double deviation)
{
    SetTXFMDeviation(TXTHREAD, deviation);
}

void rsdk::setTXFilter(double low_frequency, double high_frequency)
{
    SetTXFilter(TXTHREAD, low_frequency, high_frequency);
}

void rsdk::setTXOsc(double newfreq)
{
    SetTXOsc(TXTHREAD, (float)newfreq);
}

void rsdk::setTXSquelchSt(bool on)
{
    SetTXSquelchSt(TXTHREAD, on);
}

void rsdk::setTXSquelchVal(double value)
{
    SetTXSquelchVal(TXTHREAD, value);
}

void rsdk::setTXSquelchAtt(double value)
{
    SetTXSquelchAtt(TXTHREAD, value);
}

void rsdk::setTXALCSt(bool on)
{
    rozy->setTxAlcEnable(on);
}

void rsdk::setTXALCAttack(double attack)
{
    rozy->setTxAlcAttack(attack);
}

void rsdk::setTXALCDecay(double decay)
{
    rozy->setTxAlcDecay(decay);
}

void rsdk::setTXALCBot(double max_agc)
{
    rozy->setTxAlcBot(max_agc);
}

void rsdk::setTXALCHang(double hang)
{
    rozy->setTxAlcHang(hang);
}

bool rsdk::getTXALCSt()
{
    return rozy->getTxAlcEnable();
}

double rsdk::getTXALCAttack()
{
    return rozy->getTxAlcAttack();
}

double rsdk::getTXALCDecay()
{
    return rozy->getTxAlcAttack();
}

double rsdk::getTXALCHang()
{
    return rozy->getTxAlcHang();
}

double rsdk::getTXALCBot()
{
    return rozy->getTxAlcBot();
}

void rsdk::setTXLevelerSt(bool setit)
{
    SetTXLevelerSt(TXTHREAD, setit);
}

void rsdk::setTXLevelerAttack(double attack)
{
    SetTXLevelerAttack(TXTHREAD, attack);
}

void rsdk::setTXLevelerDecay(double decay)
{
    SetTXLevelerDecay(TXTHREAD, decay);
}

void rsdk::setTXLevelerHang(double hang)
{
    SetTXLevelerHang(TXTHREAD, hang);
}

void rsdk::setTXLevelerTop(double maxgain)
{
    SetTXLevelerTop(TXTHREAD, maxgain);
}

bool rsdk::getTXLevelerSt()
{
    return GetTXLevelerSt(TXTHREAD);
}

double rsdk::getTXLevelerAttack()
{
    return GetTXLevelerAttack(TXTHREAD);
}

double rsdk::getTXLevelerDecay()
{
    return GetTXLevelerDecay(TXTHREAD);
}

double rsdk::getTXLevelerHang()
{
    return GetTXLevelerHang(TXTHREAD);
}

double rsdk::getTXLevelerTop()
{
    return GetTXLevelerTop(TXTHREAD);
}

void rsdk::setRXEQ10(int *dbs)
{
    // DttSP
    if (VfoDock->subRxOn())
        SetGrphRXEQ10(rozy->curMercury, 1, dbs);
    else
        SetGrphRXEQ10(rozy->curMercury, 0, dbs);
}

void rsdk::setRXEQ(bool enable)
{
    if (VfoDock->subRxOn())
        SetGrphRXEQcmd (rozy->curMercury, 1, enable);
    else
        SetGrphRXEQcmd (rozy->curMercury, 0, enable);
}

bool rsdk::getRXEQ()
{
    if (VfoDock->subRxOn())
        return GetGrphRXEQcmd(rozy->curMercury, 1);
    else
        return GetGrphRXEQcmd(rozy->curMercury, 0);
}

void rsdk::setTXEQ10(int *dbs)
{
    SetGrphTXEQ10(1, dbs);
}

void rsdk::setTXEQ(bool enable)
{
    SetGrphTXEQcmd(1, enable);
}

bool rsdk::getTXEQ()
{
    return GetGrphTXEQcmd(1);
}

void rsdk::setRXDCBlock(bool blockit)
{
    if (VfoDock->subRxOn())
        SetRXDCBlock(rozy->curMercury, 1, blockit);
    else
        SetRXDCBlock(rozy->curMercury, 0, blockit);
}

bool rsdk::getRXDCBlock()
{
    if (VfoDock->subRxOn())
        return GetRXDCBlock(rozy->curMercury, 1);
    else
        return GetRXDCBlock(rozy->curMercury, 0);
}

void rsdk::setRXDCBlockGain(double val)
{
    // race possible here for curMercury
    // should be locking these, maybe add a lock in RangeControl
    if (VfoDock->subRxOn())
        SetRXDCBlockGain(rozy->curMercury, 1, (float)val);
    else
        SetRXDCBlockGain(rozy->curMercury, 0, (float)val);
}

double rsdk::getRXDCBlockGain()
{
    if (VfoDock->subRxOn())
        return GetRXDCBlockGain(rozy->curMercury, 1);
    else
        return GetRXDCBlockGain(rozy->curMercury, 0);
}

bool rsdk::getTXDCBlock()
{
    return GetTXDCBlock(TXTHREAD);
}

void rsdk::setSquelch(bool enable)
{
    if (VfoDock->subRxOn())
        SetSquelchState(rozy->curMercury, 1, enable);
    else
        SetSquelchState(rozy->curMercury, 0, enable);
    emit squelchStateChanged(getSquelch(), true);
}

bool rsdk::getSquelch()
{
    if (VfoDock->subRxOn())
        return GetSquelchState(rozy->curMercury, 1);
    else
        return GetSquelchState(rozy->curMercury, 0);
}

void rsdk::setSquelchVal(double val)
{
    if (VfoDock->subRxOn())
        SetSquelchVal(rozy->curMercury, 1, (float)val);
    else
        SetSquelchVal(rozy->curMercury, 0, (float)val);
    emit squelchGainChanged(getSquelch());
}

double rsdk::getSquelchVal()
{
    if (VfoDock->subRxOn())
        return (float) GetSquelchVal(rozy->curMercury, 1);
    else
        return (float) GetSquelchVal(rozy->curMercury, 0);
}

// save the position and size of the rsdk main window
// ripped from https://stackoverflow.com/questions/74690/how-do-i-store-the-window-size-between-sessions-in-qt#76699
void rsdk::writePos()
{
    QSettings qsets("kippa", "rsdk");
    qsets.beginGroup("rsdkMainWindow");
    qsets.setValue("geometry", saveGeometry());
    qsets.setValue("savestate", saveState());
    qsets.setValue("maximized", isMaximized());
    if (!isMaximized()) {
        qsets.setValue("pos", pos());
        qsets.setValue("size", size());
    }
    qsets.endGroup();
}

void rsdk::readPos()
{
    QSettings qsets("kippa", "rsdk");
    qsets.beginGroup("rsdkMainWindow");
    restoreGeometry(qsets.value("geometry", saveGeometry()).toByteArray());
    restoreState(qsets.value("savestate", saveState()).toByteArray());
    move(qsets.value("pos", pos()).toPoint());
    resize(qsets.value("size", size()).toSize());
    if (qsets.value("maximized", isMaximized()).toBool())
        showMaximized();
    qsets.endGroup();
}

QString rsdk::getPropFileName() const
{
    return propFileName;
}

void rsdk::setPropFileName(const QString &value)
{
    propFileName = value;
}

bool rsdk::recordAudio() const
{
    return m_recordAudio;
}

void rsdk::setRecordAudio(bool recordAudio)
{
    m_recordAudio = recordAudio;
}

bool rsdk::recordIQFloat() const
{
    return m_recordIQFloat;
}

void rsdk::setRecordIQFloat(bool recordIQFloat)
{
    m_recordIQFloat = recordIQFloat;
}

bool rsdk::recordHPSDRRaw() const
{
    return m_recordHPSDRRaw;
}

void rsdk::setRecordHPSDRRaw(bool recordHPSDRRaw)
{
    m_recordHPSDRRaw = recordHPSDRRaw;
    rozy->setRecordHPSDRRaw(m_recordHPSDRRaw);
}

float rsdk::getRsdkVol() const
{
    return AudioDock->getVol();
}

// 0.0 - 1.0
void rsdk::setRsdkVol(float value)
{
    (void)value;
    //SetRXOutputGain(rozy->curMercury, 0, value);
}

float rsdk::getRsdkSubRxVol() const
{
    return AudioDock->getVol();
}

void rsdk::setRsdkSubRxVol(float value)
{
    SetRXOutputGain(rozy->curMercury, 1, value);
}

float rsdk::getRsdkSubRxBal() const
{
    return AudioDock->getBal();
}

void rsdk::setRsdkSubRxBal(float value)
{
    SetRXPan(rozy->curMercury, 1, value);
}

void rsdk::setRsdkSamplerate(int sampleRate_prm)
{
    // double locking here, FIXME TODO
    //pthread_mutex_lock(&rozy->sampRateLock);

    /*
    if (rozy && rozy->jackInEnabled && rozy->jackConn)
        pthread_mutex_lock(&rozy->jackConn->rb_lock);
        */


    // turn off jack audio during sample rate changes
    bool jin = AudioDock->getJackInEnable();
    bool jout = AudioDock->getJackOutEnable();

    if (jin)
        AudioDock->setJackInEnable(false);
    if (jout)
        AudioDock->setJackOutEnable(false);

    if (VfoDock->pageTune())
    {
        // recenter the current listening rx freq
        VfoDock->setPageTune(false);
        VfoDock->setPageTune(true);
    }

    // Dttsp Sample Rate needs to be set too
    // hpsdr knows 4 sample rates, 48000 96000 192000 384000
    if (sampleRate_prm == 96000)
    {
        rozy->setSampleRate(hpsdr::SampleRate96000);
    }
    else if(sampleRate_prm == 192000)
    {
        rozy->setSampleRate(hpsdr::SampleRate192000);
    }
    else if(sampleRate_prm == 384000)
    {
        rozy->setSampleRate(hpsdr::SampleRate384000);
    }
    else // if(sampleRate_prm == 48000) // default
    {
        rozy->setSampleRate(hpsdr::SampleRate48000);
    }

    // get done with these locks asap
    /*
    if (rozy && rozy->jackInEnabled && rozy->jackConn)
        pthread_mutex_unlock(&rozy->jackConn->rb_lock);
        */
    //pthread_mutex_unlock(&rozy->sampRateLock);

    // the following line causes page tune mode to jump to rxFreq, not desired behavior
    //setRsdkFreq(rozy->rxFreq[rozy->curMercury][rozy->curRx]);
    // we'll use the VfoDock freq instead
    setRsdkFreq(VfoDock->getFreq());

    //pthread_mutex_lock(&rozy->sampRateLock);
    // num of threads
    for (int i=0; i<OZY_MAX_THREADS; i++)
    {
        // num of receiver sub threads
        for (int j=0; j<OZY_MAX_SUBRX; j++)
        {
            SetMode(i, j, rozy->getMode());
            SetRXPan(i, j, AudioDock->getBal());
            //SetRXOutputGain(i, j, AudioDock->getVol());
            SetRXAGC(i, j, (AGCMODE)AgcDock->mode());
            SetANF(i, j, DspDock->getDspANF());
            SetBIN(i, j, DspDock->getDspBIN());
            SetMute(i, j, DspDock->getDspMUTE());
            SetNR(i, j, DspDock->getDspNR());
            SetBNR(i, j, DspDock->getDspBNR());
            SetNB(i, j, DspDock->getDspNB1());
            SetSDROM(i, j, DspDock->getDspNB2());
            SetSpectrumType(i, j, 1); /* SPEC_PRE_FILT */
            SetScopeType(i, j, m_scopeSnapMode);
            SetPhaseType(i, j, 0);
            SetPanadapterType(i, j, 1); /* SPEC_PRE_FILT */
            // disable all dc blockers during sample rate change
            // convenient to do this here
            SetRXDCBlock(i, 0, false);
            SetTXDCBlock(i, false);
        }
    }
    //pthread_mutex_unlock(&rozy->sampRateLock);

    // notify the dc blockers change
    emit rxDCBStateChanged();
    emit txDCBStateChanged();

    // re-enable jack in/outs
    if (jin)
        AudioDock->setJackInEnable(true);
    if (jout)
        AudioDock->setJackOutEnable(true);

    DisplayDock->setRebuildBands(true);

    setRsdkFilter(rozy->getDSPFiltLo(0), rozy->getDSPFiltHi(0));
    setRsdkFilter(rozy->getDSPFiltLo(1), rozy->getDSPFiltHi(1));

    // re-arm the cw freq generator
    double cwFreq = CwDock->freq();
    CwDock->setFreq(cwFreq);
}

// set the DSP audio filters
void rsdk::setRsdkFilter(double low, double hi)
{
    double l = low;
    double h = hi;

    SDRMODE_DTTSP mode = rozy->getMode();

    if (mode == CWL || mode == CWU) // cw now arrives with offsets stradling center freq of filter
    {
        SetRXFilter(rozy->curMercury,0,h,l);
        SetRXFilter(rozy->curMercury,1,h,l);
        SetTXFilter(TXTHREAD,  h,l);
    }
    if(mode == LSB || mode == DIGL)
    {
        if (hi > 0)
            h = -hi;

        if (low > 0)
            l = -low;

        SetRXFilter(rozy->curMercury,0,h,l);
        SetRXFilter(rozy->curMercury,1,h,l);
        SetTXFilter(TXTHREAD,  h,l);
    }
    else if(mode == AM || mode == SAM || mode == DSB || mode == SPEC || mode == FMN)
    {
        h = abs(hi);
        l = -h;

        SetRXFilter(rozy->curMercury,0,l,h);
        SetRXFilter(rozy->curMercury,1,l,h);
        SetTXFilter(TXTHREAD,  l,h);
    }
    else if(mode == USB || mode == DIGU)
    {
        h = abs(hi);
        l = abs(low);

        SetRXFilter(rozy->curMercury,0,l,h);
        SetRXFilter(rozy->curMercury,1,l,h);
        SetTXFilter(TXTHREAD,  l,h);
    }

    //FilterDock->setHi(h);
    //FilterDock->setLow(l);

    if (VfoDock->subRxOn()) {
        rozy->DSPFiltHi[1]=h;
        rozy->DSPFiltLo[1]=l;
    } else {
        rozy->DSPFiltHi[0]=h;
        rozy->DSPFiltLo[0]=l;
    }

}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

// where to put this?
QStringConvert::QStringConvert(QObject *parent) :
    QObject(parent)
{

}

// ripped from Qt docs, modified for even larger values
QString QStringConvert::convertToUnits(float nvalue)
{
    QString unit;
    float value;

    if(nvalue<0)
    {
        value=nvalue*-1;
    }
    else
    {
        value=nvalue;
    }

    if(value>=1000000000000&&value<1000000000000000)
    {
        value=value/1000000000000;
        unit="P";
    }
    if(value>=1000000000&&value<1000000000000)
    {
        value=value/1000000000;
        unit="G";
    }
    if(value>=1000000&&value<1000000000)
    {
        value=value/1000000;
        unit="M";
    }
    else if(value>=1000&&value<1000000)
    {
        value=value/1000;
        unit="K";
    }
    else if((value>=1&&value<1000))
    {
        value=value*1;
    }
    else if((value*1000)>=1&&value<1000)
    {
        value=value*1000;
        unit="m";
    }
    else if((value*1000000)>=1&&value<1000000)
    {
        value=value*1000000;
        unit=QChar(0x00B5);
    }
    else if((value*1000000000)>=1&&value<1000000000)
    {
        value=value*1000000000;
        unit="n";
    }
    else if((value*1000000000000)>=1&&value<1000000000000)
    {
        value=value*1000000000000;
        unit="p";
    }
    if(nvalue>0)
        return (QString::number(value)+unit);
    if(nvalue<0)
        return (QString::number(value*-1)+unit);
    return QString::number(0);
}

// convert ascii string floating point values into float
float QStringConvert::convertToValues(QString input)
{
    QString unit,value;
    float inValue;
    bool ok=true;

    int j=0;

    for(int i=0;i<=input.count();i++)
    {
        if((input[i]>='A'&&input[i]<='Z')||
           (input[i]>='a'&&input[i]<='z')||
           (input[i]==QChar(0x2126))||(input[i]==QChar(0x00B5)))
        {
            unit[j]=input[i];
            j++;
        }
    }
    for(int k=0;k<(input.count()-unit.count());k++)
    {
        value[k]=input[k];
    }

    inValue=value.toDouble(&ok);

    if(unit[0]=='p') // pico
    {
        return(inValue/1000000000000);
    }
    else if(unit[0]=='n') // nano
    {
        return(inValue/1000000000);
    }
    else if((unit[0]==QChar(0x00B5))||(unit[0]=='u'))
    {
        return(inValue/1000000); // micro
    }
    else if(unit[0]=='m')
    {
        return(inValue/1000);
    }
    else if(unit[0]=='K' || unit[0]=='k') // Kilo kilo
    {
        return(inValue*1000);
    }
    else if(unit[0]=='M') // Mega
    {
        return(inValue*1000000);
    }
    else if(unit[0]=='G') // Giga
    {
        return(inValue*1000000000);
    }
    else if(unit[0]=='P') // Peta
    {
        return(inValue*1000000000000);
    }
    else
    {
        return(inValue*1);
    }
}

void rsdk::on_actionAll_Windows_triggered(bool checked)
{
    foreach(rsdkDockWidget *w, dockList)
    {
        if (checked)
        {
            if(!w->isVisible())
                w->setVisible(true);
        }
        else
        {
            if(w->isVisible())
                w->setVisible(false);
        }
    }
}

void rsdk::on_actionTitle_Bars_triggered(bool checked)
{
    // remove the "DockWidget" part of the objectName
    QRegExp rx ( "DockWidget");

    foreach(QDockWidget *w, dockList)
    {
        QString title = w->objectName();
        title.remove(rx);

        if (checked)
        {
            w->setTitleBarWidget(0);
            //w->setWindowTitle(title);

        }
        else
        {
            // If widget is 0, any custom title bar widget previously set on the dock widget is removed,
            // but not deleted, and the default title bar will be used instead.
            w->setTitleBarWidget(&dummyTitleBarWidget);
            //w->setWindowTitle("");
        }
        //w->show();
    }
}

void rsdk::on_actionRecord_HPSDR_Raw_triggered(bool checked)
{
    setRecordHPSDRRaw(checked);
    //m_recordHPSDRRaw = checked;
}

/**
 * @brief rsdk::setDockMenuChecked
 * @param dock
 * @param setit
 * set the menuDock as checked/unchecked
 * This is a slot
 */
void rsdk::setDockMenuChecked(QWidget *dock, bool setit)
{
    if (!dockActMap)
        return;

    // the action is in the mapper, reference by the dock widget
    QAction *act = qobject_cast<QAction*>(dockActMap->mapping(dock));
    if (act) {
        if(setit) {
            act->setChecked(true);
        } else {
            act->setChecked(false);
        }
    }
}

/**
 * @brief rsdk::handleDockMenu
 * show or hide the dock widgets as requested
 * and mark the menu with a check if show
 * by the use via the Docks menu bar menu
 * This is a signal handler
 */
void rsdk::handleDockMenu(QWidget *dock)
{
    // the mapper sent us the signal
    QSignalMapper *map = qobject_cast<QSignalMapper*>(sender());
    // the action is in the mapper, reference by the dock widget
    QAction *act = qobject_cast<QAction*>(map->mapping(dock));
    if (dock->isVisible()) {
        dock->hide();
        if(act)
            act->setChecked(false);
    } else {
        dock->show();
        if(act)
            act->setChecked(true);
    }
}

/**
 * @brief rsdk::createActions
 * @param menu
 * Create actions for the dockList menu
 */

void rsdk::createDockActions()
{
    // remove "DockWidget" from the names
    const QRegExp rx ("DockWidget");

    // remove any previous maps
    if (dockActMap)
        // qobject derivatives SHOULD delete the previous actions, I believe
        delete dockActMap;

    dockActMap = new QSignalMapper(this);

    // make a copy of the dockList to sort on
    // can't sort the dockList itself as it's ordering is important
    QList <rsdkDockWidget*> sortList = dockList;
    // sort the sortList by name
    std::sort(sortList.begin(), sortList.end(), Less(*this));

    // loop thru all of sortList and make an action
    foreach(rsdkDockWidget *w, sortList)
    {
        // use the name of the widget as the menu entry item
        QString name = w->property("objectName").toString();
        QString labelVal = name.remove(rx);

        // make a new action
        // do we have to delete these later?
        // no, we don't have to, they have QObject parents
        // so QObject takes care of deletions
        QAction *act = new QAction(labelVal, this);

        // this action is a checkable action
        act->setCheckable(true);
        if (w->isVisible())
            act->setChecked(true);
        else
            act->setChecked(false);

        // make a shortcut
        //act->setShortcuts(QKeySequence::widgeticon);

        // make a tooltip
        QString tip = "Show/hide " + name;
        act->setStatusTip(tip);

        // connect to the signal mapper so we can pass the dock widget to the handler
        connect(act, SIGNAL(triggered()), dockActMap, SLOT(map()));

        // and map this dock
        dockActMap->setMapping(act, w);

        menuDocks->addAction(act);
    }

    // now connect the signal mapper to the handler
    // and we should be able to access the dock widget
    // in a single handler (so we don't have to write individual
    // handlers for each and every item in the menu)
    connect (dockActMap, SIGNAL(mapped(QWidget*)), this, SLOT(handleDockMenu(QWidget*)));
}

void rsdk::on_actionSaveProps_triggered()
{
    storeProps(true); // do a dialog for the save action
}

void rsdk::on_actionLoadProps_triggered()
{
    loadProps(true);
}

int rsdk::saveProps(QWidget *widget, QTextStream &outStream)
{
    const QMetaObject *metaobject = widget->metaObject();
    int count = metaobject->propertyCount();

    // object delimiter
    outStream << widget->objectName() << endl;
    outStream << count << endl;
    for (int i=0; i<count; ++i) {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        QVariant value = widget->property(name);
        outStream << name << "|" << value.toString() << endl;
    }
    return count;
}

int rsdk::readProps(QWidget *w, QTextStream &inStream)
{
    while(!inStream.atEnd())
    {
        QString line = inStream.readLine();
        if (line == w->objectName())
            break;
    }

    if (inStream.atEnd())
        return 0;

    // read the number of lines to follow for widget w
    QString numLines = inStream.readLine();
    int num = numLines.toInt();
    while(num--) {
        QString line = inStream.readLine();
        if (!line.isEmpty()) {
            QStringList slist = line.split("|");
            QString name = slist[0];
            QVariant value = slist[1];
            w->setProperty(name.toStdString().c_str(), value);
        }
    }

    return 0;
}
