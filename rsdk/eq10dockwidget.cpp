#include "EqSlider.h"
#include "eq10dockwidget.h"
#include "ui_eq10dockwidget.h"
#include "rsdkdockwidget.h"

#include "dttsp.h"
#include "rsdk.h"

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

/**
 * @brief EQ10DockWidget::EQ10DockWidget
 * @param parent
 * @param name
 */
EQ10DockWidget::EQ10DockWidget(QWidget *parent, const QString &name) :
    rsdkDockWidget(parent),
    m_name(name),
    ui(new Ui::EQ10DockWidget)
{
    sliders.clear();
    resetZero();

    ui->setupUi(this);
    ui->gridLayout->setSpacing(0);

    setName(m_name);
    setObjectName(m_name);
    setWindowTitle(m_name);

    QString xname = "Enable";
    enableTB = new QToolButton(ui->dockWidgetContents);
    enableTB->setObjectName(xname);
    enableTB->setText(xname);
    enableTB->setCheckable(true);
    ui->gridLayout->addWidget(enableTB, 1, 0, 1, 1);

    xname = "Reset";
    resetTB = new QToolButton(ui->dockWidgetContents);
    resetTB->setObjectName(xname);
    resetTB->setText(xname);
    resetTB->setCheckable(false);
    ui->gridLayout->addWidget(resetTB, 1, 1, 1, 1);

    xname = "gain";
    preAmpGain = new EqSlider(ui->dockWidgetContents, 0.0, 20.0, (double)m_vals[0], xname, xname);
    sliders.append(preAmpGain);
    ui->gridLayout->addWidget(preAmpGain, 0, 0, 1, 1);

    xname = "band1";
    QString title = "31.5";
    band1 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0, (double)m_vals[0], xname, title);
    sliders.append(band1);
    ui->gridLayout->addWidget(band1, 0, 1, 1, 1);

    xname = "band2";
    title = "63";
    band2 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0, (double)m_vals[0], xname, title);
    sliders.append(band2);
    ui->gridLayout->addWidget(band2, 0, 2, 1, 1);

    xname = "band3";
    title = "125";
    band3 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0, (double)m_vals[0], xname, title);
    sliders.append(band3);
    ui->gridLayout->addWidget(band3, 0, 3, 1, 1);

    xname = "band4";
    title = "250";
    band4 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0,(double)m_vals[0], xname, title);
    sliders.append(band4);
    ui->gridLayout->addWidget(band4, 0, 4, 1, 1);

    xname = "band5";
    title = "500";
    band5 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0,(double)m_vals[0], xname, title);
    sliders.append(band5);
    ui->gridLayout->addWidget(band5, 0, 5, 1, 1);

    xname = "band6";
    title = "1K";
    band6 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0,(double)m_vals[0], xname, title);
    sliders.append(band6);
    ui->gridLayout->addWidget(band6, 0, 6, 1, 1);

    xname = "band7";
    title = "2K";
    band7 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0,(double)m_vals[0], xname, title);
    sliders.append(band7);
    ui->gridLayout->addWidget(band7, 0, 7, 1, 1);

    xname = "band8";
    title = "4K";
    band8 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0,(double)m_vals[0], xname, title);
    sliders.append(band8);
    ui->gridLayout->addWidget(band8, 0, 8, 1, 1);

    xname = "band9";
    title = "8K";
    band9 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0,(double)m_vals[0], xname, title);
    sliders.append(band9);
    ui->gridLayout->addWidget(band9, 0, 9, 1, 1);

    xname = "band10";
    title = "16K";
    band10 = new EqSlider(ui->dockWidgetContents, -20.0, 20.0,(double)m_vals[0], xname, title);
    sliders.append(band10);
    ui->gridLayout->addWidget(band10, 0, 10, 1, 1);
}

EQ10DockWidget::~EQ10DockWidget()
{
    delete ui;
}

void EQ10DockWidget::start()
{
    connect(enableTB, SIGNAL(toggled(bool)), this, SLOT(setEnable(bool)));
    connect(resetTB, SIGNAL(released()), this, SLOT(resetZero()));
    connect(preAmpGain, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band1, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band2, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band3, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band4, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band5, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band6, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band7, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band8, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band8, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));
    connect(band10, SIGNAL(eqSliderChanged(double)), this, SLOT(setValue(double)));

    if (this->objectName().contains("TX"))
    {
        setEnable(theRsdk->getTXEQ());
    }
    else // if (myName.contains("RX"))
    {
        setEnable(theRsdk->getRXEQ());
    }
}

QString EQ10DockWidget::name() const
{
    return m_name;
}

void EQ10DockWidget::setName(const QString &name)
{
    m_name = name;
}

int *EQ10DockWidget::values()
{
    return m_vals;
}

int EQ10DockWidget::value(int ndx)
{
    int ret = -1;

    if ((ndx > 0) && (ndx < 11))
    {
        ret = m_vals[ndx];
    }
    return ret;
}

void EQ10DockWidget::setValues(int *vals)
{
    // are there 11 entries?
    for (int i=0; i<12; i++)
   {
        m_vals[i] = vals[i];
    }
}

void EQ10DockWidget::setValueByNdx(int ndx, int val)
{
    if ((ndx > 0) && (ndx < 11))
    {
        m_vals[ndx] = val;
    }
}

/**
 * @brief EQ10DockWidget::setValue
 * @param val
 * Okay this is cheating as we will use this a as a SLOT
 * and extract the "sender" name to find "band1", "band2", ...
 */
void EQ10DockWidget::setValue(double val)
{
    // change an objectName to an index into the int m_vals array
    // the band "name" is like "band1" or "band6", so strip "band" off,
    // then str.toInt() on the remainder to obtain index into m_vals array
    QRegExp rx("[a-zA-Z]|,");
    QString bandName = sender()->objectName();
    int ndx = 0;

    if (bandName.length())
    {
        bool ok;
        bandName.remove(rx);
        ndx = bandName.toInt(&ok);
        if (ok)
        {
            // got a legal index?
            if ((ndx > 0) && (ndx < 11))
            {
                m_vals[ndx] = val;
            }
        } else {
            // bandName is likely "gain" which has no numbers in it
            // set the m_vals[0] gain entry in the DttSP EQ10 values
            m_vals[0] = val;
        }
        QString myName = this->objectName();
        if (myName.contains("TX"))
        {
            theRsdk->setTXEQ10(m_vals);
        }
        else // if (myName.contains("RX"))
        {
            // default is to set the rx eq I guess
            theRsdk->setRXEQ10(m_vals);
        }
    }
}

bool EQ10DockWidget::enable() const
{
    return m_enable;
}

void EQ10DockWidget::setEnable(bool enable)
{
    m_enable = enable;

    bool pstate = enableTB->blockSignals(true);
    enableTB->setChecked(m_enable);

    QString myName = this->objectName();
    if (myName.contains("TX"))
    {
        theRsdk->setTXEQ(m_enable);
    }
    else // if (myName.contains("RX"))
    {
        theRsdk->setRXEQ(m_enable);
    }
    enableTB->blockSignals(pstate);
}

void EQ10DockWidget::resetZero()
{
    for (int i=0; i<11; i++)
        m_vals[i] = 0;

    foreach(EqSlider *slider, sliders)
    {
        slider->setValue(0.0);
    }
}
