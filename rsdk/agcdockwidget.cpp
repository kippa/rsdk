
#include "agcdockwidget.h"
#include "rangecontrol.h"
#include "ui_agcdockwidget.h"

#include "common.h" // DttSP
#include "rsdk.h"

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

AgcDockWidget::AgcDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::AgcDockWidget),
    m_mode(agcLONG),
    m_txDCB(false),
    m_rxDCB(false)
{
    qRegisterMetaType<AGCMODE>();
    ui->setupUi(this);
    connect (ui->agcBtnGrp, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(agcBtnGrpKeyPress(QAbstractButton*)));
}

AgcDockWidget::~AgcDockWidget()
{
    delete ui;
}

AgcDockWidget::AGCMODE AgcDockWidget::mode() const
{
    return m_mode;
}

void AgcDockWidget::setMode(const AGCMODE &mode)
{
    m_mode = mode;
    if(m_mode == agcOFF)
        ui->agcOffPB->setChecked(true);
    else if(m_mode == agcSLOW)
        ui->agcSlowPB->setChecked(true);
    else if(m_mode == agcMED)
        ui->agcMedPB->setChecked(true);
    else if(m_mode == agcFAST)
        ui->agcFastPB->setChecked(true);
    else if(m_mode == agcLONG)
        ui->agcLongPB->setChecked(true);

    emit agcChanged(m_mode);
}

void AgcDockWidget::start()
{
    connect (ui->agcRXDCBPB, SIGNAL(toggled(bool)), SLOT(setRxDCB(bool)));
    connect (ui->agcTXDCBPB, SIGNAL(toggled(bool)), SLOT(setTxDCB(bool)));

    setMode(agcLONG);
    setTxDCB(false);
    setRxDCB(false);

    emit agcChanged(m_mode);
    emit dockChanged(this, Qt::NoDockWidgetArea);
}

void AgcDockWidget::agcBtnGrpKeyPress(QAbstractButton *btn)
{
    QString btnName=btn->objectName();

    if (btnName.contains("agcOffPB")) {
        m_mode = agcOFF;
    } else if (btnName.contains("agcSlowPB")) {
        m_mode = agcSLOW;
    } else if (btnName.contains("agcMedPB")) {
        m_mode = agcMED;
    } else if (btnName.contains("agcFastPB")) {
        m_mode = agcFAST;
    } else if (btnName.contains("agcLongPB")) {
        m_mode = agcLONG;
    }

    emit agcChanged(m_mode);
}

bool AgcDockWidget::rxDCB() const
{
    return m_rxDCB;
}

void AgcDockWidget::setRxDCB(bool rxDCB)
{
    m_rxDCB = rxDCB;

    // get the current rx dcb mode and set accordingly
    bool pstate = ui->agcRXDCBPB->blockSignals(true);
    ui->agcRXDCBPB->setChecked(m_rxDCB);
    ui->agcRXDCBPB->blockSignals(pstate);
    emit rxDcbChanged(m_rxDCB);
}

// check with, and set, the DttSP state of DCB
// no emit here! it'll feedback
void AgcDockWidget::setRxDCBState()
{
    // get the current rx dcb mode and set accordingly
    bool dcb = theRsdk->getRXDCBlock();
    if (dcb != m_rxDCB)
    {
        m_rxDCB = dcb;
        bool pstate = ui->agcRXDCBPB->blockSignals(true);
        ui->agcRXDCBPB->setChecked(m_rxDCB);
        ui->agcRXDCBPB->blockSignals(pstate);
    }
}

bool AgcDockWidget::txDCB() const
{
    return m_txDCB;
}

void AgcDockWidget::setTxDCB(bool txDCB)
{
    m_txDCB = txDCB;

    // get the current tx dcb mode and set accordingly
    //bool dcb = r->getTXDCBlock();
    bool pstate = ui->agcTXDCBPB->blockSignals(true);
    ui->agcTXDCBPB->setChecked(m_txDCB);
    ui->agcTXDCBPB->blockSignals(pstate);
    emit txDcbChanged(m_txDCB);
}

// check with, and set, the DttSP state of DCB
// no emit here! it'll feedback
void AgcDockWidget::setTxDCBState()
{
    // get the current rx dcb mode and set accordingly
    bool dcb = theRsdk->getTXDCBlock();
    if (dcb != m_txDCB)
    {
        m_txDCB = dcb;
        bool pstate = ui->agcTXDCBPB->blockSignals(true);
        ui->agcTXDCBPB->setChecked(m_txDCB);
        ui->agcTXDCBPB->blockSignals(pstate);
    }
}
