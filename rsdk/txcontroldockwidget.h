#ifndef TXCONTROLDOCKWIDGET_H
#define TXCONTROLDOCKWIDGET_H

#include <QWidget>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QDockWidget>
#else
#include <QtGui/QDockWidget>
#endif

#include "rangecontrol.h"
#include "rsdkdockwidget.h"

namespace Ui {
class TXControlDockWidget;
}

class TXControlDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(double m_AMCarrierLevel READ AMCarrierLevel WRITE setAMCarrierLevel)
    Q_PROPERTY(double m_CompanderLevel READ CompanderLevel WRITE setCompanderLevel)
    Q_PROPERTY(bool m_CompanderEnabled READ getCompanderEnabled WRITE setCompanderEnabled)

    // the tx leveler settings
    Q_PROPERTY(bool m_txLevelerState READ getTxLevelerState WRITE setTxLevelerState)
    Q_PROPERTY(double m_txLevelerAttack READ getTxLevelerAttack WRITE setTxLevelerAttack)
    Q_PROPERTY(double m_txLevelerHang READ getTxLevelerHang WRITE setTxLevelerHang)
    Q_PROPERTY(double m_txLevelerDecay READ getTxLevelerDecay WRITE setTxLevelerDecay)
    Q_PROPERTY(double m_txLevelerTop READ getTxLevelerTop WRITE setTxLevelerTop)

    // the tx leveler settings
    Q_PROPERTY(bool m_txALCState READ getTxALCState WRITE setTxALCState)
    Q_PROPERTY(double m_txALCAttack READ getTxALCAttack WRITE setTxALCAttack)
    Q_PROPERTY(double m_txALCHang READ getTxALCHang WRITE setTxALCHang)
    Q_PROPERTY(double m_txALCDecay READ getTxALCDecay WRITE setTxALCDecay)
    Q_PROPERTY(double m_txALCBot READ getTxALCBot WRITE setTxALCBot)

public:
    explicit TXControlDockWidget(QWidget *parent = 0);
    ~TXControlDockWidget();

    void start();
    double AMCarrierLevel() const;
    double CompanderLevel() const;
    bool getCompanderEnabled() const;

    bool getTxLevelerState() const;
    void setTxLevelerState(bool txLevelerState);

    double getTxLevelerHang() const;
    void setTxLevelerHang(double txLevelerHang);

    double getTxLevelerAttack() const;
    void setTxLevelerAttack(double txLevelerAttack);

    double getTxLevelerDecay() const;
    void setTxLevelerDecay(double txLevelerDecay);

    double getTxLevelerTop() const;
    void setTxLevelerTop(double txLevelerTop);

    bool getTxALCState() const;
    void setTxALCState(bool txALCState);

    double getTxALCAttack() const;
    void setTxALCAttack(double txALCAttack);

    double getTxALCHang() const;
    void setTxALCHang(double txALCHang);

    double getTxALCDecay() const;
    void setTxALCDecay(double txALCDecay);

    double getTxALCBot() const;
    void setTxALCBot(double txALCBot);

signals:
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);
    void AMCarrierLevelChanged(double level);
    void CompanderLevelChanged(double level);
    void CompanderEnable(bool enabled);

private slots:
    void on_amCarrierLevel_valueChanged(double value);
    void setAMCarrierLevel(double value);
    void on_amValue_textEdited(const QString &arg1);

    void setCompanderLevel(double value);

    void on_compandTB_toggled(bool checked);
    void setCompanderEnabled(bool enable);

    void on_agcTB_toggled(bool checked);
    void on_levelerTB_toggled(bool checked);
    void on_levelerHangSB_valueChanged(double arg1);
    void on_levelerAttackSB_valueChanged(double arg1);
    void on_levelerDecaySB_valueChanged(double arg1);
    void on_levelerTopSB_valueChanged(double arg1);

    void on_alcTB_toggled(bool checked);
    void on_alcHangSB_valueChanged(double arg1);
    void on_alcAttackSB_valueChanged(double arg1);
    void on_alcDecaySB_valueChanged(double arg1);
    void on_alcBottomSB_valueChanged(double arg1);
    void on_companderSB_valueChanged(double arg1);

private:
    // qstring to double with regex char removal
    double convertValue(const QString &arg);

    double m_AMCarrierLevel;
    double m_CompanderLevel;
    bool m_CompanderEnabled;
    RangeControl *agcCompressionRange;
    RangeControl *agcLimitRange;

    bool m_txLevelerState;
    double m_txLevelerAttack;
    double m_txLevelerHang;
    double m_txLevelerDecay;
    double m_txLevelerTop;

    bool m_txALCState;
    double m_txALCAttack;
    double m_txALCHang;
    double m_txALCDecay;
    double m_txALCBot;

    Ui::TXControlDockWidget *ui;
};

#endif // TXCONTROLDOCKWIDGET_H
