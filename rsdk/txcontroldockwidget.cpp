#include <QGroupBox>

#include "rangecontrol.h"
#include "txcontroldockwidget.h"
#include "ui_txcontroldockwidget.h"

#include "dttsp.h"
#include "rsdk.h"

TXControlDockWidget::TXControlDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    m_AMCarrierLevel(50.0),
    m_CompanderLevel(-0.01),
    m_CompanderEnabled(true),
    m_txLevelerState(true),
    m_txLevelerAttack(2.0),
    m_txLevelerHang(500.0),
    m_txLevelerDecay(500.0),
    m_txLevelerTop(1.6),
    m_txALCState(true),
    m_txALCAttack(2.0),
    m_txALCHang(500.0),
    m_txALCDecay(10.0),
    m_txALCBot(0.0),
    ui(new Ui::TXControlDockWidget)
{
    ui->setupUi(this);
    ui->gridLayout->setSpacing(0);
}

TXControlDockWidget::~TXControlDockWidget()
{
    delete ui;
}

void TXControlDockWidget::start()
{
    //m_AMCarrierLevel = theRsdk->getAMCarrierLevel();
    setAMCarrierLevel(m_AMCarrierLevel);

    //m_CompanderEnabled = theRsdk->getTXCompander();
    setCompanderEnabled(m_CompanderEnabled);

    //m_CompanderLevel = theRsdk->getTXCompanderLevel();
    setCompanderLevel(m_CompanderLevel);

    bool pstate = ui->levelerTB->blockSignals(true);
    ui->levelerTB->setChecked(m_txLevelerState);
    ui->levelerTB->blockSignals(pstate);

    pstate = ui->levelerAttackSB->blockSignals(true);
    ui->levelerAttackSB->setValue(m_txLevelerAttack);
    ui->levelerAttackSB->blockSignals(pstate);

    pstate = ui->levelerHangSB->blockSignals(true);
    ui->levelerHangSB->setValue(m_txLevelerHang);
    ui->levelerHangSB->blockSignals(pstate);

    pstate = ui->levelerDecaySB->blockSignals(true);
    ui->levelerDecaySB->setValue(m_txLevelerDecay);
    ui->levelerDecaySB->blockSignals(pstate);

    pstate = ui->levelerTopSB->blockSignals(true);
    ui->levelerTopSB->setValue(m_txLevelerTop);
    ui->levelerTopSB->blockSignals(pstate);

    pstate = ui->alcTB->blockSignals(true);
    ui->alcTB->setChecked(m_txALCState);
    ui->alcTB->blockSignals(pstate);

    pstate = ui->alcAttackSB->blockSignals(true);
    ui->alcAttackSB->setValue(m_txALCAttack);
    ui->alcAttackSB->blockSignals(pstate);

    pstate = ui->alcHangSB->blockSignals(true);
    ui->alcHangSB->setValue(m_txALCHang);
    ui->alcHangSB->blockSignals(pstate);

    pstate = ui->alcDecaySB->blockSignals(true);
    ui->alcDecaySB->setValue(m_txALCDecay);
    ui->alcDecaySB->blockSignals(pstate);

    pstate = ui->alcBottomSB->blockSignals(true);
    ui->alcBottomSB->setValue(m_txALCBot);
    ui->alcBottomSB->blockSignals(pstate);
}

double TXControlDockWidget::convertValue(const QString &arg)
{
    double ret = 0.0;
    QString value = arg;
    if (value.size())
    {
        // remove any alphas from string to make a double
        QRegExp rx ( "[a-zA-Z]|,");
        value.remove(rx);

        bool ok = false;
        double cval = QLocale(QLocale::English).toDouble(value, &ok);
        if (ok)
        {
            ret  = cval;
        }
    }
    return ret;
}

double TXControlDockWidget::getTxALCBot() const
{
    return m_txALCBot;
}

void TXControlDockWidget::setTxALCBot(double txALCBot)
{
    m_txALCBot = txALCBot;
    theRsdk->setTXALCBot (m_txALCBot);
}

double TXControlDockWidget::getTxALCDecay() const
{
    return m_txALCDecay;
}

void TXControlDockWidget::setTxALCDecay(double txALCDecay)
{
    m_txALCDecay = txALCDecay;
    theRsdk->setTXALCDecay (m_txALCDecay);
}

double TXControlDockWidget::getTxALCHang() const
{
    return m_txALCHang;
}

void TXControlDockWidget::setTxALCHang(double txALCHang)
{
    m_txALCHang = txALCHang;
    theRsdk->setTXALCHang (m_txALCHang);
}

double TXControlDockWidget::getTxALCAttack() const
{
    return m_txALCAttack;
}

void TXControlDockWidget::setTxALCAttack(double txALCAttack)
{
    m_txALCAttack = txALCAttack;
    theRsdk->setTXALCAttack (m_txALCAttack);
}

bool TXControlDockWidget::getTxALCState() const
{
    return m_txALCState;
}

void TXControlDockWidget::setTxALCState(bool txALCState)
{
    m_txALCState = txALCState;
    theRsdk->setTXALCSt (m_txALCState);
}

double TXControlDockWidget::getTxLevelerTop() const
{
    return m_txLevelerTop;
}

void TXControlDockWidget::setTxLevelerTop(double txLevelerTop)
{
    m_txLevelerTop = txLevelerTop;
    theRsdk->setTXLevelerTop(m_txLevelerTop);
}

double TXControlDockWidget::getTxLevelerDecay() const
{
    return m_txLevelerDecay;
}

void TXControlDockWidget::setTxLevelerDecay(double txLevelerDecay)
{
    m_txLevelerDecay = txLevelerDecay;
    theRsdk->setTXLevelerDecay(m_txLevelerDecay);
}

double TXControlDockWidget::getTxLevelerAttack() const
{
    return m_txLevelerAttack;
}

void TXControlDockWidget::setTxLevelerAttack(double txLevelerAttack)
{
    m_txLevelerAttack = txLevelerAttack;
    theRsdk->setTXLevelerAttack(m_txLevelerAttack);
}

double TXControlDockWidget::getTxLevelerHang() const
{
    return m_txLevelerHang;
}

void TXControlDockWidget::setTxLevelerHang(double txLevelerHang)
{
    m_txLevelerHang = txLevelerHang;
    theRsdk->setTXLevelerHang(m_txLevelerHang);
}

bool TXControlDockWidget::getTxLevelerState() const
{
    return m_txLevelerState;
}

void TXControlDockWidget::setTxLevelerState(bool txLevelerState)
{
    m_txLevelerState = txLevelerState;
    theRsdk->setTXLevelerSt(m_txLevelerState);
}

void TXControlDockWidget::on_amCarrierLevel_valueChanged(double value)
{
    // range limit and scale
    if (value < 0) value = 0;
    if (value > 100) value = 100;

    value = value / 100.0;

    setAMCarrierLevel(value);
}

double TXControlDockWidget::AMCarrierLevel() const
{
    return m_AMCarrierLevel;
}

double TXControlDockWidget::CompanderLevel() const
{
    return m_CompanderLevel;
}

bool TXControlDockWidget::getCompanderEnabled() const
{
    return m_CompanderEnabled;
}

void TXControlDockWidget::setAMCarrierLevel(double value)
{
    if (value < 0) value = 0;
    if (value > 1) value = 1;

    m_AMCarrierLevel = value;
    theRsdk->setAMCarrierLevel(m_AMCarrierLevel);
    //emit AMCarrierLevelChanged(m_AMCarrierLevel);
}

void TXControlDockWidget::on_amValue_textEdited(const QString &arg1)
{
    double dval = convertValue(arg1);
    setAMCarrierLevel(dval);
}

void TXControlDockWidget::setCompanderLevel(double value)
{
    if (value < -120.0)
        value = -120.0;
    if (value > 120.0)
        value = 120.0;

    m_CompanderLevel = value;

    theRsdk->setTXCompanderLevel(m_CompanderLevel);
    //emit CompanderLevelChanged((double)m_CompanderLevel);
}

void TXControlDockWidget::on_compandTB_toggled(bool checked)
{
    setCompanderEnabled(checked);
}

void TXControlDockWidget::setCompanderEnabled(bool enable)
{
    m_CompanderEnabled = enable;
    bool pstate = ui->compandTB->blockSignals(true);
    ui->compandTB->setChecked(m_CompanderEnabled);
    ui->compandTB->blockSignals(pstate);

    theRsdk->setTXCompander(m_CompanderEnabled);
    //emit CompanderEnable(m_CompanderEnabled);
}

void TXControlDockWidget::on_agcTB_toggled(bool checked)
{
    (void)checked;
   // set tx agc
}

void TXControlDockWidget::on_levelerTB_toggled(bool checked)
{
   // set leveler on/off
    setTxLevelerState(checked);
}

void TXControlDockWidget::on_levelerHangSB_valueChanged(double arg1)
{
    // set leveler hang value
    setTxLevelerHang (arg1);
}

void TXControlDockWidget::on_levelerAttackSB_valueChanged(double arg1)
{
    // set leveler attack value
    setTxLevelerAttack (arg1);
}

void TXControlDockWidget::on_levelerDecaySB_valueChanged(double arg1)
{
    // set leveler decay value
    setTxLevelerDecay (arg1);
}

void TXControlDockWidget::on_levelerTopSB_valueChanged(double arg1)
{
    // set leveler top value
    setTxLevelerTop (arg1);
}

void TXControlDockWidget::on_alcTB_toggled(bool checked)
{
   // set alc on/off
   setTxALCState(checked);
}

void TXControlDockWidget::on_alcHangSB_valueChanged(double arg1)
{
    // set alc hang value
    setTxALCHang(arg1);
}

void TXControlDockWidget::on_alcAttackSB_valueChanged(double arg1)
{
    // set alc attack value
    setTxALCAttack(arg1);
}

void TXControlDockWidget::on_alcDecaySB_valueChanged(double arg1)
{
    // set alc decay value
    setTxALCDecay(arg1);
}

void TXControlDockWidget::on_alcBottomSB_valueChanged(double arg1)
{
    // set alc top value
    setTxALCBot(arg1);
}

void TXControlDockWidget::on_companderSB_valueChanged(double arg1)
{
    setCompanderLevel(arg1);
}
