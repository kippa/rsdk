#ifndef RADIOOZY_H
#define RADIOOZY_H

#include </usr/include/libusb-1.0/libusb.h>
//#include <libusb-1.0/libusb.h>

#include "radiousb.h"
#include "hpsdr.h"
#include "dttsp.h"

/******************************************************************************/
/* REMOTES, i.e. we act as a server and respond to commands as if we were a
 * hardware rig
 */
// remote control from flxmlrpc clients, we are a server on default port
#ifndef FLXR_H
#include "flxr.h"
#endif
class flxr;

// remote control from hamlib emulating icom ci-v 756proIII serial protocol via pty
#ifndef ICOM_SERIAL_H
#include "icom_serial.h"
#endif
class icom_serial;
/******************************************************************************/

extern const char *filtersByMode[][10];

// DttSP "snap" functions work w/ this many samples
#define DTTSP_SNAP_SAMPLES 4096 // see DEFSPEC in DttSP/defs.h, change both at the same time!!!

// libusb USB setup
#define MAX_EPO_PACKET_SIZE (64)
#define USB_TIMEOUT_MSECS (1000)

/* OZY VID PID */
#define OZY_VID (0xFFFE)
#define OZY_PID (0x0007)

/* Vendor Request Types */
#define VENDOR_REQ_TYPE_IN (0xc0)
#define VENDOR_REQ_TYPE_OUT (0x40)

/* Vendor In Commands */
#define VENDOR_REQ_I2C_READ (0x81)    // wValueL: i2c address
#define VENDOR_REQ_SPI_READ (0x82)    // wValue: optional header bytes
                                                        // wIndexH:	enables
#define VRQ_SDR1K_CTL 0x0d
#define SDR1KCTRL_READ_VERSION 0x7
#define VRT_VENDOR_IN 0xC0
                                                        // len: how much to read
#define SPI_FMT_MSB ((0)
#define SPI_FMT_HDR_0 ((0)
#define SPI_FMT_HDR_1 ((1)
#define SPI_FMT_HDR_2 ((2)
#define SPI_EN_FPGA (0x80)

#define VENDOR_REQ_RS232_READ (0x83)
#define VENDOR_REQ_EEPROM_TYPE_READ (0x84)
#define VENDOR_REQ_I2C_SPEED_READ (0x85)

        /* Vendor Out Commands */
#define VENDOR_REQ_SET_LED (0x01)     // wValueL {0,255}
#define VENDOR_REQ_FPGA_LOAD (0x02)
#define FL_BEGIN (0)	                // wIndexL: begin fpga programming cycle.  stalls if trouble.
#define FL_XFER (1)	                // wIndexL: xfer up to 64 bytes of data
#define FL_END (2)	                // wIndexL: end programming cycle, check for success.
                                                        // stalls endpoint if trouble.

#define VENDOR_REQ_I2C_WRITE 0x08	// wValueL: i2c address

#define VENDOR_REQ_SPI_WRITE 0x09	// wValue: optional header bytes
                                                        // wIndexH:	enables
                                                        // wIndexL:	format
                                                        // len: how much to write

#define VENDOR_REQ_I2C_SPEED_SET 0x0B // wValueL 100kHz/400kHz {0,1}

#define VENDOR_REQ_CPU_SPEED_SET 0x0C // wValueL 100kHz/400kHz {0,1,3}

#define NUM_MERCURY_CARDS 2

class RadioOzy : public RadioUsb
{
public:
    RadioOzy(const char * const name_prm, ASYNCCB cb, void *cbdata);
    virtual ~RadioOzy();
    static RadioOzy *instance();

    /*!
     * \brief Read a usbBuf_t from the hpsdr ozy USB interface
     * \param buf_prm
     * \param wait_prm, 0=don't wait for a response (async)
     *                  1=wait for a response (sync)
     * \return 0=success, -1=error
     */
    int Read(usbBuf_t *buf_prm, bool wait_prm=true);

    /*!
     * \brief Write a usbBuf_t to the hpsdr ozy USB interface
     * \param buf_prm
     * \param len_prm, the length of the usbBuf_t (512)
     * \param wait_prm, 0=don't wait for a response (async)
     *                  1=wait for a response (sync)
     * \return 0=success, -1=error
     */
    int Write(usbBuf_t *buf_prm, int64 len_prm, bool wait_prm=true);

    // the main rx/tx io threads
    pthread_t ep6_ep2_io_thread_id;
    pthread_mutex_t ep6_ep2_thread_lock;
    int ep6_ep2_thread_request_kill;
    int ep6_ep2_thread_request_suspend; // thanks Linus, no pthread_suspend_np()

    // the 4k band scope io threads
    pthread_t ep4_io_thread_id;
    pthread_mutex_t ep4_thread_lock;
    int ep4_thread_request_kill;
    int ep4_thread_request_suspend; // another Linus Torvalds issue, why is there no pthread_suspend()?

    pthread_t i2c_scan_thread_id;
    pthread_mutex_t i2c_scan_thread_lock;
    int i2c_scan_thread_request_kill;
    int i2c_scan_thread_request_suspend;

    // here we emulate an Icom rig remote control
    // the spec is:
#define ICOM_SERIAL_REMOTE
#ifdef ICOM_SERIAL_REMOTE
    pthread_t icom_serial_remote_thread_id;
    pthread_mutex_t icom_serial_remote_thread_lock;
    int icom_serial_remote_thread_request_kill;
    int icom_serial_remote_thread_request_suspend;
#endif // ICOM_SERIAL_REMOTE

    int startThreads(void);
    int killThreads(void);
    int suspendThreads(void);
    int goThreads(void);

    int initOzyUSBDevice();

    flxr *xmlsrv;
    icom_serial *icsr;
    //HpSdr hpsdr[OZY_MAX_SUBRX];
    // public due to static c linkage to libusb access via user data this pointer
    //
    // ozy command and control protocol
    int curMercury; // the current mercury card
    int curMercuryMode; // this holds M1, M1, M1-M2 modes
    long long rxFreq[OZY_MAX_MERCURY][OZY_MAX_SUBRX];
    long long txFreq[OZY_MAX_MERCURY]; // there is really only one xmit frequency
    //long long lastRxFreq[OZY_MAX_SUBRX];
    //long long lastTxFreq[OZY_MAX_SUBRX];
    int setFreq(const unsigned long long f); // set both rx and tx at curRx
    int setFreq(const unsigned long long f, const unsigned int &r); // set both rx and tx at curRx
    unsigned long long getRxFreq(const unsigned int &r); // return the current rx frequency
    unsigned long long getTxFreq(const unsigned int &r); // return the current tx frequency
    // allow for setting tx freq separately in page tune mode Rx freq stays centerd and TX must vary to match tune offset
    int setTxFreq(const unsigned long long f);

    // samples
    int numSamples, sampleCount, sampleCountTotal;
    int retSampleCount, retSamplePosition;
    int sampleRate; // samples per second
    pthread_mutex_t sampRateLock; // protect sample rate changes
    int getSampleRate() const;
    int setSampleRate(hpsdr::HpsdrSampleRateEnum sampeRate_prm);
    int setSampleRate(int sampleRate_prm);

    // scaling variable for complex conjugate multiplication (attempt at noise cancellation)
    // these are maintained as -1.0 - 0 - +1.0 ranges
    double ncReal;
    double ncImag;
    double ncGain;

    // 10 MHz reference source
    hpsdr::HpsdrRef10MEnum ref10M;
    int setRef10M(hpsdr::HpsdrRef10MEnum ref10M_prm);

    // 122.88 MHz reference source
    hpsdr::HpsdrRef122MEnum ref122M; // 00=Penelope, 01=Mercury;
    int setRef122M(hpsdr::HpsdrRef122MEnum ref122M_prm);

    // hpsdr config
    unsigned char config;
    int setConfig(unsigned char config_prm);

    // mic source
    hpsdr::HpsdrMicSrcEnum micSrc;
    int setMicSrc(hpsdr::HpsdrMicSrcEnum MicSrc_prm);

    // class e mode
    hpsdr::HpsdrClassEEnum modeClassE;
    int setClassE(hpsdr::HpsdrClassEEnum classE_prm);

    // alex attenuator control bits
    hpsdr::HpsdrAttEnum alexAtt;
    int setAtt(hpsdr::HpsdrAttEnum att_prm);

    // alex tx ant
    hpsdr::HpsdrTxAntEnum txAnt;
    int setTxAnt(hpsdr::HpsdrTxAntEnum tx_ant_prm);

    // alex rx ant
    hpsdr::HpsdrRxAntEnum rxAnt;
    int setRxAnt(hpsdr::HpsdrRxAntEnum rx_ant_prm);

    // alex rx out
    hpsdr::HpsdrRxOutEnum rxOut;
    int setRxOut(hpsdr::HpsdrRxOutEnum rx_out_prm);

    // hpsdr usb duplex
    hpsdr::HpsdrDuplexEnum duplex;
    int setDuplex(hpsdr::HpsdrDuplexEnum duplex_prm);

    hpsdr::HpsdrPreAmpEnum preampOn; // 0=off, 1=on
    int setPreAmp(hpsdr::HpsdrPreAmpEnum preAmp_prm);

    hpsdr::HpsdrDitherEnum lt2208Dither; // 0=off, 1=on
    int setDither(hpsdr::HpsdrDitherEnum Dither_prm);

    hpsdr::HpsdrRandomEnum lt2208Random; // 0=off, 1=on
    int setRandom(hpsdr::HpsdrRandomEnum Random_prm);

    hpsdr::HpsdrMicGainEnum micGain; // 0=0dB, 1=+20dB
    int setMicGain(hpsdr::HpsdrMicGainEnum micGain_prm);

    hpsdr::HpsdrMicLineEnum micLine;
    int setMicLine(hpsdr::HpsdrMicLineEnum micLine_prm);

    unsigned char getDrive() const;
    void setDrive(unsigned char value);

    int getNumModes() const;
    // convert a mode string to the SDRMODE_DTTSP type (an int)
    SDRMODE_DTTSP getModeType(string mode);
    string getModeStr(SDRMODE_DTTSP mode) const;
    SDRMODE_DTTSP getMode() const;
    void setMode(const SDRMODE_DTTSP &value, int subrx);
    void setTXMode(const SDRMODE_DTTSP &value); // always TX_THREAD

    bool getGuiXmit() const;
    void setGuiXmit(bool value);

    bool getPacketXmit() const;
    void setPacketXmit(bool value);

    // get both, either above xmit values in one call
    bool getXmit() const;

    int ptt, dash, dot;
    int lt2208Overflow[OZY_MAX_MERCURY];
    int mercurySoftVer[OZY_MAX_MERCURY];
    int hermesI01, hermesI02, hermesI03;

    unsigned char mercurySN; // mercury or hermes serial number 0-255
    unsigned char penelopeSN; // penelope serial number 0-25
    unsigned char ozySN;

    uint16_t penelopeFwdPower, penelopeRevPower;
    uint16_t alexFwdPower, alexRevPower;
    uint16_t ain1, ain2, ain3, ain4, ain5, ain6; // pennylane or hermes analog input 1, 2, 3, 4, 5, 6

    unsigned char numRx = 1; // 000=1, ... 111=8
    unsigned char numSubRx = 2;
    unsigned char timestampMicData; // 0=off 1=on 1PPS on LSB of MIC data
    unsigned char drive; // ignored by penelope, hermes/pennylane drive level
    unsigned char lineIn; // 0=mic in, 1=line in metis/penelope pennylane
    unsigned char apolloFilter; // 0=off, 1=on hermes apollo filter
    unsigned char apolloTuner; // 0=off, 1=on hermes apollo tuner
    unsigned char apolloAutoTune; // 0=end, 1=stOZY_MAX_SUBRXuart hermes apollo auto tune
    unsigned char apolloFilterBoard; // 0=Alex, 1=Apollo hermes select filter board
    unsigned char alexManualFilterSelect; // 0=disable, 1=enable
    unsigned char alexHPFbits; // alex HPF bitfield
    unsigned char alexLPFbits; // alex LPF bitfileld
    unsigned char pennylane; // pennylane attached?
    float gain;
    float rfgain;
    float txLeftGain; // 0 - 1.0 gain adjust of feed to DttSP xmit I/Q generate, left channel
    float txRightGain; // 0 - 1.0 gain adjust of feed to DttSP xmit I/Q generate, right channel
    unsigned int output_sample_stride;
    bool tuneMode;
    double tuneFreq;
    bool guiXmit;
    bool packetXmit;
    double vswr;
    SDRMODE_DTTSP mode;
    bool xmit20dBAtt;

    // CW keyer stuff
    bool keyerSpacing; // from CwDock->spacing(), on or off
    bool keyerReversed; // reverse paddles
    uint16_t keyerFreq; // 0 - 4096 12 bits in USB packet
    unsigned char keyerMode; // from CwDock->mode(), 0 - 3 0 = straight key, 1 = Mode A, 2 = Mode B
    unsigned char keyerSpeed; // from CwDock->speed(), 1- 60 WPM
    unsigned char keyerWeight; // from CwDock->weight(), 0 - 100 percent
    unsigned char keyerStvolume; // side tone volume 0 - 127
    unsigned char keyerPttdelay; // 0 - 255 milliseconds
    unsigned short keyerHangtime; // 0 - 1024 milliseconds
    bool keyerEnabled; // internal CW keyer enabled or not

    // return float -1 ... 0 ... 1 buffers
    // from HPSDR
    float *I_in[OZY_MAX_SUBRX], *Q_in[OZY_MAX_SUBRX];
    float *Mic_L_in, *Mic_R_in;
    // to HPSDR
    float *I_out, *Q_out;
    float *Aud_L_out[NUM_MERCURY_CARDS], *Aud_R_out[NUM_MERCURY_CARDS];

    bool running; // is usb running? set in the ep6_ep2_io_thread;

    bool recordHPSDRRaw; // record the HPSDR USB protocal as received to a file

    /*
    char recFileName[MAX_PATH_LEN];
    int recFd;
    */

    int txC0ndx; // rotating count from zero to sizeof(tx_C0_cmd_list) entries
    int numCmdListEntries;

    /*!
     * \brief txBuf is used to keep track of the current tx buffer
     */
    usbBuf_t *txBuf;
    double amCarrierLevel;

    bool txAlcEnable;
    double txAlcAttack;
    double txAlcDecay;
    double txAlcBot;
    double txAlcHang;

    bool txCompanderEnable;
    double txCompanderLevel;

    bool rxCompanderEnable;
    double rxCompanderLevel;

    // copy inbound ep4 8k byte packets into this buffer
    // it's not critical so no locking
    int16_t bsBuf[BS_BUF_SIZE]; // this is 4k words, 8k bytes, 16 bit time sample values, continuous stream, non-critical


    bool getKeyerSpacing() const;
    void setKeyerSpacing(bool value);

    bool getKeyerReversed() const;
    void setKeyerReversed(bool value);

    uint16_t getKeyerFreq() const;
    void setKeyerFreq(const double &value);

    unsigned char getKeyerMode() const;
    void setKeyerMode(unsigned char value);

    unsigned char getKeyerSpeed() const;
    void setKeyerSpeed(unsigned char value);

    unsigned char getKeyerWeight() const;
    void setKeyerWeight(unsigned char value);

    unsigned char getKeyerStvolume() const;
    void setKeyerStvolume(unsigned char value);

    unsigned char getKeyerPttdelay() const;
    void setKeyerPttdelay(unsigned char value);

    unsigned char getKeyerHangtime() const;
    void setKeyerHangtime(unsigned short value);

    bool getKeyerEnabled() const;
    void setKeyerEnabled(bool value);

    float getTxLeftGain() const;
    void setTxLeftGain(float value);

    float getTxRightGain() const;
    void setTxRightGain(float value);

    int getCurMercury() const;
    void setCurMercury(int value);

    double getNcReal() const;
    void setNcReal(double value);

    double getNcImag() const;
    void setNcImag(double value);

    double getNcGain() const;
    void setNcGain(double value);

    bool getRestart_ep6_ep2() const;
    void setRestart_ep6_ep2(bool value);

    bool getRestart_ep4() const;
    void setRestart_ep4(bool value);

    // size of buffers in samples, L+R so 2X the single channel
    quint64 getxferSampleSize() const;
    quint64 getxferBufSize() const;
    void setxferSize(const quint64 &value);

    bool getAinEnable() const;
    void setAinEnable(bool value);

    bool getAoutEnable() const;
    void setAoutEnable(bool value);

    bool getRecordHPSDRRaw() const;
    void setRecordHPSDRRaw(bool value);

    double getAmCarrierLevel();
    void setAmCarrierLevel(double value);

    bool getTuneMode() const;
    void setTuneMode(bool value);
    double getTuneFreq() const;
    void setTuneFreq(double f);

    bool getTxCompanderEnable() const;
    void setTxCompanderEnable(bool value);

    double getTxCompanderLevel() const;
    void setTxCompanderLevel(double value);

    bool getRxCompanderEnable() const;
    void setRxCompanderEnable(bool value);

    double getRxCompanderLevel() const;
    void setRxCompanderLevel(double value);

    bool getTxAlcEnable();
    void setTxAlcEnable(bool value);

    double getTxAlcAttack();
    void setTxAlcAttack(double value);

    double getTxAlcDecay();
    void setTxAlcDecay(double value);

    double getTxAlcBot();
    void setTxAlcBot(double value);

    double getTxAlcHang();
    void setTxAlcHang(double value);

    bool getTxLevelerEnable();
    void setTxLevelerEnable(bool value);

    double getTxLevelerAttack() const;
    void setTxLevelerAttack(double value);

    double getTxLevelerDecay() const;
    void setTxLevelerDecay(double value);

    double getTxLevelerTop() const;
    void setTxLevelerTop(double value);

    double getTxLevelerHang() const;
    void setTxLevelerHang(double value);

    string curBW;
    string getCurBW() const;
    void setBW(const string &val, int rx); // like "2.4K" or "2400", then subrx
    void setBW(const double &low, const double &hi, int rx);

    double DSPFiltLo[OZY_MAX_SUBRX]; // set and maintain the low frequency of the audio DSP filter
    double DSPFiltHi[OZY_MAX_SUBRX]; // ditto the high one, together forming the bandpass for the audio data DSP filter

    double getDSPFiltLo(int subrx) const;
    void setDSPFiltLo(double value, int subrx);

    double getDSPFiltHi(int subrx) const;
    void setDSPFiltHi(double value, int subrx);

    // may be used outside of radioozy
    int readOzyI2C(unsigned int addr, unsigned char *buf, unsigned int len);
    int writeOzyI2C(unsigned int addr, unsigned char *buf, unsigned int len);
    int setLED(int led, int on);

    bool getXmit20dBAtt() const;
    void setXmit20dBAtt(bool value);

public slots:
    void setup_audio();
protected:

private:
    //static void *libusbThreadEntry(void *this_prm);
    int getOzyFWString(void);
    usbBuf_t *usbCmd(int cmd); // alloc a rx buf for a single command
    int usbDoCtlPkt(int req_type, int cmd, int addr, unsigned char *buf, int len); // handle ctl packets via async
    // default to an opaque internal c struct to hold the data
    // no distro of Ozy firwmare, fpga code
    int loadFW(char *fileName);
    int loadFPGA(char *fileName);
    int writeRAM(int start, unsigned char *buf, int count);
    int resetCpu(int reset);
    int primeOzy();

    // xmit
    int Mox;

    unsigned char ozyFWStr[16];

    // no copy, no assign
    RadioOzy(const RadioOzy &NoCopy);
    RadioOzy &operator=(const RadioOzy &NoAssign);
};

extern int hpsdrProcessBuffer(void *cbdata_prm, unsigned char *buf_prm, int int_prm);

#endif // RADIOOZY_H
