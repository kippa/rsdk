#include <asm/byteorder.h>

#include <QPointF>
#include <QMouseEvent>
#include <QWheelEvent>

#include <qwt_plot_canvas.h>

#include "rsdk.h"

#include "data-worker.h"
#include "oscope.h"
#include "ui_oscope.h"

// TODO FIXME
extern rsdk *r;

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

class OScopeGrid: public QwtPlotGrid
{
public:

    OScopeGrid() :
        m_color("grey50")
    {
        enableY(false);
        enableYMin(false);
        enableX(false);
        enableXMin(false);
        setZ(-200);
    }

//    void draw(QPainter *p, const QwtScaleMap &xMap, const QwtScaleMap &yMap, const QRectF &rect) const
//    {
//        //double x1 = rect.left();
//        //double x2 = rect.right()-1.0;
//        //const double w = x2 - x1;
//        const double y1 = rect.top();
//        //const double y2 = rect.bottom()-1.0;
//        //const double hzPerStep = MAX_BANDSCOPE_FREQ / (OSCOPE_NUM_SAMPS+1);
//        //const double hzPerStep = MAX_BANDSCOPE_FREQ / w;
//
//        p->save();
//
//        QColor labelColor("white");
//
//        if (xEnabled())
//        {
//            QwtScaleDiv xScale = xScaleDiv();
//            const QList<double>values = xScale.ticks(QwtScaleDiv::MajorTick);
//            for (int i=0; i<values.count(); i++)
//            {
//                double value = xMap.transform(values[i]);
//                QString label = QLocale(QLocale::English).toString(values[i]/1e6, 'f', 0);
//                const QFont f("Arial", 8, QFont::Light);
//                p->setFont(f);
//                QRect r = p->boundingRect(value, y1, 0, 0, Qt::AlignHCenter, label);
//                p->setPen(labelColor.darker(256 - (m_color.value()/2)));
//                p->drawText(r, Qt::AlignCenter, label);
//                //p->setPen(m_color);
//                //p->drawLine(value, r.height()+2, value, y2);
//                //p->drawRoundedRect(r, 1.0, 1.0, Qt::AbsoluteSize);
//            }
//        }
//
//        if (yEnabled())
//        {
//
//        }
//
//        QwtPlotGrid::draw(p, xMap, yMap, rect);
//
//        p->restore();
//    }

    QColor getColor() { return m_color; }
    void setColor(const QColor &color) {
        m_color =color;
        setMajorPen(m_color, (qreal)1.00, Qt::SolidLine);
        setMinorPen(m_color.darker(), (qreal)1.00, Qt::DotLine);
    }

private:
    QColor m_color;
};

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

OScope::OScope(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OScope),
    m_traceColor(QColor("darkcyan")),
    m_gridColor(QColor("navajowhite").darker()),
    m_pkColor(QColor("darkcyan").darker()),
    m_oScopeEnabled(false),
    m_pkEnabled(false),
    m_yMin(0.0),
    m_yMax(0.0),
    avgInit(false),
    m_activeDragXPos(0),
    m_smooth(17),
    m_yAutoScale(false),
    m_showScale(false)
{

    // setup all the buffers
    makeBuffers(OSCOPE_NUM_SAMPS);
    resizeBuffers(OSCOPE_NUM_SAMPS); // maybe we want to change the display rates for more, or fewer, samples

    ui->setupUi(this);

    // setup the oScope display QwtPlot
    ui->OScopePlot->setAxisAutoScale(QwtPlot::xTop, false);
    ui->OScopePlot->setAxisAutoScale(QwtPlot::xBottom, false);
    ui->OScopePlot->setAxisAutoScale(QwtPlot::yLeft, false);
    ui->OScopePlot->setAxisAutoScale(QwtPlot::yRight, false);

    ui->OScopePlot->enableAxis(QwtPlot::xTop, false);
    ui->OScopePlot->enableAxis(QwtPlot::xBottom, false);
    ui->OScopePlot->enableAxis(QwtPlot::yLeft, false);
    ui->OScopePlot->enableAxis(QwtPlot::yRight, m_showScale);
    ui->OScopePlot->setAxisFont(QwtPlot::yRight, QFont("Helvetica", 4));

    ui->OScopePlot->setAxisScale(QwtPlot::xTop, 0, (OSCOPE_NUM_SAMPS), 0);
    ui->OScopePlot->setAxisScale(QwtPlot::yRight, m_yMin, m_yMax, 0);

    grid = new OScopeGrid;
    grid->enableX(true);
    grid->enableXMin(false);
    grid->enableY(true);
    grid->enableYMin(false);
    grid->setXAxis(QwtPlot::xTop);
    grid->setYAxis(QwtPlot::yRight);
    grid->setColor(m_gridColor);
    //grid->setMajorPen(m_gridColor, 1.0, Qt::SolidLine);
    //grid->setMinorPen(m_gridColor.darker(), 1.0, Qt::DotLine);
    grid->attach(ui->OScopePlot);
    grid->setZ(-200.0);

    // settings for the drawing canvas
    QwtPlotCanvas *cnvs = (QwtPlotCanvas *)ui->OScopePlot->canvas();
    cnvs->setPaintAttribute(QwtPlotCanvas::BackingStore, false);
    cnvs->setPaintAttribute(QwtPlotCanvas::Opaque, false);
    cnvs->setPaintAttribute(QwtPlotCanvas::ImmediatePaint, true);
    if(cnvs->testPaintAttribute(QwtPlotCanvas::BackingStore))
    {
        cnvs->setAttribute(Qt::WA_PaintOnScreen, true);
        cnvs->setAttribute(Qt::WA_NoSystemBackground, true);
    }

    oScopeCurve.setStyle(QwtPlotCurve::Lines);
    oScopeCurve.setCurveAttribute(QwtPlotCurve::Fitted, false);
    oScopeCurve.setPen(m_traceColor, 1.0, Qt::SolidLine);
    oScopeCurve.setItemAttribute(QwtPlotItem::Margins, false);
    //oScopeCurve.setRenderHint(QwtPlotItem::RenderAntialiased);
    oScopeCurve.setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    oScopeCurve.setPaintAttribute(QwtPlotCurve::FilterPoints, false);
    oScopeCurve.setPaintAttribute(QwtPlotCurve::MinimizeMemory, false);
    oScopeCurve.setPaintAttribute(QwtPlotCurve::ImageBuffer, false);
    oScopeCurve.setOrientation(Qt::Horizontal);
    oScopeCurve.setRawSamples(xes, yes, (OSCOPE_NUM_SAMPS));
    oScopeCurve.setItemAttribute(QwtPlotItem::AutoScale, false);
    oScopeCurve.attach(ui->OScopePlot);
    oScopeCurve.setZ(30.0);
    oScopeCurve.setAxes(QwtPlot::xTop, QwtPlot::yRight);

    pkHiCurve.setStyle(QwtPlotCurve::Steps);
    pkHiCurve.setCurveAttribute(QwtPlotCurve::Fitted, false);
    pkHiCurve.setPen(m_pkColor, 1.0, Qt::SolidLine);
    pkHiCurve.setItemAttribute(QwtPlotItem::Margins, false);
    //pkHiCurve.setRenderHint(QwtPlotItem::RenderAntialiased);
    pkHiCurve.setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    pkHiCurve.setPaintAttribute(QwtPlotCurve::FilterPoints, false);
    pkHiCurve.setPaintAttribute(QwtPlotCurve::MinimizeMemory, false);
    pkHiCurve.setPaintAttribute(QwtPlotCurve::ImageBuffer, false);
    pkHiCurve.setOrientation(Qt::Horizontal);
    pkHiCurve.setRawSamples(xes, pkHi, (OSCOPE_NUM_SAMPS));
    pkHiCurve.setItemAttribute(QwtPlotItem::AutoScale, false);
    pkHiCurve.attach(ui->OScopePlot);
    pkHiCurve.setZ(30.0);
    pkHiCurve.setAxes(QwtPlot::xTop, QwtPlot::yRight);

    pkLoCurve.setStyle(QwtPlotCurve::Steps);
    pkLoCurve.setCurveAttribute(QwtPlotCurve::Fitted, false);
    pkLoCurve.setPen(m_pkColor, 1.0, Qt::SolidLine);
    pkLoCurve.setItemAttribute(QwtPlotItem::Margins, false);
    //pkLoCurve.setRenderHint(QwtPlotItem::RenderAntialiased);
    pkLoCurve.setPaintAttribute(QwtPlotCurve::ClipPolygons, false);
    pkLoCurve.setPaintAttribute(QwtPlotCurve::FilterPoints, false);
    pkLoCurve.setPaintAttribute(QwtPlotCurve::MinimizeMemory, false);
    pkLoCurve.setPaintAttribute(QwtPlotCurve::ImageBuffer, false);
    pkLoCurve.setOrientation(Qt::Horizontal);
    pkLoCurve.setRawSamples(xes, pkLo, (OSCOPE_NUM_SAMPS));
    pkLoCurve.setItemAttribute(QwtPlotItem::AutoScale, false);
    pkLoCurve.attach(ui->OScopePlot);
    pkLoCurve.setZ(30.0);
    pkLoCurve.setAxes(QwtPlot::xTop, QwtPlot::yRight);

    // setup the worker thread for periodic collection of DttSP scope data
    // let this run in its own thread so as not to impede other operations
    snapper = new Worker3(Process_Scope, 0, samps, OSCOPE_NUM_SAMPS);
    connect(snapper, SIGNAL(sampleDone()), this, SLOT(plotOScope()));
    //snapper->moveToThread(&collectThread);
    //collectThread.setObjectName(QString("rsdk oscope"));
    //collectThread.start();

    connect (ui->yAutoScaleTB, SIGNAL(toggled(bool)), this, SLOT(setYAutoScale(bool)));
    connect (ui->yShowScaleOnTB, SIGNAL(toggled(bool)), this, SLOT(setShowScale(bool)));
    connect (ui->scopePeakOnTB, SIGNAL(toggled(bool)), this, SLOT(setPeakEnabled(bool)));
    //connect (peakCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(setPkColor(QColor)));
    //connect (gridCSelector, SIGNAL(colorChanged(QColor)), this, SLOT(setGridColor(QColor)));

    connect (ui->snapButtonGroup, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(snapBtnGrpKeyPress(QAbstractButton*)));

    // use a timer instead of snapper
    //sampTimer.stop();
    //sampTimer.setInterval(0); // asap
    //sampTimer.setTimerType(Qt::PreciseTimer);
    //connect(&sampTimer, SIGNAL(timeout()), this, SLOT(plotOScope()));
    // start happens with QEvent::Show, stops with QEvent::Hide
    //sampTimer.start();
}

OScope::~OScope()
{
    collectThread.requestInterruption();
    collectThread.quit();
    while (collectThread.isRunning())
        collectThread.wait();

    snapper->stopTimer();
    delete snapper;

    deleteBuffers();

    delete ui;
}

void OScope::plotOScope()
{
    if (!r)
        return;

    if (r->PlotDrawOptionsDock->getScopeOn())
    {
        for (int i=0, j=0; i<(int)(OSCOPE_NUM_SAMPS); i++, j+=2)
        {
            xes[i] = (double)i;
            yes[i] = (double)samps[j]; // each I & Q are identical from DttSP snap_scope()
            if (yes[i] < m_yMin) {
                m_yMin = yes[i];
                m_yMax = -yes[i];
            }
            if (yes[i] > m_yMax) {
                m_yMax = yes[i];
                m_yMin = -yes[i];
            }
            if (yes[i] > pkHi[i]) pkHi[i] = yes[i];
            if (yes[i] < pkLo[i]) pkLo[i] = yes[i];
        }

        oScopeCurve.setStyle(QwtPlotCurve::Lines);
        oScopeCurve.setPen(QPen(m_traceColor));
        if (m_yAutoScale || m_setYNeeded)
        {
            m_setYNeeded = false;
            ui->OScopePlot->setAxisScale(QwtPlot::yRight, m_yMin, m_yMax, 0);
        }

        if (m_pkEnabled)
        {
            pkHiCurve.setStyle(QwtPlotCurve::Dots);
            pkHiCurve.setPen(QPen(m_pkColor));
            pkLoCurve.setStyle(QwtPlotCurve::Dots);
            pkLoCurve.setPen(QPen(m_pkColor));
        } else {
            pkHiCurve.setStyle(QwtPlotCurve::NoCurve);
            pkLoCurve.setStyle(QwtPlotCurve::NoCurve);
        }
    }
    else
    {
        oScopeCurve.setStyle(QwtPlotCurve::NoCurve);
    }

    // replot
    ui->OScopePlot->replot();
}

double OScope::smooth() const
{
    return m_smooth;
}

void OScope::setSmooth(int smooth)
{
    setSmooth((double)smooth);
}

// DttSP snap modes for scope
void OScope::snapBtnGrpKeyPress(QAbstractButton *b)
{
    m_snapMode = b->text();

    // TODO bounds checking here, or let DttSP handle it
    emit snapChanged(m_snapMode);
}

void OScope::makeBuffers(const int numSamps)
{
    pkHi = new double[numSamps];
    pkLo = new double[numSamps];
    avg = new double[numSamps];
    xes = new double[numSamps];
    yes = new double[numSamps];

    samps = new float[numSamps * 2]; // space for I & Q
}

void OScope::deleteBuffers()
{
    delete pkHi;
    delete pkLo;
    delete avg;
    delete xes;
    delete yes;

    delete samps;
}

void OScope::resizeBuffers(const int numSamps)
{
    clearBuffers();

    oScopeCurve.setRawSamples(xes, yes, numSamps);
    pkLoCurve.setRawSamples(xes, pkLo, numSamps);
    pkHiCurve.setRawSamples(xes, pkHi, numSamps);
}

void OScope::clearBuffers()
{
    // clear all buffers
    const int totSamps = OSCOPE_NUM_SAMPS * sizeof(double);

    memset(xes, 0, totSamps);
    memset(yes, 0, totSamps);
    memset(avg, 0, totSamps);
    memset(pkLo, 0, totSamps);
    memset(pkHi, 0, totSamps);
}

QString OScope::getSnapMode() const
{
    return m_snapMode;
}

void OScope::setSnapMode(const QString &value)
{
    m_snapMode = value;
}

void OScope::setSmooth(double smooth)
{
    (void)smooth;
    ui->OScopePlot->setAxisScale(QwtPlot::xTop, 0, OSCOPE_NUM_SAMPS, 0);
    oScopeCurve.setRawSamples(xes, yes, OSCOPE_NUM_SAMPS);
}

void OScope::clearY()
{
    memset(pkLo, 0, (OSCOPE_NUM_SAMPS)*sizeof(double));
    memset(pkHi, 0, (OSCOPE_NUM_SAMPS)*sizeof(double));
    m_yMax = 1e-10;
    m_yMin = -1e-10;
    m_setYNeeded = true;
}

QColor OScope::traceColor() const
{
    return m_traceColor;
}

void OScope::setTraceColor(const QColor &traceColor)
{
    m_traceColor = traceColor;
    //emit oScopeColorChanged(m_traceColor);
    if (r)
        r->PlotDrawOptionsDock->scopeColor(m_traceColor, false);
}

bool OScope::oScopeEnabled() const
{
   return m_oScopeEnabled;
}

void OScope::setOScopeEnabled(bool oScopeEnabled)
{
    m_oScopeEnabled = oScopeEnabled;

    clearY();
    ui->OScopePlot->setAxisScale(QwtPlot::yRight, m_yMin, m_yMax, 0);

    if (m_oScopeEnabled)
        ui->OScopePlot->setVisible(true);
    else
        ui->OScopePlot->setVisible(false);
}

bool OScope::peakEnabled() const
{
    return m_pkEnabled;
}

void OScope::setPeakEnabled(bool checked)
{
    m_pkEnabled = checked;
    clearY();
}

QColor OScope::pkColor() const
{
    return m_pkColor;
}

void OScope::setPkColor(const QColor &pkColor)
{
    m_pkColor = pkColor;
    //emit oScopePkColorChanged(m_pkColor);
    if (r)
        r->PlotDrawOptionsDock->scopePkColor(m_pkColor, false);
}

QColor OScope::gridColor() const
{
    return m_gridColor;
}

void OScope::setGridColor(const QColor &gridColor)
{
   m_gridColor = gridColor;
   grid->setColor(m_gridColor);

   //emit oScopeGridColorChanged(m_gridColor);
   if (r)
        r->PlotDrawOptionsDock->scopeGridColor(m_gridColor, false);
}

bool OScope::showScale() const
{
    return m_showScale;
}

void OScope::setShowScale(bool showScale)
{
    m_showScale = showScale;
    ui->OScopePlot->enableAxis(QwtPlot::yRight, m_showScale);
}

bool OScope::yAutoScale() const
{
    return m_yAutoScale;
}

void OScope::setYAutoScale(bool yAutoScale)
{
    m_yAutoScale = yAutoScale;
    if (!m_yAutoScale)
    {
        //clearY();
        ui->OScopePlot->setAxisScale(QwtPlot::yRight, m_yMin, m_yMax, 0);
    }
}

bool OScope::event(QEvent *event)
{
    if(event->type() == QEvent::Wheel)
    {
        //int delta = dynamic_cast<QWheelEvent*>(event)->delta();
        //int numDegrees = delta/8;
        //int numSteps = numDegrees/15;
    }
    else if (event->type() == QEvent::KeyRelease)
    {
        QString key = dynamic_cast<QKeyEvent*>(event)->text();
    }
    else if (event->type() == QEvent::MouseButtonRelease)
    {
        //QPointF pos = dynamic_cast<QMouseEvent*>(event)->posF();
        m_activeDragXPos = 0.0;
    }
    else if (event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *e = dynamic_cast<QMouseEvent*>(event);
        if (e->buttons() == Qt::LeftButton)
        {
            //QPoint pos = e->pos();
        }
        else if (e->buttons() == Qt::RightButton)
        {
            //r->VfoDock->nextTuneInc();
        }
    }
    else if (event->type() == QEvent::MouseMove)
    {
        //QPoint pos = dynamic_cast<QMouseEvent*>(event)->pos();
    }
    else if (event->type() == QEvent::MouseButtonDblClick)
    {
        // double click to this frequency
        //QPoint pos = dynamic_cast<QMouseEvent*>(event)->pos();
    }
    else if (event->type() == QEvent::Resize)
    {
        clearY();
        ui->OScopePlot->setAxisScale(QwtPlot::yRight, m_yMin, m_yMax, 0);
    }
    else if (event->type() == QEvent::Hide)
    {
        snapper->stopTimer();
        sampTimer.stop();
    }
    else if (event->type() == QEvent::Show)
    {
        snapper->startTimer();
        sampTimer.start();
    }

    return QWidget::event(event);
}
