#include "rsdk.h"

#include "txdockwidget.h"
#include "ui_txdockwidget.h"

// TODO FIXME
extern rsdk *r;

TxDockWidget::TxDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::TxDockWidget),
    m_txDriveLevel(0), // 0 - 255, start at 0% for safety, don't xmit accidentally!!
    m_txTuneDriveLevel(0), // 1/4 power level for tuning
    prevDriveLevel(0),
    prevSdrMode(USB)
{
    ui->setupUi(this);
}

TxDockWidget::~TxDockWidget()
{
    delete ui;
}

void TxDockWidget::start()
{
    setTxDriveLevel(m_txDriveLevel);
    if (r)
        setTuneMode(r->rozy->tuneMode);
    connect (ui->txPB_2, SIGNAL(toggled(bool)), this, SLOT(setTuneMode(bool)));
}

unsigned int TxDockWidget::txDriveLevel() const
{
    return m_txDriveLevel;
}

void TxDockWidget::setTxDriveLevel(unsigned int txDriveLevel)
{
    m_txDriveLevel = txDriveLevel;
    bool pstate = ui->txDriveLevel->blockSignals(true);
    ui->txDriveLevel->setValue(txDriveLevel);

    QString ss = "QSlider#txDriveLevel::add-page:horizontal { background: black } ";

    // left hand of slider changes by value
    if (m_txDriveLevel <= 63) {
        ss += "QSlider#txDriveLevel::sub-page:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 black, stop: 1 green) }";
        ss += "QSlider::handle:horizontal:hover { background: green; }";
    } else if (m_txDriveLevel > 63 && m_txDriveLevel <= 127) {
        ss += "QSlider#txDriveLevel::sub-page:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 black, stop: 0.50 green, stop: 1 yellow) }";
        ss += "QSlider::handle:horizontal:hover { background: yellow; }";
    } else if (m_txDriveLevel > 127 && m_txDriveLevel <= 195) {
        ss += "QSlider#txDriveLevel::sub-page:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 black, stop: 0.33 green, stop: 0.66 yellow, stop: 1 orange) }";
        ss += "QSlider::handle:horizontal:hover { background: orange; }";
    } else if (m_txDriveLevel > 195) {
        ss += "QSlider#txDriveLevel::sub-page:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 black, stop: 0.25 green, stop: 0.50 yellow, stop: 0.75 orange, stop: 1 red) }";
        ss += "QSlider::handle:horizontal:hover { background: red; }";
    }

    // right hand of slider is black
    ui->txDriveLevel->setStyleSheet(ss);

    ui->txDriveLevel->blockSignals(pstate);

    emit txDriveLevelChanged(m_txDriveLevel);
}

void TxDockWidget::setTxTuneDriveLevel(unsigned int level)
{
    m_txTuneDriveLevel = level;
    bool pstate = ui->txTuneDriveLevel->blockSignals(true);
    ui->txTuneDriveLevel->setValue(level);

    QString ss = "QSlider#txTuneDriveLevel::add-page:horizontal { background: black } ";


    // setup a gradient from bg color to tracecColor to red with S units
    //QColor bg = p.toImage().pixel(pf.x()-3.0, pf.y()+3);
    //sTraceLinearGradient.setColorAt(0.0, bg);
    //sTraceLinearGradient.setColorAt(0.15, traceColor);
    //sTraceLinearGradient.setColorAt(s9Pt, traceColor);
    //sTraceLinearGradient.setColorAt(1.0, QColor("red"));

    // left hand of slider changes by value
    if (m_txTuneDriveLevel <= 63) {
        ss += "QSlider#txTuneDriveLevel::sub-page:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 black, stop: 1 green) }";
        ss += "QSlider::handle:horizontal:hover { background: green; }";
    } else if (m_txTuneDriveLevel > 63 && m_txTuneDriveLevel <= 127) {
        ss += "QSlider#txTuneDriveLevel::sub-page:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 black, stop: 0.50 green, stop: 1 yellow) }";
        ss += "QSlider::handle:horizontal:hover { background: yellow; }";
    } else if (m_txTuneDriveLevel > 127 && m_txTuneDriveLevel <= 195) {
        ss += "QSlider#txTuneDriveLevel::sub-page:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 black, stop: 0.33 green, stop: 0.66 yellow, stop: 1 orange) }";
        ss += "QSlider::handle:horizontal:hover { background: orange; }";
    } else if (m_txTuneDriveLevel > 195) {
        ss += "QSlider#txTuneDriveLevel::sub-page:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop: 0 black, stop: 0.25 green, stop: 0.50 yellow, stop: 0.75 orange, stop: 1 red) }";
        ss += "QSlider::handle:horizontal:hover { background: red; }";
    }

    // right hand of slider is black
    ui->txTuneDriveLevel->setStyleSheet(ss);
    ui->txTuneDriveLevel->blockSignals(pstate);
    emit txTuneDriveLevelChanged(m_txTuneDriveLevel);
}

void TxDockWidget::on_txTuneDriveLevel_valueChanged(int value)
{
        m_txTuneDriveLevel = (unsigned int)value;
        setTxTuneDriveLevel(m_txTuneDriveLevel);
}

void TxDockWidget::on_txDriveLevel_valueChanged(int value)
{
        m_txDriveLevel = (unsigned int)value;
        setTxDriveLevel(m_txDriveLevel);
}

bool TxDockWidget::getTuneMode() const
{
    return tuneMode;
}

void TxDockWidget::setTuneMode(bool value)
{
    if (!r)
        return;

    tuneMode = value;
    bool pstate = ui->txPB_2->blockSignals(true);
    ui->txPB_2->setChecked(value);
    ui->txPB_2->blockSignals(pstate);
    if (value) {
        // remember previous drive setting
        prevDriveLevel = m_txDriveLevel;
        prevSdrMode = r->getRsdkMode();
        // set the level to a sane value
        setTxDriveLevel(m_txTuneDriveLevel);
        // key down
        emit txPressed(true);
        r->setRsdkTuneMode(true);
        //r->setRsdkMode("CWL");
    } else {
        // back to previous level
        setTxDriveLevel(prevDriveLevel);
        // key up
        r->setRsdkTuneMode(false);
        r->setRsdkMode(prevSdrMode);
        emit txPressed(false);
    }
}

void TxDockWidget::on_txPB_toggled(bool checked)
{
   emit txPressed(checked);
}

unsigned int TxDockWidget::getTxTuneDriveLevel() const
{
    return m_txTuneDriveLevel;
}
