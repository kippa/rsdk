#include <assert.h>


#include "rsdk.h"
//#include "audioplatform.h"
#include "audiodockwidget.h"
#include "ui_audiodockwidget.h"

// audio range sliders are floats, kept in 0 - 100 % ranges
AudioDockWidget::AudioDockWidget(QWidget *parent) :
    rsdkDockWidget(parent),
    ui(new Ui::AudioDockWidget),
    m_rxVol(0.0),
    m_rxBal(0.5),
    m_subRxVol(0.0),
    m_subRxBal(0.5),
    m_txLeft(0.0),
    m_jackOutLevel(0.0),
    m_jackInLevel(0.0)
{
    ui->setupUi(this);

    connect(ui->rxVol, SIGNAL(valueChanged(int)), this, SLOT(setIntVol(int)));
    connect(ui->rxBal, SIGNAL(valueChanged(int)), this, SLOT(setIntBal(int)));
    connect(ui->subRxVol, SIGNAL(valueChanged(int)), this, SLOT(setIntSubRxVol(int)));
    connect(ui->subRxBal, SIGNAL(valueChanged(int)), this, SLOT(setIntSubRxBal(int)));
    connect(ui->txLeft, SIGNAL(valueChanged(int)), this, SLOT(setIntTxLeft(int)));
    connect(ui->jackInCB, SIGNAL(toggled(bool)), this, SLOT(setJackInEnable(bool)));
    connect(ui->jackOutCB, SIGNAL(toggled(bool)), this, SLOT(setJackOutEnable(bool)));
    connect(ui->jackOutVol, SIGNAL(valueChanged(int)), this, SLOT(setJackOutLevel(int)));
    connect(ui->jackInVol, SIGNAL(valueChanged(int)), this, SLOT(setJackInLevel(int)));
}

AudioDockWidget::~AudioDockWidget()
{
    delete ui;
}

void AudioDockWidget::start()
{
    // synchronise the rsdk, radioozy jack and the dock widget
    /*
    bool ret = theRsdk->rozy->jackInEnabled;
    bool origState = ui->jackInCB->blockSignals(true);
    ui->jackInCB->setChecked(ret);
    ui->jackInCB->blockSignals(origState);
    origState = ui->jackOutCB->blockSignals(true);
    ui->jackOutCB->setChecked(ret);
    ui->jackOutCB->blockSignals(origState);
    */
}

float AudioDockWidget::getVol() const
{
    return m_rxVol;
}

// 0.0 - 1.0 value
void AudioDockWidget::setVol(float value)
{
    // range limit 0.0 - 1.0
    m_rxVol = value < 0.0f ? 0.0f : value > 1.0f ? 1.0f : value;
    bool origState = ui->rxVol->blockSignals(true);
    ui->rxVol->setValue(static_cast<int>(ceilf(m_rxVol*static_cast<float>(ui->rxVol->maximum()))));
    ui->rxVol->blockSignals(origState);
    emit rxVolChanged(m_rxVol);
}

void AudioDockWidget::setIntVol(int value)
{
    setVol(static_cast<float>(value)/static_cast<float>(ui->rxVol->maximum()));
}

void AudioDockWidget::setBal(float value)
{
    // range limit 0.0 - 1.0
    m_rxBal = value < 0.0f ? 0.0f : value > 1.0f ? 1.0f : value;
    bool origState = ui->rxBal->blockSignals(true);
    ui->rxBal->setValue(static_cast<int>(ceilf(m_rxBal*static_cast<float>(ui->rxBal->maximum()))));
    ui->rxBal->blockSignals(origState);
    emit rxBalChanged(m_rxBal);
}

float AudioDockWidget::getBal() const
{
    return m_rxBal;
}

void AudioDockWidget::setIntBal(int value)
{
    setBal(static_cast<float>(value)/static_cast<float>(ui->rxBal->maximum()));
}

float AudioDockWidget::getSubRxBal() const
{
    return m_subRxBal;
}

void AudioDockWidget::setSubRxBal(float value)
{
    // range limit 0.0 - 1.0
    m_subRxBal = value < 0.0f ? 0.0f : value > 1.0f ? 1.0f : value;
    bool origState = ui->subRxBal->blockSignals(true);
    ui->subRxBal->setValue(static_cast<int>(ceilf(m_subRxBal*static_cast<float>(ui->subRxBal->maximum()))));
    ui->subRxBal->blockSignals(origState);
    emit subRxBalChanged(m_subRxBal);
}

void AudioDockWidget::setIntSubRxBal(int value)
{
    setSubRxBal(static_cast<float>(value)/static_cast<float>(ui->subRxBal->maximum()));
}

float AudioDockWidget::getSubRxVol() const
{
    return m_subRxVol;
}

void AudioDockWidget::setSubRxVol(float value)
{
    // range limit 0.0 - 1.0
    m_subRxVol = value < 0.0f ? 0.0f : value > 1.0f ? 1.0f : value;
    bool origState = ui->subRxVol->blockSignals(true);
    ui->subRxVol->setValue(static_cast<int>(ceilf(m_subRxVol*static_cast<float>(ui->subRxVol->maximum()))));
    ui->subRxVol->blockSignals(origState);
    emit subRxVolChanged(m_subRxVol);
}

void AudioDockWidget::setIntSubRxVol(int value)
{
    setSubRxVol(static_cast<float>(value)/static_cast<float>(ui->subRxVol->maximum()));
}

float AudioDockWidget::txLeft() const
{
    return m_txLeft;
}

void AudioDockWidget::setTxLeft(float value)
{
    // range limit 0.0 - 1.0
    m_txLeft = value < 0.0f ? 0.0f : value > 1.0f ? 1.0f : value;
    bool origState = ui->txLeft->blockSignals(true);
    ui->txLeft->setValue(static_cast<int>(ceilf(m_txLeft*static_cast<float>(ui->txLeft->maximum()))));
    ui->txLeft->blockSignals(origState);
    emit txLeftChanged(m_txLeft);
}

void AudioDockWidget::setIntTxLeft(int value)
{
    setTxLeft(static_cast<float>(value)/static_cast<float>(ui->txLeft->maximum()));
}

void AudioDockWidget::enableSubRx(bool showit)
{
    if (showit)
    {
        // remember current rx level
        m_prevVol = m_rxVol;

        // start subRx at current rx levels
        setSubRxVol(m_rxVol);
        setSubRxBal(m_rxBal);

        // set current rx to zero as a convenience
        // don't
        //setVol(0.0);

        //ui->subRxFrame->show();
        ui->subRxVol->show();
        ui->subRxVolLabel->show();
        ui->subRxBal->show();
        ui->subRxBalLabel->show();
    }
    else
    {
        //ui->subRxFrame->hide();

        // restore to pre-subRx levels
        //setVol(m_prevVol);
        ui->subRxVol->hide();
        ui->subRxVolLabel->hide();
        ui->subRxBal->hide();
        ui->subRxBalLabel->hide();
    }

    update();
    qApp->processEvents();

}

float AudioDockWidget::getJackInLevel() const
{
    return m_jackInLevel;
}

void AudioDockWidget::setJackInLevel(int value)
{
    setJackInLevelF(static_cast<float>(value)/static_cast<float>(ui->jackInVol->maximum()));
}

void AudioDockWidget::setJackInLevelF(float value)
{
    // range limit 0.0 - 1.0
    m_jackInLevel = value < 0.0f ? 0.0f : value > 1.0f ? 1.0f : value;
    theRsdk->rozy->setJackInLevel(m_jackInLevel);
    bool origState = ui->jackInVol->blockSignals(true);
    ui->jackInVol->setValue(static_cast<int>(ceilf(m_jackInLevel*static_cast<float>(ui->jackInVol->maximum()))));
    ui->jackInVol->blockSignals(origState);
    emit jackInChanged(m_jackInLevel);
}

float AudioDockWidget::getJackOutLevel() const
{
    return m_jackOutLevel;
}

void AudioDockWidget::setJackOutLevel(int value)
{
    setJackOutLevelF(static_cast<float>(value)/static_cast<float>(ui->jackOutVol->maximum()));
}

void AudioDockWidget::setJackOutLevelF(float value)
{
    // range limit 0.0 - 1.0
    m_jackOutLevel = value < 0.0f ? 0.0f : value > 1.0f ? 1.0f : value;
    theRsdk->rozy->setJackOutLevel(m_jackOutLevel);
    bool origState = ui->jackOutVol->blockSignals(true);
    ui->jackOutVol->setValue(static_cast<int>(ceilf(m_jackOutLevel*static_cast<float>(ui->jackOutVol->maximum()))));
    ui->jackOutVol->blockSignals(origState);
    emit jackOutChanged(m_jackOutLevel);
}
float AudioDockWidget::prevVol() const
{
    return m_prevVol;
}

void AudioDockWidget::setPrevVol(float prevVol)
{
    m_prevVol = prevVol;
}


bool AudioDockWidget::getJackInEnable() const
{
    return theRsdk->rozy->jackInEnabled;
}

void AudioDockWidget::setJackInEnable(bool jackInEnable)
{
    m_jackInEnable = jackInEnable;
    theRsdk->rozy->setjackInEnabled(m_jackInEnable);
    bool pstate = ui->jackInCB->blockSignals(true);
    ui->jackInCB->setChecked(m_jackInEnable);
    ui->jackInCB->blockSignals(pstate);
}

bool AudioDockWidget::getJackOutEnable() const
{
    return theRsdk->rozy->jackOutEnabled;
}

void AudioDockWidget::setJackOutEnable(bool jackOutEnable)
{
    m_jackOutEnable = jackOutEnable;
    theRsdk->rozy->setJackOutEnabled(jackOutEnable);
    bool pstate = ui->jackOutCB->blockSignals(true);
    ui->jackOutCB->setChecked(m_jackOutEnable);
    ui->jackOutCB->blockSignals(pstate);
}
