#ifndef DSPDOCKWIDGET_H
#define DSPDOCKWIDGET_H
#include <iostream>
//#include <glib.h>
#include <stdio.h>
#include <QTextStream>
#include <QDockWidget>
#include <QDataStream>
#include <QAbstractButton>

#include <dttsp.h> // libDttSp types, enums, and defines

//#include "serializationcontext.h"
#include "rsdkdockwidget.h"

namespace Ui {
    class DspDockWidget;
}

class DspDockWidget : public rsdkDockWidget
{
    Q_OBJECT
    Q_PROPERTY(bool ANF READ getDspANF WRITE setDspANF)
    Q_PROPERTY(bool NR READ getDspNR WRITE setDspNR)
    Q_PROPERTY(bool BNR READ getDspBNR WRITE setDspBNR)
    Q_PROPERTY(bool NB1 READ getDspNB1 WRITE setDspNB1)
    Q_PROPERTY(bool NB2 READ getDspNB2 WRITE setDspNB2)
    Q_PROPERTY(bool MUTE READ getDspMUTE WRITE setDspMUTE)
    Q_PROPERTY(bool BIN READ getDspBIN WRITE setDspBIN)

public:
    explicit DspDockWidget(QWidget *parent = 0);
    // copy ctor
    //DspDockWidget(const DspDockWidget& other);
    ~DspDockWidget();

public slots:
    void setOpacity(bool b);
    void start();

    bool getDspANF(void) const { return m_ANF; }
    void setDspANF(bool m);

    bool getDspNR(void) const { return m_NR; }
    void setDspNR(bool m);

    bool getDspBNR(void) const { return m_BNR; }
    void setDspBNR(bool m);

    bool getDspNB1(void) const { return m_NB1; }
    void setDspNB1(bool m);

    bool getDspNB2(void) const { return m_NB2; }
    void setDspNB2(bool m);

    bool getDspMUTE(void) const { return m_MUTE; }
    void setDspMUTE(bool m);

    bool getDspBIN(void) const { return m_BIN; }
    void setDspBIN(bool m);

    double getDspGain() const { return m_dspGain; }
    void setDspGain(double dspGain);

private slots:
    void on_DspToolButtonANF_toggled(bool checked);
    void on_DspToolButtonBIN_toggled(bool checked);
    void on_DspToolButtonMUTE_toggled(bool checked);
    void on_DspToolButtonNB1_toggled(bool checked);
    void on_DspToolButtonNB2_toggled(bool checked);
    void on_DspToolButtonNR_toggled(bool checked);
    void on_DspToolButtonBNR_toggled(bool checked);
    void on_dspGainSlider_valueChanged(int value);

signals:
    void dspANFChanged(bool m_ANF);
    void dspNRChanged(bool m_NR);
    void dspBNRChanged(bool m_BNR);
    void dspNB1Changed(bool m_NB1);
    void dspNB2Changed(bool m_NB2);
    void dspMUTEChanged(bool m_MUTE);
    void dspBINChanged(bool m_BIN);
    void dockChanged(QDockWidget *, Qt::DockWidgetArea);

private:
    Ui::DspDockWidget *ui;
    bool m_ANF;
    bool m_BIN;
    bool m_MUTE;
    bool m_NB1;
    bool m_NB2;
    bool m_NR;
    bool m_BNR;
    double m_dspGain;
};

#endif // DSPDOCKWIDGET_H
