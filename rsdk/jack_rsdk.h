#ifndef JACK_H
#define JACK_H

#include </usr/include/jack/types.h>
#include </usr/include/jack/ringbuffer.h>

#include "exception.h"

class jack
{
public:
    jack(const char * const name_prm);
    ~jack();
    int startJack(void);
    int stopJack(void);

    int fillRingbuffer(const char *buf, size_t cnt, jack_ringbuffer_t *rb);

    int fetchRingbuffer(char *buf, size_t cnt, jack_ringbuffer_t *rb);

    int cbs, xcbs;

    string name;
    jack_client_t *jc;

    jack_port_t *inL, *outL;
    jack_port_t *inR, *outR;
    jack_ringbuffer_t *rb_inL, *rb_outL;
    jack_ringbuffer_t *rb_inR, *rb_outR;

    jack_nframes_t size; // uint32_t really
    jack_nframes_t sampleRate;

    void *user_data;

    bool isRunning() const;

protected:
    // return -1 on error in init
    int init(void *arg, int (*proc_cb)(jack_nframes_t, void *), int (*xrun_cb)(void *));
    void jackActionCb(jack_nframes_t numFrames, void *arg);
    bool m_isRunning;
};

#endif // JACK_H
