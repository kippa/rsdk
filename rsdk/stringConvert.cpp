/*! 
 * stringConvert
 *
 * utility to convert std::string to values needed by rsdk
 *
 * THIS IS NOT GENERAL PURPOSE!
 *
 */

#include <string>
#include "stringConvert.h"

using namespace std;

/*************************************************************************/
/********************* stringConvert utility *************************/
/*************************************************************************/

// ripped from Qt docs, modified for even larger values
std::string stringConvert::convertToUnits(double nvalue)
{
    std::string unit;
    double value;

    if(nvalue<0) {
        value=nvalue*-1;
    } else {
        value=nvalue;
    }

    if(value>=1000000000000&&value<1000000000000000) {
        value=value/1000000000000;
        unit="P";
    } else if(value>=1000000000&&value<1000000000000) {
        value=value/1000000000;
        unit="G";
    } else if(value>=1000000&&value<1000000000) {
        value=value/1000000;
        unit="M";
    } else if(value>=1000&&value<1000000) {
        value=value/1000;
        unit="K";
    } else if((value>=1&&value<1000)) {
        value=value*1;
    } else if((value*1000)>=1&&value<1000) {
        value=value*1000;
        unit="m";
    } else if((value*1000000)>=1&&value<1000000) {
        value=value*1000000;
        unit="u";
    } else if((value*1000000000)>=1&&value<1000000000) {
        value=value*1000000000;
        unit="n";
    } else if((value*1000000000000)>=1&&value<1000000000000) {
        value=value*1000000000000;
        unit="p";
    }
    if(nvalue>0)
        return (std::to_string(value) + unit);
    else if(nvalue<0)
        return (std::to_string(value*-1) + unit);
    else
        return std::string("0");
}

// convert ascii string floating point values into float
// THIS IS NOT GENERAL PURPOSE!
// It takes K M G (like "2.4K")values and converts them back and forth
double stringConvert::convertToValues(const std::string &input)
{
    std::string unit,value;
    double inValue;
    int j=0;
    for(unsigned int i=0;i<=input.size();i++) {
        if((input[i]>='A'&&input[i]<='Z')|| (input[i]>='a'&&input[i]<='z')) {
            unit[j]=input[i];
            j++;
        }
    }
    for(unsigned int k=0;k<(input.size()-unit.size());k++)
        value[k]=input[k];

    inValue = atof(input.c_str());

    if(unit[0]=='p')  {
        return(inValue/1000000000000);
    }  else if(unit[0]=='n')  {
        return(inValue/1000000000);
    } else if(unit[0]=='u') {
        return(inValue/1000000);
    } else if(unit[0]=='m') {
        return(inValue/1000);
    } else if(unit[0]=='K' || unit[0]=='k') {
        return(inValue*1000);
    } else if(unit[0]=='M') {
        return(inValue*1000000);
    } else if(unit[0]=='G') {
        return(inValue*1000000000);
    } else if(unit[0]=='P') {
        return(inValue*1000000000000);
    } else
        return(inValue*1);
}
