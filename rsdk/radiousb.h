#ifndef RADIOUSB_H
#define RADIOUSB_H

#include </usr/include/libusb-1.0/libusb.h>
//#include <libusb-1.0/libusb.h>

#include "buffer.h"
#include "common.h"
#include "radiofile.h"

#define BUF_SIZE 512 // USB buffer sizes for usb_create_buffer_pool
#define BS_BUF_SIZE 8192
#define NUM_BS_BUFS 16
#define NUM_RX_BUFS 128
#define NUM_TX_BUFS 128
#define NUM_CTL_BUFS 16

using namespace std;

class RadioUsb : public RadioFile
{
public:
    RadioUsb(const char * const name_prm, ASYNCCB cb, void *cbdata, int VID, int PID);
    virtual ~RadioUsb();

    int Read(unsigned char *buf_prm, int64 len_prm, bool wait_prm=true);
    int Write(unsigned char *buf_prm, int64 len_prm, bool wait_prm=true);

    // busyBufs are in flight to the libusb-1.0 and ultimately the device
    // track in flight bufs to allow clearing them off the async queues
    // in flight bufs are busyBufs
    usbBufPool_t *rxBufs, *txBufs, *busyBufs;
    usbBufPool_t *bsBufs, *busyBsBufs;
    usbBufPool_t *ctlBufs;

    libusb_device *dev;
    libusb_device_handle *devHandle;
    libusb_context *usbContext;

    struct libusb_transfer *usbCtl;
    struct libusb_transfer *usbXfr;

    int bufcnt;

    virtual int libusbOpenDev(int VID, int PID);
    virtual void libusbCloseDev(void);

protected:
    int attached;

private:
    bool usbInitialized;

    // a pthread event reaper for libusb
    pthread_t event_handler_thread_id;
    pthread_mutex_t event_handler_thread_lock;
    int event_handler_thread_request_kill;
    int event_handler_thread_request_suspend; // thanks Linus, no pthread_suspend_np()

    int isDev(libusb_device *dev, int vid, int pid);
    libusb_device *findDev(libusb_context *c, int vid, int pid);

    static void *libusbThreadEntry(void *this_prm);

    // no copy, no assign
	RadioUsb(const RadioUsb &NoCopy);
    RadioUsb &operator=(const RadioUsb &NoAssign);
};

#endif // RADIOUSB_H
