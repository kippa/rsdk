HEADERS += \
../xmlrpc/XmlRpcBase64.h \
../xmlrpc/XmlRpcClient.h \
../xmlrpc/XmlRpcDispatch.h \
../xmlrpc/XmlRpcException.h \
../xmlrpc/XmlRpc.h \
../xmlrpc/XmlRpcMutex.h \
../xmlrpc/XmlRpcServerConnection.h \
../xmlrpc/XmlRpcServer.h \
../xmlrpc/XmlRpcServerMethod.h \
../xmlrpc/XmlRpcSocket.h \
../xmlrpc/XmlRpcSource.h \
../xmlrpc/XmlRpcUtil.h \
../xmlrpc/XmlRpcValue.h

SOURCES += \
../xmlrpc/XmlRpcClient.cpp \
../xmlrpc/XmlRpcDispatch.cpp \
../xmlrpc/XmlRpcMutex.cpp \
../xmlrpc/XmlRpcServerConnection.cpp \
../xmlrpc/XmlRpcServer.cpp \
../xmlrpc/XmlRpcServerMethod.cpp \
../xmlrpc/XmlRpcSocket.cpp \
../xmlrpc/XmlRpcSource.cpp \
../xmlrpc/XmlRpcUtil.cpp \
../xmlrpc/XmlRpcValue.cpp
