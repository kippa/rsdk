#include <list>

#include "common.h"
#include "exception.h"
#include "dbstore.h"

static const char *defaultDbName = "rsdk.db";

dbStore::dbStore() :
    dbFileName(NULL)
{
    /* Open the default database */
    dbFileName = defaultDbName;
    makeConnection (dbFileName);
}

dbStore::dbStore(const char *name)
{
    /* Open the database file */
    dbFileName = name;
    makeConnection (dbFileName);
}

dbStore::~dbStore()
{
    close();
}

int dbStore::close()
{
    return sqlite3_close_v2(dbConn);
}

int dbStore::makeConnection(const char *db)
{
    if (!db)
    {
        THROW_EXCEPTION(CRITICAL_LEVEL, 100, "dsStore ctor with no database file name!");
    }

    // if "db" does not exist it will be created in the path that the executable is running under
    // it will be empty if created, TODO FIXME we don't want that
    int rc = sqlite3_open(db, &dbConn);
    if(rc)
    {
        THROW_EXCEPTION(CRITICAL_LEVEL, 101, "dsStore makeConnection(): sqlite3_open %s failed!", db);
    }
    return rc;
}

sqlite3_stmt * dbStore::doQuery(const char * const query)
{
    if (!dbConn)
    {
        //THROW_EXCEPTION(CRITICAL_LEVEL, 102, "dbStore doQuery(): %s failed!", query);
        fprintf(stderr, "%s: No db connection!\n", __func__);
    }

    sqlite3_stmt *stmt;

    int rc = sqlite3_prepare_v2(dbConn, query, -1, &stmt, NULL);
    if (rc != SQLITE_OK)
    {
        //THROW_EXCEPTION(CRITICAL_LEVEL, 103, "dbStore doQuery(): %s failed!", query);
        fprintf(stderr, "%s: sqlite3 error (%s)!\n", __func__, sqlite3_errmsg(dbConn));
        sqlite3_finalize(stmt);
        return NULL;
    }

    return stmt;
}

// service the banddockwidget request for all the bands in the bands table
// via the common.h typedef struct band {} band_t
int dbStore::getBands(list<band *> &bands_prm)
{
    int rc = 0;

    bands_prm.clear();

    const char *query = "select * from bands as A JOIN modes as B ON A.prefMode=B.name order by startKHz";

    sqlite3_stmt *stmt = doQuery(query);
    do
    {
        rc = sqlite3_step(stmt);
        if (rc != SQLITE_ROW && rc != SQLITE_DONE)
        {
            fprintf(stderr, "%s: sqlite3_step error: No row available. (%s)!\n", __func__, sqlite3_errmsg(dbConn));
            break;
        }

        if (rc == SQLITE_ROW)
        {
            // do something with the returned columns of data
            int count = sqlite3_column_count(stmt);
            if (count <= 0)
                break;

            // prepare a new band entry
            struct band *b = new struct band();

            b->count = 0; // initialize now and use later for tracking band segments

            for (int i=0; i<count; i++)
            {
                char *col = (char *)sqlite3_column_name(stmt, i);
                char *val = (char *)sqlite3_column_text(stmt, i);
                if (!col)
                {
                    // this should not happen
                    delete b;
                    break;
                }
                if (val)
                {
                    if (!strncasecmp(col, "startKHz", 8))
                    {
                        b->start = atof(val);
                    }
                    else if (!strncasecmp(col, "endKHz", 6))
                    {
                        b->end = atof(val);
                    }
                    else if (!strncasecmp(col, "prefMode", 8))
                    {
                        std::string s(val);
                        b->prefMode = s;
                    }
                    else if (!strncasecmp(col, "hamClass", 8))
                    {
                        std::string s(val);
                        b->hamClass = s;
                    }
                    else if (!strncasecmp(col, "bandType", 8))
                    {
                        std::string s(val);
                        b->bandType = s;
                    }
                    else if (!strncasecmp(col, "desc", 4))
                    {
                        std::string s(val);
                        b->desc = s;
                    }
                    else if (!strncasecmp(col, "notes", 5))
                    {
                        std::string s(val);
                        b->notes = s;
                    }
                    else if (!strncasecmp(col, "color", 5))
                    {
                        std::string s(val);
                        b->color = s;
                    }
                }
            }

            bands_prm.push_back(b);
        }

    }while (rc != SQLITE_DONE);

    // we've copied off all the data from the db to the struct, we're done with the db return
    rc = sqlite3_finalize(stmt);

    return rc;
}
