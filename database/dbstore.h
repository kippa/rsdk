#ifndef DBSTORE_H
#define DBSTORE_H

#include <stdlib.h>
#include <list>

#include "sqlite3.h"

#include "common.h" // struct band

class dbStore
{
public:
    dbStore();
    dbStore(const char *name);
    ~dbStore();

    // the SDR interface to the database for memories, bands, and other things
    // directly coded w/o an abstraction layer
    // use this interface to convert database queries into various return types depending upon caller

    // download and cache all the bands at once, so we don't roundtrip the database all the time
    // this means changes in the database bands table must trigger a reload here
     int getBands(std::list<band *> &bands_prm);

     // enter a dock widget
     //int setMemDocks();
     //int getMemDocks();
     //int setMemGrps();
     //int getMemGrps();

private:
    int close();
    int makeConnection(const char *db);
    sqlite3_stmt *doQuery(const char * const query);

    sqlite3 *dbConn;
    const char *dbFileName;
};

#endif // DBSTORE_H
