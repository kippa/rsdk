# <b>rsdk</b>

![rsdk hello](images/rsdk-hello-screenshot.png)

<br>

## <b>rsdk</b> -- a C++ & Qt-based HPSDR Software Defined Radio Project
### <b>rsdk</b> -- for Amateur Radio & Shortwave Listening 
---

<b>rsdk</b> is a Software Defined Radio (SDR) app for Linux. It is based upon
C++ and Qt. It controls the HPSDR FPGA-based digital radio.

* <b>rsdk</b> lays out the radio controls in the form of dockable, closable,
  independently positionable widgets, allowing customized radio layouts.

* <b>rsdk</b> uses the Qt StyleSheet capabilities; totally custom looks, or
  skins, without recompilation. 

* <b>rsdk</b> uses a modified version of the DttSP Digital Signal Processing
  (DSP) library as the backend for radio signal software processsing.

* <b>rsdk</b> uses the JACK audio connection kit for low latency local audio

### HPSDR

High Performance Software Defined Radio (HPSDR) is a collection of open source
hardware components consisting of receivers, exciters, interfaces, amplifiers
and filters, and software designed to work together as a high quality radio.
<br>
HPSDR radios consist of a broadband 16-bit direct-sampling receiver card --
Mercury -- and an equally capable 1/2 watt exciter/transmitter card -- Penny
Lane.  Interface to the Linux host computer is the via the Magister USB2.0
interface.  Various other components add filtering and power amplification
to make a very capable HF shortwave radio, the ultimate modern homebrew
transceiver.

#### Hardware Lineup

* Mercury 16-bit DDS Direct Sampling Receiver;
* PennyLane 1/2 Watt DUC Exciter/Transmitter;
* Magister USB2.0 Computer Interface;
* Atlas Backplane
* Excalibur 10 MHz TXCO (not required, but nice to have;);
* Alex Hi/Lo/Band Pass filters w/ 6M preamp
* PennyWhistle 15W PA Amplifier

Some of these components are sometimes available for sale at:

http://www.tapr.org/kits.html

And sometimes the various compenents, or entire systems, come up for sale on the
HPSDR discussion list:

http://lists.openhpsdr.org/listinfo.cgi/hpsdr-openhpsdr.org.

### <b>rsdk</b> 

<b>rsdk</b> started as a hobby project. I created <b>rsdk</b> to teach myself Qt
programming, and set out to make the software defined radio I always wanted to
use. I also wanted to explore, learn, and harness for myself, the power of radio
and DSP. 

This project is based upon the Magister USB interface, and does not (yet)
support the Metis network interface. Instead, it uses the  most excellent
asynchronous USB interface provided by libusb-1.0 system library. It has been
optimized for superior USB operation.  

Build instructions will soon follow. <b>rsdk</b> has been built to run on
Linux/Ubuntu16.04LTS/Mint and Android. Basically it's a Qt project. Use
qtcreator and load the <b>rsdk</b>.pro project file, and set about satisfying
all the necessary dependencies. 

###build

* Install qt5.7
** https://www.qt.io/download-open-source/
* Load rsdk.pro
* Get things compiling
* Install sh/30-ozy.rules
** sudo cp sh/30-ozy.rules /etc/udev/rules.d/
** inotify picks it up, so no restarts or anything

I love this radio! Enjoy... 

73 de KF5OV Kipp Aldrich

